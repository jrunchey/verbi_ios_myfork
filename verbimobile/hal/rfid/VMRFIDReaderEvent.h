//
//  VMRFIDReaderEvent.h
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMEvent.h"
#import "VMRFIDTag.h"

@interface VMRFIDReaderEvent : VMEvent

@property(nonatomic,strong) NSArray *tags;

@end
