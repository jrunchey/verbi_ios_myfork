//
//  VMRFIDReader.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMConstants.h"
#import "VMRFIDReaderEvent.h"
#import "BLE.h"

#pragma mark -
#pragma mark Delegate Specification
/****************************************************************************
 * Delegate Specification
 ****************************************************************************/

@protocol VMRFIDReaderDelegate
@optional
/**
 Notification sent when a Verbi Reader button is pressed
 @param which the Verbi Reader button pressed
 **/
-(void)handleRFIDReaderButtonPressedEvent:(int)which;

/**
 Notification sent when a Verbi Reader button is released
 @param which the Verbi Reader button pressed
 **/
-(void)handleRFIDReaderButtonReleasedEvent:(int)which;

/**
 Notification sent when a Verbi Reader status event occurs
 @param description the status event's description
 @param event the full details of status event
 **/
-(void)handleRFIDReaderStatusEvent:(NSString *)description event:(VMRFIDReaderEvent *)event;

/**
 Notification sent when a Verbi Reader read event occurs
 @param tagIds the list of tag ids in a read cycle
 @param event the full details of read event
 **/
-(void)handleRFIDReaderReadEvent:(NSArray *)tagIds event:(VMRFIDReaderEvent *)event;

/**
 Notification sent when a Verbi Reader write event occurs
 @param tagIds the list of tag ids in a write cycle
 @param event the full details of write event
 **/
-(void)handleRFIDReaderWriteEvent:(NSArray *)tagIds event:(VMRFIDReaderEvent *)event;

@required
@end

#pragma mark -
#pragma mark Interface Specification
/****************************************************************************
 * Interface Specification
 ****************************************************************************/

@interface VMRFIDReader : NSObject <BLEDelegate>

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@property (nonatomic, weak) id <VMRFIDReaderDelegate> delegate;
@property (nonatomic, weak) NSString *name;
@property (nonatomic) int type;
@property (nonatomic) BOOL initialized;
@property (nonatomic) BOOL connected;
@property (nonatomic) BOOL readCycleStarted;
@property (nonatomic) BOOL writeCycleStarted;
@property (nonatomic, strong) NSDictionary *supportedAutoIdModes;
@property (nonatomic, strong) BLE *bleShield;
@property (nonatomic, strong) NSString *UUID;
@property (nonatomic, weak) NSNumber *RSSI;

// Barcode Reader Properties
@property (nonatomic, weak) NSString *status;
@property (nonatomic, weak) NSString *debug;

#pragma mark -
#pragma mark Constructors
/****************************************************************************
 * Constructors
 ****************************************************************************/

- (id)init;

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose;

#pragma mark -
#pragma mark Auto-Id Methods
/****************************************************************************
 * Auto-Id Methods
 ****************************************************************************/

/**
 Resets the connection state with the device
 **/
- (void)resetConnectionState;

/**
 Updates the reader using the 'Barcode Scanner' application settings
 **/
-(BOOL)updateReader;

/**
 Triggers a single inventory read cycle
 **/
- (void)triggerRead;

/**
 Starts an asynchronous read cycle
 **/
- (void)startRFIDReading;

/**
 Stops an asynchronous read cycle
 **/
- (void)stopRFIDReading;

/**
 Write data (up to 19-bytes at a time) to the RFID reader using the write function.
 **/
- (void)send:(NSString *)string;

@end
