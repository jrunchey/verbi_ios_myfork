//
//  VMRFIDReaderEvent.m
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMRFIDReaderEvent.h"

@implementation VMRFIDReaderEvent

@synthesize tags = _tags;

@end
