//
//  VMRFIDReader.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMRFIDReader.h"

@interface VMRFIDReader()
@property(nonatomic) int lastReaderType;
@end

@implementation VMRFIDReader

#pragma mark -
#pragma mark GETTERS/SETTERS
/****************************************************************************
 * GETTERS/SETTERS
 ****************************************************************************/

@synthesize delegate = _delegate;
@synthesize name = _name;
@synthesize type = _type;
@synthesize initialized = _initialized;
@synthesize connected = _connected;
@synthesize readCycleStarted = _readCycleStarted;
@synthesize writeCycleStarted = _writeCycleStarted;
@synthesize supportedAutoIdModes = _supportedAutoIdModes;
@synthesize bleShield = _bleShield;
@synthesize UUID = _UUID;
@synthesize RSSI = _RSSI;
@synthesize lastReaderType = _lastReaderType;

- (NSString *)name {
    if (_name == nil)
        _name = @"Generic RFID Reader";
    return _name;
}

- (int)type {
    if ([IS_SIMULATOR intValue] == 1)
        _type = -1;
    else
        _type = [[NSUserDefaults standardUserDefaults] integerForKey:@"rfidReader"];
    return _type;
}

- (NSDictionary *)supportedAutoIdModes {
    if (_supportedAutoIdModes == nil)
        _supportedAutoIdModes = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 AUTO_ID_MODE_NONE_VALUE, AUTO_ID_MODE_NONE_KEY,
                                 nil];
    return _supportedAutoIdModes;
}

- (NSNumber *)RSSI {
    if (_RSSI == nil)
        _RSSI = [NSNumber numberWithFloat:0];
    return _RSSI;
}

#pragma mark -
#pragma mark CONSTRUCTOR METHODS
/****************************************************************************
 * CONSTRUCTOR METHODS
 ****************************************************************************/

- (id)init {
    self = [super init];
    if (self) {
        self.type = -1;
        [self updateReader];
        self.initialized = YES;
    }
    return self;
}

-(void)createBLEShield {
    if (self.bleShield == nil) {
        self.bleShield = [[BLE alloc] init];
        [self.bleShield controlSetup:1];        // if not already enabled, prompt user to enable Bluetooth at OS-level
        CBPeripheral *activeDevice = [self.bleShield activePeripheral];
        if (activeDevice) {
            self.name = activeDevice.name;
            self.UUID = (__bridge NSString*)CFUUIDCreateString(nil, activeDevice.UUID);
            self.RSSI = activeDevice.RSSI;
            [self.bleShield connectPeripheral:activeDevice];
            [self.bleShield setActivePeripheral:activeDevice];
        }
    }
}

#pragma mark -
#pragma mark DECONSTRUCTOR METHODS
/****************************************************************************
 * DECONSTRUCTOR METHODS
 ****************************************************************************/

-(void)dispose {
    self.delegate = nil;
    [self destroyBLEShield];
    self.supportedAutoIdModes = nil;
    self.status = nil;
    self.debug = nil;
}

-(void)destroyBLEShield {
    if (self.bleShield) {
        if (self.bleShield.activePeripheral) {
            CBCentralManager *cbCentralManager = [self.bleShield CM];
            if (cbCentralManager)
                [cbCentralManager cancelPeripheralConnection:self.bleShield.activePeripheral];
            self.bleShield.activePeripheral = nil;
        }
        self.bleShield.delegate = nil;
        self.bleShield = nil;
    }
}

#pragma mark -
#pragma mark AUTO-ID METHODS
/****************************************************************************
 * AUTO-ID METHODS
 ****************************************************************************/

- (void)resetConnectionState {
    @try {
        switch (self.type) {
            case 0: // App setting for no reader
                break;
            case 1: // App setting for Verbi Reader
                break;
            default:    // default to none
                break;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Failed to reset the connection state for VMBarcodeReader. An exception occurred: %@", [exception reason]);
    }
}

- (BOOL)updateReader {
    BOOL updated = NO;
    @try {
        self.type = [[NSUserDefaults standardUserDefaults] integerForKey:@"rfidReader"];
        switch (self.type) {
            case -1: // Simulator
                if (self.lastReaderType != -1) {
                    [self destroyBLEShield];
                    self.connected = NO;
                    self.supportedAutoIdModes = [[NSDictionary alloc] initWithObjectsAndKeys:
                                                 AUTO_ID_MODE_NONE_VALUE, AUTO_ID_MODE_NONE_KEY,
                                                 nil];
                    self.initialized = YES;
                    updated = YES;
                }
                self.lastReaderType = -1;
                break;
            case 0: // None
                if (self.lastReaderType != 0) {
                    [self destroyBLEShield];
                    self.connected = NO;
                    self.supportedAutoIdModes = [[NSDictionary alloc] initWithObjectsAndKeys:
                                             AUTO_ID_MODE_NONE_VALUE, AUTO_ID_MODE_NONE_KEY,
                                             nil];
                    self.initialized = YES;
                    updated = YES;
                }
                self.lastReaderType = 0;
                break;
            case 1: // App setting for RFID is Verbi Reader
                if (self.bleShield == nil || self.lastReaderType != 1) {
                    [self destroyBLEShield];
                    self.connected = NO;
                    self.initialized = NO;
                    [self createBLEShield];
                    [self resetConnectionState];
                    
                    // Update supported auto id modes - TODO: Check linea version to determine supported modes
                    if (self.supportedAutoIdModes == nil || [self.supportedAutoIdModes objectForKey:AUTO_ID_MODE_RFID_C1G2_KEY] == nil)
                        self.supportedAutoIdModes = [[NSDictionary alloc] initWithObjectsAndKeys:
                                                     AUTO_ID_MODE_RFID_C1G2_VALUE, AUTO_ID_MODE_RFID_C1G2_KEY,
                                                     nil];
                    self.initialized = YES;
                    updated = YES;
                }
                if (self.bleShield.delegate == nil)
                    self.bleShield.delegate = self;
                self.lastReaderType = 1;
                break;
            case 2: // App setting for RFID is UGrokIt
                /*
                if (self.bleShield == nil || self.lastReaderType != 2) {
                    [self createBLEShield];
                    [self resetConnectionState];
                }
                 */
                self.lastReaderType = 2;
                break;
            default:    // default to simulator
                [self destroyBLEShield];
                self.connected = NO;
                self.supportedAutoIdModes = [[NSDictionary alloc] initWithObjectsAndKeys:
                                             AUTO_ID_MODE_NONE_VALUE, AUTO_ID_MODE_NONE_KEY,
                                             nil];
                self.initialized = YES;
                updated = YES;
                self.lastReaderType = 0;
                break;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Failed to update VMBarcodeReader. An exception occurred: %@", [exception reason]);
    }
    return updated;
}

- (void)triggerRead {
    [self updateReader];
    
    @try {
        switch (self.type) {
            case 0: // App setting for no reader
                break;
            case 1: // App setting for Verbi Reader
                break;
            default:    // default to none
                break;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Failed to trigger read for VMBarcodeReader. An exception occurred: %@", [exception reason]);
    }
}

- (void)startRFIDReading {
    uint8_t buf[2];
    buf[0] = 0xAA;
    buf[1] = 0x55;
    NSData *data = [[NSData alloc] initWithBytes:buf length:2];
    [self.bleShield write:data];
}

- (void)stopRFIDReading {
    uint8_t buf[2];
    buf[0] = 0xAB;
    buf[1] = 0x55;
    NSData *data = [[NSData alloc] initWithBytes:buf length:2];
    [self.bleShield write:data];
}

// Write data (up to 19-bytes at a time) to BLE device using the write function.
- (void)send:(NSString *)string {
    NSString *s;
    NSData *d;
    
    if (string.length > 16)
        s = [string substringToIndex:16];
    else
        s = string;
    
    s = [NSString stringWithFormat:@"%@\r\n", s];
    d = [s dataUsingEncoding:NSUTF8StringEncoding];
    
    [self.bleShield write:d];
}

#pragma mark -
#pragma mark RedBearLab BLE Methods
/****************************************************************************
 * RedBearLab BLE Methods
 ****************************************************************************/

// Called asynchronously when the BLE device sends data to your App
- (void) bleDidConnect {
    self.connected = YES;
    CBPeripheral *activeDevice = [self.bleShield activePeripheral];
    if (activeDevice) {
        self.name = activeDevice.name;
        self.UUID = (__bridge NSString*)CFUUIDCreateString(nil, activeDevice.UUID);
        self.RSSI = activeDevice.RSSI;
    }
    
    self.status = @"Connected";
    NSDictionary *details = [[NSDictionary alloc] initWithObjectsAndKeys:
                             self.status,@"status",
                             self.name,@"name",
                             self.UUID,@"uuid",
                             self.RSSI,@"rssi",
                             nil];
    VMRFIDReaderEvent *event = [[VMRFIDReaderEvent alloc] init:EVENT_STATUS details:details];
    if (self.delegate)
        [self.delegate handleRFIDReaderStatusEvent:self.status event:event];
}

// Called asynchronously when the BLE device sends data to your App
- (void) bleDidDisconnect {
    self.connected = NO;
    if (self.bleShield.activePeripheral && self.bleShield.activePeripheral.isConnected)
        [[self.bleShield CM] cancelPeripheralConnection:[self.bleShield activePeripheral]];
    self.status = @"Disconnected";
    NSDictionary *details = [[NSDictionary alloc] initWithObjectsAndKeys:
                             self.status,@"status",
                             self.name,@"name",
                             self.UUID,@"uuid",
                             nil];
    VMRFIDReaderEvent *event = [[VMRFIDReaderEvent alloc] init:EVENT_STATUS details:details];
    if (self.delegate)
        [self.delegate handleRFIDReaderStatusEvent:self.status event:event];
}

// Called asynchronously when the BLE device sends data to your App
- (void) bleDidUpdateRSSI:(NSNumber *)rssi {
    self.status = @"RSSI Update";
    self.RSSI = rssi;
    NSDictionary *details = [[NSDictionary alloc] initWithObjectsAndKeys:
                             self.status,@"status",
                             self.name,@"name",
                             self.UUID,@"uuid",
                             self.RSSI,@"rssi",
                             nil];
    VMRFIDReaderEvent *event = [[VMRFIDReaderEvent alloc] init:EVENT_STATUS details:details];
    if (self.delegate)
        [self.delegate handleRFIDReaderStatusEvent:self.status event:event];
}

// This method is asynchronously called when the BLE device sends data to your App
- (void) bleDidReceiveData:(unsigned char *)data length:(int)length {
    NSData *receivedDataFromBLEDevice = [NSData dataWithBytes:data length:length];
    unsigned char *bleBytes = (unsigned char *)receivedDataFromBLEDevice.bytes;
    
    NSDictionary *details = nil;
    VMRFIDReaderEvent *event = [[VMRFIDReaderEvent alloc] init:EVENT_READ details:details];
    NSMutableArray *tags = [[NSMutableArray alloc] init];
    NSMutableArray *tagIds = [[NSMutableArray alloc] init];
    NSString *tagId = [[NSString alloc] init];
    for (int i = 0; i < length; i++) {
        tagId = [tagId stringByAppendingString:[NSString stringWithFormat:@"%02X",bleBytes[i]]];
    }
    if (tagId && [tagId length] > 0) {
        tagId = [tagId substringWithRange:NSMakeRange(4, [tagId length] - 8)];  // chop off leading and trailing 4 digits
        VMRFIDTag *tag = [[VMRFIDTag alloc] init:tagId];
        tag.type = TAG_RFID;
        [tags addObject:tag];
        [tagIds addObject:tagId];
        event.tags = [[NSArray alloc] initWithArray:tags];
        if (self.delegate)
            [self.delegate handleRFIDReaderReadEvent:[[NSArray alloc] initWithArray:tagIds] event:event];
    }
}

@end
