//
//  VMBarcodeReader.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMBarcodeReader.h"
#import "VMAppDelegate.h"
#import "VMUtilityService.h"

@interface VMBarcodeReader()
@property(nonatomic) int lastReaderType;
@property(nonatomic) BOOL scanProcessed;
@end

@implementation VMBarcodeReader

#pragma mark -
#pragma mark GETTERS/SETTERS
/****************************************************************************
 * GETTERS/SETTERS
 ****************************************************************************/

@synthesize delegate = _delegate;
@synthesize type = _type;
@synthesize initialized = _initialized;
@synthesize connected = _connected;
@synthesize supportedAutoIdModes = _supportedAutoIdModes;
@synthesize scandit = _scandit;
@synthesize linea = _linea;
@synthesize currentViewController = _currentViewController;
@synthesize releaseOnAppear = _releaseOnAppear;
@synthesize scaledSubviewActive = _scaledSubviewActive;
@synthesize pickerSubview = _pickerSubview;
@synthesize pickerSubviewButton = _pickerSubviewButton;
@synthesize lastScanValue = _lastScanValue;
@synthesize lastBarcodeSymbology = _lastBarcodeSymbology;
@synthesize status = _status;
@synthesize debug = _debug;
@synthesize lastCardName = _lastCardName;
@synthesize lastCardNumber = _lastCardNumber;
@synthesize lastExpDate = _lastExpDate;
@synthesize lastMagneticCardTrack1 = _lastMagneticCardTrack1;
@synthesize lastMagneticCardTrack2 = _lastMagneticCardTrack2;
@synthesize lastMagneticCardTrack3 = _lastMagneticCardTrack3;
@synthesize lastReaderType = _lastReaderType;
@synthesize scanProcessed = _scanProcessed;

- (int)type {
    if ([IS_SIMULATOR intValue] == 1)
        _type = -1;
    else
        _type = [[NSUserDefaults standardUserDefaults] integerForKey:@"barcodeScanner"];
    return _type;
}

- (NSDictionary *)supportedAutoIdModes {
    if (_supportedAutoIdModes == nil)
        _supportedAutoIdModes = [[NSDictionary alloc] initWithObjectsAndKeys:
            AUTO_ID_MODE_NONE_VALUE, AUTO_ID_MODE_NONE_KEY,
            nil];
    return _supportedAutoIdModes;
}

#pragma mark -
#pragma mark CONSTRUCTOR METHODS
/****************************************************************************
 * CONSTRUCTOR METHODS
 ****************************************************************************/

- (id)init {
    self = [super init];
    if (self) {
        self.type = -1; //[[NSUserDefaults standardUserDefaults] integerForKey:@"barcodeScanner"];
        [self updateReader];
        self.initialized = YES;
    }
    return self;
}

-(void)createScandit {
    if (self.scandit == nil) {
        self.scandit = [[ScanditSDKBarcodePicker alloc] initWithAppKey:SCANDIT_SDK_APP_KEY];
    }
}

-(void)createLinea {
    if (self.linea == nil) {
        self.linea = [Linea sharedDevice];
        if (self.linea) {
            [self.linea addDelegate:self];
            [self.linea connect];
        }
    }
}

#pragma mark -
#pragma mark DECONSTRUCTOR METHODS
/****************************************************************************
 * DECONSTRUCTOR METHODS
 ****************************************************************************/

-(void)dispose {
    self.delegate = nil;
    [self destroyScandit];
    [self destroyLinea];
    self.supportedAutoIdModes = nil;
    self.lastScanValue = nil;
    self.lastBarcodeSymbology = nil;
    self.status = nil;
    self.debug = nil;
    self.currentViewController = nil;
    self.currentViewController = nil;
    self.pickerSubview = nil;
    self.pickerSubviewButton = nil;
}

-(void)destroyScandit {
    if (self.scandit) {
        [self.scandit stopScanning];
        [self closePickerSubview];
        self.scandit = nil;
    }
}

-(void)destroyLinea {
    if (self.linea) {
        [self.linea disconnect];
        [self.linea removeDelegate:self];
        self.linea = nil;
    }
}

#pragma mark -
#pragma mark AUTO-ID METHODS
/****************************************************************************
 * AUTO-ID METHODS
 ****************************************************************************/

- (void)resetConnectionState {
    @try {
        switch (self.type) {
            case 0: // App setting for barcode is camera (aka scandit)
                self.releaseOnAppear = NO;
                //self.scandit = nil;
                break;
            case 1: // App setting for barcode is linea
                [self connectionState:self.linea.connstate];    //update display according to current linea state
                break;
            default:    // default to camera
                break;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Failed to reset the connection state for VMBarcodeReader. An exception occurred: %@", [exception reason]);
    }
}

-(BOOL)updateReader {
    BOOL updated = NO;
    @try {
        self.type = [[NSUserDefaults standardUserDefaults] integerForKey:@"barcodeScanner"];
        switch (self.type) {
            case -1: // Simulator
                if (self.lastReaderType != -1) {
                    [self destroyScandit];
                    [self destroyLinea];
                    self.connected = NO;
                    
                    self.supportedAutoIdModes = [[NSDictionary alloc] initWithObjectsAndKeys:
                                            AUTO_ID_MODE_NONE_VALUE, AUTO_ID_MODE_NONE_KEY,
                                            nil];
                    self.initialized = YES;
                    updated = YES;
                }
                self.lastReaderType = -1;
                break;
            case 0: // App setting for barcode is camera (aka scandit)
                if (self.scandit == nil || self.lastReaderType != 0) {
                    [self destroyLinea];
                    self.connected = NO;
                    self.initialized = NO;
                    // Using lazy initialization in overlayAsView so do not instantiate now
                    //[self createScandit];
                    //[self resetConnectionState];
                    
                    if (self.supportedAutoIdModes == nil || [self.supportedAutoIdModes objectForKey:AUTO_ID_MODE_BARCODE_1D_KEY] == nil || [self.supportedAutoIdModes objectForKey:AUTO_ID_MODE_BARCODE_2D_KEY] == nil)
                        self.supportedAutoIdModes = [[NSDictionary alloc] initWithObjectsAndKeys:
                                                     AUTO_ID_MODE_BARCODE_1D_VALUE, AUTO_ID_MODE_BARCODE_1D_KEY,
                                                     AUTO_ID_MODE_BARCODE_2D_VALUE, AUTO_ID_MODE_BARCODE_2D_KEY,
                                                     nil];
                    self.initialized = YES;
                    self.connected = YES;
                    updated = YES;
                }
                self.lastReaderType = 0;
                break;
            case 1: // App setting for barcode is linea
                if (self.linea == nil || self.lastReaderType != 1) {
                    [self destroyScandit];
                    self.connected = NO;
                    self.initialized = NO;
                    [self createLinea];
                    [self resetConnectionState];
                    
                    // Update supported auto id modes - TODO: Check linea version to determine supported modes
                    if (self.supportedAutoIdModes == nil || [self.supportedAutoIdModes objectForKey:AUTO_ID_MODE_MAGSTRIPE_KEY] == nil || [self.supportedAutoIdModes objectForKey:AUTO_ID_MODE_BARCODE_1D_KEY] == nil)
                        self.supportedAutoIdModes = [[NSDictionary alloc] initWithObjectsAndKeys:
                                                     AUTO_ID_MODE_MAGSTRIPE_VALUE, AUTO_ID_MODE_MAGSTRIPE_KEY,
                                                     AUTO_ID_MODE_BARCODE_1D_VALUE, AUTO_ID_MODE_BARCODE_1D_KEY,
                                                     nil];
                    self.initialized = YES;
                    self.connected = YES;
                    updated = YES;
                }
                self.lastReaderType = 1;
                break;
            default:    // Default to simulator
                [self destroyScandit];
                [self destroyLinea];
                self.connected = NO;
                
                self.supportedAutoIdModes = [[NSDictionary alloc] initWithObjectsAndKeys:
                                             AUTO_ID_MODE_NONE_VALUE, AUTO_ID_MODE_NONE_KEY,
                                             nil];
                self.initialized = YES;
                updated = YES;
                self.lastReaderType = -1;
                break;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Failed to update VMBarcodeReader. An exception occurred: %@", [exception reason]);
    }
    return updated;
}

-(void)triggerRead {
    [self updateReader];
    
    @try {
        NSError *error=nil;
        switch (self.type) {
            case 0: // App setting for barcode is camera (aka scandit)
                [self overlayAsView];
                break;
            case 1: // App setting for barcode is linea
                [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.01]];
                int scanMode;
                BOOL scanSucceeded = [self.linea getScanMode:&scanMode error:&error];
                if(scanSucceeded && scanMode==MODE_MOTION_DETECT)
                {
                    if(self.scanProcessed)
                    {
                        self.scanProcessed=NO;
                        [self.linea stopScan:&error];
                    }else {
                        self.scanProcessed=YES;
                        [self.linea startScan:&error];
                    }
                }else {
                    [self.linea startScan:&error];
                }
                break;
            default:    // default to camera
            break;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Failed to trigger read for VMBarcodeReader. An exception occurred: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark Scandit Methods
/****************************************************************************
 * Scandit Methods
 ****************************************************************************/

/**
 * Makes different buttons visible for picker previews. Adjusts the overlaying picker's video
 * preview orientation and changes its dimensions to fit the screen in it's current orientation.
 */
- (void)adjustPickerToOrientation:(AVCaptureVideoOrientation)orientation {
    // Different buttons are visible depending on the rotation (modal and navigation picker
    // examples do not rotate, they can however be rotated by using the example class
    // ScanditSDKRotatingBarcodePicker from this project)
    if (orientation == AVCaptureVideoOrientationLandscapeLeft
        || orientation == AVCaptureVideoOrientationLandscapeRight) {
        // TODO - update view for landscape orientation
    } else {
        // TODO - update view for portrait orientation
    }
    
    if (self.pickerSubview && self.pickerSubviewButton && self.scandit) {
        // Change the picker's preview orientation. It is necessary to call this whenever the picker is
        // not in portrait mode or the orientation just changed.
        //self.scandit.cameraPreviewOrientation = orientation;
        /*
        //[self.scandit setCameraPreviewOrientation:orientation];
        
        // Add inputs and outputs.
        NSArray *devices = [AVCaptureDevice devices];
        
        //Print out all devices on phone
        for (AVCaptureDevice *device in devices) {
            if ([device hasMediaType:AVMediaTypeVideo]) {
            }
        }
         */
        
        // Adjust the picker's frame, bounds and the offset of the info banner to fit the new dimensions.
        CGRect screen = [[UIScreen mainScreen] bounds];
        CGRect subviewRect;
        if (orientation == AVCaptureVideoOrientationLandscapeLeft
            || orientation == AVCaptureVideoOrientationLandscapeRight) {
            if (self.scaledSubviewActive) {
                subviewRect = CGRectMake(0, 0, 2 * screen.size.height / 3, 2 * screen.size.width / 3);
                [self.scandit.overlayController setInfoBannerOffset:-3 * screen.size.width / 16];
                // Adjust the size to landscape mode.
                self.scandit.size = CGSizeMake(2 * screen.size.height / 3, 2 * screen.size.width / 3);
                
            } else {
                // Push the info banner up/down to make it visible.
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                    [self.scandit.overlayController setInfoBannerOffset:screen.size.width / 8];
                } else {
                    [self.scandit.overlayController setInfoBannerOffset:0];
                }
                subviewRect = CGRectMake(40, screen.size.width / 12, screen.size.height - 80,
                                         3 * screen.size.width / 4);
            }
            self.pickerSubviewButton.frame = CGRectMake(0, 0, screen.size.height, screen.size.width);
            
        } else {
            if (self.scaledSubviewActive) {
                subviewRect = CGRectMake(0, 0, 2 * screen.size.width / 3, 2 * screen.size.height / 3);
                [self.scandit.overlayController setInfoBannerOffset:-screen.size.height / 16];
                // Adjust the size to portrait mode.
                self.scandit.size = CGSizeMake(2 * screen.size.width / 3, 2 * screen.size.height / 3);
                
            } else {
                // Push the info banner up to make it visible.
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                    [self.scandit.overlayController setInfoBannerOffset:screen.size.height / 8];
                } else {
                    [self.scandit.overlayController setInfoBannerOffset:screen.size.height / 16];
                }
                subviewRect = CGRectMake(0, screen.size.height / 6 + 20, screen.size.width,
                                         2 * screen.size.height / 3);
            }
            self.pickerSubviewButton.frame = CGRectMake(0, 0, screen.size.width, screen.size.height);
        }
        self.pickerSubview.frame = subviewRect;
        if (self.scaledSubviewActive) {
            // If the status bar is visible we have to move the subviews content up 20 pixels because
            // the preview automatically gives the status bar room.
            self.pickerSubview.bounds = CGRectMake(0, 20, subviewRect.size.width, subviewRect.size.height);
        } else {
            self.pickerSubview.bounds = subviewRect;
        }
    }
}

// ScanditSDKOverlayControllerDelegate methods

- (void)scanditSDKOverlayController:(ScanditSDKOverlayController *)scanditSDKOverlayController1
                     didScanBarcode:(NSDictionary *)barcodeResult {
    
	
	[self.scandit stopScanning];
    
	if (barcodeResult == nil) return;
	
    NSString *symbology = [barcodeResult objectForKey:@"symbology"];
	NSString *barcode = [barcodeResult objectForKey:@"barcode"];
	NSString *title = [NSString stringWithFormat:@"Scanned %@ code: %@", symbology, barcode];
    
	self.lastScanValue = barcode;
	self.lastBarcodeSymbology = symbology;
	
	self.status = @"";
    self.status = [self.status stringByAppendingFormat:@"ISO Type: %@\n",symbology];
    self.status = [self.status stringByAppendingFormat:@"Barcode: %@",barcode];
    
    // Send read event
    NSDictionary *details = [[NSDictionary alloc] initWithObjectsAndKeys:
                             symbology,@"symbology",
                             barcode,@"scanValue",
                             nil];
    VMBarcodeReaderEvent *event = [[VMBarcodeReaderEvent alloc] init:EVENT_READ details:details];
    VMBarcodeTag *tag = [[VMBarcodeTag alloc] init:barcode symbology:symbology];
    event.tag = tag;
    if (self.delegate)
        [self.delegate handleBarcodeReaderReadEvent:barcode symbology:symbology event:event];
    
	UIActionSheet *decodedBarcodeActionSheet = [[UIActionSheet alloc] initWithTitle:title
																		   delegate:self
                                                                  cancelButtonTitle:@"OK"
															 destructiveButtonTitle:nil
																  otherButtonTitles:nil];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self closePickerSubview];
    } else {
        [decodedBarcodeActionSheet showInView:[(VMAppDelegate*)[[UIApplication sharedApplication] delegate] window]];
	}
    
	decodedBarcodeActionSheet = nil;
}

/**
 * This delegate method of the ScanditSDKOverlayController protocol needs to be implemented by
 * every app that uses the ScanditSDK and this is where the custom application logic goes.
 * In the example below, we are just showing an alert view that asks the user whether he
 * wants to continue scanning.
 */
- (void)scanditSDKOverlayController:(ScanditSDKOverlayController *)scanditSDKOverlayController1
                didCancelWithStatus:(NSDictionary *)status {
	
	// Stop the scanning process:
	[self.scandit stopScanning];
	
    /*
	[[self tabBarController] dismissModalViewControllerAnimated:YES];
     */
	
	self.scandit = nil;
	[[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

/**
 * This delegate method of the ScanditSDKOverlayController protocol needs to be implemented by
 * every app that uses the ScanditSDK and this is where the custom application logic goes.
 * In the example below, we are just showing an alert view that asks the user whether he
 * wants to continue scanning.
 */
- (void)scanditSDKOverlayController:(ScanditSDKOverlayController *)scanditSDKOverlayController
                    didManualSearch:(NSString *)input {
	
    [self.scandit.overlayController resetUI];
	// Stop the scanning process:
	[self.scandit stopScanning];
	
	NSString *title = [NSString stringWithFormat:@"User entered: %@", input];
	UIActionSheet *cancelBarcodeActionSheet = [[UIActionSheet alloc] initWithTitle:title
																		  delegate:self
                                                                 cancelButtonTitle:@"OK"
															destructiveButtonTitle:nil
																 otherButtonTitles:nil];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [cancelBarcodeActionSheet showInView:self.scandit.view];
    } else {
        [cancelBarcodeActionSheet showInView:[(VMAppDelegate*)[[UIApplication sharedApplication] delegate] window]];
	}
    cancelBarcodeActionSheet = nil;
}

/**
 * Delegate method of the actionsheet implemented above
 */
- (void)actionSheet:(UIActionSheet *)actionSheet
didDismissWithButtonIndex:(NSInteger)buttonIndex {
	switch (buttonIndex) {
		default: {
			// Reset recognition engine, to be able to recognize the same code again,
			// even without first moving away from it:
			[self.scandit reset];
            
            // Manually reset UI, in case we scanned already something before, to
            // make sure no information from the last scan is displayed until the
            // camera has been initialized:
            //[self.scandit.overlayController resetUI];
			
			// Start the recognition again:
			[self.scandit startScanning];
		}
	}
}

#pragma mark -
#pragma mark Showing the ScanditSDKBarcodePicker overlayed as a view

/**
 * A simple example of how the barcode picker can be used in a simple view of various dimensions
 * and how it can be added to any other view.
 */
- (IBAction)overlayAsView {
    if (!self.scandit) {
		// Instantiate the barcode picker.
		self.scandit = [[ScanditSDKBarcodePicker alloc] initWithAppKey:SCANDIT_SDK_APP_KEY];
	}
    
    self.pickerSubview = [[UIView alloc] init];
    [self.pickerSubview addSubview:self.scandit.view];
    [self.pickerSubview setClipsToBounds:YES];
    
    // Add a button behind the subview to close it.
    self.pickerSubviewButton = [[UIButton alloc] init];
    [self.pickerSubviewButton addTarget:self
                            action:@selector(closePickerSubview)
                  forControlEvents:UIControlEventTouchUpInside];
    
    [self.currentViewController.view addSubview:self.pickerSubviewButton];
    [self.currentViewController.view addSubview:self.pickerSubview];
    
    // Show the search bar for manual entry of a barcode.
    [self.scandit.overlayController showSearchBar:YES];
	
	// Show the tool bar with cancel button.
    [self.scandit.overlayController showToolBar:YES];
	
    // Set this class as the delegate for the overlay controller. It will now receive events when
    // a barcode was successfully scanned, manually entered or the cancel button was pressed.
	self.scandit.overlayController.delegate = self;
    
	// Set the center of the area where barcodes are detected successfully.
    // The default is the center of the screen (0.5, 0.5)
    [self.scandit setScanningHotSpotToX:0.5 andY:0.5];
    
    // Change the picker's preview orientation depending on the current orientation.
    // This is only necessary if the view that contains the picker is able to rotate.
    if (self.currentViewController.interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
        [self adjustPickerToOrientation:AVCaptureVideoOrientationPortraitUpsideDown];
    } else if (self.currentViewController.interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        [self adjustPickerToOrientation:AVCaptureVideoOrientationLandscapeRight];
    } else if (self.currentViewController.interfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
        [self adjustPickerToOrientation:AVCaptureVideoOrientationLandscapeLeft];
    } else {
        [self adjustPickerToOrientation:AVCaptureVideoOrientationPortrait];
    }
    
	[self.scandit startScanning];
}

/**
 * A simple example of how the barcode picker can be used in a simple view of various dimensions
 * and how it can be added to any other view. This example scales the view instead of cropping it.
 */
- (IBAction)overlayAsScaledView {
    if (!self.scandit) {
		// Instantiate the barcode picker.
		self.scandit = [[ScanditSDKBarcodePicker alloc]
                                   initWithAppKey:SCANDIT_SDK_APP_KEY];
	}
    
    self.pickerSubview = [[UIView alloc] init];
    [self.pickerSubview addSubview:self.scandit.view];
    [self.pickerSubview setClipsToBounds:YES];
    
    // Add a button behind the subview to close it.
    self.pickerSubviewButton = [[UIButton alloc] init];
    [self.pickerSubviewButton addTarget:self
                            action:@selector(closePickerSubview)
                  forControlEvents:UIControlEventTouchUpInside];
    
    [self.currentViewController.view addSubview:self.pickerSubviewButton];
    [self.currentViewController.view addSubview:self.pickerSubview];
    
	// Set the center of the area where barcodes are detected successfully.
    // The default is the center of the screen (0.5, 0.5)
    [self.scandit setScanningHotSpotToX:0.5 andY:0.5];
    
    self.scaledSubviewActive = YES;
    
    // Change the picker's preview orientation depending on the current orientation.
    // This is only necessary if the view that contains the picker is able to rotate.
    if (self.currentViewController.interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
        [self adjustPickerToOrientation:AVCaptureVideoOrientationPortraitUpsideDown];
    } else if (self.currentViewController.interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        [self adjustPickerToOrientation:AVCaptureVideoOrientationLandscapeRight];
    } else if (self.currentViewController.interfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
        [self adjustPickerToOrientation:AVCaptureVideoOrientationLandscapeLeft];
    } else {
        [self adjustPickerToOrientation:AVCaptureVideoOrientationPortrait];
    }
    
	[self.scandit startScanning];
}

- (void)closePickerSubview {
    if (self.scandit) {
        [self.scandit.view removeFromSuperview];
        //[self.scandit release];
        self.scandit = nil;
    }
    if (self.pickerSubview) {
        [self.pickerSubview removeFromSuperview];
        self.pickerSubview = nil;   //[self.pickerSubview release];
        if (self.pickerSubviewButton) {
            [self.pickerSubviewButton removeFromSuperview];
            self.pickerSubviewButton = nil; //[self.pickerSubviewButton release];
        }
        self.pickerSubview = nil;
    }
    
    self.scaledSubviewActive = NO;
}

#pragma mark -
#pragma mark Linea Methods
/****************************************************************************
 * Linea Methods
 ****************************************************************************/

-(void)connectionState:(int)state {
    switch (state) {
        case CONN_DISCONNECTED:
            @autoreleasepool {
                self.initialized = NO;
                self.status = @"Disconnected";
                NSDictionary *details = [[NSDictionary alloc] initWithObjectsAndKeys:
                                         self.status,@"status",
                                         nil];
                VMBarcodeReaderEvent *event = [[VMBarcodeReaderEvent alloc] init:EVENT_STATUS details:details];
                if (self.delegate)
                    [self.delegate handleBarcodeReaderStatusEvent:self.status event:event];
            }
            break;
        case CONN_CONNECTING:
            @autoreleasepool {
                self.status = @"Connecting";
                NSDictionary *details = [[NSDictionary alloc] initWithObjectsAndKeys:
                                         self.status,@"description",
                                         nil];
                VMBarcodeReaderEvent *event = [[VMBarcodeReaderEvent alloc] init:EVENT_STATUS details:details];
                if (self.delegate)
                    [self.delegate handleBarcodeReaderStatusEvent:self.status event:event];
            }
            break;
        case CONN_CONNECTED:
            @autoreleasepool {
                //keep the engine on by default, this is useful for 2D barcode engine that takes several seconds to power on
                if (!self.initialized) {
                    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"BarcodeEngineOn"]) {
                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"BarcodeEngineOn"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                    
                    if([self.linea.deviceModel rangeOfString:@"CM"].location!=NSNotFound) {
                        NSNumber *emsrAlgorithm=[[NSUserDefaults standardUserDefaults] objectForKey:@"emsrAlgorithm"];
                        if(emsrAlgorithm==nil)
                            emsrAlgorithm=[NSNumber numberWithInt:ALG_EH_AES256];
                        [self.linea emsrSetEncryption:[emsrAlgorithm intValue]+1 params:nil error:nil];
                        //NSArray *supported=[linea emsrGetSupportedEncryptions:&error];
                        //[self.linea emsrSetEncryption:ALG_EH_IDTECH params:nil error:nil];
                    }

                    /*
                    NSString *barcode1D = [self.supportedAutoIdModes objectForKey:AUTO_ID_MODE_BARCODE_1D_KEY];
                    if (barcode1D) {
                        NSError *error = [[NSError alloc] init];
                        @try {
                            [self.linea enableBarcode:BAR_UPC enabled:YES error:&error];
                            [self.linea enableBarcode:BAR_EAN13 enabled:YES error:&error];
                            [self.linea enableBarcode:BAR_CODE39 enabled:YES error:&error];
                            [self.linea enableBarcode:BAR_CODE128 enabled:YES error:&error];
                        }
                        @catch (NSException *exception) {
                            NSLog(@"Failed to enable barcode symbologies. An exception occurred: %@", [exception reason]);
                        }
                    }
                     */
                    
                    //calling this function last, after all notifications has been called in all registered delegates,
                    //because enabling/disabling charge in firmware versions <2.34 will force disconnect and reconnect
                    [self performSelectorOnMainThread:@selector(enableCharging) withObject:nil waitUntilDone:NO];
                    self.initialized = YES;
                }
                
                self.scanProcessed = NO;
                NSString *status = @"Connected";
                NSString *sdkVersion = [[NSString alloc] initWithFormat:@"%d.%d", self.linea.sdkVersion/100,self.linea.sdkVersion%100];
                self.status = [NSString stringWithFormat:@"SDK version: %@\n%@ %@ connected\nHardware revision: %@\nFirmware revision: %@\nSerial number: %@",sdkVersion,self.linea.deviceName,self.linea.deviceModel,self.linea.hardwareRevision,self.linea.firmwareRevision,self.linea.serialNumber];
                NSDictionary *details = [[NSDictionary alloc] initWithObjectsAndKeys:
                                         status,@"status",
                                         sdkVersion,@"sdkVersion",
                                         self.linea.deviceName,@"deviceName",
                                         self.linea.deviceModel,@"deviceModel",
                                         self.linea.hardwareRevision,@"hardwareRevision",
                                         self.linea.firmwareRevision,@"firmwareRevision",
                                         self.linea.serialNumber,@"serialNumber",
                                         nil];
                VMBarcodeReaderEvent *event = [[VMBarcodeReaderEvent alloc] init:EVENT_STATUS details:details];
                if (self.delegate)
                    [self.delegate handleBarcodeReaderStatusEvent:status event:event];
            }
            break;
    }
}

-(void)enableCharging {
    [self.linea setCharging:[[NSUserDefaults standardUserDefaults] boolForKey:@"AutoCharging"] error:nil];
}

-(void)lineaButtonPressed:(int)which {
	if (self.delegate) {
        self.debug = @"";
        [self.delegate handleBarcodeReaderButtonPressedEvent:which];
    }
}

-(void)lineaButtonReleased:(int)which {
    if (self.delegate) {
        [self.delegate handleBarcodeReaderButtonReleasedEvent:which];
    }
}

-(void)barcodeData:(NSString *)barcode isotype:(NSString *)isotype {
    @autoreleasepool {
        if (self.delegate) {
            // If barcodeScanner has LineaPro enabled, then proceed to process barcode data
            NSInteger barcodeScannerSetting = [[NSUserDefaults standardUserDefaults] integerForKey:@"barcodeScanner"];
            if (barcodeScannerSetting == 1) {
                self.lastScanValue = barcode;
                self.lastBarcodeSymbology = isotype;
                
                // Send read event
                NSDictionary *details = [[NSDictionary alloc] initWithObjectsAndKeys:
                                         isotype,@"isotype",
                                         barcode,@"scanValue",
                                         nil];
                VMBarcodeReaderEvent *event = [[VMBarcodeReaderEvent alloc] init:EVENT_READ details:details];
                VMBarcodeTag *tag = [[VMBarcodeTag alloc] init:self.lastScanValue symbology:self.lastBarcodeSymbology];
                event.tag = tag;
                if (self.delegate)
                    [self.delegate handleBarcodeReaderReadEvent:barcode symbology:isotype event:event];
            }
        }
    }
}

-(void)barcodeData:(NSString *)barcode type:(int)pType {
    @autoreleasepool {
        if (self.delegate) {
            // If barcodeScanner has LineaPro enabled, then proceed to process barcode data
            NSInteger barcodeScannerSetting = [[NSUserDefaults standardUserDefaults] integerForKey:@"barcodeScanner"];
            if (barcodeScannerSetting == 1) {
                self.lastScanValue = barcode;
                self.lastBarcodeSymbology = [self.linea barcodeType2Text:pType];
                
                // Send read event
                NSDictionary *details = [[NSDictionary alloc] initWithObjectsAndKeys:
                                         [NSNumber numberWithInteger:pType],@"type",
                                         [self.linea barcodeType2Text:pType],@"symbology",
                                         barcode,@"scanValue",
                                         nil];
                VMBarcodeReaderEvent *event = [[VMBarcodeReaderEvent alloc] init:EVENT_READ details:details];
                VMBarcodeTag *tag = [[VMBarcodeTag alloc] init:self.lastScanValue symbology:self.lastBarcodeSymbology];
                event.tag = tag;
                if (self.delegate)
                    [self.delegate handleBarcodeReaderReadEvent:barcode symbology:[self.linea barcodeType2Text:pType] event:event];
            }
        }
    }
}

// LineaPro Magnetic Card Reader Methods

-(void)magneticCardData:(NSString *)track1 track2:(NSString *)track2 track3:(NSString *)track3 {
    // Get the magneticCardReader setting from application defaults
    NSInteger magneticCardReader = [[NSUserDefaults standardUserDefaults] integerForKey:@"magneticCardReader"];
    // If magneticCardReader has LineaPro enabled, then proceed to process magnetic card data
    if (magneticCardReader == 1) {
        // Reset fields then update magnetic card and symbology properties
        self.lastMagneticCardTrack1=track1;
        self.lastMagneticCardTrack2=track2;
        self.lastMagneticCardTrack3=track3;
        
        // Update status
        self.status = @"";
        NSDictionary *card=[self.linea msProcessFinancialCard:track1 track2:track2];
        if(card)
        {
            self.lastCardName=[card valueForKey:@"cardholderName"];
            self.lastCardNumber=[card valueForKey:@"accountNumber"];
            self.lastExpDate=[NSString stringWithFormat:@"%@/%@\n",[card valueForKey:@"expirationMonth"],[card valueForKey:@"expirationYear"]];
            
            if(self.lastCardName)
                self.status = [self.status stringByAppendingFormat:@"Name: %@\n",self.lastCardName];
            if(self.lastCardNumber)
                self.status = [self.status stringByAppendingFormat:@"Number: %@\n",self.lastCardNumber];
            if(self.lastExpDate)
                self.status = [self.status stringByAppendingFormat:@"Expiration: %@\n",self.lastExpDate];
            self.status = [self.status stringByAppendingFormat:@"\n"];
        }
        if(track1!=NULL)
            self.status = [self.status stringByAppendingFormat:@"Track1: %@\n",track1];
        if(track2!=NULL)
            self.status = [self.status stringByAppendingFormat:@"Track2: %@\n",track2];
        if(track3!=NULL)
            self.status = [self.status stringByAppendingFormat:@"Track3: %@\n",track3];
        
        // Send read event
        NSDictionary *details = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 self.status,@"data",
                                 track1,@"Track1",
                                 track2,@"Track2",
                                 track3,@"Track3",
                                 self.lastCardName,@"cardholderName",
                                 self.lastCardNumber,@"accountNumber",
                                 self.lastExpDate,@"expiration",
                                 nil];
        if (self.status == nil && track1 != nil)
            self.status = track1;
        if (self.status == nil && track2 != nil)
            self.status = track2;
        if (self.status == nil && track3 != nil)
            self.status = track3;
        VMMagneticCardReaderEvent *event = [[VMMagneticCardReaderEvent alloc] init:EVENT_READ details:details];
        VMMagneticCardTag *tag = [[VMMagneticCardTag alloc] init:self.status cardName:self.lastCardName cardNumber:self.lastCardNumber expirationDate:self.lastExpDate magneticCardTrack1:track1 magneticCardTrack2:track2 magneticCardTrack3:track3];
        event.tag = tag;
        if (self.delegate)
            [self.delegate handleMagneticCardReaderReadEvent:self.status event:event];

        
        int sound[]={2730,150,0,30,2730,150};
        [self.linea playSound:100 beepData:sound length:sizeof(sound) error:nil];
    }
}

-(void)magneticCardRawData:(NSData *)tracks {
    // Get the magneticCardReader setting from application defaults
    NSInteger magneticCardReader = [[NSUserDefaults standardUserDefaults] integerForKey:@"magneticCardReader"];
    // If magneticCardReader has LineaPro enabled, then proceed to process magnetic card data
    if (magneticCardReader == 1) {
        self.status = [VMUtilityService toHexString:(void *)[tracks bytes] length:[tracks length] space:YES];
        //NSLog(@"Scanned magnetic card and captured raw data: %@",self.status);
        // Send read event
        NSDictionary *details = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 self.status,@"data",
                                 tracks,@"tracks",
                                 nil];
        VMMagneticCardReaderEvent *event = [[VMMagneticCardReaderEvent alloc] init:EVENT_READ details:details];
        VMMagneticCardTag *tag = [[VMMagneticCardTag alloc] init:self.status];
        event.tag = tag;
        if (self.delegate)
            [self.delegate handleMagneticCardReaderReadEvent:self.status event:event];
        
        int sound[]={2700,150,5400,150};
        [self.linea playSound:100 beepData:sound length:sizeof(sound) error:nil];
    }
}

-(void)magneticJISCardData:(NSString *)data {
    // Get the magneticCardReader setting from application defaults
    NSInteger magneticCardReader = [[NSUserDefaults standardUserDefaults] integerForKey:@"magneticCardReader"];
    // If magneticCardReader has LineaPro enabled, then proceed to process magnetic card data
    if (magneticCardReader == 1) {
        //NSString *cardData = [NSString stringWithFormat:@"JIS card data:\n%@",data];
        //NSLog(@"Scanned magnetic card and captured raw data: %@",cardData);
        
        // Send read event
        NSDictionary *details = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 data,@"data",
                                 nil];
        VMMagneticCardReaderEvent *event = [[VMMagneticCardReaderEvent alloc] init:EVENT_READ details:details];
        VMMagneticCardTag *tag = [[VMMagneticCardTag alloc] init:self.status];
        event.tag = tag;
        if (self.delegate)
            [self.delegate handleMagneticCardReaderReadEvent:data event:event];
        
        int sound[]={2730,150,0,30,2730,150};
        [self.linea playSound:100 beepData:sound length:sizeof(sound) error:nil];
    }
}

-(uint16_t)crc16:(uint8_t *)data  length:(int)length crc16:(uint16_t)crc16
{
	if(length==0) return 0;
	int i=0;
	while(length--)
	{
		crc16=(uint8_t)(crc16>>8)|(crc16<<8);
		crc16^=*data++;
		crc16^=(uint8_t)(crc16&0xff)>>4;
		crc16^=(crc16<<8)<<4;
		crc16^=((crc16&0xff)<<4)<<1;
		i++;
	}
	return crc16;
}

@end
