//
//  VMBarcodeReader.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMConstants.h"
#import "VMBarcodeReaderEvent.h"
#import "VMMagneticCardReaderEvent.h"
#import "LineaSDK.h"
#import "ScanditSDKOverlayController.h"
#import "ScanditSDKBarcodePicker.h"

#pragma mark -
#pragma mark Delegate Specification
/****************************************************************************
 * Delegate Specification
 ****************************************************************************/

@protocol VMBarcodeReaderDelegate
@optional
/**
 Notification sent when a barcode button is pressed
 @param which the barcode button pressed
 **/
-(void)handleBarcodeReaderButtonPressedEvent:(int)which;

/**
 Notification sent when a barcode button is released
 @param which the barcode button pressed
 **/
-(void)handleBarcodeReaderButtonReleasedEvent:(int)which;

/**
 Notification sent when a barcode status event occurs
 @param description the status event's description
 @param event the full details of status event
 **/
-(void)handleBarcodeReaderStatusEvent:(NSString *)description event:(VMBarcodeReaderEvent *)event;

/**
 Notification sent when a barcode read event occurs
 @param barcode the barcode value
 @param symbology the barcode symbology
 @param event the full details of read event
 **/
-(void)handleBarcodeReaderReadEvent:(NSString *)barcode symbology:(NSString *)symbology event:(VMBarcodeReaderEvent *)event;

/**
 Notification sent when a magnetic card read event occurs
 @param data the magnetic card data
 @param event the full details of read event
 **/
-(void)handleMagneticCardReaderReadEvent:(NSString *)data event:(VMMagneticCardReaderEvent *)event;

@required
@end

#pragma mark -
#pragma mark Interface Specification
/****************************************************************************
 * Interface Specification
 ****************************************************************************/

@interface VMBarcodeReader : NSObject <ScanditSDKOverlayControllerDelegate, UIActionSheetDelegate, LineaDelegate>

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@property (nonatomic, weak) id <VMBarcodeReaderDelegate> delegate;
@property (nonatomic) int type;
@property (nonatomic) BOOL initialized;
@property (nonatomic) BOOL connected;
@property (nonatomic, strong) NSDictionary *supportedAutoIdModes;
@property (nonatomic, strong) ScanditSDKBarcodePicker *scandit;
@property (nonatomic, strong) Linea *linea;
@property (nonatomic, weak) UIViewController *currentViewController;
@property (nonatomic) BOOL releaseOnAppear;
@property (nonatomic) BOOL scaledSubviewActive;
@property (nonatomic, strong) UIView *pickerSubview;
@property (nonatomic, strong) UIButton *pickerSubviewButton;

// Barcode Reader Properties
@property (nonatomic, weak) NSString *lastScanValue;
@property (nonatomic, weak) NSString *lastBarcodeSymbology;
@property (nonatomic, weak) NSString *status;
@property (nonatomic, weak) NSString *debug;

// Magnetic Card Reader Properties
@property (nonatomic,weak) NSString *lastCardName;
@property (nonatomic,weak) NSString *lastCardNumber;
@property (nonatomic,weak) NSString *lastExpDate;
@property (nonatomic,weak) NSString *lastMagneticCardTrack1;
@property (nonatomic,weak) NSString *lastMagneticCardTrack2;
@property (nonatomic,weak) NSString *lastMagneticCardTrack3;

#pragma mark -
#pragma mark Constructors
/****************************************************************************
 * Constructors
 ****************************************************************************/

- (id)init;

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose;

#pragma mark -
#pragma mark Auto-Id Methods
/****************************************************************************
 * Auto-Id Methods
 ****************************************************************************/

/**
 Resets the connection state with the device
 **/
- (void)resetConnectionState;

/**
 Updates the reader using the 'Barcode Scanner' application settings
 **/
-(BOOL)updateReader;

/**
 Triggers a single barcode read
 **/
- (void)triggerRead;

@end
