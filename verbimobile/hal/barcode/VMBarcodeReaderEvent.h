//
//  VMBarcodeReaderEvent.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMEvent.h"
#import "VMBarcodeTag.h"

@interface VMBarcodeReaderEvent : VMEvent

@property(nonatomic,weak) VMBarcodeTag *tag;

@end
