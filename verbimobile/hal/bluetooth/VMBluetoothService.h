//
//  VMBluetoothBLEShield.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BLE.h"

@interface VMBluetoothService : NSObject <BLEDelegate> {
    BLE *bleShield;
    int timeout;
}

- (id)init;

@end
