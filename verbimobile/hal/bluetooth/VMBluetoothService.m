//
//  VMBluetoothBLEShield.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMBluetoothService.h"

@implementation VMBluetoothService

- (id)init {
    self = [super init];
    if (self) {
        timeout = 3;
        bleShield = [[BLE alloc] init];
        [bleShield controlSetup:1];
        bleShield.delegate = self;
    }
    return self;
}

// Called when scan period is over to connect to the first found peripheral
-(void) connectionTimer:(NSTimer *)timer
{
    if(bleShield.peripherals.count > 0)
    {
        [bleShield connectPeripheral:[bleShield.peripherals objectAtIndex:0]];
    }
    else
    {
        //[self.spinner stopAnimating];
    }
}

/*
-(void) bleDidReceiveData:(unsigned char *)data length:(int)length
{
    
    NSData *d = [NSData dataWithBytes:data length:length];
    NSString *s = [[NSString alloc] initWithData:d encoding:NSUTF8StringEncoding];
    //self.label.text = s;
    
}
*/

- (void) bleDidDisconnect
{
    //[self.buttonConnect setTitle:@"Connect" forState:UIControlStateNormal];
}

-(void) bleDidConnect
{
    //[self.spinner stopAnimating];
    //[self.buttonConnect setTitle:@"Disconnect" forState:UIControlStateNormal];
}

-(void) bleDidUpdateRSSI:(NSNumber *)rssi
{
    //self.labelRSSI.text = rssi.stringValue;
}

- (IBAction)send:(NSString *)string
{
    NSString *formattedString;
    NSData *data;
    
    if (string.length > 16)
        formattedString = [string substringToIndex:16];
    else
        formattedString = string;
    
    formattedString = [NSString stringWithFormat:@"%@\r\n", formattedString];
    data = [formattedString dataUsingEncoding:NSUTF8StringEncoding];
    
    [bleShield write:data];
}

- (void)scan:(id)sender
{
    if (bleShield.activePeripheral)
        if(bleShield.activePeripheral.isConnected)
        {
            [[bleShield CM] cancelPeripheralConnection:[bleShield activePeripheral]];
            return;
        }
    
    if (bleShield.peripherals)
        bleShield.peripherals = nil;
    
    [bleShield findBLEPeripherals:timeout];
}

@end
