//
//  VMVerbiReaderBTDiscovery.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

#import "VMVerbiReaderBTService.h"

/****************************************************************************/
/*							UI protocols									*/
/****************************************************************************/
@protocol VMVerbiReaderBTDiscoveryDelegate <NSObject>
- (void) discoveryDidRefresh;
- (void) discoveryStatePoweredOff;
@end



/****************************************************************************/
/*							Discovery class									*/
/****************************************************************************/
@interface VMVerbiReaderBTDiscovery : NSObject
+ (id) sharedInstance;


/****************************************************************************/
/*								UI controls									*/
/****************************************************************************/
@property (nonatomic, unsafe_unretained) id<VMVerbiReaderBTDiscoveryDelegate> discoveryDelegate;
@property (nonatomic, unsafe_unretained) id<VMVerbiReaderBTServiceProtocol> peripheralDelegate;


/****************************************************************************/
/*								Actions										*/
/****************************************************************************/
- (void) startScanningForUUIDString:(NSString *)uuidString;
- (void) stopScanning;

- (void) connectPeripheral:(CBPeripheral*)peripheral;
- (void) disconnectPeripheral:(CBPeripheral*)peripheral;


/****************************************************************************/
/*							Access to the devices							*/
/****************************************************************************/
@property ( nonatomic) NSMutableArray    *foundPeripherals;
@property ( nonatomic) NSMutableArray	*connectedServices;	// Array of VMVerbiReaderBTService
@end
