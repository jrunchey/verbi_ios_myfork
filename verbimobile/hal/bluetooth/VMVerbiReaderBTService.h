//
//  VMVerbiReaderBTService.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

/****************************************************************************/
/*						Service Characteristics								*/
/****************************************************************************/
extern NSString *kTemperatureServiceUUIDString;                 // DEADF154-0000-0000-0000-0000DEADF154     Service UUID
extern NSString *kCurrentTemperatureCharacteristicUUIDString;   // CCCCFFFF-DEAD-F154-1319-740381000000     Current Temperature Characteristic
extern NSString *kMinimumTemperatureCharacteristicUUIDString;   // C0C0C0C0-DEAD-F154-1319-740381000000     Minimum Temperature Characteristic
extern NSString *kMaximumTemperatureCharacteristicUUIDString;   // EDEDEDED-DEAD-F154-1319-740381000000     Maximum Temperature Characteristic
extern NSString *kAlarmCharacteristicUUIDString;                // AAAAAAAA-DEAD-F154-1319-740381000000     Alarm Characteristic

extern NSString *kAlarmServiceEnteredBackgroundNotification;
extern NSString *kAlarmServiceEnteredForegroundNotification;

/****************************************************************************/
/*								Protocol									*/
/****************************************************************************/
@class VMVerbiReaderBTService;

typedef enum {
    kAlarmHigh  = 0,
    kAlarmLow   = 1,
} AlarmType;

@protocol VMVerbiReaderBTServiceProtocol<NSObject>
- (void) alarmService:(VMVerbiReaderBTService*)service didSoundAlarmOfType:(AlarmType)alarm;
- (void) alarmServiceDidStopAlarm:(VMVerbiReaderBTService*)service;
- (void) alarmServiceDidChangeTemperature:(VMVerbiReaderBTService*)service;
- (void) alarmServiceDidChangeTemperatureBounds:(VMVerbiReaderBTService*)service;
- (void) alarmServiceDidChangeStatus:(VMVerbiReaderBTService*)service;
- (void) alarmServiceDidReset;
@end

@interface VMVerbiReaderBTService : NSObject

- (id) initWithPeripheral:(CBPeripheral *)peripheral controller:(id<VMVerbiReaderBTServiceProtocol>)controller;
- (void) reset;
- (void) start;

/* Querying Sensor */
@property (readonly) CGFloat temperature;
@property (readonly) CGFloat minimumTemperature;
@property (readonly) CGFloat maximumTemperature;

/* Set the alarm cutoffs */
- (void) writeLowAlarmTemperature:(int)low;
- (void) writeHighAlarmTemperature:(int)high;

/* Behave properly when heading into and out of the background */
- (void)enteredBackground;
- (void)enteredForeground;

@property (weak, readonly) CBPeripheral *peripheral;@end
