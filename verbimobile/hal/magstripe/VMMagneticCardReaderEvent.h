//
//  VMMagneticCardReaderEvent.h
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMEvent.h"
#import "VMMagneticCardTag.h"

@interface VMMagneticCardReaderEvent : VMEvent

@property(nonatomic,weak) VMMagneticCardTag *tag;

@end
