//
//  VMLocationService.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMLocationService.h"

@implementation VMLocationService

#pragma mark -
#pragma mark CRUD Methods
/****************************************************************************
 * CRUD Methods
 ****************************************************************************/

// CREATE METHODS

+ (void)create:(VMLocation *)location {
    @try {
        if ([[VMLocationDAO sharedInstance] open])
            [[VMLocationDAO sharedInstance] create:location];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMLocationDAO sharedInstance] close];
    }
}

+ (void)createLocations:(NSMutableArray *)locations {
    @try {
        if ([[VMLocationDAO sharedInstance] open])
            [[VMLocationDAO sharedInstance] createList:locations];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMLocationDAO sharedInstance] close];
    }
}

// READ METHODS

+ (int)totalNumberOfRecords {
    int count = 0;
    @try {
        if ([[VMLocationDAO sharedInstance] open])
            count = [[VMLocationDAO sharedInstance] totalNumberOfRecords];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMLocationDAO sharedInstance] close];
    }
    return count;
}

+ (id)readByName:(NSString *)name {
    VMLocation *location;
    @try {
        if ([[VMLocationDAO sharedInstance] open])
            location = [[VMLocationDAO sharedInstance] readByName:name];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMLocationDAO sharedInstance] close];
    }
    return location;
}

+ (id)readByTagId:(NSString *)tagId {
    VMLocation *location;
    @try {
        if ([[VMLocationDAO sharedInstance] open])
            location = [(VMLocationDAO *)[VMLocationDAO sharedInstance] readByTagId:tagId];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMLocationDAO sharedInstance] close];
    }
    return location;
}

+ (id)readByEPC:(NSString *)epc {
    VMLocation *location;
    @try {
        if ([[VMLocationDAO sharedInstance] open])
            location = [(VMLocationDAO *)[VMLocationDAO sharedInstance] readByEPC:epc];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMLocationDAO sharedInstance] close];
    }
    return location;
}

+ (NSMutableArray *)readLikeName:(NSString *)name {
    NSMutableArray *locations;
    @try {
        if ([[VMLocationDAO sharedInstance] open])
            locations = [(VMLocationDAO *)[VMLocationDAO sharedInstance] readLikeName:name];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMLocationDAO sharedInstance] close];
    }
    return locations;
}

+ (NSMutableArray *)readAll {
    NSMutableArray *locations;
    @try {
        if ([[VMLocationDAO sharedInstance] open])
            locations = [[VMLocationDAO sharedInstance] readAll];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMLocationDAO sharedInstance] close];
    }
    return locations;
}

+ (NSMutableDictionary *)readAllAsDictionary {
    NSMutableDictionary *locationDictionary;
    @try {
        if ([[VMLocationDAO sharedInstance] open])
            locationDictionary = [[VMLocationDAO sharedInstance] readAllAsDictionary];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMLocationDAO sharedInstance] close];
    }
    return locationDictionary;
}


// UPDATE METHODS

+ (void)update:(VMLocation *)location {
    @try {
        if ([[VMLocationDAO sharedInstance] open])
            [[VMLocationDAO sharedInstance] update:location];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMLocationDAO sharedInstance] close];
    }
}

// DELETE METHODS

+ (void)deleteById:(NSString *)guid {
    @try {
        if ([[VMLocationDAO sharedInstance] open])
            [[VMLocationDAO sharedInstance] deleteById:guid];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMLocationDAO sharedInstance] close];
    }
}

+ (void)deleteByName:(NSString *)name {
    @try {
        if ([[VMLocationDAO sharedInstance] open])
            [[VMLocationDAO sharedInstance] deleteByName:name];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMLocationDAO sharedInstance] close];
    }
}

+ (void)deleteByNames:(NSMutableArray *)names {
    @try {
        if ([[VMLocationDAO sharedInstance] open])
            [[VMLocationDAO sharedInstance] deleteByNames:names];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMLocationDAO sharedInstance] close];
    }
}

@end
