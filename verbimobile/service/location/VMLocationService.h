//
//  VMLocationService.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMService.h"
#import "VMLocationDAO.h"

@interface VMLocationService : VMService

#pragma mark -
#pragma mark CRUD Methods
/****************************************************************************
 * CRUD Methods
 ****************************************************************************/

// CREATE METHODS

+ (void)create:(VMLocation *)location;
+ (void)createLocations:(NSMutableArray *)locations;

// READ METHODS

+ (int)totalNumberOfRecords;
+ (id)readByName:(NSString *)name;
+ (id)readByTagId:(NSString *)tagId;
+ (id)readByEPC:(NSString *)epc;
+ (NSMutableArray *)readLikeName:(NSString *)name;
+ (NSMutableArray *)readAll;
+ (NSMutableDictionary *)readAllAsDictionary;

// UPDATE METHODS

+ (void)update:(VMLocation *)location;

// DELETE METHODS

+ (void)deleteById:(NSString *)guid;
+ (void)deleteByName:(NSString *)name;
+ (void)deleteByNames:(NSMutableArray *)names;

@end
