//
//  VMReportingService.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMReportService.h"

@implementation VMReportService

#pragma mark -
#pragma mark CRUD Methods
/****************************************************************************
 * CRUD Methods
 ****************************************************************************/

// CREATE METHODS

+ (void)create:(VMReport *)report {
    @try {
        if ([[VMReportDAO sharedInstance] open])
            [[VMReportDAO sharedInstance] create:report];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMReportDAO sharedInstance] close];
    }
}

// READ METHODS

+ (int)totalNumberOfRecords {
    int count = 0;
    @try {
        if ([[VMReportDAO sharedInstance] open])
            count = [[VMReportDAO sharedInstance] totalNumberOfRecords];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMReportDAO sharedInstance] close];
    }
    return count;
}

+ (id)readByName:(NSString *)name {
    VMReport *report;
    @try {
        if ([[VMReportDAO sharedInstance] open])
            report = [[VMReportDAO sharedInstance] readByName:name];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMReportDAO sharedInstance] close];
    }
    return report;
}

+ (NSMutableArray *)readLikeName:(NSString *)name {
    NSMutableArray *reports;
    @try {
        if ([[VMReportDAO sharedInstance] open])
            reports = [(VMReportDAO *)[VMReportDAO sharedInstance] readLikeName:name];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMReportDAO sharedInstance] close];
    }
    return reports;
}

+ (NSMutableArray *)readAll {
    NSMutableArray *reports;
    @try {
        if ([[VMReportDAO sharedInstance] open])
            reports = [[VMReportDAO sharedInstance] readAll];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMReportDAO sharedInstance] close];
    }
    return reports;
}

+ (NSMutableDictionary *)readAllAsDictionary {
    NSMutableDictionary *reportDictionary;
    @try {
        if ([[VMReportDAO sharedInstance] open])
            reportDictionary = [[VMReportDAO sharedInstance] readAllAsDictionary];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMReportDAO sharedInstance] close];
    }
    return reportDictionary;
}


// UPDATE METHODS

+ (void)update:(VMReport *)report {
    @try {
        if ([[VMReportDAO sharedInstance] open])
            [[VMReportDAO sharedInstance] update:report];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMReportDAO sharedInstance] close];
    }
}

// DELETE METHODS

+ (void)deleteById:(NSString *)guid {
    @try {
        if ([[VMReportDAO sharedInstance] open])
            [[VMReportDAO sharedInstance] deleteById:guid];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMReportDAO sharedInstance] close];
    }
}

+ (void)deleteByName:(NSString *)name {
    @try {
        if ([[VMReportDAO sharedInstance] open])
            [[VMReportDAO sharedInstance] deleteByName:name];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMReportDAO sharedInstance] close];
    }
}

+ (void)deleteByNames:(NSMutableArray *)names {
    @try {
        if ([[VMReportDAO sharedInstance] open])
            [[VMReportDAO sharedInstance] deleteByNames:names];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMReportDAO sharedInstance] close];
    }
}

#pragma mark -
#pragma mark Business Logic Methods
/****************************************************************************
 * Business Logic Methods
 ****************************************************************************/

+ (NSMutableArray *)processReport:(VMReport *)report {
    NSMutableArray *reportResults = nil;
    if (report) {
        if ([[report.type lowercaseString] caseInsensitiveCompare:INVENTORY_ITEM] == NSOrderedSame) {
            NSMutableArray *inventoryItems = [VMInventoryService readAllItems];
            if (inventoryItems && inventoryItems.count > 0) {
                reportResults = [[NSMutableArray alloc] init];
                //loop thru inventory items and add them to reportResults
            }
        }
        else if ([[report.type lowercaseString] caseInsensitiveCompare:INVENTORY_EVENT] == NSOrderedSame) {
        }
    }
    return reportResults;
}

@end
