//
//  VMReportingService.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMService.h"
#import "VMReportDAO.h"
#import "VMReportGroupMapDAO.h"
#import "VMReportUserMapDAO.h"
#import "VMReportItemMasterMapDAO.h"
#import "VMReportLocationMapDAO.h"
#import "VMReportResult.h"
#import "VMInventoryService.h"

@interface VMReportService : VMService

#pragma mark -
#pragma mark CRUD Methods
/****************************************************************************
 * CRUD Methods
 ****************************************************************************/

// CREATE METHODS

+ (void)create:(VMReport *)report;

// READ METHODS

+ (int)totalNumberOfRecords;
+ (id)readByName:(NSString *)name;
+ (NSMutableArray *)readLikeName:(NSString *)name;
+ (NSMutableArray *)readAll;
+ (NSMutableDictionary *)readAllAsDictionary;

// UPDATE METHODS

+ (void)update:(VMReport *)report;

// DELETE METHODS

+ (void)deleteById:(NSString *)guid;
+ (void)deleteByName:(NSString *)name;
+ (void)deleteByNames:(NSMutableArray *)names;


#pragma mark -
#pragma mark Business Logic Methods
/****************************************************************************
 * Business Logic Methods
 ****************************************************************************/

+ (NSMutableArray *)processReport:(VMReport *)report;

@end
