//
//  VMUtilityService.h
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMService.h"

@interface VMUtilityService : VMService

#pragma mark -
#pragma mark Business Logic Methods
/****************************************************************************
 * Business Logic Methods
 ****************************************************************************/

+ (NSString *)toHexString:(void *)data length:(int)length space:(BOOL)space;
+ (NSMutableArray *)getCheckedOrSelectedIndexes:(UITableView *)tableView;
+ (BOOL)isTouchWithinBounds:(CGPoint *)touchPoint bounds:(CGRect *) bounds;
+ (BOOL)compareImage:(UIImage *)image1 isEqualTo:(UIImage *)image2;

@end
