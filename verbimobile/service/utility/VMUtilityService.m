//
//  VMUtilityService.m
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMUtilityService.h"

@implementation VMUtilityService

#pragma mark -
#pragma mark Business Logic Methods
/****************************************************************************
 * Business Logic Methods
 ****************************************************************************/

+ (NSString *)toHexString:(void *)data length:(int)length space:(BOOL)space {
	const char HEX[]="0123456789ABCDEF";
	char s[2000];
	
	int len=0;
	for(int i=0;i<length;i++)
	{
		s[len++]=HEX[((uint8_t *)data)[i]>>4];
		s[len++]=HEX[((uint8_t *)data)[i]&0x0f];
        if(space)
            s[len++]=' ';
	}
	s[len]=0;
	return [NSString stringWithCString:s encoding:NSASCIIStringEncoding];
}

+ (NSMutableArray *)getCheckedOrSelectedIndexes:(UITableView *)tableView {
    NSMutableArray *indexes = [[NSMutableArray alloc] init];
    
    // Add selected indexes
    NSArray *selectedIndexes = [tableView indexPathsForSelectedRows];
    if (selectedIndexes.count > 0)
        [indexes addObjectsFromArray:selectedIndexes];
    
    // Add checked indexes
    NSMutableArray *checkedIndexes = [[NSMutableArray alloc] init];
    int rowCount = [tableView numberOfRowsInSection:0];
    for (int i = 0; i < rowCount; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];    // convert iterator to indexPath
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];    // get cell using indexPath
        if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
            [checkedIndexes addObject:indexPath];
        }
    }
    if (checkedIndexes.count > 0)
        [indexes addObjectsFromArray:checkedIndexes];
    
    return indexes;
}

+ (BOOL)isTouchWithinBounds:(CGPoint *)touchPoint bounds:(CGRect *) bounds {
    CGPoint origin = bounds->origin;
    CGSize size = bounds->size;
    int x1 = origin.x;
    int x2 = origin.x + size.width;
    int y1 = origin.y;
    int y2 = origin.y + size.height;
    if (touchPoint->x < x1 || touchPoint->x > x2 || touchPoint->y < y1 || touchPoint->y > y2)
        return YES;
    return NO;
}

+ (BOOL)compareImage:(UIImage *)image1 isEqualTo:(UIImage *)image2 {
    NSData *data1 = UIImagePNGRepresentation(image1);
    NSData *data2 = UIImagePNGRepresentation(image2);
    return [data1 isEqual:data2];
}

@end
