//
//  VMAccessControlService.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMService.h"
#import "VMUserDAO.h"
#import "VMGroupDAO.h"

@class VMSession;

@interface VMAccessControlService : VMService;

#pragma mark -
#pragma mark Access Control Methods
/****************************************************************************
 * Access Control Methods
 ****************************************************************************/

+ (VMSession *)authenticate:(NSString *)username password:(NSString *)password;

#pragma mark -
#pragma mark CRUD Methods - ITEM MASTER
/****************************************************************************
 * CRUD Methods - ITEM MASTER
 ****************************************************************************/

// CREATE METHODS

+ (void)createUser:(VMUser *)user;

// READ METHODS

+ (int)totalNumberOfUserRecords;
+ (id)readUserByName:(NSString *)name;
+ (NSMutableArray *)readAllUsers;

// UPDATE METHODS

+ (void)updateUser:(VMUser *)user;

// DELETE METHODS

+ (void)deleteUserById:(NSString *)guid;
+ (void)deleteUserByName:(NSString *)name;
+ (void)deleteUserByNames:(NSMutableArray *)names;

@end
