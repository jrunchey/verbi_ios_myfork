//
//  VMAuthenticator.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMAccessControlService.h"
#import "VMSession.h"
#import "VMUser.h"
#import "VMBarcodeReader.h"

@implementation VMAccessControlService {
}

#pragma mark ACCESS CONTROL METHODS

+ (VMSession *)authenticate:(NSString *)username password:(NSString *)password {
    @autoreleasepool {
        VMUser *matchUser = [self readUserByName:username];
        if (matchUser && [matchUser.password isEqualToString:password]){
            VMSession *session = [[VMSession alloc] initWithUser:matchUser]; // create session with this user associated
            
            /*
            // If not simulator, instantiate hardware objects
            if ([IS_SIMULATOR intValue] == 0) {
                // BARCODE
                session.barcodeReader = [[VMBarcodeReader alloc] init];
                if (session.barcodeReader && session.barcodeReader.supportedAutoIdModes && session.barcodeReader.supportedAutoIdModes.count > 0) {
                    [session.availableAutoIdModes addEntriesFromDictionary:session.barcodeReader.supportedAutoIdModes];
                }
                
                // RFID
                session.rfidReader = [[VMRFIDReader alloc] init];
                if (session.rfidReader && session.rfidReader.supportedAutoIdModes && session.rfidReader.supportedAutoIdModes.count > 0) {
                    
                    [session.availableAutoIdModes addEntriesFromDictionary:session.rfidReader.supportedAutoIdModes];
                }
            }
            */
            
            session.valid = YES;
            return session;
        }
    }
    return nil;
}

+ (void)initializeBLEShield:(VMSession *)session {
    /*
    if (session.bleShield == nil) {
        session.bleShield = [[BLE alloc] init];
        [session.bleShield controlSetup:1];
        
        if (session.availableAutoIdModes) {
            [session.availableAutoIdModes setValue:AUTO_ID_MODE_RFID_C1G2_VALUE forKey:AUTO_ID_MODE_RFID_C1G2_KEY];
        }
    }
     */
}

#pragma mark CRUD METHODS

// CREATE METHODS

+ (void)createUser:(VMUser *)user {
    @try {
        if ([[VMUserDAO sharedInstance] open])
            [[VMUserDAO sharedInstance] create:user];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMUserDAO sharedInstance] close];
    }
}

// READ METHODS

+ (int)totalNumberOfUserRecords {
    int count = 0;
    @try {
        if ([[VMUserDAO sharedInstance] open])
            count = [[VMUserDAO sharedInstance] totalNumberOfRecords];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMUserDAO sharedInstance] close];
    }
    return count;
}

+ (id)readUserByName:(NSString *)name {
    VMUser *user;
    @try {
        if ([[VMUserDAO sharedInstance] open])
            user = [[VMUserDAO sharedInstance] readByName:name];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMUserDAO sharedInstance] close];
    }
    return user;
}

+ (NSMutableArray *)readAllUsers {
    NSMutableArray *users;
    @try {
        if ([[VMUserDAO sharedInstance] open])
            users = [[VMUserDAO sharedInstance] readAll];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMUserDAO sharedInstance] close];
    }
    return users;
}

// UPDATE METHODS

+ (void)updateUser:(VMUser *)user {
    @try {
        if ([[VMUserDAO sharedInstance] open])
            [[VMUserDAO sharedInstance] update:user];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMUserDAO sharedInstance] close];
    }
}

// DELETE METHODS

+ (void)deleteUserById:(NSString *)guid {
    @try {
        if ([[VMUserDAO sharedInstance] open])
            [[VMUserDAO sharedInstance] deleteById:guid];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMUserDAO sharedInstance] close];
    }
}

+ (void)deleteUserByName:(NSString *)name {
    @try {
        if ([[VMUserDAO sharedInstance] open])
            [[VMUserDAO sharedInstance] deleteByName:name];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMUserDAO sharedInstance] close];
    }
}

+ (void)deleteUserByNames:(NSMutableArray *)names {
    @try {
        if ([[VMUserDAO sharedInstance] open])
            [[VMUserDAO sharedInstance] deleteByNames:names];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMUserDAO sharedInstance] close];
    }
}

@end
