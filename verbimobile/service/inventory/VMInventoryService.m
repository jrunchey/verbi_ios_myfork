//
//  VMInventoryService.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMInventoryService.h"

@implementation VMInventoryService

#pragma mark -
#pragma mark CRUD Methods - ITEM MASTER
/****************************************************************************
 * CRUD Methods - ITEM MASTER
 ****************************************************************************/

// CREATE METHODS

+ (void)createItemMaster:(VMItemMaster *)itemMaster {
    @try {
        if ([[VMItemMasterDAO sharedInstance] open])
            [[VMItemMasterDAO sharedInstance] create:itemMaster];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemMasterDAO sharedInstance] close];
    }
}

+ (void)createItemMasters:(NSMutableArray *)itemMasters {
    @try {
        if ([[VMItemMasterDAO sharedInstance] open])
            [[VMItemMasterDAO sharedInstance] createList:itemMasters];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemMasterDAO sharedInstance] close];
    }
}

// READ METHODS

+ (int)itemMasterCount {
    int count = 0;
    @try {
        if ([[VMItemMasterDAO sharedInstance] open])
            count = [[VMItemMasterDAO sharedInstance] totalNumberOfRecords];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemMasterDAO sharedInstance] close];
    }
    return count;
}

+ (id)readItemMasterById:(NSString *)guid {
    VMItemMaster *itemMaster;
    @try {
        if ([[VMItemMasterDAO sharedInstance] open])
            itemMaster = [[VMItemMasterDAO sharedInstance] readById:guid];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemMasterDAO sharedInstance] close];
    }
    return itemMaster;
}

+ (id)readItemMasterByName:(NSString *)name {
    VMItemMaster *itemMaster;
    @try {
        if ([[VMItemMasterDAO sharedInstance] open])
            itemMaster = [[VMItemMasterDAO sharedInstance] readByName:name];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemMasterDAO sharedInstance] close];
    }
    return itemMaster;
}

+ (id)readItemMasterByDescription:(NSString *)description {
    VMItemMaster *itemMaster;
    @try {
        if ([[VMItemMasterDAO sharedInstance] open])
            itemMaster = [(VMItemMasterDAO *)[VMItemMasterDAO sharedInstance] readByDescription:description];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemMasterDAO sharedInstance] close];
    }
    return itemMaster;
}

+ (id)readItemMasterByUPC:(NSString *)upc {
    VMItemMaster *itemMaster;
    @try {
        if ([[VMItemMasterDAO sharedInstance] open])
            itemMaster = [(VMItemMasterDAO *)[VMItemMasterDAO sharedInstance] readByUPC:upc];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemMasterDAO sharedInstance] close];
    }
    return itemMaster;
}

+ (id)readItemMasterByVendorCode:(NSString *)vendorCode {
    VMItemMaster *itemMaster;
    @try {
        if ([[VMItemMasterDAO sharedInstance] open])
            itemMaster = [(VMItemMasterDAO *)[VMItemMasterDAO sharedInstance] readByVendorCode:vendorCode];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemMasterDAO sharedInstance] close];
    }
    return itemMaster;
}

+ (NSMutableArray *)readLikeName:(NSString *)name {
    NSMutableArray *itemMasters;
    @try {
        if ([[VMItemMasterDAO sharedInstance] open])
            itemMasters = [(VMItemMasterDAO *)[VMItemMasterDAO sharedInstance] readLikeName:name];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemMasterDAO sharedInstance] close];
    }
    return itemMasters;
}

+ (NSMutableArray *)readAllItemMasters {
    NSMutableArray *itemMasters;
    @try {
        if ([[VMItemMasterDAO sharedInstance] open])
            itemMasters = [[VMItemMasterDAO sharedInstance] readAll];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemMasterDAO sharedInstance] close];
    }
    return itemMasters;
}

+ (NSMutableDictionary *)readAllItemMastersAsDictionary {
    NSMutableDictionary *itemMasterDictionary;
    @try {
        if ([[VMItemMasterDAO sharedInstance] open])
            itemMasterDictionary = [[VMItemMasterDAO sharedInstance] readAllAsDictionary];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemMasterDAO sharedInstance] close];
    }
    return itemMasterDictionary;
}

// UPDATE METHODS

+ (void)updateItemMaster:(VMItemMaster *)itemMaster {
    @try {
        if ([[VMItemMasterDAO sharedInstance] open])
            [[VMItemMasterDAO sharedInstance] update:itemMaster];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemMasterDAO sharedInstance] close];
    }
}

// DELETE METHODS

+ (void)deleteItemMasterById:(NSString *)guid {
    @try {
        if ([[VMItemMasterDAO sharedInstance] open])
            [[VMItemMasterDAO sharedInstance] deleteById:guid];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemMasterDAO sharedInstance] close];
    }
}

+ (void)deleteItemMasterByName:(NSString *)name {
    @try {
        if ([[VMItemMasterDAO sharedInstance] open])
            [[VMItemMasterDAO sharedInstance] deleteByName:name];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemMasterDAO sharedInstance] close];
    }
}

+ (void)deleteItemMasterByNames:(NSMutableArray *)names {
    @try {
        if ([[VMItemMasterDAO sharedInstance] open])
            [[VMItemMasterDAO sharedInstance] deleteByNames:names];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemMasterDAO sharedInstance] close];
    }
}

#pragma mark -
#pragma mark CRUD Methods - ITEM
/****************************************************************************
 * CRUD Methods - ITEM
 ****************************************************************************/

// CREATE METHODS

+ (void)createItem:(VMItem *)item {
    @try {
        if ([[VMItemDAO sharedInstance] open])
            [[VMItemDAO sharedInstance] create:item];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemDAO sharedInstance] close];
    }
}

+ (void)createItems:(NSMutableArray *)items {
    @try {
        if ([[VMItemDAO sharedInstance] open])
            [[VMItemDAO sharedInstance] createList:items];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemDAO sharedInstance] close];
    }
}

// READ METHODS

+ (int)itemCount {
    int count = 0;
    @try {
        if ([[VMItemDAO sharedInstance] open])
            count = [[VMItemDAO sharedInstance] totalNumberOfRecords];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemDAO sharedInstance] close];
    }
    return count;
}

+ (int)itemCount:(NSString *)itemMasterId locationId:(NSString *)locationId {
    int count = 0;
    @try {
        if ([[VMItemDAO sharedInstance] open])
            count = [(VMItemDAO *)[VMItemDAO sharedInstance] totalNumberOfRecords:itemMasterId locationId:locationId];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemDAO sharedInstance] close];
    }
    return count;
}

+ (int)itemCount:(NSString *)itemMasterId locationId:(NSString *)locationId status:(NSString *)status {
    int count = 0;
    @try {
        if ([[VMItemDAO sharedInstance] open])
            count = [(VMItemDAO *)[VMItemDAO sharedInstance] totalNumberOfRecords:itemMasterId locationId:locationId status:status];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemDAO sharedInstance] close];
    }
    return count;
}

+ (id)readItemById:(NSString *)guid {
    VMItem *item;
    @try {
        if ([[VMItemDAO sharedInstance] open])
            item = [[VMItemDAO sharedInstance] readById:guid];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemDAO sharedInstance] close];
    }
    return item;
}

+ (NSMutableArray *)readItems:(NSString *)itemMasterId locationId:(NSString *)locationId {
    NSMutableArray *items;
    @try {
        if ([[VMItemDAO sharedInstance] open])
            items = [(VMItemDAO *)[VMItemDAO sharedInstance] read:itemMasterId locationId:locationId];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemDAO sharedInstance] close];
    }
    return items;
}

+ (NSMutableArray *)readItems:(NSString *)itemMasterId locationId:(NSString *)locationId status:(NSString *)status {
    NSMutableArray *items;
    @try {
        if ([[VMItemDAO sharedInstance] open])
            items = [(VMItemDAO *)[VMItemDAO sharedInstance] read:itemMasterId locationId:locationId status:status];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemDAO sharedInstance] close];
    }
    return items;
}

+ (NSMutableArray *)readItemsByItemMasterId:(NSString *)itemMasterId {
    NSMutableArray *items;
    @try {
        if ([[VMItemDAO sharedInstance] open])
            items = [(VMItemDAO *)[VMItemDAO sharedInstance] readByItemMasterId:itemMasterId];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemDAO sharedInstance] close];
    }
    return items;
}

+ (NSMutableArray *)readItemsByLocationId:(NSString *)locationId {
    NSMutableArray *items;
    @try {
        if ([[VMItemDAO sharedInstance] open])
            items = [(VMItemDAO *)[VMItemDAO sharedInstance] readByLocationId:locationId];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemDAO sharedInstance] close];
    }
    return items;
}

+ (NSMutableArray *)readItemsByPlaceOfOrigin:(NSString *)placeOfOrigin {
    NSMutableArray *items;
    @try {
        if ([[VMItemDAO sharedInstance] open])
            items = [(VMItemDAO *)[VMItemDAO sharedInstance] readByPlaceOfOrigin:placeOfOrigin];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemDAO sharedInstance] close];
    }
    return items;
}

+ (NSMutableArray *)readItemsByLotNumber:(NSString *)lotNumber {
    NSMutableArray *items;
    @try {
        if ([[VMItemDAO sharedInstance] open])
            items = [(VMItemDAO *)[VMItemDAO sharedInstance] readByLotNumber:lotNumber];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemDAO sharedInstance] close];
    }
    return items;
}

+ (NSMutableArray *)readAllItems {
    NSMutableArray *items;
    @try {
        if ([[VMItemDAO sharedInstance] open])
            items = [[VMItemDAO sharedInstance] readAll];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemDAO sharedInstance] close];
    }
    return items;
}

+ (NSMutableDictionary *)readAllItemsAsDictionary {
    NSMutableDictionary *itemDictionary;
    @try {
        if ([[VMItemDAO sharedInstance] open])
            itemDictionary = [[VMItemDAO sharedInstance] readAllAsDictionary];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemDAO sharedInstance] close];
    }
    return itemDictionary;
}

// UPDATE METHODS

+ (void)updateItem:(VMItem *)item {
    @try {
        if ([[VMItemDAO sharedInstance] open])
            [[VMItemDAO sharedInstance] update:item];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemDAO sharedInstance] close];
    }
}

+ (void)updateItems:(NSMutableArray *)items {
    @try {
        if ([[VMItemDAO sharedInstance] open])
            [[VMItemDAO sharedInstance] updateList:items];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemDAO sharedInstance] close];
    }
}

// DELETE METHODS

+ (void)deleteItemById:(NSString *)guid {
    @try {
        if ([[VMItemDAO sharedInstance] open])
            [[VMItemDAO sharedInstance] deleteById:guid];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemDAO sharedInstance] close];
    }
}

#pragma mark -
#pragma mark Business Methods - ITEM
/****************************************************************************
 * Business Methods - ITEM
 ****************************************************************************/

+ (void)updateItemWithItem:(VMItem *)source target:(VMItem *)target incrementQty:(BOOL)incrementQty {
    target.itemMasterGUID = [source.itemMasterGUID copy];
    target.itemMaster = source.itemMaster;
    target.locationGUID = [source.locationGUID copy];
    target.location = source.location;
    target.originationDate = [source.originationDate copy];
    target.maturityDate = [source.maturityDate copy];
    target.bestByDate = [source.bestByDate copy];
    target.changeDate = [source.changeDate copy];
    target.retestDate = [source.retestDate copy];
    target.expirationDate = [source.expirationDate copy];
    target.territoryCode = [source.territoryCode copy];
    target.placeOfOrigin = [source.placeOfOrigin copy];
    target.lotNumber = [source.lotNumber copy];
    target.dateCode = [source.dateCode copy];
    target.grade = [source.grade copy];
    target.color = [source.color copy];
    target.status = [source.status copy];
    target.condition = [source.condition copy];
    if (incrementQty)
        target.quantity += source.quantity; // increment quantity by new amount
    else
        target.quantity = source.quantity; // increment quantity by new amount
    target.value = target.quantity * target.itemMaster.price; // update based on new incremented quantity of target
    target.ageInDays = source.ageInDays;
    target.length = source.length;
    target.thickness = source.thickness;
    target.volume = source.volume;
    target.width = source.width;
    target.recycledContent = source.recycledContent;
    target.lastUpdatedDate = [NSDate date];
    target.lastUpdatedUserId = [source.lastUpdatedUserId copy];
    target.lastUpdatedUser = source.lastUpdatedUser;
    target.enabled = source.enabled;
    target.archived = source.archived;
}

#pragma mark -
#pragma mark CRUD Methods - ITEM EVENT
/****************************************************************************
 * CRUD Methods - ITEM EVENT
 ****************************************************************************/

// CREATE METHODS

+ (void)createItemEvent:(VMItemEvent *)itemEvent {
    @try {
        if ([[VMItemEventDAO sharedInstance] open])
            [[VMItemEventDAO sharedInstance] create:itemEvent];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemEventDAO sharedInstance] close];
    }
}

+ (void)createItemEvents:(NSMutableArray *)itemEvents {
    @try {
        if ([[VMItemEventDAO sharedInstance] open])
            [[VMItemEventDAO sharedInstance] createList:itemEvents];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemEventDAO sharedInstance] close];
    }
}

// READ METHODS

+ (int)itemEventCount {
    int count = 0;
    @try {
        if ([[VMItemEventDAO sharedInstance] open])
            count = [[VMItemEventDAO sharedInstance] totalNumberOfRecords];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemEventDAO sharedInstance] close];
    }
    return count;
}

+ (id)readItemEventById:(NSString *)guid {
    VMItemEvent *itemEvent;
    @try {
        if ([[VMItemEventDAO sharedInstance] open])
            itemEvent = [[VMItemEventDAO sharedInstance] readById:guid];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemEventDAO sharedInstance] close];
    }
    return itemEvent;
}

+ (NSMutableArray *)readItemEventsByItemId:(NSString *)itemId {
    NSMutableArray *itemEvents;
    @try {
        if ([[VMItemEventDAO sharedInstance] open])
            itemEvents = [(VMItemEventDAO *)[VMItemEventDAO sharedInstance] readByItemId:itemId];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemEventDAO sharedInstance] close];
    }
    return itemEvents;
}

+ (NSMutableArray *)readItemEventsByLocationId:(NSString *)locationId {
    NSMutableArray *itemEvents;
    @try {
        if ([[VMItemEventDAO sharedInstance] open])
            itemEvents = [(VMItemEventDAO *)[VMItemEventDAO sharedInstance] readByLocationId:locationId];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemEventDAO sharedInstance] close];
    }
    return itemEvents;
}

+ (NSMutableArray *)readItemEventsByLocationName:(NSString *)locationName {
    NSMutableArray *itemEvents;
    @try {
        if ([[VMItemEventDAO sharedInstance] open])
            itemEvents = [(VMItemEventDAO *)[VMItemEventDAO sharedInstance] readByLocationName:locationName];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemEventDAO sharedInstance] close];
    }
    return itemEvents;
}

+ (NSMutableArray *)readItemEventsByItemIdAndLocationId:(NSString *)itemId locationId:(NSString *)locationId {
    NSMutableArray *itemEvents;
    @try {
        if ([[VMItemEventDAO sharedInstance] open])
            itemEvents = [(VMItemEventDAO *)[VMItemEventDAO sharedInstance] readByItemIdAndLocationId:itemId locationId:locationId];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemEventDAO sharedInstance] close];
    }
    return itemEvents;
}

+ (NSMutableArray *)readItemEventsByItemIdAndLocationName:(NSString *)itemId locationName:(NSString *)locationName {
    NSMutableArray *itemEvents;
    @try {
        if ([[VMItemEventDAO sharedInstance] open])
            itemEvents = [(VMItemEventDAO *)[VMItemEventDAO sharedInstance] readByItemIdAndLocationName:itemId locationName:locationName];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemEventDAO sharedInstance] close];
    }
    return itemEvents;
}

+ (NSMutableArray *)readAllItemEvents {
    NSMutableArray *itemEvents;
    @try {
        if ([[VMItemEventDAO sharedInstance] open])
            itemEvents = [[VMItemEventDAO sharedInstance] readAll];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemEventDAO sharedInstance] close];
    }
    return itemEvents;
}

+ (NSMutableDictionary *)readAllItemEventsAsDictionary {
    NSMutableDictionary *itemEventDictionary;
    @try {
        if ([[VMItemEventDAO sharedInstance] open])
            itemEventDictionary = [[VMItemEventDAO sharedInstance] readAllAsDictionary];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemEventDAO sharedInstance] close];
    }
    return itemEventDictionary;
}

// UPDATE METHODS

+ (void)updateItemEvent:(VMItemEvent *)itemEvent {
    @try {
        if ([[VMItemEventDAO sharedInstance] open])
            [[VMItemEventDAO sharedInstance] update:itemEvent];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemEventDAO sharedInstance] close];
    }
}

+ (void)updateItemEvents:(NSMutableArray *)itemEvents {
    @try {
        if ([[VMItemEventDAO sharedInstance] open])
            [[VMItemEventDAO sharedInstance] updateList:itemEvents];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemEventDAO sharedInstance] close];
    }
}

// DELETE METHODS

+ (void)deleteItemEventById:(NSString *)guid {
    @try {
        if ([[VMItemEventDAO sharedInstance] open])
            [[VMItemEventDAO sharedInstance] deleteById:guid];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
    @finally {
        [[VMItemEventDAO sharedInstance] close];
    }
}

@end
