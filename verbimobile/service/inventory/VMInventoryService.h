//
//  VMInventoryService.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMService.h"
#import "VMItemMasterDAO.h"
#import "VMItemDAO.h"
#import "VMItemEventDAO.h"

@interface VMInventoryService : VMService

#pragma mark -
#pragma mark CRUD Methods - ITEM MASTER
/****************************************************************************
 * CRUD Methods - ITEM MASTER
 ****************************************************************************/

// CREATE METHODS

+ (void)createItemMaster:(VMItemMaster *)itemMaster;
+ (void)createItemMasters:(NSMutableArray *)itemMasters;

// READ METHODS

+ (int)itemMasterCount;
+ (id)readItemMasterById:(NSString *)guid;
+ (id)readItemMasterByName:(NSString *)name;
+ (id)readItemMasterByDescription:(NSString *)description;
+ (id)readItemMasterByUPC:(NSString *)upc;
+ (id)readItemMasterByVendorCode:(NSString *)vendorCode;
+ (NSMutableArray *)readLikeName:(NSString *)name;
+ (NSMutableArray *)readAllItemMasters;
+ (NSMutableDictionary *)readAllItemMastersAsDictionary;

// UPDATE METHODS

+ (void)updateItemMaster:(VMItemMaster *)itemMaster;

// DELETE METHODS

+ (void)deleteItemMasterById:(NSString *)guid;
+ (void)deleteItemMasterByName:(NSString *)name;
+ (void)deleteItemMasterByNames:(NSMutableArray *)names;

#pragma mark -
#pragma mark CRUD Methods - ITEM
/****************************************************************************
 * CRUD Methods - ITEM
 ****************************************************************************/

// CREATE METHODS

+ (void)createItem:(VMItem *)item;
+ (void)createItems:(NSMutableArray *)items;

// READ METHODS

+ (int)itemCount;
+ (int)itemCount:(NSString *)itemMasterId locationId:(NSString *)locationId;
+ (int)itemCount:(NSString *)itemMasterId locationId:(NSString *)locationId status:(NSString *)status;
+ (id)readItemById:(NSString *)guid;
+ (NSMutableArray *)readItems:(NSString *)itemMasterId locationId:(NSString *)locationId;
+ (NSMutableArray *)readItems:(NSString *)itemMasterId locationId:(NSString *)locationId status:(NSString *)status;
+ (NSMutableArray *)readItemsByItemMasterId:(NSString *)itemMasterId;
+ (NSMutableArray *)readItemsByLocationId:(NSString *)locationId;
+ (NSMutableArray *)readItemsByPlaceOfOrigin:(NSString *)placeOfOrigin;
+ (NSMutableArray *)readItemsByLotNumber:(NSString *)lotNumber;
+ (NSMutableArray *)readAllItems;
+ (NSMutableDictionary *)readAllItemsAsDictionary;

// UPDATE METHODS

+ (void)updateItem:(VMItem *)item;
+ (void)updateItems:(NSMutableArray *)items;

// DELETE METHODS

+ (void)deleteItemById:(NSString *)guid;

#pragma mark -
#pragma mark Business Methods - ITEM
/****************************************************************************
 * Business Methods - ITEM
 ****************************************************************************/

+ (void)updateItemWithItem:(VMItem *)source target:(VMItem *)target incrementQty:(BOOL)incrementQty;

#pragma mark -
#pragma mark CRUD Methods - ITEM EVENT
/****************************************************************************
 * CRUD Methods - ITEM EVENT
 ****************************************************************************/

// CREATE METHODS

+ (void)createItemEvent:(VMItemEvent *)itemEvent;
+ (void)createItemEvents:(NSMutableArray *)itemEvents;

// READ METHODS

+ (int)itemEventCount;
+ (id)readItemEventById:(NSString *)guid;
+ (NSMutableArray *)readItemEventsByItemId:(NSString *)itemId;
+ (NSMutableArray *)readItemEventsByLocationId:(NSString *)locationId;
+ (NSMutableArray *)readItemEventsByLocationName:(NSString *)locationName;
+ (NSMutableArray *)readItemEventsByItemIdAndLocationId:(NSString *)itemId locationId:(NSString *)locationId;
+ (NSMutableArray *)readItemEventsByItemIdAndLocationName:(NSString *)itemId locationName:(NSString *)locationName;
+ (NSMutableArray *)readAllItemEvents;
+ (NSMutableDictionary *)readAllItemEventsAsDictionary;

// UPDATE METHODS

+ (void)updateItemEvent:(VMItemEvent *)itemEvent;
+ (void)updateItemEvents:(NSMutableArray *)itemEvents;

// DELETE METHODS

+ (void)deleteItemEventById:(NSString *)guid;

@end
