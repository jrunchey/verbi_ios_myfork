//
//  VMDatabase.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMDAO.h"
#import "sqlite3.h"

@implementation VMDAO

#pragma mark GETTERS/SETTERS

@synthesize initialized = _initialized;
@synthesize dbFilePath = _dbFilePath;
@synthesize dbHandle = _dbHandle;
@synthesize opened = _opened;
@synthesize tableName = _tableName;

#pragma mark INITIALIZATION

- (id)initWithFile:(NSString *)dbFileName {
    self = [super init];
    if (self) {
        [self setDBFileName:dbFileName];
        _initialized = YES;
    }
    return self;
}

-(void)setDBFileName:(NSString *)dbFileName {
    @try {
        // First attempt
        /*
        NSString *paths = [[NSBundle mainBundle] resourcePath];
        if (dbFileName == nil)
            dbFileName = DB_FULL_NAME;
        dbFilePath = [paths stringByAppendingPathComponent:dbFileName];
         */
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        _dbFilePath = [documentsDirectory stringByAppendingPathComponent:DB_FULL_NAME];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        BOOL success = [fileManager fileExistsAtPath:_dbFilePath];
        if (!success) {
            NSLog(@"Cannot locate database file '%@'.", _dbFilePath);
        }
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
    }
}

/*
-(NSString *)pathToDB {
    NSString *dbName = DB_NAME;
    NSString *originalDBPath = [[NSBundle mainBundle] pathForResource:dbName ofType:DB_EXTENSION];
    NSString *path = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *appSupportDir = [paths objectAtIndex:0];
    NSString *dbNameDir = [NSString stringWithFormat:@"%@/%@", appSupportDir, DB_NAME];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDir = NO;
    BOOL dirExists = [fileManager fileExistsAtPath:dbNameDir isDirectory:&isDir];
    NSString *dbPath = [NSString stringWithFormat:@"%@/%@.%@", dbNameDir, dbName, DB_EXTENSION];
    if(dirExists && isDir) {
        BOOL dbExists = [fileManager fileExistsAtPath:dbPath];
        if(!dbExists) {
            NSError *error = nil;
            BOOL success = [fileManager copyItemAtPath:originalDBPath toPath:dbPath error:&error];
            if(!success) {
                NSLog(@"error = %@", error);
            } else {
                path = dbPath;
            }
        } else {
            path = dbPath;
        }
    } else if(!dirExists) {
        NSError *error = nil;
        BOOL success =[fileManager createDirectoryAtPath:dbNameDir attributes:nil];
        if(!success) {
            NSLog(@"failed to create dir");
        }
        success = [fileManager copyItemAtPath:originalDBPath toPath:dbPath error:&error];
        if(!success) {
            NSLog(@"error = %@", error);
        } else {
            path = dbPath;
        }
    }
    return path;
}
 */

#pragma mark DATABASE HANDLE METHODS

- (BOOL)open {
    BOOL success = NO;
    if (!_initialized)
        [self setDBFileName:NULL];
    if (_opened)
        return YES;
    if (_dbFilePath && !_opened) {
        @try {
            int result = sqlite3_open([_dbFilePath UTF8String], &_dbHandle);
            if (result == SQLITE_OK) {
                _opened = YES;
                success = YES;
            }
            else {
                _opened = NO;
                success = NO;
                NSLocalizedStringFromTable(@"Unable to open the sqlite database (%@).",
                     @"Database", @""),
                    [NSString stringWithUTF8String:sqlite3_errmsg(_dbHandle)];
            }
        }
        @catch (NSException *e) {
            success = NO;
            NSLog(@"An exception has occurred: %@", [e reason]);
        }
    }
    return success;
}

- (BOOL)close {
    BOOL success = NO;
    if (!_opened)
        return YES;
    if (_dbHandle && _opened) {
        @try {
            sqlite3_close(_dbHandle);
            _opened = NO;
            success = YES;
        }
        @catch (NSException *e) {
            success = NO;
            NSLog(@"An exception has occurred: %@", [e reason]);
        }
    }
    return success;
}

- (sqlite3_stmt *)prepare:(NSString *)sql {
    @autoreleasepool {
        @try {
            const char *utfsql = [sql UTF8String];
            sqlite3_stmt *statement;
            if (sqlite3_prepare_v2([self dbHandle], utfsql, -1, &statement, NULL) == SQLITE_OK) {
                return statement;
            } else {
                NSLog(@"Problem creating SQLite statement with SQL: %@", sql);
                return 0;
            }
        }
        @catch (NSException *e) {
            NSLog(@"An exception has occurred: %@", [e reason]);
        }
    }
    return 0;
}

- (id) lookupSingularSQL:(NSString *)sql forType:(NSString *)rettype {
    sqlite3_stmt *statement;
    id result;
    @autoreleasepool {
        @try {
            if ((statement = [self prepare:sql])) {
                if (sqlite3_step(statement) == SQLITE_ROW) {
                    if ([rettype compare:@"text"] == NSOrderedSame) {
                        result = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement,0)];
                    } else if ([rettype compare:@"integer"] == NSOrderedSame) {
                        int intResult = sqlite3_column_int(statement,0);
                        result = [NSNumber numberWithInt:intResult];
                    }
                }
            }
            sqlite3_finalize(statement);
        }
        @catch (NSException *e) {
            NSLog(@"An exception has occurred: %@", [e reason]);
        }
    }
    return result;
}

- (BOOL)startTransaction:(sqlite3 *)pDBHandle {
    BOOL success = NO;
    @autoreleasepool {
        @try {
            char *errorMsg;
            sqlite3_exec(pDBHandle, "BEGIN TRANSACTION", NULL, NULL, &errorMsg);
            if (errorMsg) {
                [NSException raise:@"Failed to start transaction" format:@"Failed to start transaction. Reason: %s", errorMsg];
                errorMsg = nil;
            }
            else
                success = YES;
        }
        @catch (NSException *e) {
            [NSException raise:@"Failed to start transaction" format:@"Failed to start transaction. Reason: %@", [e reason]];
        }
    }
    return success;
}

- (BOOL)commitTransaction:(sqlite3 *)pDBHandle {
    BOOL success = NO;
    @autoreleasepool {
        @try {
            char *errorMsg;
            sqlite3_exec(pDBHandle, "COMMIT TRANSACTION", NULL, NULL, &errorMsg);
            if (errorMsg) {
                [NSException raise:@"Failed to commit transaction" format:@"Failed to commit transaction. Reason: %s", errorMsg];
                errorMsg = nil;
            }
            else
                success = YES;
        }
        @catch (NSException *e) {
            [NSException raise:@"Failed to commit transaction" format:@"Failed to commit transaction. Reason: %@", [e reason]];
        }
    }
    return success;
}

- (BOOL)rollbackTransaction:(sqlite3 *)pDBHandle {
    BOOL success = NO;
    @autoreleasepool {
        @try {
            char *errorMsg;
            sqlite3_exec(pDBHandle, "ROLLBACK TRANSACTION", NULL, NULL, &errorMsg);
            if (errorMsg) {
                [NSException raise:@"Failed to rollback transaction" format:@"Failed to rollback transaction. Reason: %s", errorMsg];
                errorMsg = nil;
            }
            else
                success = YES;
        }
        @catch (NSException *e) {
            [NSException raise:@"Failed to start transaction" format:@"Failed to start transaction. Reason: %@", [e reason]];
        }
    }
    return success;
}

#pragma mark DATABASE STATEMENT GET METHODS

- (int)getIntFromColumn:(sqlite3_stmt *)statement column:(int)column {
    return sqlite3_column_int(statement,column);
}

- (BOOL)getBoolFromColumn:(sqlite3_stmt *)statement column:(int)column {
    return sqlite3_column_int(statement,column);
}

- (NSString *)getStringFromColumn:(sqlite3_stmt *)statement column:(int)column {
    NSString *string;
    @autoreleasepool {
        const char *result = (char *)sqlite3_column_text(statement,column);
        if (result)
            string = [NSString stringWithUTF8String:result];
    }
    return string;
}

// Uses default date formatter by passing nil
- (NSDate *)getDateFromColumn:(sqlite3_stmt *)statement column:(int)column {
    return [self getDateFromColumn:statement column:column dateFormatter:nil];
}

- (NSDate *)getDateFromColumn:(sqlite3_stmt *)statement column:(int)column dateFormatter:(NSDateFormatter *)dateFormatter {
    NSDate *date;
    @autoreleasepool {
        if (dateFormatter == nil) {
            dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        }
        NSString *string = [self getStringFromColumn:statement column:column];
        if (string)
            date = [dateFormatter dateFromString:string];
    }
    return date;
}

- (UIImage *)getImageFromColumn:(sqlite3_stmt *)statement column:(int)column {
    UIImage *image;
    @autoreleasepool {
        const char *imageRaw = sqlite3_column_blob(statement,column);
        int imageRawLength = sqlite3_column_bytes(statement,column);
        NSData *imageData;
        if (imageRaw && imageRawLength > 0) {
            imageData = [NSData dataWithBytes:imageRaw length:imageRawLength];
            if (imageData)
                image = [[UIImage alloc] initWithData:imageData];
        }
    }
    return image;
}

#pragma mark DATABASE STATEMENT SET METHODS

- (void)setIntToColumn:(sqlite3_stmt *)statement column:(int)column intValue:(int)intValue {
    sqlite3_bind_int(statement, column, intValue);
}

- (void)setBoolToColumn:(sqlite3_stmt *)statement column:(int)column boolValue:(BOOL)boolValue {
    sqlite3_bind_int(statement, column, boolValue);
}

- (void)setStringToColumn:(sqlite3_stmt *)statement column:(int)column dataTransient:(BOOL)dataTransient string:(NSString *)string {
    if (string) {
        if (!dataTransient) {
            // SQLite assumes that the information is in static, unmanaged space and does not need to be freed
            sqlite3_bind_text(statement, column, [string UTF8String], -1, SQLITE_STATIC);
        }
        else {
            // SQLite makes its own private copy of the data immediately, before the sqlite3_bind_*() routine returns
            sqlite3_bind_text(statement, column, [string UTF8String], -1, SQLITE_TRANSIENT);
        }
    }
}

- (void)setDateToColumn:(sqlite3_stmt *)statement column:(int)column dataTransient:(BOOL)dataTransient date:(NSDate *)date {
    [self setDateToColumn:statement column:column dataTransient:dataTransient date:date dateFormatter:nil];
}

- (void)setDateToColumn:(sqlite3_stmt *)statement column:(int)column dataTransient:(BOOL)dataTransient date:(NSDate *)date dateFormatter:(NSDateFormatter *)dateFormatter {
    if (date) {
        @autoreleasepool {
            if (dateFormatter == nil) {
                dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            }
            if (!dataTransient) {
                // SQLite assumes that the information is in static, unmanaged space and does not need to be freed
                sqlite3_bind_text(statement, column, [[dateFormatter stringFromDate:date] UTF8String] , -1, SQLITE_STATIC);
            }
            else {
                // SQLite makes its own private copy of the data immediately, before the sqlite3_bind_*() routine returns
                sqlite3_bind_text(statement, column, [[dateFormatter stringFromDate:date] UTF8String] , -1, SQLITE_TRANSIENT);
            }
        }
    }
}

- (void)setImageToColumn:(sqlite3_stmt *)statement column:(int)column dataTransient:(BOOL)dataTransient image:(UIImage *)image {
    if (image) {
        @autoreleasepool {
            NSData *imageData = UIImagePNGRepresentation(image);
            if (imageData) {
                if (!dataTransient) {
                    // SQLite assumes that the information is in static, unmanaged space and does not need to be freed
                    sqlite3_bind_blob(statement, column, [imageData bytes], [imageData length], SQLITE_STATIC);
                }
                else {
                    // SQLite makes its own private copy of the data immediately, before the sqlite3_bind_*() routine returns
                    sqlite3_bind_blob(statement, column, [imageData bytes], [imageData length], SQLITE_TRANSIENT);
                }
            }
        }
    }
}

#pragma mark CRUD methods

// CREATE METHODS

- (void)create:(id)object {
}


- (void)createList:(NSMutableArray *)objects {
}

// READ METHODS

- (int)totalNumberOfRecords {
    int count = 0;
    @autoreleasepool {
        if (_opened) {
            @try {
                NSString *sql = [NSString stringWithFormat:@"SELECT COUNT(*) FROM %@", _tableName];
                count = [[self lookupSingularSQL:sql forType:@"integer"] integerValue];
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
            }
        }
    }
    return count;
}

- (int)totalNumberOfRecordsByIds:(NSArray *)guids {
    return 0;
}

- (int)totalNumberOfRecordsByNames:(NSArray *)names {
    return 0;
}

- (id)readById:(NSString *)guid {
    return nil;
}

- (NSMutableArray *)readByIds:(NSArray *)guids {
    return nil;
}

- (NSMutableDictionary *)readByIdsAsDictionary:(NSArray *)guids {
    return nil;
}

- (id)readByName:(NSString *)name {
    return nil;
}

- (NSMutableArray *)readByNames:(NSArray *)names {
    return nil;
}

- (NSMutableDictionary *)readByNamesAsDictionary:(NSArray *)names {
    return nil;
}

- (NSMutableArray *)readAll{
    return nil;
}

- (NSMutableDictionary *)readAllAsDictionary{
    return nil;
}

// UPDATE METHODS

- (void)update:(id)object {
}

- (void)updateList:(NSMutableArray *)objects {
}

// DELETE METHODS

- (void)deleteById:(NSString *)guid {
}

- (void)deleteByIds:(NSMutableArray *)guids {
}

- (void)deleteByName:(NSString *)name {
}

- (void)deleteByNames:(NSMutableArray *)names {
}

- (void)deleteAll {
}

@end
