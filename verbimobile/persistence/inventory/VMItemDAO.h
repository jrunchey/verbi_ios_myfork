//
//  VMItemDAO.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMDAO.h"
#import "VMItem.h"

@interface VMItemDAO : VMDAO

#pragma mark SINGELTON METHODS

+ (VMDAO *)sharedInstance;

#pragma mark SQL DEFINES

#define ITEM_SQL_INSERT_SELECT_PARAMS_STATEMENT   @"\"guid\", \"item_master_id\", \"location_id\", \"origination_date\", \"maturity_date\", \"best_by_date\", \"change_date\", \"retest_date\", \"expiration_date\", \"territory_code\" ,\"place_of_origin\", \"lot_number\", \"date_code\", \"grade\", \"color\", \"status\", \"condition\", \"quantity\", \"value\", \"age_in_days\", \"length\", \"thickness\", \"volume\", \"width\", \"recycled_content\", \"created_date\", \"last_updated_date\", \"last_updated_user_id\", \"enabled\", \"archived\""

#define ITEM_SQL_UPDATE_PARAMS_STATEMENT       @"\"item_master_id\" = ?1, \"location_id\" = ?2, \"origination_date\" = ?3, \"maturity_date\" = ?4, \"best_by_date\" = ?5, \"change_date\" = ?6, \"retest_date\" = ?7, \"expiration_date\" = ?8, \"territory_code\" = ?9, \"place_of_origin\" = ?10, \"lot_number\" = ?11, \"date_code\" = ?12, \"grade\" = ?13, \"color\" = ?14, \"status\" = ?15, \"condition\" = ?16, \"quantity\" = ?17, \"value\" = ?18, \"age_in_days\" = ?19, \"length\" = ?20, \"thickness\" = ?21, \"volume\" = ?22, \"width\" = ?23, \"recycled_content\" = ?24, \"created_date\" = ?25, \"last_updated_date\" = ?26, \"last_updated_user_id\" = ?27, \"enabled\" = ?28, \"archived\" = ?29"

#pragma mark INSTANCE METHODS

- (int)totalNumberOfRecords:(NSString *)itemMasterId locationId:(NSString *)locationId;
- (int)totalNumberOfRecords:(NSString *)itemMasterId locationId:(NSString *)locationId status:(NSString *)status;

- (NSMutableArray *)read:(NSString *)itemMasterId locationId:(NSString *)locationId;
- (NSMutableArray *)read:(NSString *)itemMasterId locationId:(NSString *)locationId status:(NSString *)status;
- (NSMutableArray *)readByItemMasterId:(NSString *)itemMasterId;
- (NSMutableArray *)readByLocationId:(NSString *)locationId;
- (NSMutableArray *)readByPlaceOfOrigin:(NSString *)placeOfOrigin;
- (NSMutableArray *)readByLotNumber:(NSString *)lotNumber;

@end
