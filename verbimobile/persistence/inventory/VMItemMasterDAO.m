//
//  VMItemMasterDAO.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMItemMasterDAO.h"

static VMItemMasterDAO *_sharedInstanceVMItemMasterDAO;

@implementation VMItemMasterDAO

#pragma mark SINGELTON METHODS

+(VMItemMasterDAO *)sharedInstance {
    @synchronized (self) {
        if (_sharedInstanceVMItemMasterDAO == nil){
            _sharedInstanceVMItemMasterDAO = [[self alloc] init];
            [_sharedInstanceVMItemMasterDAO setDBFileName:DB_FULL_NAME];
            _sharedInstanceVMItemMasterDAO.tableName = DB_TABLE_ITEM_MASTER;
            _sharedInstanceVMItemMasterDAO.initialized = YES;
        }
    }
    return _sharedInstanceVMItemMasterDAO;
}

#pragma mark CRUD methods

- (id)readRecordIntoDomainObject:(sqlite3_stmt *)statement {
    VMItemMaster *itemMaster = nil;
    @autoreleasepool {
        @try {
            const char *result;
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            // guid
            NSString *guid;
            result = (char *)sqlite3_column_text(statement,0);
            if (result)
                guid = [NSString stringWithUTF8String:result];
            
            // name
            NSString *name;
            result = (char *)sqlite3_column_text(statement,1);
            if (result)
                name = [NSString stringWithUTF8String:result];
            
            // type
            NSString *type;
            result = (char *)sqlite3_column_text(statement,2);
            if (result)
                type = [NSString stringWithUTF8String:result];
            
            // image
            const char *imageRaw = sqlite3_column_blob(statement,3);
            int imageRawLength = sqlite3_column_bytes(statement,3);
            NSData *imageData;
            UIImage *image;
            if (imageRaw && imageRawLength > 0) {
                imageData = [NSData dataWithBytes:imageRaw length:imageRawLength];
                if (imageData)
                    image = [[UIImage alloc] initWithData:imageData];
            }
            
            // description
            NSString *description;
            result = (char *)sqlite3_column_text(statement,4);
            if (result)
                description = [NSString stringWithUTF8String:result];
            
            // upc
            NSString *upc;
            result = (char *)sqlite3_column_text(statement,5);
            if (result)
                upc = [NSString stringWithUTF8String:result];
            
            // vendor
            NSString *vendor;
            result = (char *)sqlite3_column_text(statement,6);
            if (result)
                vendor = [NSString stringWithUTF8String:result];
            
            // vendor_code
            NSString *vendorCode;
            result = (char *)sqlite3_column_text(statement,7);
            if (result)
                vendorCode = [NSString stringWithUTF8String:result];
            
            // reorder_point_qty
            float reorderPointQuantity = sqlite3_column_double(statement, 8);
            
            // lead_time_days
            float leadTimeDays = sqlite3_column_int(statement, 9);
            
            // safety_stock_qty
            float safetyStockQuantity = sqlite3_column_double(statement, 10);
            
            // optimum_ordering_qty
            float optimumOrderingQuantity = sqlite3_column_double(statement, 11);
            
            // increment_qty
            float incrementQuantity = sqlite3_column_double(statement, 12);
            
            // price
            float price = sqlite3_column_double(statement, 13);
            
            // uom
            NSString *uom;
            result = (char *)sqlite3_column_text(statement,14);
            if (result)
                uom = [NSString stringWithUTF8String:result];
            
            // expiration
            BOOL expiration = sqlite3_column_int(statement,15);
            
            // expiration_time_period
            float expirationTimePeriod = sqlite3_column_int(statement, 16);
            
            // expiration_time_unit
            NSString *expirationTimeUnit;
            result = (char *)sqlite3_column_text(statement,17);
            if (result)
                expirationTimeUnit = [NSString stringWithUTF8String:result];
            
            // created_date
            NSDate *createdDate;
            result = (char *)sqlite3_column_text(statement,18);
            if (result)
                createdDate =[dateFormat dateFromString:[NSString stringWithUTF8String:result]];
            
            // last_updated_date
            NSDate *lastUpdatedDate;
            result = (char *)sqlite3_column_text(statement,19);
            if (result)
                lastUpdatedDate =[dateFormat dateFromString:[NSString stringWithUTF8String:result]];
            
            // last_updated_user_id
            NSString *lastUpdatedUserId;
            result = (char *)sqlite3_column_text(statement,20);
            if (result)
                lastUpdatedUserId = [NSString stringWithUTF8String:result];
            
            // enabled
            BOOL enabled = sqlite3_column_int(statement,21);
            
            // archived
            BOOL archived = sqlite3_column_int(statement,22);
            
            itemMaster = [[VMItemMaster alloc] init:guid name:name type:type description:description upc:upc vendor:vendor vendorCode:vendorCode incrementQuantity:incrementQuantity price:price uom:uom image:image];
            itemMaster.guid = guid;
            itemMaster.reorderPointQuantity = reorderPointQuantity;
            itemMaster.leadTimeDays = leadTimeDays;
            itemMaster.safetyStockQuantity = safetyStockQuantity;
            itemMaster.optimumOrderingQuantity = optimumOrderingQuantity;
            itemMaster.expiration = expiration;
            itemMaster.expirationTimePeriod = expirationTimePeriod;
            itemMaster.expirationTimeUnit = expirationTimeUnit;
            itemMaster.createdDate = createdDate;
            itemMaster.lastUpdatedDate = lastUpdatedDate;
            itemMaster.lastUpdatedUserId = lastUpdatedUserId;
            itemMaster.enabled = enabled;
            itemMaster.archived = archived;
        }
        @catch (NSException *e) {
            NSLog(@"An exception has occurred: %@", [e reason]);
        }
    }
    return itemMaster;
}

// CREATE METHODS

- (void)create:(id)object {
    if (self.opened && [object isKindOfClass:[VMItemMaster class]]) {
        @autoreleasepool {
            @try {
                VMItemMaster *itemMaster = (VMItemMaster *)object;
                
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"INSERT INTO \"%@\" (%@) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15, ?16, ?17, ?18, ?19, ?20, ?21, ?22, ?23)", self.tableName, ITEM_MASTER_SQL_INSERT_SELECT_PARAMS_STATEMENT];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // Create dateFormatter
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                
                // guid
                sqlite3_bind_text(statement, 1, [itemMaster.guid UTF8String], -1, SQLITE_STATIC);
                
                // name
                sqlite3_bind_text(statement, 2, [itemMaster.name UTF8String], -1, SQLITE_STATIC);
                
                // type
                sqlite3_bind_text(statement, 3, [itemMaster.type UTF8String], -1, SQLITE_STATIC);
                
                // image
                NSData *imageData = UIImagePNGRepresentation(itemMaster.image);
                if (imageData)
                    sqlite3_bind_blob(statement, 4, [imageData bytes], [imageData length], SQLITE_STATIC);
                
                // description
                sqlite3_bind_text(statement, 5, [itemMaster.description UTF8String], -1, SQLITE_STATIC);
                
                // upc
                sqlite3_bind_text(statement, 6, [itemMaster.upc UTF8String], -1, SQLITE_STATIC);
                
                // vendor
                sqlite3_bind_text(statement, 7, [itemMaster.vendor UTF8String], -1, SQLITE_STATIC);
                
                // vendor_code
                sqlite3_bind_text(statement, 8, [itemMaster.vendorCode UTF8String], -1, SQLITE_STATIC);
                
                // increment_qty
                sqlite3_bind_double(statement, 9, itemMaster.reorderPointQuantity);
                
                // increment_qty
                sqlite3_bind_int(statement, 10, itemMaster.leadTimeDays);
                
                // increment_qty
                sqlite3_bind_double(statement, 11, itemMaster.safetyStockQuantity);
                
                // increment_qty
                sqlite3_bind_double(statement, 12, itemMaster.optimumOrderingQuantity);
                
                // increment_qty
                sqlite3_bind_double(statement, 13, itemMaster.incrementQuantity);
                
                // price
                sqlite3_bind_double(statement, 14, itemMaster.price);
                
                // uom
                sqlite3_bind_text(statement, 15, [itemMaster.uom UTF8String], -1, SQLITE_STATIC);
             
                // expiration
                sqlite3_bind_int(statement, 16, itemMaster.expiration);
                
                // expiration_time_period
                sqlite3_bind_int(statement, 17, itemMaster.expirationTimePeriod);
                
                // expiration_time_unit
                sqlite3_bind_text(statement, 18, [itemMaster.expirationTimeUnit UTF8String], -1, SQLITE_STATIC);
                
                // created_date
                sqlite3_bind_text(statement, 19, [[dateFormat stringFromDate:itemMaster.createdDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // last_updated_date
                sqlite3_bind_text(statement, 20, [[dateFormat stringFromDate:itemMaster.lastUpdatedDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // last_updated_user_id
                if (itemMaster.lastUpdatedUserId)
                    sqlite3_bind_text(statement, 21, [itemMaster.lastUpdatedUserId UTF8String], -1, SQLITE_STATIC);
                
                // enabled
                sqlite3_bind_int(statement, 22, itemMaster.enabled);
                
                // archived
                sqlite3_bind_int(statement, 23, itemMaster.archived);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while inserting data. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

- (void)createList:(NSMutableArray *)objects {
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"INSERT INTO \"%@\" (%@) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15, ?16, ?17, ?18, ?19, ?20, ?21, ?22, ?23)", self.tableName, ITEM_MASTER_SQL_INSERT_SELECT_PARAMS_STATEMENT];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // Create dateFormatter
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                
                for (NSObject *object in objects) {
                    if ([object isKindOfClass:[VMItemMaster class]]) {
                        VMItemMaster *itemMaster = (VMItemMaster *)object;
                        
                        // guid
                        sqlite3_bind_text(statement, 1, [itemMaster.guid UTF8String], -1, SQLITE_STATIC);
                        
                        // name
                        sqlite3_bind_text(statement, 2, [itemMaster.name UTF8String], -1, SQLITE_STATIC);
                        
                        // type
                        sqlite3_bind_text(statement, 3, [itemMaster.type UTF8String], -1, SQLITE_STATIC);
                        
                        // image
                        NSData *imageData = UIImagePNGRepresentation(itemMaster.image);
                        if (imageData)
                            sqlite3_bind_blob(statement, 4, [imageData bytes], [imageData length], SQLITE_STATIC);
                        
                        // description
                        sqlite3_bind_text(statement, 5, [itemMaster.description UTF8String], -1, SQLITE_STATIC);
                        
                        // upc
                        sqlite3_bind_text(statement, 6, [itemMaster.upc UTF8String], -1, SQLITE_STATIC);
                        
                        // vendor
                        sqlite3_bind_text(statement, 7, [itemMaster.vendor UTF8String], -1, SQLITE_STATIC);
                        
                        // vendor_code
                        sqlite3_bind_text(statement, 8, [itemMaster.vendorCode UTF8String], -1, SQLITE_STATIC);
                        
                        // increment_qty
                        sqlite3_bind_double(statement, 9, itemMaster.reorderPointQuantity);
                        
                        // increment_qty
                        sqlite3_bind_int(statement, 10, itemMaster.leadTimeDays);
                        
                        // increment_qty
                        sqlite3_bind_double(statement, 11, itemMaster.safetyStockQuantity);
                        
                        // increment_qty
                        sqlite3_bind_double(statement, 12, itemMaster.optimumOrderingQuantity);
                        
                        // increment_qty
                        sqlite3_bind_double(statement, 13, itemMaster.incrementQuantity);
                        
                        // price
                        sqlite3_bind_double(statement, 14, itemMaster.price);
                        
                        // uom
                        sqlite3_bind_text(statement, 15, [itemMaster.uom UTF8String], -1, SQLITE_STATIC);
                        
                        // expiration
                        sqlite3_bind_int(statement, 16, itemMaster.expiration);
                        
                        // expiration_time_period
                        sqlite3_bind_int(statement, 17, itemMaster.expirationTimePeriod);
                        
                        // expiration_time_unit
                        sqlite3_bind_text(statement, 18, [itemMaster.expirationTimeUnit UTF8String], -1, SQLITE_STATIC);
                        
                        // created_date
                        sqlite3_bind_text(statement, 19, [[dateFormat stringFromDate:itemMaster.createdDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // last_updated_date
                        sqlite3_bind_text(statement, 20, [[dateFormat stringFromDate:itemMaster.lastUpdatedDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // last_updated_user_id
                        if (itemMaster.lastUpdatedUserId)
                            sqlite3_bind_text(statement, 21, [itemMaster.lastUpdatedUserId UTF8String], -1, SQLITE_STATIC);
                        
                        // enabled
                        sqlite3_bind_int(statement, 22, itemMaster.enabled);
                        
                        // archived
                        sqlite3_bind_int(statement, 23, itemMaster.archived);
                        
                        while(YES){
                            NSInteger result = sqlite3_step(statement);
                            if(result == SQLITE_DONE){
                                break;
                            }
                            else if(result != SQLITE_BUSY){
                                printf("db error: %s\n", sqlite3_errmsg(self.dbHandle));
                                break;
                            }
                        }
                        sqlite3_reset(statement);
                    }
                }
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

// READ METHODS

- (id)readById:(NSString *)guid {
    VMItemMaster *itemMaster = nil;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"guid\" = ?1", ITEM_MASTER_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [guid UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    itemMaster = [self readRecordIntoDomainObject:statement];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return itemMaster;
}

- (NSMutableArray *)readByIds:(NSArray *)guids {
    return nil;
}

- (id)readByName:(NSString *)name {
    VMItemMaster *itemMaster = nil;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"name\" = ?1", ITEM_MASTER_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [name UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    itemMaster = [self readRecordIntoDomainObject:statement];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return itemMaster;
}

- (NSMutableArray *)readByNames:(NSArray *)names {
    return nil;
}

- (NSMutableArray *)readLikeName:(NSString *)name {
    NSMutableArray *itemMasters;
    if (self.opened && name && [name length] > 0) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"name\" LIKE ?1", ITEM_MASTER_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                
                NSMutableString *param = nil;
                if ([name length] < 3)
                    param = [NSMutableString stringWithFormat:@"%@%%", name];
                else
                    param = [NSMutableString stringWithFormat:@"%%%@%%", name];
                
                sqlite3_bind_text(statement, 1, [param UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (itemMasters == nil)
                        itemMasters = [[NSMutableArray alloc] init];
                    
                    VMItemMaster *itemMaster = [self readRecordIntoDomainObject:statement];
                    if (itemMaster) {
                        [itemMasters addObject:itemMaster];
                    }
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return itemMasters;
}

- (id)readByDescription:(NSString *)description {
    VMItemMaster *itemMaster = nil;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"description\" = ?1", ITEM_MASTER_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [description UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    itemMaster = [self readRecordIntoDomainObject:statement];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return itemMaster;
}

- (NSMutableArray *)readLikeDescription:(NSString *)description {
    NSMutableArray *itemMasters;
    if (self.opened && description && [description length] > 0) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"name\" LIKE ?1", ITEM_MASTER_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                
                NSMutableString *param = nil;
                if ([description length] < 3)
                    param = [NSMutableString stringWithFormat:@"%@%%", description];
                else
                    param = [NSMutableString stringWithFormat:@"%%%@%%", description];
                
                sqlite3_bind_text(statement, 1, [param UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (itemMasters == nil)
                        itemMasters = [[NSMutableArray alloc] init];
                    
                    VMItemMaster *itemMaster = [self readRecordIntoDomainObject:statement];
                    if (itemMaster) {
                        [itemMasters addObject:itemMaster];
                    }
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return itemMasters;
}

- (id)readByUPC:(NSString *)upc {
    VMItemMaster *itemMaster = nil;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"upc\" = ?1", ITEM_MASTER_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [upc UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    itemMaster = [self readRecordIntoDomainObject:statement];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return itemMaster;
}

- (id)readByVendorCode:(NSString *)vendorCode {
    VMItemMaster *itemMaster = nil;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"vendor_code\" = ?1", ITEM_MASTER_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                
                // name
                sqlite3_bind_text(statement, 1, [vendorCode UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    itemMaster = [self readRecordIntoDomainObject:statement];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return itemMaster;
}

- (NSMutableArray *)readAll{
    NSMutableArray *itemMasters;
    @try {
        @autoreleasepool {
            // Get dictionary and sort in alphabetical order
            NSMutableDictionary *dictionary = [self readAllAsDictionary];
            NSArray *keys = [dictionary allKeys];
            NSArray *sortedKeys = [keys sortedArrayUsingSelector:@selector(compare:)];
            itemMasters = [[NSMutableArray alloc] init];
            for(NSString *key in sortedKeys) {
                VMItemMaster *itemMaster = [dictionary objectForKey:key];
                [itemMasters addObject:itemMaster];
            }
        }
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
        [self rollbackTransaction:self.dbHandle];
    }
    return itemMasters;
}

- (NSMutableDictionary *)readAllAsDictionary {
    NSMutableDictionary *itemMasterDictionary;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\"", ITEM_MASTER_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (itemMasterDictionary == nil)
                        itemMasterDictionary = [[NSMutableDictionary alloc] init];
                    VMItemMaster *itemMaster = [self readRecordIntoDomainObject:statement];
                    if (itemMaster)
                        [itemMasterDictionary setObject:itemMaster forKey:[itemMaster.name copy]];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return itemMasterDictionary;
}

// UPDATE METHODS

- (void)update:(id)object {
    if (self.opened && [object isKindOfClass:[VMItemMaster class]]) {
        @autoreleasepool {
            @try {
                VMItemMaster *itemMaster = (VMItemMaster *)object;
                
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"UPDATE %@ SET %@ WHERE \"guid\" = ?23", self.tableName, ITEM_MASTER_SQL_UPDATE_PARAMS_STATEMENT];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // Create dateFormatter
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                
                // name
                sqlite3_bind_text(statement, 1, [itemMaster.name UTF8String], -1, SQLITE_STATIC);
                
                // type
                sqlite3_bind_text(statement, 2, [itemMaster.type UTF8String], -1, SQLITE_STATIC);
                
                // image
                NSData *imageData = UIImagePNGRepresentation(itemMaster.image);
                sqlite3_bind_blob(statement, 3, [imageData bytes], [imageData length], SQLITE_STATIC);
                
                // description
                sqlite3_bind_text(statement, 4, [itemMaster.description UTF8String], -1, SQLITE_STATIC);
                
                // upc
                sqlite3_bind_text(statement, 5, [itemMaster.upc UTF8String], -1, SQLITE_STATIC);
                
                // vendor
                sqlite3_bind_text(statement, 6, [itemMaster.vendor UTF8String], -1, SQLITE_STATIC);
                
                // vendor_code
                sqlite3_bind_text(statement, 7, [itemMaster.vendorCode UTF8String], -1, SQLITE_STATIC);
                
                // reorder_point_qty
                sqlite3_bind_double(statement, 8, itemMaster.reorderPointQuantity);
                
                // lead_time_days
                sqlite3_bind_int(statement, 9, itemMaster.leadTimeDays);
                
                // safety_stock_qty
                sqlite3_bind_double(statement, 10, itemMaster.safetyStockQuantity);
                
                // optimum_ordering_qty
                sqlite3_bind_double(statement, 11, itemMaster.optimumOrderingQuantity);
                
                // increment_qty
                sqlite3_bind_double(statement, 12, itemMaster.incrementQuantity);
                
                // price
                sqlite3_bind_double(statement, 13, itemMaster.price);
                
                // uom
                sqlite3_bind_text(statement, 14, [itemMaster.uom UTF8String], -1, SQLITE_STATIC);
                
                // expiration
                sqlite3_bind_int(statement, 15, itemMaster.expiration);
                
                // expiration_time_period
                sqlite3_bind_int(statement, 16, itemMaster.expirationTimePeriod);
                
                // expiration_time_unit
                sqlite3_bind_text(statement, 17, [itemMaster.expirationTimeUnit UTF8String], -1, SQLITE_STATIC);
                
                // created_date
                sqlite3_bind_text(statement, 18, [[dateFormat stringFromDate:itemMaster.createdDate] UTF8String], -1, SQLITE_STATIC);
                
                // last_updated_date
                sqlite3_bind_text(statement, 19, [[dateFormat stringFromDate:itemMaster.lastUpdatedDate] UTF8String], -1, SQLITE_STATIC);
                
                // last_updated_user_id
                if (itemMaster.lastUpdatedUserId)
                    sqlite3_bind_text(statement, 20, [itemMaster.lastUpdatedUserId UTF8String], -1, SQLITE_STATIC);
                
                // enabled
                sqlite3_bind_int(statement, 21, itemMaster.enabled);
                
                // archived
                sqlite3_bind_int(statement, 22, itemMaster.archived);
                
                // guid
                sqlite3_bind_text(statement, 23, [itemMaster.guid UTF8String], -1, SQLITE_STATIC);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while updating. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

// DELETE METHODS

- (void)deleteById:(NSString *)guid {
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"DELETE FROM \"%@\" WHERE \"guid\" = ?1", self.tableName];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // name
                sqlite3_bind_text(statement, 1, [guid UTF8String], -1, SQLITE_STATIC);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while deleting. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

- (void)deleteByIds:(NSMutableArray *)guids {
}

- (void)deleteByName:(NSString *)name {
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"DELETE FROM \"%@\" WHERE \"name\" = ?1", self.tableName];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // name
                sqlite3_bind_text(statement, 1, [name UTF8String], -1, SQLITE_STATIC);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while deleting. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

- (void)deleteByNames:(NSMutableArray *)names {
    if (self.opened) {
        @autoreleasepool {
            @try {
                // Combine array elements to become a string of comma separated elements.
                NSMutableArray *quotedNames = [[NSMutableArray alloc] init];
                for (NSString *name in names) {
                    NSString *quotedName = [[NSString alloc] initWithFormat:@"'%@'", name];
                    [quotedNames addObject:quotedName];
                }
                NSString *namesToDeleteCommaDelimited = [quotedNames componentsJoinedByString:@","];
                NSString *namesToDeleteEnclosed = [NSString stringWithFormat:@"(%@)", namesToDeleteCommaDelimited];
                
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"DELETE FROM \"%@\" WHERE \"name\" IN %@", self.tableName, namesToDeleteEnclosed];
                
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while deleting. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

- (void)deleteAll {
}

@end
