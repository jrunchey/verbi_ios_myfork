//
//  VMItemMasterDAO.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMDAO.h"
#import "VMItemMaster.h"

@interface VMItemMasterDAO : VMDAO

#pragma mark SINGELTON METHODS

+ (VMDAO *)sharedInstance;

#pragma mark SQL DEFINES

#define ITEM_MASTER_SQL_INSERT_SELECT_PARAMS_STATEMENT   @"\"guid\", \"name\", \"type\", \"image\", \"description\", \"upc\", \"vendor\", \"vendor_code\", \"reorder_point_qty\", \"lead_time_days\", \"safety_stock_qty\", \"optimum_ordering_qty\", \"increment_qty\", \"price\", \"uom\", \"expiration\", \"expiration_time_period\", \"expiration_time_unit\", \"created_date\", \"last_updated_date\", \"last_updated_user_id\", \"enabled\", \"archived\""

#define ITEM_MASTER_SQL_UPDATE_PARAMS_STATEMENT       @"\"name\" = ?1, \"type\" = ?2, \"image\" = ?3, \"description\" = ?4, \"upc\" = ?5, \"vendor\" = ?6, \"vendor_code\" = ?7, \"reorder_point_qty\" = ?8, \"lead_time_days\" = ?9, \"safety_stock_qty\" = ?10, \"optimum_ordering_qty\" = ?11, \"increment_qty\" = ?12, \"price\" = ?13, \"uom\" = ?14, \"expiration\" = ?15, \"expiration_time_period\" = ?16, \"expiration_time_unit\" = ?17, \"created_date\" = ?18, \"last_updated_date\" = ?19, \"last_updated_user_id\" = ?20, \"enabled\" = ?21, \"archived\" = ?22"

#pragma mark INSTANCE METHODS

- (NSMutableArray *)readLikeName:(NSString *)name;
- (id)readByDescription:(NSString *)description;
- (NSMutableArray *)readLikeDescription:(NSString *)description;
- (id)readByUPC:(NSString *)upc;
- (id)readByVendorCode:(NSString *)vendorCode;

@end
