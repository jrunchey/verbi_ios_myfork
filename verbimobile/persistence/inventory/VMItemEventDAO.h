//
//  VMItemEventDAO.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMDAO.h"
#import "VMItemEvent.h"

@interface VMItemEventDAO : VMDAO

#pragma mark SINGELTON METHODS

+ (VMDAO *)sharedInstance;

#pragma mark SQL DEFINES

#define ITEM_EVENT_SQL_INSERT_SELECT_PARAMS_STATEMENT   @"\"guid\", \"item_id\", \"type\", \"location_id\", \"location_name\", \"origination_date\", \"maturity_date\", \"best_by_date\", \"change_date\", \"retest_date\", \"expiration_date\", \"territory_code\" ,\"place_of_origin\", \"lot_number\", \"date_code\", \"grade\", \"color\", \"status\", \"condition\", \"quantity\", \"value\", \"age_in_days\", \"length\", \"thickness\", \"volume\", \"width\", \"recycled_content\", \"created_date\", \"last_updated_date\", \"last_updated_user_id\""

#define ITEM_EVENT_SQL_UPDATE_PARAMS_STATEMENT       @"\"item_id\" = ?1, \"type\" = ?2, \"location_id\" = ?3, \"location_name\" = ?4, \"origination_date\" = ?5, \"maturity_date\" = ?6, \"best_by_date\" = ?7, \"change_date\" = ?8, \"retest_date\" = ?9, \"expiration_date\" = ?10, \"territory_code\" = ?11, \"place_of_origin\" = ?12, \"lot_number\" = ?13, \"date_code\" = ?14, \"grade\" = ?15, \"color\" = ?16, \"status\" = ?17, \"condition\" = ?18, \"quantity\" = ?19, \"value\" = ?20, \"age_in_days\" = ?21, \"length\" = ?22, \"thickness\" = ?23, \"volume\" = ?24, \"width\" = ?25, \"recycled_content\" = ?26, \"created_date\" = ?27, \"last_updated_date\" = ?28, \"last_updated_user_id\" = ?29"

#pragma mark INSTANCE METHODS

- (NSMutableArray *)readByItemId:(NSString *)itemId;
- (NSMutableArray *)readByLocationId:(NSString *)locationId;
- (NSMutableArray *)readByLocationName:(NSString *)locationName;
- (NSMutableArray *)readByItemIdAndLocationId:(NSString *)itemId locationId:(NSString *)locationId;
- (NSMutableArray *)readByItemIdAndLocationName:(NSString *)itemId locationName:(NSString *)locationName;

@end
