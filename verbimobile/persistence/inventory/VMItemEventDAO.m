//
//  VMItemEventDAO.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMItemEventDAO.h"

static VMItemEventDAO *_sharedInstanceVMItemEventDAO;

@implementation VMItemEventDAO

#pragma mark SINGELTON METHODS

+(VMItemEventDAO *)sharedInstance {
    @synchronized (self) {
        if (_sharedInstanceVMItemEventDAO == nil){
            _sharedInstanceVMItemEventDAO = [[self alloc] init];
            [_sharedInstanceVMItemEventDAO setDBFileName:DB_FULL_NAME];
            _sharedInstanceVMItemEventDAO.tableName = DB_TABLE_ITEM_EVENT;
            _sharedInstanceVMItemEventDAO.initialized = YES;
        }
    }
    return _sharedInstanceVMItemEventDAO;
}

#pragma mark CRUD methods

- (id)readRecordIntoDomainObject:(sqlite3_stmt *)statement {
    VMItemEvent *itemEvent = nil;
    @autoreleasepool {
        @try {
            const char *result;
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            // guid
            NSString *guid;
            result = (char *)sqlite3_column_text(statement,0);
            if (result)
                guid = [NSString stringWithUTF8String:result];
            
            // item_id
            NSString *itemGUID;
            result = (char *)sqlite3_column_text(statement,1);
            if (result)
                itemGUID = [NSString stringWithUTF8String:result];
            
            // type
            NSString *type;
            result = (char *)sqlite3_column_text(statement,2);
            if (result)
                type = [NSString stringWithUTF8String:result];
            
            // location_id
            NSString *locationGUID;
            result = (char *)sqlite3_column_text(statement,3);
            if (result)
                locationGUID = [NSString stringWithUTF8String:result];
            
            // location_name
            NSString *locationName;
            result = (char *)sqlite3_column_text(statement,4);
            if (result)
                locationName = [NSString stringWithUTF8String:result];
            
            // origination_date
            NSDate *originationDate;
            result = (char *)sqlite3_column_text(statement,5);
            if (result)
                originationDate =[dateFormat dateFromString:[NSString stringWithUTF8String:result]];
            
            // maturity_date
            NSDate *maturityDate;
            result = (char *)sqlite3_column_text(statement,6);
            if (result)
                maturityDate =[dateFormat dateFromString:[NSString stringWithUTF8String:result]];
            
            // best_by_date
            NSDate *bestByDate;
            result = (char *)sqlite3_column_text(statement,7);
            if (result)
                bestByDate =[dateFormat dateFromString:[NSString stringWithUTF8String:result]];
            
            // change_date
            NSDate *changeDate;
            result = (char *)sqlite3_column_text(statement,8);
            if (result)
                changeDate =[dateFormat dateFromString:[NSString stringWithUTF8String:result]];
            
            // retest_date
            NSDate *retestDate;
            result = (char *)sqlite3_column_text(statement,9);
            if (result)
                retestDate =[dateFormat dateFromString:[NSString stringWithUTF8String:result]];
            
            // expiration_date
            NSDate *expirationDate;
            result = (char *)sqlite3_column_text(statement,10);
            if (result)
                expirationDate =[dateFormat dateFromString:[NSString stringWithUTF8String:result]];
            
            // territory_code
            NSString *territoryCode;
            result = (char *)sqlite3_column_text(statement,11);
            if (result)
                territoryCode = [NSString stringWithUTF8String:result];
            
            // place_of_origin
            NSString *placeOfOrigin;
            result = (char *)sqlite3_column_text(statement,12);
            if (result)
                placeOfOrigin = [NSString stringWithUTF8String:result];
            
            // lot_number
            NSString *lotNumber;
            result = (char *)sqlite3_column_text(statement,13);
            if (result)
                lotNumber = [NSString stringWithUTF8String:result];
            
            // date_code
            NSString *dateCode;
            result = (char *)sqlite3_column_text(statement,14);
            if (result)
                dateCode = [NSString stringWithUTF8String:result];
            
            // grade
            NSString *grade;
            result = (char *)sqlite3_column_text(statement,15);
            if (result)
                grade = [NSString stringWithUTF8String:result];
            
            // color
            NSString *color;
            result = (char *)sqlite3_column_text(statement,16);
            if (result)
                color = [NSString stringWithUTF8String:result];
            
            // status
            NSString *status;
            result = (char *)sqlite3_column_text(statement,17);
            if (result)
                status = [NSString stringWithUTF8String:result];
            
            // condition
            NSString *condition;
            result = (char *)sqlite3_column_text(statement,18);
            if (result)
                condition = [NSString stringWithUTF8String:result];
            
            // quantity
            float quantity = sqlite3_column_double(statement, 19);
            
            // value
            float value = sqlite3_column_double(statement, 20);
            
            // age_in_days
            int ageInDays = sqlite3_column_int(statement,21);
            
            // length
            int length = sqlite3_column_int(statement,22);
            
            // thickness
            int thickness = sqlite3_column_int(statement,23);
            
            // volume
            int volume = sqlite3_column_int(statement,24);
            
            // width
            int width = sqlite3_column_int(statement,25);
            
            // recycled_content
            int recycledContent = sqlite3_column_int(statement,26);
            
            // created_date
            NSDate *createdDate;
            result = (char *)sqlite3_column_text(statement,27);
            if (result)
                createdDate =[dateFormat dateFromString:[NSString stringWithUTF8String:result]];
            
            // last_updated_date
            NSDate *lastUpdatedDate;
            result = (char *)sqlite3_column_text(statement,28);
            if (result)
                lastUpdatedDate =[dateFormat dateFromString:[NSString stringWithUTF8String:result]];
            
            // last_updated_user_id
            NSString *lastUpdatedUserId;
            result = (char *)sqlite3_column_text(statement,29);
            if (result)
                lastUpdatedUserId = [NSString stringWithUTF8String:result];
            
            itemEvent = [[VMItemEvent alloc] init:guid itemGUID:itemGUID locationGUID:locationGUID locationName:locationName quantity:quantity];
            
            itemEvent.type = type;
            itemEvent.locationName = locationName;
            itemEvent.originationDate = originationDate;
            itemEvent.maturityDate = maturityDate;
            itemEvent.bestByDate = bestByDate;
            itemEvent.changeDate = changeDate;
            itemEvent.retestDate = retestDate;
            itemEvent.expirationDate = expirationDate;
            itemEvent.territoryCode = territoryCode;
            itemEvent.placeOfOrigin = placeOfOrigin;
            itemEvent.lotNumber = lotNumber;
            itemEvent.dateCode = dateCode;
            itemEvent.grade = grade;
            itemEvent.color = color;
            itemEvent.status = status;
            itemEvent.condition = condition;
            itemEvent.value = value;
            itemEvent.ageInDays = ageInDays;
            itemEvent.length = length;
            itemEvent.thickness = thickness;
            itemEvent.volume = volume;
            itemEvent.width = width;
            itemEvent.recycledContent = recycledContent;
            itemEvent.createdDate = createdDate;
            itemEvent.lastUpdatedDate = lastUpdatedDate;
            itemEvent.lastUpdatedUserId = lastUpdatedUserId;
        }
        @catch (NSException *e) {
            NSLog(@"An exception has occurred: %@", [e reason]);
        }
    }
    return itemEvent;
}

// CREATE METHODS

- (void)create:(id)object {
    if (self.opened && [object isKindOfClass:[VMItemEvent class]]) {
        @autoreleasepool {
            @try {
                VMItemEvent *itemEvent = (VMItemEvent *)object;
                
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"INSERT INTO \"%@\" (%@) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15, ?16, ?17, ?18, ?19, ?20, ?21, ?22, ?23, ?24, ?25, ?26, ?27, ?28, ?29, ?30)", self.tableName, ITEM_EVENT_SQL_INSERT_SELECT_PARAMS_STATEMENT];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // Create dateFormatter
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                
                // guid
                sqlite3_bind_text(statement, 1, [itemEvent.guid UTF8String], -1, SQLITE_STATIC);
                
                // item_id
                sqlite3_bind_text(statement, 2, [itemEvent.itemGUID UTF8String], -1, SQLITE_STATIC);
                
                // type
                sqlite3_bind_text(statement, 3, [itemEvent.type UTF8String], -1, SQLITE_STATIC);
                
                // location_id
                sqlite3_bind_text(statement, 4, [itemEvent.locationGUID UTF8String], -1, SQLITE_STATIC);
                
                // location_name
                sqlite3_bind_text(statement, 5, [itemEvent.locationName UTF8String], -1, SQLITE_STATIC);
                
                // origination_date
                sqlite3_bind_text(statement, 6, [[dateFormat stringFromDate:itemEvent.originationDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // maturity_date
                sqlite3_bind_text(statement, 7, [[dateFormat stringFromDate:itemEvent.maturityDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // best_by_date
                sqlite3_bind_text(statement, 8, [[dateFormat stringFromDate:itemEvent.bestByDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // change_date
                sqlite3_bind_text(statement, 9, [[dateFormat stringFromDate:itemEvent.changeDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // retest_date
                sqlite3_bind_text(statement, 10, [[dateFormat stringFromDate:itemEvent.retestDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // expiration_date
                sqlite3_bind_text(statement, 11, [[dateFormat stringFromDate:itemEvent.expirationDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // territory_code
                sqlite3_bind_text(statement, 12, [itemEvent.territoryCode UTF8String], -1, SQLITE_STATIC);
                
                // place_of_origin
                sqlite3_bind_text(statement, 13, [itemEvent.placeOfOrigin UTF8String], -1, SQLITE_STATIC);
                
                // lot_number
                sqlite3_bind_text(statement, 14, [itemEvent.lotNumber UTF8String], -1, SQLITE_STATIC);
                
                // date_code
                sqlite3_bind_text(statement, 15, [itemEvent.dateCode UTF8String], -1, SQLITE_STATIC);
                
                // grade
                sqlite3_bind_text(statement, 16, [itemEvent.grade UTF8String], -1, SQLITE_STATIC);
                
                // color
                sqlite3_bind_text(statement, 17, [itemEvent.color UTF8String], -1, SQLITE_STATIC);
                
                // status
                sqlite3_bind_text(statement, 18, [itemEvent.status UTF8String], -1, SQLITE_STATIC);
                
                // condition
                sqlite3_bind_text(statement, 19, [itemEvent.condition UTF8String], -1, SQLITE_STATIC);
                
                // quantity
                sqlite3_bind_double(statement, 20, itemEvent.quantity);
                
                // value
                sqlite3_bind_double(statement, 21, itemEvent.value);
                
                // age_in_days
                sqlite3_bind_int(statement, 22, itemEvent.ageInDays);
                
                // length
                sqlite3_bind_int(statement, 23, itemEvent.length);
                
                // thickness
                sqlite3_bind_int(statement, 24, itemEvent.thickness);
                
                // volume
                sqlite3_bind_int(statement, 25, itemEvent.volume);
                
                // width
                sqlite3_bind_int(statement, 26, itemEvent.width);
                
                // recycled_content
                sqlite3_bind_int(statement, 27, itemEvent.recycledContent);
                
                // created_date
                sqlite3_bind_text(statement, 28, [[dateFormat stringFromDate:itemEvent.createdDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // last_updated_date
                sqlite3_bind_text(statement, 29, [[dateFormat stringFromDate:itemEvent.lastUpdatedDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // last_updated_user_id
                if (itemEvent.lastUpdatedUserId)
                    sqlite3_bind_text(statement, 30, [itemEvent.lastUpdatedUserId UTF8String], -1, SQLITE_STATIC);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while inserting data. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

- (void)createList:(NSMutableArray *)objects {
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"INSERT INTO \"%@\" (%@) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15, ?16, ?17, ?18, ?19, ?20, ?21, ?22, ?23, ?24, ?25, ?26, ?27, ?28, ?29, ?30)", self.tableName, ITEM_EVENT_SQL_INSERT_SELECT_PARAMS_STATEMENT];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // Create dateFormatter
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                
                for (NSObject *object in objects) {
                    if ([object isKindOfClass:[VMItemEvent class]]) {
                        VMItemEvent *itemEvent = (VMItemEvent *)object;
                        
                        // guid
                        sqlite3_bind_text(statement, 1, [itemEvent.guid UTF8String], -1, SQLITE_STATIC);
                        
                        // item_id
                        sqlite3_bind_text(statement, 2, [itemEvent.itemGUID UTF8String], -1, SQLITE_STATIC);
                        
                        // type
                        sqlite3_bind_text(statement, 3, [itemEvent.type UTF8String], -1, SQLITE_STATIC);
                        
                        // location_id
                        sqlite3_bind_text(statement, 4, [itemEvent.locationGUID UTF8String], -1, SQLITE_STATIC);
                        
                        // location_name
                        sqlite3_bind_text(statement, 5, [itemEvent.locationName UTF8String], -1, SQLITE_STATIC);
                        
                        // origination_date
                        sqlite3_bind_text(statement, 6, [[dateFormat stringFromDate:itemEvent.originationDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // maturity_date
                        sqlite3_bind_text(statement, 7, [[dateFormat stringFromDate:itemEvent.maturityDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // best_by_date
                        sqlite3_bind_text(statement, 8, [[dateFormat stringFromDate:itemEvent.bestByDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // change_date
                        sqlite3_bind_text(statement, 9, [[dateFormat stringFromDate:itemEvent.changeDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // retest_date
                        sqlite3_bind_text(statement, 10, [[dateFormat stringFromDate:itemEvent.retestDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // expiration_date
                        sqlite3_bind_text(statement, 11, [[dateFormat stringFromDate:itemEvent.expirationDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // territory_code
                        sqlite3_bind_text(statement, 12, [itemEvent.territoryCode UTF8String], -1, SQLITE_STATIC);
                        
                        // place_of_origin
                        sqlite3_bind_text(statement, 13, [itemEvent.placeOfOrigin UTF8String], -1, SQLITE_STATIC);
                        
                        // lot_number
                        sqlite3_bind_text(statement, 14, [itemEvent.lotNumber UTF8String], -1, SQLITE_STATIC);
                        
                        // date_code
                        sqlite3_bind_text(statement, 15, [itemEvent.dateCode UTF8String], -1, SQLITE_STATIC);
                        
                        // grade
                        sqlite3_bind_text(statement, 16, [itemEvent.grade UTF8String], -1, SQLITE_STATIC);
                        
                        // color
                        sqlite3_bind_text(statement, 17, [itemEvent.color UTF8String], -1, SQLITE_STATIC);
                        
                        // status
                        sqlite3_bind_text(statement, 18, [itemEvent.status UTF8String], -1, SQLITE_STATIC);
                        
                        // condition
                        sqlite3_bind_text(statement, 19, [itemEvent.condition UTF8String], -1, SQLITE_STATIC);
                        
                        // quantity
                        sqlite3_bind_double(statement, 20, itemEvent.quantity);
                        
                        // value
                        sqlite3_bind_double(statement, 21, itemEvent.value);
                        
                        // age_in_days
                        sqlite3_bind_int(statement, 22, itemEvent.ageInDays);
                        
                        // length
                        sqlite3_bind_int(statement, 23, itemEvent.length);
                        
                        // thickness
                        sqlite3_bind_int(statement, 24, itemEvent.thickness);
                        
                        // volume
                        sqlite3_bind_int(statement, 25, itemEvent.volume);
                        
                        // width
                        sqlite3_bind_int(statement, 26, itemEvent.width);
                        
                        // recycled_content
                        sqlite3_bind_int(statement, 27, itemEvent.recycledContent);
                        
                        // created_date
                        sqlite3_bind_text(statement, 28, [[dateFormat stringFromDate:itemEvent.createdDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // last_updated_date
                        sqlite3_bind_text(statement, 29, [[dateFormat stringFromDate:itemEvent.lastUpdatedDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // last_updated_user_id
                        if (itemEvent.lastUpdatedUserId)
                            sqlite3_bind_text(statement, 30, [itemEvent.lastUpdatedUserId UTF8String], -1, SQLITE_STATIC);
                        
                        while(YES){
                            NSInteger result = sqlite3_step(statement);
                            if(result == SQLITE_DONE){
                                break;
                            }
                            else if(result != SQLITE_BUSY){
                                printf("db error: %s\n", sqlite3_errmsg(self.dbHandle));
                                break;
                            }
                        }
                        sqlite3_reset(statement);
                    }
                }
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

// READ METHODS

- (id)readById:(NSString *)guid {
    VMItemEvent *item = nil;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"guid\" = ?1", ITEM_EVENT_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [guid UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    item = [self readRecordIntoDomainObject:statement];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return item;
    
}

- (NSMutableArray *)readByIds:(NSArray *)guids {
    return nil;
}

- (id)readByName:(NSString *)name {
    return nil;
}

- (NSMutableArray *)readByItemId:(NSString *)itemId {
    NSMutableArray *items;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"item_id\" = ?1", ITEM_EVENT_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [itemId UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (items == nil)
                        items = [[NSMutableArray alloc] init];
                    VMItemEvent *item = [self readRecordIntoDomainObject:statement];
                    [items addObject:item];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return items;
}

- (NSMutableArray *)readByLocationId:(NSString *)locationId {
    NSMutableArray *items;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"location_id\" = ?1", ITEM_EVENT_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [locationId UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (items == nil)
                        items = [[NSMutableArray alloc] init];
                    VMItemEvent *item = [self readRecordIntoDomainObject:statement];
                    [items addObject:item];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return items;
}

- (NSMutableArray *)readByLocationName:(NSString *)locationName {
    NSMutableArray *items;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"location_name\" = ?1", ITEM_EVENT_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [locationName UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (items == nil)
                        items = [[NSMutableArray alloc] init];
                    VMItemEvent *item = [self readRecordIntoDomainObject:statement];
                    [items addObject:item];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return items;
}

- (NSMutableArray *)readByItemIdAndLocationId:(NSString *)itemId locationId:(NSString *)locationId {
    NSMutableArray *items;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"item_id\" = ?1 AND \"location_id\" = ?2", ITEM_EVENT_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [itemId UTF8String], -1, SQLITE_STATIC);
                sqlite3_bind_text(statement, 2, [locationId UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (items == nil)
                        items = [[NSMutableArray alloc] init];
                    VMItemEvent *item = [self readRecordIntoDomainObject:statement];
                    [items addObject:item];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return items;
}

- (NSMutableArray *)readByItemIdAndLocationName:(NSString *)itemId locationName:(NSString *)locationName {
    NSMutableArray *items;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"item_id\" = ?1 AND \"location_name\" = ?2", ITEM_EVENT_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [itemId UTF8String], -1, SQLITE_STATIC);
                sqlite3_bind_text(statement, 2, [locationName UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (items == nil)
                        items = [[NSMutableArray alloc] init];
                    VMItemEvent *item = [self readRecordIntoDomainObject:statement];
                    [items addObject:item];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return items;
}

- (NSMutableArray *)readByNames:(NSArray *)names {
    return nil;
}

- (NSMutableArray *)readAll{
    NSMutableArray *items;
    @try {
        @autoreleasepool {
            // Get dictionary and sort in alphabetical order
            NSMutableDictionary *dictionary = [self readAllAsDictionary];
            NSArray *keys = [dictionary allKeys];
            NSArray *sortedKeys = [keys sortedArrayUsingSelector:@selector(compare:)];
            items = [[NSMutableArray alloc] init];
            for(NSString *key in sortedKeys) {
                VMItemEvent *item = [dictionary objectForKey:key];
                [items addObject:item];
            }
        }
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
        [self rollbackTransaction:self.dbHandle];
    }
    return items;
}

- (NSMutableDictionary *)readAllAsDictionary {
    NSMutableDictionary *itemDictionary;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\"", ITEM_EVENT_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (itemDictionary == nil)
                        itemDictionary = [[NSMutableDictionary alloc] init];
                    VMItemEvent *itemEvent = [self readRecordIntoDomainObject:statement];
                    if (itemEvent)
                        [itemDictionary setObject:itemEvent forKey:[itemEvent.guid copy]];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return itemDictionary;
}

// UPDATE METHODS

- (void)update:(id)object {
    if (self.opened && [object isKindOfClass:[VMItemEvent class]]) {
        @autoreleasepool {
            @try {
                VMItemEvent *itemEvent = (VMItemEvent *)object;
                
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"UPDATE %@ SET %@ WHERE \"guid\" = ?30", self.tableName, ITEM_EVENT_SQL_UPDATE_PARAMS_STATEMENT];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // Create dateFormatter
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                
                // item_id
                sqlite3_bind_text(statement, 1, [itemEvent.itemGUID UTF8String], -1, SQLITE_STATIC);
                
                // type
                sqlite3_bind_text(statement, 2, [itemEvent.type UTF8String], -1, SQLITE_STATIC);
                
                // location_id
                sqlite3_bind_text(statement, 3, [itemEvent.locationGUID UTF8String], -1, SQLITE_STATIC);
                
                // location_name
                sqlite3_bind_text(statement, 4, [itemEvent.locationName UTF8String], -1, SQLITE_STATIC);
                
                // origination_date
                sqlite3_bind_text(statement, 5, [[dateFormat stringFromDate:itemEvent.originationDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // maturity_date
                sqlite3_bind_text(statement, 6, [[dateFormat stringFromDate:itemEvent.maturityDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // best_by_date
                sqlite3_bind_text(statement, 7, [[dateFormat stringFromDate:itemEvent.bestByDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // change_date
                sqlite3_bind_text(statement, 8, [[dateFormat stringFromDate:itemEvent.changeDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // retest_date
                sqlite3_bind_text(statement, 9, [[dateFormat stringFromDate:itemEvent.retestDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // expiration_date
                sqlite3_bind_text(statement, 10, [[dateFormat stringFromDate:itemEvent.expirationDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // territory_code
                sqlite3_bind_text(statement, 11, [itemEvent.territoryCode UTF8String], -1, SQLITE_STATIC);
                
                // place_of_origin
                sqlite3_bind_text(statement, 12, [itemEvent.placeOfOrigin UTF8String], -1, SQLITE_STATIC);
                
                // lot_number
                sqlite3_bind_text(statement, 13, [itemEvent.lotNumber UTF8String], -1, SQLITE_STATIC);
                
                // date_code
                sqlite3_bind_text(statement, 14, [itemEvent.dateCode UTF8String], -1, SQLITE_STATIC);
                
                // grade
                sqlite3_bind_text(statement, 15, [itemEvent.grade UTF8String], -1, SQLITE_STATIC);
                
                // color
                sqlite3_bind_text(statement, 16, [itemEvent.color UTF8String], -1, SQLITE_STATIC);
                
                // status
                sqlite3_bind_text(statement, 17, [itemEvent.status UTF8String], -1, SQLITE_STATIC);
                
                // condition
                sqlite3_bind_text(statement, 18, [itemEvent.condition UTF8String], -1, SQLITE_STATIC);
                
                // quantity
                sqlite3_bind_double(statement, 19, itemEvent.quantity);
                
                // value
                sqlite3_bind_double(statement, 20, itemEvent.value);
                
                // age_in_days
                sqlite3_bind_int(statement, 21, itemEvent.ageInDays);
                
                // length
                sqlite3_bind_int(statement, 22, itemEvent.length);
                
                // thickness
                sqlite3_bind_int(statement, 23, itemEvent.thickness);
                
                // volume
                sqlite3_bind_int(statement, 24, itemEvent.volume);
                
                // width
                sqlite3_bind_int(statement, 25, itemEvent.width);
                
                // recycled_content
                sqlite3_bind_int(statement, 26, itemEvent.recycledContent);
                
                // created_date
                sqlite3_bind_text(statement, 27, [[dateFormat stringFromDate:itemEvent.createdDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // last_updated_date
                sqlite3_bind_text(statement, 28, [[dateFormat stringFromDate:itemEvent.lastUpdatedDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // last_updated_user_id
                if (itemEvent.lastUpdatedUserId)
                    sqlite3_bind_text(statement, 29, [itemEvent.lastUpdatedUserId UTF8String], -1, SQLITE_STATIC);
                
                // guid
                sqlite3_bind_text(statement, 30, [itemEvent.guid UTF8String], -1, SQLITE_STATIC);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while updating. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

- (void)updateList:(NSMutableArray *)objects {
}

// DELETE METHODS

- (void)deleteById:(NSString *)guid {
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"DELETE FROM \"%@\" WHERE \"guid\" = ?1", self.tableName];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // name
                sqlite3_bind_text(statement, 1, [guid UTF8String], -1, SQLITE_STATIC);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while deleting. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

- (void)deleteByIds:(NSMutableArray *)guids {
}

- (void)deleteByName:(NSString *)name {
}

- (void)deleteByNames:(NSMutableArray *)names {
}

- (void)deleteAll {
}

@end
