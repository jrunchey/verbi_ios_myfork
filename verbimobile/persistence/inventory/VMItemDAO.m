//
//  VMItemDAO.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMItemDAO.h"

static VMItemDAO *_sharedInstanceVMItemDAO;

@implementation VMItemDAO

#pragma mark SINGELTON METHODS

+(VMItemDAO *)sharedInstance {
    @synchronized (self) {
        if (_sharedInstanceVMItemDAO == nil){
            _sharedInstanceVMItemDAO = [[self alloc] init];
            [_sharedInstanceVMItemDAO setDBFileName:DB_FULL_NAME];
            _sharedInstanceVMItemDAO.tableName = DB_TABLE_ITEM;
            _sharedInstanceVMItemDAO.initialized = YES;
        }
    }
    return _sharedInstanceVMItemDAO;
}

#pragma mark CRUD methods

- (VMItem *)readRecordIntoDomainObject:(sqlite3_stmt *)statement {
    VMItem *item = nil;
    @autoreleasepool {
        @try {
            const char *result;
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            // guid
            NSString *guid;
            result = (char *)sqlite3_column_text(statement,0);
            if (result)
                guid = [NSString stringWithUTF8String:result];
            
            // item_master_id
            NSString *itemMasterGUID;
            result = (char *)sqlite3_column_text(statement,1);
            if (result)
                itemMasterGUID = [NSString stringWithUTF8String:result];
            
            // location_id
            NSString *locationGUID;
            result = (char *)sqlite3_column_text(statement,2);
            if (result)
                locationGUID = [NSString stringWithUTF8String:result];
            
            // origination_date
            NSDate *originationDate;
            result = (char *)sqlite3_column_text(statement,3);
            if (result)
                originationDate =[dateFormat dateFromString:[NSString stringWithUTF8String:result]];
            
            // maturity_date
            NSDate *maturityDate;
            result = (char *)sqlite3_column_text(statement,4);
            if (result)
                maturityDate =[dateFormat dateFromString:[NSString stringWithUTF8String:result]];
            
            // best_by_date
            NSDate *bestByDate;
            result = (char *)sqlite3_column_text(statement,5);
            if (result)
                bestByDate =[dateFormat dateFromString:[NSString stringWithUTF8String:result]];
            
            // change_date
            NSDate *changeDate;
            result = (char *)sqlite3_column_text(statement,6);
            if (result)
                changeDate =[dateFormat dateFromString:[NSString stringWithUTF8String:result]];
            
            // retest_date
            NSDate *retestDate;
            result = (char *)sqlite3_column_text(statement,7);
            if (result)
                retestDate =[dateFormat dateFromString:[NSString stringWithUTF8String:result]];
            
            // expiration_date
            NSDate *expirationDate;
            result = (char *)sqlite3_column_text(statement,8);
            if (result)
                expirationDate =[dateFormat dateFromString:[NSString stringWithUTF8String:result]];
            
            // territory_code
            NSString *territoryCode;
            result = (char *)sqlite3_column_text(statement,9);
            if (result)
                territoryCode = [NSString stringWithUTF8String:result];
            
            // place_of_origin
            NSString *placeOfOrigin;
            result = (char *)sqlite3_column_text(statement,10);
            if (result)
                placeOfOrigin = [NSString stringWithUTF8String:result];
            
            // lot_number
            NSString *lotNumber;
            result = (char *)sqlite3_column_text(statement,11);
            if (result)
                lotNumber = [NSString stringWithUTF8String:result];
            
            // date_code
            NSString *dateCode;
            result = (char *)sqlite3_column_text(statement,12);
            if (result)
                dateCode = [NSString stringWithUTF8String:result];
            
            // grade
            NSString *grade;
            result = (char *)sqlite3_column_text(statement,13);
            if (result)
                grade = [NSString stringWithUTF8String:result];
            
            // color
            NSString *color;
            result = (char *)sqlite3_column_text(statement,14);
            if (result)
                color = [NSString stringWithUTF8String:result];
            
            // status
            NSString *status;
            result = (char *)sqlite3_column_text(statement,15);
            if (result)
                status = [NSString stringWithUTF8String:result];
            
            // condition
            NSString *condition;
            result = (char *)sqlite3_column_text(statement,16);
            if (result)
                condition = [NSString stringWithUTF8String:result];
            
            // quantity
            float quantity = sqlite3_column_double(statement, 17);
            
            // value
            float value = sqlite3_column_double(statement, 18);
            
            // age_in_days
            int ageInDays = sqlite3_column_int(statement,19);
            
            // length
            int length = sqlite3_column_int(statement,20);
            
            // thickness
            int thickness = sqlite3_column_int(statement,21);
            
            // volume
            int volume = sqlite3_column_int(statement,22);
            
            // width
            int width = sqlite3_column_int(statement,23);
            
            // recycled_content
            int recycledContent = sqlite3_column_int(statement,24);
            
            // created_date
            NSDate *createdDate;
            result = (char *)sqlite3_column_text(statement,25);
            if (result)
                createdDate =[dateFormat dateFromString:[NSString stringWithUTF8String:result]];
            
            // last_updated_date
            NSDate *lastUpdatedDate;
            result = (char *)sqlite3_column_text(statement,26);
            if (result)
                lastUpdatedDate =[dateFormat dateFromString:[NSString stringWithUTF8String:result]];
            
            // last_updated_user_id
            NSString *lastUpdatedUserId;
            result = (char *)sqlite3_column_text(statement,27);
            if (result)
                lastUpdatedUserId = [NSString stringWithUTF8String:result];
            
            // enabled
            BOOL enabled = sqlite3_column_int(statement,28);
            
            // archived
            BOOL archived = sqlite3_column_int(statement,29);
            
            item = [[VMItem alloc] init:guid itemMasterGUID:itemMasterGUID location:locationGUID quantity:quantity];

            item.originationDate = originationDate;
            item.maturityDate = maturityDate;
            item.bestByDate = bestByDate;
            item.changeDate = changeDate;
            item.retestDate = retestDate;
            item.expirationDate = expirationDate;
            item.territoryCode = territoryCode;
            item.placeOfOrigin = placeOfOrigin;
            item.lotNumber = lotNumber;
            item.dateCode = dateCode;
            item.grade = grade;
            item.color = color;
            item.status = status;
            item.condition = condition;
            item.value = value;
            item.ageInDays = ageInDays;
            item.length = length;
            item.thickness = thickness;
            item.volume = volume;
            item.width = width;
            item.recycledContent = recycledContent;
            item.createdDate = createdDate;
            item.lastUpdatedDate = lastUpdatedDate;
            item.enabled = enabled;
            item.archived = archived;
        }
        @catch (NSException *e) {
            NSLog(@"An exception has occurred: %@", [e reason]);
        }
    }
    return item;
}

// CREATE METHODS

- (void)create:(id)object {
    if (self.opened && [object isKindOfClass:[VMItem class]]) {
        @autoreleasepool {
            @try {
                VMItem *item = (VMItem *)object;
                
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"INSERT INTO \"%@\" (%@) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15, ?16, ?17, ?18, ?19, ?20, ?21, ?22, ?23, ?24, ?25, ?26, ?27, ?28, ?29, ?30)", self.tableName, ITEM_SQL_INSERT_SELECT_PARAMS_STATEMENT];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // Create dateFormatter
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                
                // guid
                sqlite3_bind_text(statement, 1, [item.guid UTF8String], -1, SQLITE_STATIC);
                
                // item_master_id
                sqlite3_bind_text(statement, 2, [item.itemMasterGUID UTF8String], -1, SQLITE_STATIC);
                
                // location_id
                sqlite3_bind_text(statement, 3, [item.locationGUID UTF8String], -1, SQLITE_STATIC);
                
                // origination_date
                sqlite3_bind_text(statement, 4, [[dateFormat stringFromDate:item.originationDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // maturity_date
                sqlite3_bind_text(statement, 5, [[dateFormat stringFromDate:item.maturityDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // best_by_date
                sqlite3_bind_text(statement, 6, [[dateFormat stringFromDate:item.bestByDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // change_date
                sqlite3_bind_text(statement, 7, [[dateFormat stringFromDate:item.changeDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // retest_date
                sqlite3_bind_text(statement, 8, [[dateFormat stringFromDate:item.retestDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // expiration_date
                sqlite3_bind_text(statement, 9, [[dateFormat stringFromDate:item.expirationDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // territory_code
                sqlite3_bind_text(statement, 10, [item.territoryCode UTF8String], -1, SQLITE_STATIC);
                
                // place_of_origin
                sqlite3_bind_text(statement, 11, [item.placeOfOrigin UTF8String], -1, SQLITE_STATIC);
                
                // lot_number
                sqlite3_bind_text(statement, 12, [item.lotNumber UTF8String], -1, SQLITE_STATIC);
                
                // date_code
                sqlite3_bind_text(statement, 13, [item.dateCode UTF8String], -1, SQLITE_STATIC);
                
                // grade
                sqlite3_bind_text(statement, 14, [item.grade UTF8String], -1, SQLITE_STATIC);
                
                // color
                sqlite3_bind_text(statement, 15, [item.color UTF8String], -1, SQLITE_STATIC);
                
                // status
                sqlite3_bind_text(statement, 16, [item.status UTF8String], -1, SQLITE_STATIC);
                
                // condition
                sqlite3_bind_text(statement, 17, [item.condition UTF8String], -1, SQLITE_STATIC);
                
                // quantity
                sqlite3_bind_double(statement, 18, item.quantity);
                
                // value
                sqlite3_bind_double(statement, 19, item.value);

                // age_in_days
                sqlite3_bind_int(statement, 20, item.ageInDays);
                
                // length
                sqlite3_bind_int(statement, 21, item.length);
                
                // thickness
                sqlite3_bind_int(statement, 22, item.thickness);
                
                // volume
                sqlite3_bind_int(statement, 23, item.volume);
                
                // width
                sqlite3_bind_int(statement, 24, item.width);
                
                // recycled_content
                sqlite3_bind_int(statement, 25, item.recycledContent);
                
                // created_date
                sqlite3_bind_text(statement, 26, [[dateFormat stringFromDate:item.createdDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // last_updated_date
                sqlite3_bind_text(statement, 27, [[dateFormat stringFromDate:item.lastUpdatedDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // last_updated_user_id
                if (item.lastUpdatedUserId)
                    sqlite3_bind_text(statement, 28, [item.lastUpdatedUserId UTF8String], -1, SQLITE_STATIC);
                
                // enabled
                sqlite3_bind_int(statement, 29, item.enabled);
                
                // archived
                sqlite3_bind_int(statement, 30, item.archived);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while inserting data. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

- (void)createList:(NSMutableArray *)objects {
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"INSERT INTO \"%@\" (%@) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15, ?16, ?17, ?18, ?19, ?20, ?21, ?22, ?23, ?24, ?25, ?26, ?27, ?28, ?29, ?30)", self.tableName, ITEM_SQL_INSERT_SELECT_PARAMS_STATEMENT];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // Create dateFormatter
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                
                for (NSObject *object in objects) {
                    if ([object isKindOfClass:[VMItem class]]) {
                        VMItem *item = (VMItem *)object;
                        
                        // guid
                        sqlite3_bind_text(statement, 1, [item.guid UTF8String], -1, SQLITE_STATIC);
                        
                        // item_master_id
                        sqlite3_bind_text(statement, 2, [item.itemMasterGUID UTF8String], -1, SQLITE_STATIC);
                        
                        // location_id
                        sqlite3_bind_text(statement, 3, [item.locationGUID UTF8String], -1, SQLITE_STATIC);
                        
                        // origination_date
                        sqlite3_bind_text(statement, 4, [[dateFormat stringFromDate:item.originationDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // maturity_date
                        sqlite3_bind_text(statement, 5, [[dateFormat stringFromDate:item.maturityDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // best_by_date
                        sqlite3_bind_text(statement, 6, [[dateFormat stringFromDate:item.bestByDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // change_date
                        sqlite3_bind_text(statement, 7, [[dateFormat stringFromDate:item.changeDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // retest_date
                        sqlite3_bind_text(statement, 8, [[dateFormat stringFromDate:item.retestDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // expiration_date
                        sqlite3_bind_text(statement, 9, [[dateFormat stringFromDate:item.expirationDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // territory_code
                        sqlite3_bind_text(statement, 10, [item.territoryCode UTF8String], -1, SQLITE_STATIC);
                        
                        // place_of_origin
                        sqlite3_bind_text(statement, 11, [item.placeOfOrigin UTF8String], -1, SQLITE_STATIC);
                        
                        // lot_number
                        sqlite3_bind_text(statement, 12, [item.lotNumber UTF8String], -1, SQLITE_STATIC);
                        
                        // date_code
                        sqlite3_bind_text(statement, 13, [item.dateCode UTF8String], -1, SQLITE_STATIC);
                        
                        // grade
                        sqlite3_bind_text(statement, 14, [item.grade UTF8String], -1, SQLITE_STATIC);
                        
                        // color
                        sqlite3_bind_text(statement, 15, [item.color UTF8String], -1, SQLITE_STATIC);
                        
                        // status
                        sqlite3_bind_text(statement, 16, [item.status UTF8String], -1, SQLITE_STATIC);
                        
                        // condition
                        sqlite3_bind_text(statement, 17, [item.condition UTF8String], -1, SQLITE_STATIC);
                        
                        // quantity
                        sqlite3_bind_double(statement, 18, item.quantity);
                        
                        // value
                        sqlite3_bind_double(statement, 19, item.value);
                        
                        // age_in_days
                        sqlite3_bind_int(statement, 20, item.ageInDays);
                        
                        // length
                        sqlite3_bind_int(statement, 21, item.length);
                        
                        // thickness
                        sqlite3_bind_int(statement, 22, item.thickness);
                        
                        // volume
                        sqlite3_bind_int(statement, 23, item.volume);
                        
                        // width
                        sqlite3_bind_int(statement, 24, item.width);
                        
                        // recycled_content
                        sqlite3_bind_int(statement, 25, item.recycledContent);
                        
                        // created_date
                        sqlite3_bind_text(statement, 26, [[dateFormat stringFromDate:item.createdDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // last_updated_date
                        sqlite3_bind_text(statement, 27, [[dateFormat stringFromDate:item.lastUpdatedDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // last_updated_user_id
                        if (item.lastUpdatedUserId)
                            sqlite3_bind_text(statement, 28, [item.lastUpdatedUserId UTF8String], -1, SQLITE_STATIC);
                        
                        // enabled
                        sqlite3_bind_int(statement, 29, item.enabled);
                        
                        // archived
                        sqlite3_bind_int(statement, 30, item.archived);
                        
                        while(YES){
                            NSInteger result = sqlite3_step(statement);
                            if(result == SQLITE_DONE){
                                break;
                            }
                            else if(result != SQLITE_BUSY){
                                printf("db error: %s\n", sqlite3_errmsg(self.dbHandle));
                                break;
                            }
                        }
                        sqlite3_reset(statement);
                    }
                }
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

// READ METHODS

- (int)totalNumberOfRecords:(NSString *)itemMasterId locationId:(NSString *)locationId {
    int count = 0;
    if (self.opened) {
        @autoreleasepool {
            @try {
                NSString *sql = [NSString stringWithFormat:@"SELECT COUNT(*) FROM \"%@\" WHERE \"item_master_id\" = ?1 AND \"location_id\" = ?2", self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [itemMasterId UTF8String], -1, SQLITE_STATIC);
                sqlite3_bind_text(statement, 2, [locationId UTF8String], -1, SQLITE_STATIC);
                
                count = [[self lookupSingularSQL:sql forType:@"integer"] integerValue];
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
            }
        }
    }
    return count;
}

- (int)totalNumberOfRecords:(NSString *)itemMasterId locationId:(NSString *)locationId status:(NSString *)status {
    int count = 0;
    if (self.opened) {
        @autoreleasepool {
            @try {
                NSString *sql = [NSString stringWithFormat:@"SELECT COUNT(*) FROM \"%@\" WHERE \"item_master_id\" = ?1 AND \"location_id\" = ?2 AND \"status\" = ?3", self.tableName];
                NSLog(@"sql: %@ | %@ | %@ | %@", sql, itemMasterId, locationId, status);
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [itemMasterId UTF8String], -1, SQLITE_STATIC);
                sqlite3_bind_text(statement, 2, [locationId UTF8String], -1, SQLITE_STATIC);
                sqlite3_bind_text(statement, 3, [status UTF8String], -1, SQLITE_STATIC);

                
                count = [[self lookupSingularSQL:sql forType:@"integer"] integerValue];
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
            }
        }
    }
    return count;
}

- (id)readById:(NSString *)guid {
    VMItem *item = nil;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"guid\" = ?1", ITEM_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [guid UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    item = [self readRecordIntoDomainObject:statement];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return item;

}

- (NSMutableArray *)readByIds:(NSArray *)guids {
    return nil;
}

- (id)readByName:(NSString *)name {
    VMItem *item = nil;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"name\" = ?1", ITEM_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [name UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    item = [self readRecordIntoDomainObject:statement];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return item;
}

- (NSMutableArray *)read:(NSString *)itemMasterId locationId:(NSString *)locationId {
    NSMutableArray *items;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"item_master_id\" = ?1 AND \"location_id\" = ?2", ITEM_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [itemMasterId UTF8String], -1, SQLITE_STATIC);
                sqlite3_bind_text(statement, 2, [locationId UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (items == nil)
                        items = [[NSMutableArray alloc] init];
                    VMItem *item = [self readRecordIntoDomainObject:statement];
                    [items addObject:item];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return items;
}

- (NSMutableArray *)read:(NSString *)itemMasterId locationId:(NSString *)locationId status:(NSString *)status {
    NSMutableArray *items;
    VMItem *item = nil;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"item_master_id\" = ?1 AND \"location_id\" = ?2 AND \"status\" = ?3", ITEM_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [itemMasterId UTF8String], -1, SQLITE_STATIC);
                sqlite3_bind_text(statement, 2, [locationId UTF8String], -1, SQLITE_STATIC);
                sqlite3_bind_text(statement, 3, [status UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (items == nil)
                        items = [[NSMutableArray alloc] init];
                    item = [self readRecordIntoDomainObject:statement];
                    [items addObject:item];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return items;
}

- (NSMutableArray *)readByItemMasterId:(NSString *)itemMasterId {
    NSMutableArray *items;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"item_master_id\" = ?1", ITEM_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [itemMasterId UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (items == nil)
                        items = [[NSMutableArray alloc] init];
                    VMItem *item = [self readRecordIntoDomainObject:statement];
                    [items addObject:item];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return items;
}

- (NSMutableArray *)readByLocationId:(NSString *)locationId {
    NSMutableArray *items;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"location_id\" = ?1", ITEM_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [locationId UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (items == nil)
                        items = [[NSMutableArray alloc] init];
                    VMItem *item = [self readRecordIntoDomainObject:statement];
                    [items addObject:item];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return items;
}

- (NSMutableArray *)readByPlaceOfOrigin:(NSString *)placeOfOrigin {
    NSMutableArray *items;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"place_of_origin\" = ?1", ITEM_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [placeOfOrigin UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (items == nil)
                        items = [[NSMutableArray alloc] init];
                    VMItem *item = [self readRecordIntoDomainObject:statement];
                    [items addObject:item];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return items;
}

- (NSMutableArray *)readByLotNumber:(NSString *)lotNumber {
    NSMutableArray *items;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"lot_number\" = ?1", ITEM_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [lotNumber UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (items == nil)
                        items = [[NSMutableArray alloc] init];
                    VMItem *item = [self readRecordIntoDomainObject:statement];
                    [items addObject:item];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return items;
}

- (NSMutableArray *)readByNames:(NSArray *)names {
    return nil;
}

- (NSMutableArray *)readAll{
    NSMutableArray *items;
    @try {
        @autoreleasepool {
            // Get dictionary and sort in alphabetical order
            NSMutableDictionary *dictionary = [self readAllAsDictionary];
            NSArray *keys = [dictionary allKeys];
            NSArray *sortedKeys = [keys sortedArrayUsingSelector:@selector(compare:)];
            items = [[NSMutableArray alloc] init];
            for(NSString *key in sortedKeys) {
                VMItem *item = [dictionary objectForKey:key];
                [items addObject:item];
            }
        }
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
        [self rollbackTransaction:self.dbHandle];
    }
    return items;
}

- (NSMutableDictionary *)readAllAsDictionary {
    NSMutableDictionary *itemDictionary;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\"", ITEM_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (itemDictionary == nil)
                        itemDictionary = [[NSMutableDictionary alloc] init];
                    VMItem *item = [self readRecordIntoDomainObject:statement];
                    if (item)
                        [itemDictionary setObject:item forKey:[item.guid copy]];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return itemDictionary;
}

// UPDATE METHODS

- (void)update:(id)object {
    if (self.opened && [object isKindOfClass:[VMItem class]]) {
        @autoreleasepool {
            @try {
                VMItem *item = (VMItem *)object;
                
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"UPDATE %@ SET %@ WHERE \"guid\" = ?30", self.tableName, ITEM_SQL_UPDATE_PARAMS_STATEMENT];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // Create dateFormatter
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                
                // item_master_id
                sqlite3_bind_text(statement, 1, [item.itemMasterGUID UTF8String], -1, SQLITE_STATIC);
                
                // location_id
                sqlite3_bind_text(statement, 2, [item.locationGUID UTF8String], -1, SQLITE_STATIC);
                
                // origination_date
                sqlite3_bind_text(statement, 3, [[dateFormat stringFromDate:item.originationDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // maturity_date
                sqlite3_bind_text(statement, 4, [[dateFormat stringFromDate:item.maturityDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // best_by_date
                sqlite3_bind_text(statement, 5, [[dateFormat stringFromDate:item.bestByDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // change_date
                sqlite3_bind_text(statement, 6, [[dateFormat stringFromDate:item.changeDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // retest_date
                sqlite3_bind_text(statement, 7, [[dateFormat stringFromDate:item.retestDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // expiration_date
                sqlite3_bind_text(statement, 8, [[dateFormat stringFromDate:item.expirationDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // territory_code
                sqlite3_bind_text(statement, 9, [item.territoryCode UTF8String], -1, SQLITE_STATIC);
                
                // place_of_origin
                sqlite3_bind_text(statement, 10, [item.placeOfOrigin UTF8String], -1, SQLITE_STATIC);
                
                // lot_number
                sqlite3_bind_text(statement, 11, [item.lotNumber UTF8String], -1, SQLITE_STATIC);
                
                // date_code
                sqlite3_bind_text(statement, 12, [item.dateCode UTF8String], -1, SQLITE_STATIC);
                
                // grade
                sqlite3_bind_text(statement, 13, [item.grade UTF8String], -1, SQLITE_STATIC);
                
                // color
                sqlite3_bind_text(statement, 14, [item.color UTF8String], -1, SQLITE_STATIC);
                
                // status
                sqlite3_bind_text(statement, 15, [item.status UTF8String], -1, SQLITE_STATIC);
                
                // condition
                sqlite3_bind_text(statement, 16, [item.condition UTF8String], -1, SQLITE_STATIC);
                
                // quantity
                sqlite3_bind_double(statement, 17, item.quantity);
                
                // value
                sqlite3_bind_double(statement, 18, item.value);
                
                // age_in_days
                sqlite3_bind_int(statement, 19, item.ageInDays);
                
                // length
                sqlite3_bind_int(statement, 20, item.length);
                
                // thickness
                sqlite3_bind_int(statement, 21, item.thickness);
                
                // volume
                sqlite3_bind_int(statement, 22, item.volume);
                
                // width
                sqlite3_bind_int(statement, 23, item.width);
                
                // recycled_content
                sqlite3_bind_int(statement, 24, item.recycledContent);
                
                // created_date
                sqlite3_bind_text(statement, 25, [[dateFormat stringFromDate:item.createdDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // last_updated_date
                sqlite3_bind_text(statement, 26, [[dateFormat stringFromDate:item.lastUpdatedDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // last_updated_user_id
                if (item.lastUpdatedUserId)
                    sqlite3_bind_text(statement, 27, [item.lastUpdatedUserId UTF8String], -1, SQLITE_STATIC);
                
                // enabled
                sqlite3_bind_int(statement, 28, item.enabled);
                
                // archived
                sqlite3_bind_int(statement, 29, item.archived);
                
                // guid
                sqlite3_bind_text(statement, 30, [item.guid UTF8String], -1, SQLITE_STATIC);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while updating. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

- (void)updateList:(NSMutableArray *)objects {
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"UPDATE %@ SET %@ WHERE \"guid\" = ?30", self.tableName, ITEM_SQL_UPDATE_PARAMS_STATEMENT];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // Create dateFormatter
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                
                for (NSObject *object in objects) {
                    if ([object isKindOfClass:[VMItem class]]) {
                        VMItem *item = (VMItem *)object;

                        // item_master_id
                        sqlite3_bind_text(statement, 1, [item.itemMasterGUID UTF8String], -1, SQLITE_STATIC);
                        
                        // location_id
                        sqlite3_bind_text(statement, 2, [item.locationGUID UTF8String], -1, SQLITE_STATIC);
                        
                        // origination_date
                        sqlite3_bind_text(statement, 3, [[dateFormat stringFromDate:item.originationDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // maturity_date
                        sqlite3_bind_text(statement, 4, [[dateFormat stringFromDate:item.maturityDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // best_by_date
                        sqlite3_bind_text(statement, 5, [[dateFormat stringFromDate:item.bestByDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // change_date
                        sqlite3_bind_text(statement, 6, [[dateFormat stringFromDate:item.changeDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // retest_date
                        sqlite3_bind_text(statement, 7, [[dateFormat stringFromDate:item.retestDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // expiration_date
                        sqlite3_bind_text(statement, 8, [[dateFormat stringFromDate:item.expirationDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // territory_code
                        sqlite3_bind_text(statement, 9, [item.territoryCode UTF8String], -1, SQLITE_STATIC);
                        
                        // place_of_origin
                        sqlite3_bind_text(statement, 10, [item.placeOfOrigin UTF8String], -1, SQLITE_STATIC);
                        
                        // lot_number
                        sqlite3_bind_text(statement, 11, [item.lotNumber UTF8String], -1, SQLITE_STATIC);
                        
                        // date_code
                        sqlite3_bind_text(statement, 12, [item.dateCode UTF8String], -1, SQLITE_STATIC);
                        
                        // grade
                        sqlite3_bind_text(statement, 13, [item.grade UTF8String], -1, SQLITE_STATIC);
                        
                        // color
                        sqlite3_bind_text(statement, 14, [item.color UTF8String], -1, SQLITE_STATIC);
                        
                        // status
                        sqlite3_bind_text(statement, 15, [item.status UTF8String], -1, SQLITE_STATIC);
                        
                        // condition
                        sqlite3_bind_text(statement, 16, [item.condition UTF8String], -1, SQLITE_STATIC);
                        
                        // quantity
                        sqlite3_bind_double(statement, 17, item.quantity);
                        
                        // value
                        sqlite3_bind_double(statement, 18, item.value);
                        
                        // age_in_days
                        sqlite3_bind_int(statement, 19, item.ageInDays);
                        
                        // length
                        sqlite3_bind_int(statement, 20, item.length);
                        
                        // thickness
                        sqlite3_bind_int(statement, 21, item.thickness);
                        
                        // volume
                        sqlite3_bind_int(statement, 22, item.volume);
                        
                        // width
                        sqlite3_bind_int(statement, 23, item.width);
                        
                        // recycled_content
                        sqlite3_bind_int(statement, 24, item.recycledContent);
                        
                        // created_date
                        sqlite3_bind_text(statement, 25, [[dateFormat stringFromDate:item.createdDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // last_updated_date
                        sqlite3_bind_text(statement, 26, [[dateFormat stringFromDate:item.lastUpdatedDate] UTF8String] , -1, SQLITE_TRANSIENT);
                        
                        // last_updated_user_id
                        if (item.lastUpdatedUser)
                            sqlite3_bind_text(statement, 27, [item.lastUpdatedUser.ID UTF8String], -1, SQLITE_STATIC);
                        
                        // enabled
                        sqlite3_bind_int(statement, 28, item.enabled);
                        
                        // archived
                        sqlite3_bind_int(statement, 29, item.archived);
                        
                        // guid
                        sqlite3_bind_text(statement, 30, [item.guid UTF8String], -1, SQLITE_STATIC);
                
                        while(YES){
                            NSInteger result = sqlite3_step(statement);
                            if(result == SQLITE_DONE){
                                break;
                            }
                            else if(result != SQLITE_BUSY){
                                printf("db error: %s\n", sqlite3_errmsg(self.dbHandle));
                                break;
                            }
                        }
                        sqlite3_reset(statement);
                    }
                }
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

// DELETE METHODS

- (void)deleteById:(NSString *)guid {
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"DELETE FROM \"%@\" WHERE \"guid\" = ?1", self.tableName];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // name
                sqlite3_bind_text(statement, 1, [guid UTF8String], -1, SQLITE_STATIC);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while deleting. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

- (void)deleteByIds:(NSMutableArray *)guids {
}

- (void)deleteByName:(NSString *)name {
}

- (void)deleteByNames:(NSMutableArray *)names {
}

- (void)deleteAll {
}

@end
