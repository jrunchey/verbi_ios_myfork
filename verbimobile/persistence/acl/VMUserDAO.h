//
//  VMUserDAO.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMUser.h"
#import "VMDAO.h"

@interface VMUserDAO : VMDAO

#pragma mark SINGELTON METHODS

+ (VMDAO *)sharedInstance;

@end
