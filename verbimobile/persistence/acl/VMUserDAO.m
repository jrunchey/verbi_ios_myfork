//
//  VMUserDAO.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMUserDAO.h"

static VMUserDAO *_sharedInstanceVMUserDAO;

@implementation VMUserDAO

#pragma mark SINGELTON METHODS

+(VMUserDAO *)sharedInstance {
    @synchronized (self) {
        if (_sharedInstanceVMUserDAO == nil){
            _sharedInstanceVMUserDAO = [[self alloc] init];
            [_sharedInstanceVMUserDAO setDBFileName:DB_FULL_NAME];
            _sharedInstanceVMUserDAO.tableName = DB_TABLE_USER;
            _sharedInstanceVMUserDAO.initialized = YES;
        }
    }
    return _sharedInstanceVMUserDAO;
}

#pragma mark CONSTANTS

#define SELECT_ALL_USER_TABLE_COLUMNS       @"\"id\", \"enabled\", \"archived\", \"access_level\", \"account_id\", \"person_id\", \"name\", \"username\", \"email\", \"password\", \"pin\", \"secret_question_1\", \"secret_answer_1\", \"secret_question_2\", \"secret_answer_2\", \"language\", \"timezone\", \"image\", \"last_login\", \"created\", \"updated\", \"updated_by\""

#define SET_ALL_USER_TABLE_COLUMNS      @"\"enabled\" = ?1, \"enabled\" = ?2, \"access_level\" = ?3, \"account_id\" = ?4, \"person_id\" = ?5, \"name\" = ?6, \"username\" = ?7, \"email\" = ?8, \"password\" = ?9, \"pin\" = ?10, \"secret_question_1\" = ?11, \"secret_answer_1\" = ?12, \"secret_question_2\" = ?13, \"secret_answer_2\" = ?14, \"language\" = ?15, \"timezone\" = ?16, \"image\" = ?17, \"last_login\" = ?18, \"created\" = ?19, \"updated\" = ?20, \"updated_by\" = ?21"

#pragma mark CRUD methods

// CREATE METHODS

- (void)create:(id)object {
    if (self.opened && [object isKindOfClass:[VMUser class]]) {
        @autoreleasepool {
            @try {
                VMUser *user = (VMUser *)object;
                
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"INSERT INTO \"%@\" (%@) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15, ?16, ?17, ?18, ?19, ?20, ?21, ?22)", self.tableName, SELECT_ALL_USER_TABLE_COLUMNS];
                NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // Create dateFormatter
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                
                // Bind column values
                [self setStringToColumn:statement column:1 dataTransient:NO string:user.ID];
                [self setBoolToColumn:statement column:2 boolValue:user.enabled];
                [self setBoolToColumn:statement column:3 boolValue:user.archived];
                [self setIntToColumn:statement column:4 intValue:user.accessLevel];
                [self setStringToColumn:statement column:5 dataTransient:NO string:user.accountId];
                [self setStringToColumn:statement column:6 dataTransient:NO string:user.personId];
                [self setStringToColumn:statement column:7 dataTransient:NO string:user.name];
                [self setStringToColumn:statement column:8 dataTransient:NO string:user.username];
                [self setStringToColumn:statement column:9 dataTransient:NO string:user.email];
                [self setStringToColumn:statement column:10 dataTransient:NO string:user.password];
                [self setStringToColumn:statement column:11 dataTransient:NO string:user.pin];
                [self setStringToColumn:statement column:12 dataTransient:NO string:user.secretQuestion1];
                [self setStringToColumn:statement column:13 dataTransient:NO string:user.secretAnswer1];
                [self setStringToColumn:statement column:14 dataTransient:NO string:user.secretQuestion2];
                [self setStringToColumn:statement column:15 dataTransient:NO string:user.secretAnswer2];
                [self setStringToColumn:statement column:16 dataTransient:NO string:user.language];
                [self setStringToColumn:statement column:17 dataTransient:NO string:user.timezone];
                [self setImageToColumn:statement column:18 dataTransient:NO image:user.image];
                [self setDateToColumn:statement column:19 dataTransient:NO date:user.lastLogin];
                [self setDateToColumn:statement column:20 dataTransient:NO date:user.created];
                [self setDateToColumn:statement column:21 dataTransient:NO date:user.updated];
                [self setStringToColumn:statement column:22 dataTransient:NO string:user.updatedBy];
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while inserting data. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

// READ METHODS

- (id)readById:(NSString *)guid {
    VMUser *user = nil;
    return user;
}

- (NSMutableArray *)readByIds:(NSArray *)guids {
    return nil;
}

- (id)readByName:(NSString *)name {
    VMUser *user = nil;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"username\" = ?1", SELECT_ALL_USER_TABLE_COLUMNS, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                
                // username
                sqlite3_bind_text(statement, 1, [name UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    @try {
                        // Get values from columns
                        NSString *ID = [self getStringFromColumn:statement column:0];
                        BOOL enabled = [self getBoolFromColumn:statement column:1];
                        BOOL archived = [self getBoolFromColumn:statement column:2];
                        int accessLevel = [self getIntFromColumn:statement column:3];
                        NSString *accountId = [self getStringFromColumn:statement column:4];
                        NSString *personId = [self getStringFromColumn:statement column:5];
                        NSString *name = [self getStringFromColumn:statement column:6];
                        NSString *username = [self getStringFromColumn:statement column:7];
                        NSString *email = [self getStringFromColumn:statement column:8];
                        NSString *password = [self getStringFromColumn:statement column:9];
                        NSString *pin = [self getStringFromColumn:statement column:10];
                        NSString *secretQuestion1 = [self getStringFromColumn:statement column:11];
                        NSString *secretAnswer1 = [self getStringFromColumn:statement column:12];
                        NSString *secretQuestion2 = [self getStringFromColumn:statement column:13];
                        NSString *secretAnswer2 = [self getStringFromColumn:statement column:14];
                        NSString *language = [self getStringFromColumn:statement column:15];
                        NSString *timezone = [self getStringFromColumn:statement column:16];
                        UIImage *image = [self getImageFromColumn:statement column:17];
                        NSDate *lastLogin = [self getDateFromColumn:statement column:18];
                        NSDate *created = [self getDateFromColumn:statement column:19];
                        NSDate *updated = [self getDateFromColumn:statement column:20];
                        NSString *updatedBy = [self getStringFromColumn:statement column:21];
                        
                        user = [[VMUser alloc] init:name username:username email:email password:password language:language timezone:timezone image:image];
                        user.ID = ID;
                        user.enabled = enabled;
                        user.archived = archived;
                        user.accessLevel = accessLevel;
                        user.accountId = accountId;
                        user.personId = personId;
                        user.pin = pin;
                        user.secretQuestion1 = secretQuestion1;
                        user.secretAnswer1 = secretAnswer1;
                        user.secretQuestion2 = secretQuestion2;
                        user.secretAnswer2 = secretAnswer2;
                        user.lastLogin = lastLogin;
                        user.created = created;
                        user.updated = updated;
                        user.updatedBy = updatedBy;
                        break;
                    }
                    @catch (NSException *e) {
                        NSLog(@"An exception has occurred: %@", [e reason]);
                    }
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return user;
}

- (NSMutableArray *)readByNames:(NSArray *)names {
    return nil;
}

- (NSMutableArray *)readAll{
    NSMutableArray *users;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\"", SELECT_ALL_USER_TABLE_COLUMNS, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (users == nil)
                        users = [[NSMutableArray alloc] init];
                    
                    @try {
                        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                        
                        // Get values from columns
                        NSString *ID = [self getStringFromColumn:statement column:0];
                        BOOL enabled = [self getBoolFromColumn:statement column:1];
                        BOOL archived = [self getBoolFromColumn:statement column:2];
                        int accessLevel = [self getIntFromColumn:statement column:3];
                        NSString *accountId = [self getStringFromColumn:statement column:4];
                        NSString *personId = [self getStringFromColumn:statement column:5];
                        NSString *name = [self getStringFromColumn:statement column:6];
                        NSString *username = [self getStringFromColumn:statement column:7];
                        NSString *email = [self getStringFromColumn:statement column:8];
                        NSString *password = [self getStringFromColumn:statement column:9];
                        NSString *pin = [self getStringFromColumn:statement column:10];
                        NSString *secretQuestion1 = [self getStringFromColumn:statement column:11];
                        NSString *secretAnswer1 = [self getStringFromColumn:statement column:12];
                        NSString *secretQuestion2 = [self getStringFromColumn:statement column:13];
                        NSString *secretAnswer2 = [self getStringFromColumn:statement column:14];
                        NSString *language = [self getStringFromColumn:statement column:15];
                        NSString *timezone = [self getStringFromColumn:statement column:16];
                        UIImage *image = [self getImageFromColumn:statement column:17];
                        NSDate *lastLogin = [self getDateFromColumn:statement column:18];
                        NSDate *created = [self getDateFromColumn:statement column:19];
                        NSDate *updated = [self getDateFromColumn:statement column:20];
                        NSString *updatedBy = [self getStringFromColumn:statement column:21];
                        
                        VMUser *user = [[VMUser alloc] init:name username:username email:email password:password language:language timezone:timezone image:image];
                        user.ID = ID;
                        user.enabled = enabled;
                        user.archived = archived;
                        user.accessLevel = accessLevel;
                        user.accountId = accountId;
                        user.personId = personId;
                        user.pin = pin;
                        user.secretQuestion1 = secretQuestion1;
                        user.secretAnswer1 = secretAnswer1;
                        user.secretQuestion2 = secretQuestion2;
                        user.secretAnswer2 = secretAnswer2;
                        user.lastLogin = lastLogin;
                        user.created = created;
                        user.updated = updated;
                        user.updatedBy = updatedBy;
                        [users addObject:user];
                    }
                    @catch (NSException *e) {
                        NSLog(@"An exception has occurred: %@", [e reason]);
                    }
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return users;
}

- (NSMutableDictionary *)readAllAsDictionary{
    NSMutableDictionary *users;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\"", SELECT_ALL_USER_TABLE_COLUMNS, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (users == nil)
                        users = [[NSMutableDictionary alloc] init];
                    
                    @try {
                        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                        
                        // Get values from columns
                        NSString *ID = [self getStringFromColumn:statement column:0];
                        BOOL enabled = [self getBoolFromColumn:statement column:1];
                        BOOL archived = [self getBoolFromColumn:statement column:2];
                        int accessLevel = [self getIntFromColumn:statement column:3];
                        NSString *accountId = [self getStringFromColumn:statement column:4];
                        NSString *personId = [self getStringFromColumn:statement column:5];
                        NSString *name = [self getStringFromColumn:statement column:6];
                        NSString *username = [self getStringFromColumn:statement column:7];
                        NSString *email = [self getStringFromColumn:statement column:8];
                        NSString *password = [self getStringFromColumn:statement column:9];
                        NSString *pin = [self getStringFromColumn:statement column:10];
                        NSString *secretQuestion1 = [self getStringFromColumn:statement column:11];
                        NSString *secretAnswer1 = [self getStringFromColumn:statement column:12];
                        NSString *secretQuestion2 = [self getStringFromColumn:statement column:13];
                        NSString *secretAnswer2 = [self getStringFromColumn:statement column:14];
                        NSString *language = [self getStringFromColumn:statement column:15];
                        NSString *timezone = [self getStringFromColumn:statement column:16];
                        UIImage *image = [self getImageFromColumn:statement column:17];
                        NSDate *lastLogin = [self getDateFromColumn:statement column:18];
                        NSDate *created = [self getDateFromColumn:statement column:19];
                        NSDate *updated = [self getDateFromColumn:statement column:20];
                        NSString *updatedBy = [self getStringFromColumn:statement column:21];
                        
                        VMUser *user = [[VMUser alloc] init:name username:username email:email password:password language:language timezone:timezone image:image];
                        user.ID = ID;
                        user.enabled = enabled;
                        user.archived = archived;
                        user.accessLevel = accessLevel;
                        user.accountId = accountId;
                        user.personId = personId;
                        user.pin = pin;
                        user.secretQuestion1 = secretQuestion1;
                        user.secretAnswer1 = secretAnswer1;
                        user.secretQuestion2 = secretQuestion2;
                        user.secretAnswer2 = secretAnswer2;
                        user.lastLogin = lastLogin;
                        user.created = created;
                        user.updated = updated;
                        user.updatedBy = updatedBy;
                        [users setObject:user forKey:username];
                    }
                    @catch (NSException *e) {
                        NSLog(@"An exception has occurred: %@", [e reason]);
                    }
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return users;
}

// UPDATE METHODS

- (void)update:(id)object {
    if (self.opened && [object isKindOfClass:[VMUser class]]) {
        @autoreleasepool {
            @try {
                VMUser *user = (VMUser *)object;
                
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"UPDATE \"%@\" SET %@ WHERE \"guid\" = ?13", self.tableName, SET_ALL_USER_TABLE_COLUMNS];
                NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // Create dateFormatter
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                
                // Bind column values
                [self setBoolToColumn:statement column:1 boolValue:user.enabled];
                [self setBoolToColumn:statement column:2 boolValue:user.archived];
                [self setIntToColumn:statement column:3 intValue:user.accessLevel];
                [self setStringToColumn:statement column:4 dataTransient:NO string:user.accountId];
                [self setStringToColumn:statement column:5 dataTransient:NO string:user.personId];
                [self setStringToColumn:statement column:6 dataTransient:NO string:user.name];
                [self setStringToColumn:statement column:7 dataTransient:NO string:user.username];
                [self setStringToColumn:statement column:8 dataTransient:NO string:user.email];
                [self setStringToColumn:statement column:9 dataTransient:NO string:user.password];
                [self setStringToColumn:statement column:10 dataTransient:NO string:user.pin];
                [self setStringToColumn:statement column:11 dataTransient:NO string:user.secretQuestion1];
                [self setStringToColumn:statement column:12 dataTransient:NO string:user.secretAnswer1];
                [self setStringToColumn:statement column:13 dataTransient:NO string:user.secretQuestion2];
                [self setStringToColumn:statement column:14 dataTransient:NO string:user.secretAnswer2];
                [self setStringToColumn:statement column:15 dataTransient:NO string:user.language];
                [self setStringToColumn:statement column:16 dataTransient:NO string:user.timezone];
                [self setImageToColumn:statement column:17 dataTransient:NO image:user.image];
                [self setDateToColumn:statement column:18 dataTransient:NO date:user.lastLogin];
                [self setDateToColumn:statement column:19 dataTransient:NO date:user.created];
                [self setDateToColumn:statement column:20 dataTransient:NO date:user.updated];
                [self setStringToColumn:statement column:21 dataTransient:NO string:user.updatedBy];
                [self setStringToColumn:statement column:22 dataTransient:NO string:user.ID];
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while updating. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

// DELETE METHODS

- (void)deleteById:(NSString *)guid {
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"DELETE FROM \"%@\" WHERE \"guid\" = ?1", self.tableName];
                NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // name
                sqlite3_bind_text(statement, 1, [guid UTF8String], -1, SQLITE_STATIC);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while deleting. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

- (void)deleteByIds:(NSMutableArray *)guids {
}

- (void)deleteByName:(NSString *)name {
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"DELETE FROM \"%@\" WHERE \"username\" = ?1", self.tableName];
                NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // name
                sqlite3_bind_text(statement, 1, [name UTF8String], -1, SQLITE_STATIC);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while deleting. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

- (void)deleteByNames:(NSMutableArray *)names {
    if (self.opened) {
        @autoreleasepool {
            @try {
                // Combine array elements to become a string of comma separated elements.
                NSMutableArray *quotedNames = [[NSMutableArray alloc] init];
                for (NSString *name in names) {
                    NSString *quotedName = [[NSString alloc] initWithFormat:@"'%@'", name];
                    [quotedNames addObject:quotedName];
                }
                NSString *namesToDeleteCommaDelimited = [quotedNames componentsJoinedByString:@","];
                NSString *namesToDeleteEnclosed = [NSString stringWithFormat:@"(%@)", namesToDeleteCommaDelimited];
                
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"DELETE FROM \"%@\" WHERE \"username\" IN %@", self.tableName, namesToDeleteEnclosed];
                
                NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while deleting. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

- (void)deleteAll {
}

@end
