//
//  VMReportGroupMapDAO.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMDAO.h"
#import "VMReportGroupMap.h"

@interface VMReportGroupMapDAO : VMDAO

#pragma mark SINGELTON METHODS

+ (VMDAO *)sharedInstance;

#pragma mark SQL DEFINES

#define REPORT_GROUP_MAP_SQL_INSERT_SELECT_PARAMS_STATEMENT     @"\"report_guid\", \"group_guid\""

#pragma mark INSTANCE METHODS

- (id)read:(NSString *)reportGUID groupGUID:(NSString *)groupGUID;
- (void)deleteById:(NSString *)reportGUID groupGUID:(NSString *)groupGUID;

@end
