//
//  VMReportDAO.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMReportDAO.h"

static VMReportDAO *_sharedInstanceVMReportDAO;

@implementation VMReportDAO

#pragma mark SINGELTON METHODS

+(VMReportDAO *)sharedInstance {
    @synchronized (self) {
        if (_sharedInstanceVMReportDAO == nil){
            _sharedInstanceVMReportDAO = [[self alloc] init];
            [_sharedInstanceVMReportDAO setDBFileName:DB_FULL_NAME];
            _sharedInstanceVMReportDAO.tableName = DB_TABLE_REPORT;
            _sharedInstanceVMReportDAO.initialized = YES;
        }
    }
    return _sharedInstanceVMReportDAO;
}

#pragma mark CRUD methods

- (id)readRecordIntoDomainObject:(sqlite3_stmt *)statement {
    VMReport *report = nil;
    @autoreleasepool {
        @try {
            const char *result;
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            // guid
            NSString *guid;
            result = (char *)sqlite3_column_text(statement,0);
            if (result)
                guid = [NSString stringWithUTF8String:result];
            
            // name
            NSString *name;
            result = (char *)sqlite3_column_text(statement,1);
            if (result)
                name = [NSString stringWithUTF8String:result];
            
            // type
            NSString *type;
            result = (char *)sqlite3_column_text(statement,2);
            if (result)
                type = [NSString stringWithUTF8String:result];
            
            // created_date
            NSDate *createdDate;
            result = (char *)sqlite3_column_text(statement,3);
            if (result)
                createdDate =[dateFormat dateFromString:[NSString stringWithUTF8String:result]];
            
            // last_updated_date
            NSDate *lastUpdatedDate;
            result = (char *)sqlite3_column_text(statement,4);
            if (result)
                lastUpdatedDate =[dateFormat dateFromString:[NSString stringWithUTF8String:result]];
            
            // last_updated_user_id
            NSString *lastUpdatedUserId;
            result = (char *)sqlite3_column_text(statement,5);
            if (result)
                lastUpdatedUserId = [NSString stringWithUTF8String:result];
            
            // enabled
            BOOL enabled = sqlite3_column_int(statement,6);
            
            // archived
            BOOL archived = sqlite3_column_int(statement,7);
            
            report = [[VMReport alloc] init:guid name:name type:type];
            report.guid = guid;
            report.createdDate = createdDate;
            report.lastUpdatedDate = lastUpdatedDate;
            report.lastUpdatedUserId = lastUpdatedUserId;
            report.enabled = enabled;
            report.archived = archived;
        }
        @catch (NSException *e) {
            NSLog(@"An exception has occurred: %@", [e reason]);
        }
    }
    return report;
}

// CREATE METHODS

- (void)create:(id)object {
    if (self.opened && [object isKindOfClass:[VMReport class]]) {
        @autoreleasepool {
            @try {
                VMReport *report = (VMReport *)object;
                
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"INSERT INTO \"%@\" (%@) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8)", self.tableName, REPORT_SQL_INSERT_SELECT_PARAMS_STATEMENT];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // Create dateFormatter
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                
                // guid
                sqlite3_bind_text(statement, 1, [report.guid UTF8String], -1, SQLITE_STATIC);
                
                // name
                sqlite3_bind_text(statement, 2, [report.name UTF8String], -1, SQLITE_STATIC);
                
                // type
                sqlite3_bind_text(statement, 3, [report.type UTF8String], -1, SQLITE_STATIC);
                
                // created_date
                sqlite3_bind_text(statement, 4, [[dateFormat stringFromDate:report.createdDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // last_updated_date
                sqlite3_bind_text(statement, 5, [[dateFormat stringFromDate:report.lastUpdatedDate] UTF8String] , -1, SQLITE_TRANSIENT);
                
                // last_updated_user_id
                if (report.lastUpdatedUserId)
                    sqlite3_bind_text(statement, 6, [report.lastUpdatedUserId UTF8String], -1, SQLITE_STATIC);
                
                // enabled
                sqlite3_bind_int(statement, 7, report.enabled);
                
                // archived
                sqlite3_bind_int(statement, 8, report.archived);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while inserting data. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

// READ METHODS

- (id)readById:(NSString *)guid {
    VMReport *report = nil;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"guid\" = ?1", REPORT_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [guid UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    report = [self readRecordIntoDomainObject:statement];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return report;
}

- (NSMutableArray *)readByIds:(NSArray *)guids {
    return nil;
}

- (id)readByName:(NSString *)name {
    VMReport *report = nil;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"name\" = ?1", REPORT_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [name UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    report = [self readRecordIntoDomainObject:statement];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return report;
}

- (NSMutableArray *)readByNames:(NSArray *)names {
    return nil;
}

- (NSMutableArray *)readLikeName:(NSString *)name {
    NSMutableArray *reports;
    if (self.opened && name && [name length] > 0) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"name\" LIKE ?1", REPORT_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                
                NSMutableString *param = nil;
                if ([name length] < 3)
                    param = [NSMutableString stringWithFormat:@"%@%%", name];
                else
                    param = [NSMutableString stringWithFormat:@"%%%@%%", name];
                
                sqlite3_bind_text(statement, 1, [param UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (reports == nil)
                        reports = [[NSMutableArray alloc] init];
                    
                    VMReport *report = [self readRecordIntoDomainObject:statement];
                    if (report) {
                        [reports addObject:report];
                    }
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return reports;
}

- (NSMutableArray *)readAll{
    NSMutableArray *reports;
    @try {
        @autoreleasepool {
            // Get dictionary and sort in alphabetical order
            NSMutableDictionary *dictionary = [self readAllAsDictionary];
            NSArray *keys = [dictionary allKeys];
            NSArray *sortedKeys = [keys sortedArrayUsingSelector:@selector(compare:)];
            reports = [[NSMutableArray alloc] init];
            for(NSString *key in sortedKeys) {
                VMReport *report = [dictionary objectForKey:key];
                [reports addObject:report];
            }
        }
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
        [self rollbackTransaction:self.dbHandle];
    }
    return reports;
}

- (NSMutableDictionary *)readAllAsDictionary {
    NSMutableDictionary *reportDictionary;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\"", REPORT_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (reportDictionary == nil)
                        reportDictionary = [[NSMutableDictionary alloc] init];
                    VMReport *report = [self readRecordIntoDomainObject:statement];
                    if (report)
                        [reportDictionary setObject:report forKey:[report.name copy]];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return reportDictionary;
}

// UPDATE METHODS

- (void)update:(id)object {
    if (self.opened && [object isKindOfClass:[VMReport class]]) {
        @autoreleasepool {
            @try {
                VMReport *report = (VMReport *)object;
                
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"UPDATE \"%@\" SET %@ WHERE \"guid\" = ?8", self.tableName, REPORT_SQL_UPDATE_PARAMS_STATEMENT];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // Create dateFormatter
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                
                // name
                sqlite3_bind_text(statement, 1, [report.name UTF8String], -1, SQLITE_STATIC);
                
                // type
                sqlite3_bind_text(statement, 2, [report.type UTF8String], -1, SQLITE_STATIC);
                
                // created_date
                sqlite3_bind_text(statement, 3, [[dateFormat stringFromDate:report.createdDate] UTF8String], -1, SQLITE_STATIC);
                
                // last_updated_date
                sqlite3_bind_text(statement, 4, [[dateFormat stringFromDate:report.lastUpdatedDate] UTF8String], -1, SQLITE_STATIC);
                
                // last_updated_user_id
                if (report.lastUpdatedUserId)
                    sqlite3_bind_text(statement, 5, [report.lastUpdatedUserId UTF8String], -1, SQLITE_STATIC);
                
                // enabled
                sqlite3_bind_int(statement, 6, report.enabled);
                
                // archived
                sqlite3_bind_int(statement, 7, report.archived);
                
                // guid
                sqlite3_bind_text(statement, 8, [report.guid UTF8String], -1, SQLITE_STATIC);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while updating. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

// DELETE METHODS

- (void)deleteById:(NSString *)guid {
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"DELETE FROM \"%@\" WHERE \"guid\" = ?1", self.tableName];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // name
                sqlite3_bind_text(statement, 1, [guid UTF8String], -1, SQLITE_STATIC);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while deleting. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

- (void)deleteByIds:(NSMutableArray *)guids {
}

- (void)deleteByName:(NSString *)name {
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"DELETE FROM \"%@\" WHERE \"name\" = ?1", self.tableName];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // name
                sqlite3_bind_text(statement, 1, [name UTF8String], -1, SQLITE_STATIC);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while deleting. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

- (void)deleteByNames:(NSMutableArray *)names {
    if (self.opened) {
        @autoreleasepool {
            @try {
                // Combine array elements to become a string of comma separated elements.
                NSMutableArray *quotedNames = [[NSMutableArray alloc] init];
                for (NSString *name in names) {
                    NSString *quotedName = [[NSString alloc] initWithFormat:@"'%@'", name];
                    [quotedNames addObject:quotedName];
                }
                NSString *namesToDeleteCommaDelimited = [quotedNames componentsJoinedByString:@","];
                NSString *namesToDeleteEnclosed = [NSString stringWithFormat:@"(%@)", namesToDeleteCommaDelimited];
                
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"DELETE FROM \"%@\" WHERE \"name\" IN %@", self.tableName, namesToDeleteEnclosed];
                
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while deleting. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

- (void)deleteAll {
}

@end
