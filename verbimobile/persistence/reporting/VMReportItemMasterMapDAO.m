//
//  VMReportItemMasterMapDAO.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMReportItemMasterMapDAO.h"

static VMReportItemMasterMapDAO *_sharedInstanceVMReportItemMasterMapDAO;

@implementation VMReportItemMasterMapDAO

#pragma mark SINGELTON METHODS

+(VMReportItemMasterMapDAO *)sharedInstance {
    @synchronized (self) {
        if (_sharedInstanceVMReportItemMasterMapDAO == nil){
            _sharedInstanceVMReportItemMasterMapDAO = [[self alloc] init];
            [_sharedInstanceVMReportItemMasterMapDAO setDBFileName:DB_FULL_NAME];
            _sharedInstanceVMReportItemMasterMapDAO.tableName = DB_TABLE_REPORT_ITEM_MASTER_MAP;
            _sharedInstanceVMReportItemMasterMapDAO.initialized = YES;
        }
    }
    return _sharedInstanceVMReportItemMasterMapDAO;
}

#pragma mark CRUD methods

- (id)readRecordIntoDomainObject:(sqlite3_stmt *)statement {
    VMReportItemMasterMap *reportItemMasterMap = nil;
    @autoreleasepool {
        @try {
            const char *result;
            
            // reportGUID
            NSString *reportGUID;
            result = (char *)sqlite3_column_text(statement,0);
            if (result)
                reportGUID = [NSString stringWithUTF8String:result];
            
            // itemMasterGUID
            NSString *itemMasterGUID;
            result = (char *)sqlite3_column_text(statement,1);
            if (result)
                itemMasterGUID = [NSString stringWithUTF8String:result];
            
            reportItemMasterMap = [[VMReportItemMasterMap alloc] init:reportGUID itemMasterGUID:itemMasterGUID];
        }
        @catch (NSException *e) {
            NSLog(@"An exception has occurred: %@", [e reason]);
        }
    }
    return reportItemMasterMap;
}

// CREATE METHODS

- (void)create:(id)object {
    if (self.opened && [object isKindOfClass:[VMReportItemMasterMap class]]) {
        @autoreleasepool {
            @try {
                VMReportItemMasterMap *reportItemMasterMap = (VMReportItemMasterMap *)object;
                
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"INSERT INTO \"%@\" (%@) VALUES (?1, ?2)", self.tableName, REPORT_ITEM_MASTER_MAP_SQL_INSERT_SELECT_PARAMS_STATEMENT];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // reportGUID
                sqlite3_bind_text(statement, 1, [reportItemMasterMap.reportGUID UTF8String], -1, SQLITE_STATIC);
                
                // itemMasterGUID
                sqlite3_bind_text(statement, 2, [reportItemMasterMap.itemMasterGUID UTF8String], -1, SQLITE_STATIC);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while inserting data. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

// READ METHODS

- (id)read:(NSString *)reportGUID itemMasterGUID:(NSString *)itemMasterGUID {
    NSMutableArray *reportItemMasterMaps;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"report_guid\" = ?1 AND \"item_master_guid\" = ?2", REPORT_ITEM_MASTER_MAP_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [reportGUID UTF8String], -1, SQLITE_STATIC);
                sqlite3_bind_text(statement, 2, [itemMasterGUID UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (reportItemMasterMaps == nil)
                        reportItemMasterMaps = [[NSMutableArray alloc] init];
                    VMReportItemMasterMap *reportItemMasterMap = [self readRecordIntoDomainObject:statement];
                    [reportItemMasterMaps addObject:reportItemMasterMap];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return reportItemMasterMaps;
}

- (NSMutableArray *)readAll{
    NSMutableArray *reportItemMasterMaps;
    @try {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\"", REPORT_ITEM_MASTER_MAP_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (reportItemMasterMaps == nil)
                        reportItemMasterMaps = [[NSMutableArray alloc] init];
                    VMReportItemMasterMap *reportItemMasterMap = [self readRecordIntoDomainObject:statement];
                    if (reportItemMasterMap)
                        [reportItemMasterMaps addObject:reportItemMasterMap];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
        [self rollbackTransaction:self.dbHandle];
    }
    return reportItemMasterMaps;
}

// DELETE METHODS

- (void)deleteById:(NSString *)reportGUID itemMasterGUID:(NSString *)itemMasterGUID {
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"DELETE FROM \"%@\" WHERE \"report_guid\" = ?1 AND \"item_master_guid\" = ?2", self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                
                sqlite3_bind_text(statement, 1, [reportGUID UTF8String], -1, SQLITE_STATIC);
                sqlite3_bind_text(statement, 2, [itemMasterGUID UTF8String], -1, SQLITE_STATIC);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while deleting. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

@end
