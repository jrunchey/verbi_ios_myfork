//
//  VMReportLocationMapDAO.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMDAO.h"
#import "VMReportLocationMap.h"

@interface VMReportLocationMapDAO : VMDAO

#pragma mark SINGELTON METHODS

+ (VMDAO *)sharedInstance;

#pragma mark SQL DEFINES

#define REPORT_LOCATION_MAP_SQL_INSERT_SELECT_PARAMS_STATEMENT     @"\"report_guid\", \"location_guid\""

#pragma mark INSTANCE METHODS

- (id)read:(NSString *)reportGUID locationGUID:(NSString *)locationGUID;
- (void)deleteById:(NSString *)reportGUID locationGUID:(NSString *)locationGUID;

@end
