//
//  VMReportDAO.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMDAO.h"
#import "VMReport.h"

@interface VMReportDAO : VMDAO

#pragma mark SINGELTON METHODS

+ (VMDAO *)sharedInstance;

#pragma mark SQL DEFINES

#define REPORT_SQL_INSERT_SELECT_PARAMS_STATEMENT     @"\"guid\", \"name\", \"type\", \"created_date\", \"last_updated_date\", \"last_updated_user_id\", \"enabled\", \"archived\""

#define REPORT_SQL_UPDATE_PARAMS_STATEMENT       @"\"name\" = ?1, \"type\" = ?2, \"created_date\" = ?3, \"last_updated_date\" = ?4, \"last_updated_user_id\" = ?5, \"enabled\" = ?6, \"archived\" = ?7"

#pragma mark INSTANCE METHODS

- (NSMutableArray *)readLikeName:(NSString *)name;

@end
