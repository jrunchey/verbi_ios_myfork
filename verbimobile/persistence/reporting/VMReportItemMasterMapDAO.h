//
//  VMReportItemMasterMapDAO.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMDAO.h"
#import "VMReportItemMasterMap.h"

@interface VMReportItemMasterMapDAO : VMDAO

#pragma mark SINGELTON METHODS

+ (VMDAO *)sharedInstance;

#pragma mark SQL DEFINES

#define REPORT_ITEM_MASTER_MAP_SQL_INSERT_SELECT_PARAMS_STATEMENT     @"\"report_guid\", \"item_master_guid\""

#pragma mark INSTANCE METHODS

- (id)read:(NSString *)reportGUID itemMasterGUID:(NSString *)itemMasterGUID;
- (void)deleteById:(NSString *)reportGUID itemMasterGUID:(NSString *)itemMasterGUID;

@end
