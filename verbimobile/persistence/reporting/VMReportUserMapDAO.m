//
//  VMReportUserMapDAO.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMReportUserMapDAO.h"

static VMReportUserMapDAO *_sharedInstanceVMReportUserMapDAO;

@implementation VMReportUserMapDAO

#pragma mark SINGELTON METHODS

+(VMReportUserMapDAO *)sharedInstance {
    @synchronized (self) {
        if (_sharedInstanceVMReportUserMapDAO == nil){
            _sharedInstanceVMReportUserMapDAO = [[self alloc] init];
            [_sharedInstanceVMReportUserMapDAO setDBFileName:DB_FULL_NAME];
            _sharedInstanceVMReportUserMapDAO.tableName = DB_TABLE_REPORT_USER_MAP;
            _sharedInstanceVMReportUserMapDAO.initialized = YES;
        }
    }
    return _sharedInstanceVMReportUserMapDAO;
}

#pragma mark CRUD methods

- (id)readRecordIntoDomainObject:(sqlite3_stmt *)statement {
    VMReportUserMap *reportUserMap = nil;
    @autoreleasepool {
        @try {
            const char *result;
            
            // reportGUID
            NSString *reportGUID;
            result = (char *)sqlite3_column_text(statement,0);
            if (result)
                reportGUID = [NSString stringWithUTF8String:result];
            
            // userGUID
            NSString *userGUID;
            result = (char *)sqlite3_column_text(statement,1);
            if (result)
                userGUID = [NSString stringWithUTF8String:result];
            
            reportUserMap = [[VMReportUserMap alloc] init:reportGUID userGUID:userGUID];
        }
        @catch (NSException *e) {
            NSLog(@"An exception has occurred: %@", [e reason]);
        }
    }
    return reportUserMap;
}

// CREATE METHODS

- (void)create:(id)object {
    if (self.opened && [object isKindOfClass:[VMReportUserMap class]]) {
        @autoreleasepool {
            @try {
                VMReportUserMap *reportUserMap = (VMReportUserMap *)object;
                
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"INSERT INTO \"%@\" (%@) VALUES (?1, ?2)", self.tableName, REPORT_USER_MAP_SQL_INSERT_SELECT_PARAMS_STATEMENT];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // reportGUID
                sqlite3_bind_text(statement, 1, [reportUserMap.reportGUID UTF8String], -1, SQLITE_STATIC);
                
                // userGUID
                sqlite3_bind_text(statement, 2, [reportUserMap.userGUID UTF8String], -1, SQLITE_STATIC);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while inserting data. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

// READ METHODS

- (id)read:(NSString *)reportGUID userGUID:(NSString *)userGUID {
    NSMutableArray *reportUserMaps;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"report_guid\" = ?1 AND \"user_guid\" = ?2", REPORT_USER_MAP_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [reportGUID UTF8String], -1, SQLITE_STATIC);
                sqlite3_bind_text(statement, 2, [userGUID UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (reportUserMaps == nil)
                        reportUserMaps = [[NSMutableArray alloc] init];
                    VMReportUserMap *reportUserMap = [self readRecordIntoDomainObject:statement];
                    [reportUserMaps addObject:reportUserMap];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return reportUserMaps;
}

- (NSMutableArray *)readAll{
    NSMutableArray *reportUserMaps;
    @try {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\"", REPORT_USER_MAP_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (reportUserMaps == nil)
                        reportUserMaps = [[NSMutableArray alloc] init];
                    VMReportUserMap *reportUserMap = [self readRecordIntoDomainObject:statement];
                    if (reportUserMap)
                        [reportUserMaps addObject:reportUserMap];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
        [self rollbackTransaction:self.dbHandle];
    }
    return reportUserMaps;
}

// DELETE METHODS

- (void)deleteById:(NSString *)reportGUID userGUID:(NSString *)userGUID {
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"DELETE FROM \"%@\" WHERE \"report_guid\" = ?1 AND \"user_guid\" = ?2", self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                
                sqlite3_bind_text(statement, 1, [reportGUID UTF8String], -1, SQLITE_STATIC);
                sqlite3_bind_text(statement, 2, [userGUID UTF8String], -1, SQLITE_STATIC);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while deleting. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

@end
