//
//  VMReportUserMapDAO.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMDAO.h"
#import "VMReportUserMap.h"

@interface VMReportUserMapDAO : VMDAO

#pragma mark SINGELTON METHODS

+ (VMDAO *)sharedInstance;

#pragma mark SQL DEFINES

#define REPORT_USER_MAP_SQL_INSERT_SELECT_PARAMS_STATEMENT     @"\"report_guid\", \"user_guid\""

#pragma mark INSTANCE METHODS

- (id)read:(NSString *)reportGUID userGUID:(NSString *)userGUID;
- (void)deleteById:(NSString *)reportGUID userGUID:(NSString *)userGUID;

@end
