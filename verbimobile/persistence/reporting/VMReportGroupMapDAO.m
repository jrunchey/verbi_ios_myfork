//
//  VMReportGroupMapDAO.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMReportGroupMapDAO.h"

static VMReportGroupMapDAO *_sharedInstanceVMReportGroupMapDAO;

@implementation VMReportGroupMapDAO

#pragma mark SINGELTON METHODS

+(VMReportGroupMapDAO *)sharedInstance {
    @synchronized (self) {
        if (_sharedInstanceVMReportGroupMapDAO == nil){
            _sharedInstanceVMReportGroupMapDAO = [[self alloc] init];
            [_sharedInstanceVMReportGroupMapDAO setDBFileName:DB_FULL_NAME];
            _sharedInstanceVMReportGroupMapDAO.tableName = DB_TABLE_REPORT_GROUP_MAP;
            _sharedInstanceVMReportGroupMapDAO.initialized = YES;
        }
    }
    return _sharedInstanceVMReportGroupMapDAO;
}

#pragma mark CRUD methods

- (id)readRecordIntoDomainObject:(sqlite3_stmt *)statement {
    VMReportGroupMap *reportGroupMap = nil;
    @autoreleasepool {
        @try {
            const char *result;
            
            // reportGUID
            NSString *reportGUID;
            result = (char *)sqlite3_column_text(statement,0);
            if (result)
                reportGUID = [NSString stringWithUTF8String:result];
            
            // groupGUID
            NSString *groupGUID;
            result = (char *)sqlite3_column_text(statement,1);
            if (result)
                groupGUID = [NSString stringWithUTF8String:result];
            
            reportGroupMap = [[VMReportGroupMap alloc] init:reportGUID groupGUID:groupGUID];
        }
        @catch (NSException *e) {
            NSLog(@"An exception has occurred: %@", [e reason]);
        }
    }
    return reportGroupMap;
}

// CREATE METHODS

- (void)create:(id)object {
    if (self.opened && [object isKindOfClass:[VMReportGroupMap class]]) {
        @autoreleasepool {
            @try {
                VMReportGroupMap *reportGroupMap = (VMReportGroupMap *)object;
                
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"INSERT INTO \"%@\" (%@) VALUES (?1, ?2)", self.tableName, REPORT_GROUP_MAP_SQL_INSERT_SELECT_PARAMS_STATEMENT];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // reportGUID
                sqlite3_bind_text(statement, 1, [reportGroupMap.reportGUID UTF8String], -1, SQLITE_STATIC);
                
                // groupGUID
                sqlite3_bind_text(statement, 2, [reportGroupMap.groupGUID UTF8String], -1, SQLITE_STATIC);
                                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while inserting data. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

// READ METHODS

- (id)read:(NSString *)reportGUID groupGUID:(NSString *)groupGUID {
    NSMutableArray *reportGroupMaps;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"report_guid\" = ?1 AND \"group_guid\" = ?2", REPORT_GROUP_MAP_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [reportGUID UTF8String], -1, SQLITE_STATIC);
                sqlite3_bind_text(statement, 2, [groupGUID UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (reportGroupMaps == nil)
                        reportGroupMaps = [[NSMutableArray alloc] init];
                    VMReportGroupMap *reportGroupMap = [self readRecordIntoDomainObject:statement];
                    [reportGroupMaps addObject:reportGroupMap];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return reportGroupMaps;
}

- (NSMutableArray *)readAll{
    NSMutableArray *reportGroupMaps;
    @try {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\"", REPORT_GROUP_MAP_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (reportGroupMaps == nil)
                        reportGroupMaps = [[NSMutableArray alloc] init];
                    VMReportGroupMap *reportGroupMap = [self readRecordIntoDomainObject:statement];
                    if (reportGroupMap)
                        [reportGroupMaps addObject:reportGroupMap];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
        [self rollbackTransaction:self.dbHandle];
    }
    return reportGroupMaps;
}

// DELETE METHODS

- (void)deleteById:(NSString *)reportGUID groupGUID:(NSString *)groupGUID {
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"DELETE FROM \"%@\" WHERE \"report_guid\" = ?1 AND \"group_guid\" = ?2", self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                
                sqlite3_bind_text(statement, 1, [reportGUID UTF8String], -1, SQLITE_STATIC);
                sqlite3_bind_text(statement, 2, [groupGUID UTF8String], -1, SQLITE_STATIC);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while deleting. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

@end
