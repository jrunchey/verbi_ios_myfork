//
//  VMReportLocationMapDAO.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMReportLocationMapDAO.h"

static VMReportLocationMapDAO *_sharedInstanceVMReportLocationMapDAO;

@implementation VMReportLocationMapDAO

#pragma mark SINGELTON METHODS

+(VMReportLocationMapDAO *)sharedInstance {
    @synchronized (self) {
        if (_sharedInstanceVMReportLocationMapDAO == nil){
            _sharedInstanceVMReportLocationMapDAO = [[self alloc] init];
            [_sharedInstanceVMReportLocationMapDAO setDBFileName:DB_FULL_NAME];
            _sharedInstanceVMReportLocationMapDAO.tableName = DB_TABLE_REPORT_LOCATION_MAP;
            _sharedInstanceVMReportLocationMapDAO.initialized = YES;
        }
    }
    return _sharedInstanceVMReportLocationMapDAO;
}

#pragma mark CRUD methods

- (id)readRecordIntoDomainObject:(sqlite3_stmt *)statement {
    VMReportLocationMap *reportLocationMap = nil;
    @autoreleasepool {
        @try {
            const char *result;
            
            // reportGUID
            NSString *reportGUID;
            result = (char *)sqlite3_column_text(statement,0);
            if (result)
                reportGUID = [NSString stringWithUTF8String:result];
            
            // locationGUID
            NSString *locationGUID;
            result = (char *)sqlite3_column_text(statement,1);
            if (result)
                locationGUID = [NSString stringWithUTF8String:result];
            
            reportLocationMap = [[VMReportLocationMap alloc] init:reportGUID locationGUID:locationGUID];
        }
        @catch (NSException *e) {
            NSLog(@"An exception has occurred: %@", [e reason]);
        }
    }
    return reportLocationMap;
}

// CREATE METHODS

- (void)create:(id)object {
    if (self.opened && [object isKindOfClass:[VMReportLocationMap class]]) {
        @autoreleasepool {
            @try {
                VMReportLocationMap *reportLocationMap = (VMReportLocationMap *)object;
                
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"INSERT INTO \"%@\" (%@) VALUES (?1, ?2)", self.tableName, REPORT_LOCATION_MAP_SQL_INSERT_SELECT_PARAMS_STATEMENT];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // reportGUID
                sqlite3_bind_text(statement, 1, [reportLocationMap.reportGUID UTF8String], -1, SQLITE_STATIC);
                
                // locationGUID
                sqlite3_bind_text(statement, 2, [reportLocationMap.locationGUID UTF8String], -1, SQLITE_STATIC);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while inserting data. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

// READ METHODS

- (id)read:(NSString *)reportGUID locationGUID:(NSString *)locationGUID {
    NSMutableArray *reportLocationMaps;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"report_guid\" = ?1 AND \"location_guid\" = ?2", REPORT_LOCATION_MAP_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [reportGUID UTF8String], -1, SQLITE_STATIC);
                sqlite3_bind_text(statement, 2, [locationGUID UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (reportLocationMaps == nil)
                        reportLocationMaps = [[NSMutableArray alloc] init];
                    VMReportLocationMap *reportLocationMap = [self readRecordIntoDomainObject:statement];
                    [reportLocationMaps addObject:reportLocationMap];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return reportLocationMaps;
}

- (NSMutableArray *)readAll{
    NSMutableArray *reportLocationMaps;
    @try {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\"", REPORT_LOCATION_MAP_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (reportLocationMaps == nil)
                        reportLocationMaps = [[NSMutableArray alloc] init];
                    VMReportLocationMap *reportLocationMap = [self readRecordIntoDomainObject:statement];
                    if (reportLocationMap)
                        [reportLocationMaps addObject:reportLocationMap];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
        [self rollbackTransaction:self.dbHandle];
    }
    return reportLocationMaps;
}

// DELETE METHODS

- (void)deleteById:(NSString *)reportGUID locationGUID:(NSString *)locationGUID {
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"DELETE FROM \"%@\" WHERE \"report_guid\" = ?1 AND \"location_guid\" = ?2", self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                
                sqlite3_bind_text(statement, 1, [reportGUID UTF8String], -1, SQLITE_STATIC);
                sqlite3_bind_text(statement, 2, [locationGUID UTF8String], -1, SQLITE_STATIC);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while deleting. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

@end
