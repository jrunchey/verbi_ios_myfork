//
//  VMLocation.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMLocation.h"
#import "VMDAO.h"

@interface VMLocationDAO : VMDAO

#pragma mark SINGELTON METHODS

+ (VMDAO *)sharedInstance;

#pragma mark SQL DEFINES

#define LOCATION_SQL_INSERT_SELECT_PARAMS_STATEMENT     @"\"guid\", \"name\", \"type\", \"image\", \"tag_id\", \"epc\", \"latitude\", \"longitude\", \"altitude\", \"address1\", \"address2\", \"town_or_city\", \"county_or_district\", \"state_or_region\", \"postal\", \"country\", \"enabled\""

#define LOCATION_SQL_UPDATE_PARAMS_STATEMENT       @"\"name\" = ?1, \"type\" = ?2, \"image\" = ?3, \"tag_id\" = ?4, \"epc\" = ?5, \"latitude\" = ?6, \"longitude\" = ?7, \"altitude\" = ?8, \"address1\" = ?9, \"address2\" = ?10, \"town_or_city\" = ?11, \"county_or_district\" = ?12, \"state_or_region\" = ?13, \"postal\" = ?14, \"country\" = ?15, \"enabled\" = ?16"

#pragma mark INSTANCE METHODS

- (NSMutableArray *)readLikeName:(NSString *)name;
- (id)readByTagId:(NSString *)tagId;
- (id)readByEPC:(NSString *)epc;

@end
