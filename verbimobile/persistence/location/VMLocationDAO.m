//
//  VMLocation.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMLocationDAO.h"

static VMLocationDAO *_sharedInstanceVMLocationDAO;

@implementation VMLocationDAO

#pragma mark SINGELTON METHODS

+(VMLocationDAO *)sharedInstance {
    @synchronized (self) {
        if (_sharedInstanceVMLocationDAO == nil){
            _sharedInstanceVMLocationDAO = [[self alloc] init];
            [_sharedInstanceVMLocationDAO setDBFileName:DB_FULL_NAME];
            _sharedInstanceVMLocationDAO.tableName = DB_TABLE_LOCATION;
            _sharedInstanceVMLocationDAO.initialized = YES;
        }
    }
    return _sharedInstanceVMLocationDAO;
}

#pragma mark CRUD methods

- (id)readRecordIntoDomainObject:(sqlite3_stmt *)statement {
    VMLocation *location = nil;
    @autoreleasepool {
        @try {
            const char *result;
            
            // guid
            NSString *guid;
            result = (char *)sqlite3_column_text(statement,0);
            if (result)
                guid = [NSString stringWithUTF8String:result];
            
            // name
            NSString *name;
            result = (char *)sqlite3_column_text(statement,1);
            if (result)
                name = [NSString stringWithUTF8String:result];
            
            // type
            NSString *type;
            result = (char *)sqlite3_column_text(statement,2);
            if (result)
                type = [NSString stringWithUTF8String:result];
            
            // image
            const char *imageRaw = sqlite3_column_blob(statement,3);
            int imageRawLength = sqlite3_column_bytes(statement,3);
            NSData *imageData;
            UIImage *image;
            if (imageRaw && imageRawLength > 0) {
                imageData = [NSData dataWithBytes:imageRaw length:imageRawLength];
                if (imageData)
                    image = [[UIImage alloc] initWithData:imageData];
            }
            
            // tagId
            NSString *tagId;
            result = (char *)sqlite3_column_text(statement,4);
            if (result)
                tagId = [NSString stringWithUTF8String:result];
            
            // epc
            NSString *epc;
            result = (char *)sqlite3_column_text(statement,5);
            if (result)
                epc = [NSString stringWithUTF8String:result];
            
            // latitude
            NSString *latitude;
            result = (char *)sqlite3_column_text(statement,6);
            if (result)
                latitude = [NSString stringWithUTF8String:result];
            
            // longitude
            NSString *longitude;
            result = (char *)sqlite3_column_text(statement,7);
            if (result)
                longitude = [NSString stringWithUTF8String:result];
            
            // altitude
            NSString *altitude;
            result = (char *)sqlite3_column_text(statement,8);
            if (result)
                altitude = [NSString stringWithUTF8String:result];
            
            // address1
            NSString *address1;
            result = (char *)sqlite3_column_text(statement,9);
            if (result)
                address1 = [NSString stringWithUTF8String:result];
            
            // address2
            NSString *address2;
            result = (char *)sqlite3_column_text(statement,10);
            if (result)
                address2 = [NSString stringWithUTF8String:result];
            
            // town_or_city
            NSString *townOrCity;
            result = (char *)sqlite3_column_text(statement,11);
            if (result)
                townOrCity = [NSString stringWithUTF8String:result];
            
            // county_or_district
            NSString *countyOrDistrict;
            result = (char *)sqlite3_column_text(statement,12);
            if (result)
                countyOrDistrict = [NSString stringWithUTF8String:result];
            
            // state_or_region
            NSString *stateOrRegion;
            result = (char *)sqlite3_column_text(statement,13);
            if (result)
                stateOrRegion = [NSString stringWithUTF8String:result];
            
            // postal
            NSString *postal;
            result = (char *)sqlite3_column_text(statement,14);
            if (result)
                postal = [NSString stringWithUTF8String:result];
            
            // country
            NSString *country;
            result = (char *)sqlite3_column_text(statement,15);
            if (result)
                country = [NSString stringWithUTF8String:result];
            
            BOOL enabled = sqlite3_column_int(statement,16);
            
            location = [[VMLocation alloc] init:guid name:name type:type image:image tagId:tagId epc:epc latitude:latitude longitude:longitude altitude:altitude address1:address1 address2:address2 townOrCity:townOrCity countyOrDistrict:countyOrDistrict stateOrRegion:stateOrRegion postal:postal country:country];
            location.guid = guid;
            location.enabled = enabled;
        }
        @catch (NSException *e) {
            NSLog(@"An exception has occurred: %@", [e reason]);
        }
    }
    return location;
}

// CREATE METHODS

- (void)create:(id)object {
    if (self.opened && [object isKindOfClass:[VMLocation class]]) {
        @autoreleasepool {
            @try {
                VMLocation *location = (VMLocation *)object;
                
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"INSERT INTO \"%@\" (%@) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15, ?16, ?17)", self.tableName, LOCATION_SQL_INSERT_SELECT_PARAMS_STATEMENT];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // guid
                sqlite3_bind_text(statement, 1, [location.guid UTF8String], -1, SQLITE_STATIC);
                
                // name
                sqlite3_bind_text(statement, 2, [location.name UTF8String], -1, SQLITE_STATIC);
                
                // type
                sqlite3_bind_text(statement, 3, [location.type UTF8String], -1, SQLITE_STATIC);
                
                // image
                NSData *imageData = UIImagePNGRepresentation(location.image);
                sqlite3_bind_blob(statement, 4, [imageData bytes], [imageData length], SQLITE_STATIC);
                
                // tagId
                sqlite3_bind_text(statement, 5, [location.tagId UTF8String], -1, SQLITE_STATIC);
                
                // epc
                sqlite3_bind_text(statement, 6, [location.epc UTF8String], -1, SQLITE_STATIC);
                
                // latitude
                sqlite3_bind_text(statement, 7, [location.latitude UTF8String], -1, SQLITE_STATIC);
                
                // longitude
                sqlite3_bind_text(statement, 8, [location.longitude UTF8String], -1, SQLITE_STATIC);
                
                // altitude
                sqlite3_bind_text(statement, 9, [location.altitude UTF8String], -1, SQLITE_STATIC);
                
                // address1
                sqlite3_bind_text(statement, 10, [location.address1 UTF8String], -1, SQLITE_STATIC);
                
                // address2
                sqlite3_bind_text(statement, 11, [location.address2 UTF8String], -1, SQLITE_STATIC);
                
                // town_or_city
                sqlite3_bind_text(statement, 12, [location.townOrCity UTF8String], -1, SQLITE_STATIC);
                
                // county_or_district
                sqlite3_bind_text(statement, 13, [location.countyOrDistrict UTF8String], -1, SQLITE_STATIC);
                
                // state_or_region
                sqlite3_bind_text(statement, 14, [location.stateOrRegion UTF8String], -1, SQLITE_STATIC);
                
                // postal
                sqlite3_bind_text(statement, 15, [location.postal UTF8String], -1, SQLITE_STATIC);
                
                // country
                sqlite3_bind_text(statement, 16, [location.country UTF8String], -1, SQLITE_STATIC);
                
                // enabled
                sqlite3_bind_int(statement, 17, location.enabled);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while inserting data. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

- (void)createList:(NSMutableArray *)objects {
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"INSERT INTO \"%@\" (%@) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15, ?16, ?17)", self.tableName, LOCATION_SQL_INSERT_SELECT_PARAMS_STATEMENT];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // Create dateFormatter
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                
                for (NSObject *object in objects) {
                    if ([object isKindOfClass:[VMLocation class]]) {
                        VMLocation *location = (VMLocation *)object;
                        
                        // guid
                        sqlite3_bind_text(statement, 1, [location.guid UTF8String], -1, SQLITE_STATIC);
                        
                        // name
                        sqlite3_bind_text(statement, 2, [location.name UTF8String], -1, SQLITE_STATIC);
                        
                        // type
                        sqlite3_bind_text(statement, 3, [location.type UTF8String], -1, SQLITE_STATIC);
                        
                        // image
                        NSData *imageData = UIImagePNGRepresentation(location.image);
                        sqlite3_bind_blob(statement, 4, [imageData bytes], [imageData length], SQLITE_STATIC);
                        
                        // tagId
                        sqlite3_bind_text(statement, 5, [location.tagId UTF8String], -1, SQLITE_STATIC);
                        
                        // epc
                        sqlite3_bind_text(statement, 6, [location.epc UTF8String], -1, SQLITE_STATIC);
                        
                        // latitude
                        sqlite3_bind_text(statement, 7, [location.latitude UTF8String], -1, SQLITE_STATIC);
                        
                        // longitude
                        sqlite3_bind_text(statement, 8, [location.longitude UTF8String], -1, SQLITE_STATIC);
                        
                        // altitude
                        sqlite3_bind_text(statement, 9, [location.altitude UTF8String], -1, SQLITE_STATIC);
                        
                        // address1
                        sqlite3_bind_text(statement, 10, [location.address1 UTF8String], -1, SQLITE_STATIC);
                        
                        // address2
                        sqlite3_bind_text(statement, 11, [location.address2 UTF8String], -1, SQLITE_STATIC);
                        
                        // town_or_city
                        sqlite3_bind_text(statement, 12, [location.townOrCity UTF8String], -1, SQLITE_STATIC);
                        
                        // county_or_district
                        sqlite3_bind_text(statement, 13, [location.countyOrDistrict UTF8String], -1, SQLITE_STATIC);
                        
                        // state_or_region
                        sqlite3_bind_text(statement, 14, [location.stateOrRegion UTF8String], -1, SQLITE_STATIC);
                        
                        // postal
                        sqlite3_bind_text(statement, 15, [location.postal UTF8String], -1, SQLITE_STATIC);
                        
                        // country
                        sqlite3_bind_text(statement, 16, [location.country UTF8String], -1, SQLITE_STATIC);
                        
                        // enabled
                        sqlite3_bind_int(statement, 17, location.enabled);
                        
                        while(YES){
                            NSInteger result = sqlite3_step(statement);
                            if(result == SQLITE_DONE){
                                break;
                            }
                            else if(result != SQLITE_BUSY){
                                printf("db error: %s\n", sqlite3_errmsg(self.dbHandle));
                                break;
                            }
                        }
                        sqlite3_reset(statement);
                    }
                }
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

// READ METHODS

- (id)readById:(NSString *)guid {
    VMLocation *location = nil;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"guid\" = ?1", LOCATION_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [guid UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    location = [self readRecordIntoDomainObject:statement];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return location;
}

- (NSMutableArray *)readByIds:(NSArray *)guids {
    return nil;
}

- (id)readByName:(NSString *)name {
    VMLocation *location = nil;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"name\" = ?1", LOCATION_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [name UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    location = [self readRecordIntoDomainObject:statement];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return location;
}

- (NSMutableArray *)readByNames:(NSArray *)names {
    return nil;
}

- (NSMutableArray *)readLikeName:(NSString *)name {
    NSMutableArray *locations;
    if (self.opened && name && [name length] > 0) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"name\" LIKE ?1", LOCATION_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                
                NSMutableString *param = nil;
                if ([name length] < 3)
                    param = [NSMutableString stringWithFormat:@"%@%%", name];
                else
                    param = [NSMutableString stringWithFormat:@"%%%@%%", name];
                
                sqlite3_bind_text(statement, 1, [param UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (locations == nil)
                        locations = [[NSMutableArray alloc] init];
                    
                    VMLocation *location = [self readRecordIntoDomainObject:statement];
                    if (location) {
                        [locations addObject:location];
                    }
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return locations;
}

- (id)readByTagId:(NSString *)tagId {
    VMLocation *location = nil;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"tag_id\" = ?1", LOCATION_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [tagId UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    location = [self readRecordIntoDomainObject:statement];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return location;
}

- (id)readByEPC:(NSString *)epc {
    VMLocation *location = nil;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\" WHERE \"epc\" = ?1", LOCATION_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                sqlite3_bind_text(statement, 1, [epc UTF8String], -1, SQLITE_STATIC);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    location = [self readRecordIntoDomainObject:statement];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return location;
}

- (NSMutableArray *)readAll{
    NSMutableArray *locations;
    @try {
        @autoreleasepool {
            // Get dictionary and sort in alphabetical order
            NSMutableDictionary *dictionary = [self readAllAsDictionary];
            NSArray *keys = [dictionary allKeys];
            NSArray *sortedKeys = [keys sortedArrayUsingSelector:@selector(compare:)];
            locations = [[NSMutableArray alloc] init];
            for(NSString *key in sortedKeys) {
                VMLocation *location = [dictionary objectForKey:key];
                [locations addObject:location];
            }
        }
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", [e reason]);
        [self rollbackTransaction:self.dbHandle];
    }
    return locations;
}

- (NSMutableDictionary *)readAllAsDictionary {
    NSMutableDictionary *locationDictionary;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM \"%@\"", LOCATION_SQL_INSERT_SELECT_PARAMS_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (locationDictionary == nil)
                        locationDictionary = [[NSMutableDictionary alloc] init];
                    VMLocation *location = [self readRecordIntoDomainObject:statement];
                    if (location)
                        [locationDictionary setObject:location forKey:[location.name copy]];
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return locationDictionary;
}

/*
- (NSMutableArray *)readAll{
    NSMutableArray *locations;
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"%@ FROM \"%@\"", SQL_LOCATION_READ_SELECT_STATEMENT, self.tableName];
                sqlite3_stmt *statement = [self prepare:sql];
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    if (locations == nil)
                        locations = [[NSMutableArray alloc] init];
                    
                    VMLocation *location = [self readRecordIntoDomainObject:statement];
                    if (location) {
                        NSDictionary *locationDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                                      location, @"object",
                                                      location.name, @"name",
                                                      location.guid, @"id", nil];
                        [locations addObject:locationDict];
                    }
                }
                
                // Commit and finalize
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
    return locations;
}
 */

// UPDATE METHODS

- (void)update:(id)object {
    if (self.opened && [object isKindOfClass:[VMLocation class]]) {
        @autoreleasepool {
            @try {
                VMLocation *location = (VMLocation *)object;
                
                [self startTransaction:self.dbHandle];
                
                NSString *sql = [NSString stringWithFormat:@"UPDATE \"%@\" SET %@ WHERE \"guid\" = ?17", self.tableName, LOCATION_SQL_UPDATE_PARAMS_STATEMENT];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // name
                sqlite3_bind_text(statement, 1, [location.name UTF8String], -1, SQLITE_STATIC);
                
                // type
                sqlite3_bind_text(statement, 2, [location.type UTF8String], -1, SQLITE_STATIC);
                
                // image
                NSData *imageData = UIImagePNGRepresentation(location.image);
                sqlite3_bind_blob(statement, 3, [imageData bytes], [imageData length], SQLITE_STATIC);
                
                // tagId
                sqlite3_bind_text(statement, 4, [location.tagId UTF8String], -1, SQLITE_STATIC);
                
                // epc
                sqlite3_bind_text(statement, 5, [location.epc UTF8String], -1, SQLITE_STATIC);
                
                // latitude
                sqlite3_bind_text(statement, 6, [location.latitude UTF8String], -1, SQLITE_STATIC);
                
                // longitude
                sqlite3_bind_text(statement, 7, [location.longitude UTF8String], -1, SQLITE_STATIC);
                
                // altitude
                sqlite3_bind_text(statement, 8, [location.altitude UTF8String], -1, SQLITE_STATIC);
                
                // address1
                sqlite3_bind_text(statement, 9, [location.address1 UTF8String], -1, SQLITE_STATIC);
                
                // address2
                sqlite3_bind_text(statement, 10, [location.address2 UTF8String], -1, SQLITE_STATIC);
                
                // town_or_city
                sqlite3_bind_text(statement, 11, [location.townOrCity UTF8String], -1, SQLITE_STATIC);
                
                // county_or_district
                sqlite3_bind_text(statement, 12, [location.countyOrDistrict UTF8String], -1, SQLITE_STATIC);
                
                // state_or_region
                sqlite3_bind_text(statement, 13, [location.stateOrRegion UTF8String], -1, SQLITE_STATIC);
                
                // postal
                sqlite3_bind_text(statement, 14, [location.postal UTF8String], -1, SQLITE_STATIC);
                
                // country
                sqlite3_bind_text(statement, 15, [location.country UTF8String], -1, SQLITE_STATIC);
                
                // enabled
                sqlite3_bind_int(statement, 16, location.enabled);
                
                // guid
                sqlite3_bind_text(statement, 17, [location.guid UTF8String], -1, SQLITE_STATIC);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while updating. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

// DELETE METHODS

- (void)deleteById:(NSString *)guid {
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"DELETE FROM \"%@\" WHERE \"guid\" = ?1", self.tableName];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // name
                sqlite3_bind_text(statement, 1, [guid UTF8String], -1, SQLITE_STATIC);
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while deleting. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

- (void)deleteByIds:(NSMutableArray *)guids {
}

- (void)deleteByName:(NSString *)name {
    if (self.opened) {
        @autoreleasepool {
            @try {
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"DELETE FROM \"%@\" WHERE \"name\" = ?1", self.tableName];
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // name
                sqlite3_bind_text(statement, 1, [name UTF8String], -1, SQLITE_STATIC);
                            
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while deleting. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

- (void)deleteByNames:(NSMutableArray *)names {
    if (self.opened) {
        @autoreleasepool {
            @try {
                // Combine array elements to become a string of comma separated elements.
                NSMutableArray *quotedNames = [[NSMutableArray alloc] init];
                for (NSString *name in names) {
                    NSString *quotedName = [[NSString alloc] initWithFormat:@"'%@'", name];
                    [quotedNames addObject:quotedName];
                }
                NSString *namesToDeleteCommaDelimited = [quotedNames componentsJoinedByString:@","];
                NSString *namesToDeleteEnclosed = [NSString stringWithFormat:@"(%@)", namesToDeleteCommaDelimited];
                
                [self startTransaction:self.dbHandle];
                
                NSMutableString *sql = [NSString stringWithFormat:@"DELETE FROM \"%@\" WHERE \"name\" IN %@", self.tableName, namesToDeleteEnclosed];
                
                //NSLog(@"Executing SQL: %@", sql);
                sqlite3_stmt *statement = [self prepare:sql];
                
                // Execute the statement, commit and finalize
                if(sqlite3_step(statement) != SQLITE_DONE)
                    NSLog(@"Error while deleting. %s", sqlite3_errmsg(self.dbHandle));
                [self commitTransaction:self.dbHandle];
                sqlite3_finalize(statement);
            }
            @catch (NSException *e) {
                NSLog(@"An exception has occurred: %@", [e reason]);
                [self rollbackTransaction:self.dbHandle];
            }
        }
    }
}

- (void)deleteAll {
}

@end
