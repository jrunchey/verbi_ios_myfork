//
//  VMDatabase.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMConstants.h"
#import "sqlite3.h"

@interface VMDAO : NSObject

#pragma mark GETTERS/SETTERS

@property(nonatomic) BOOL initialized;
@property(nonatomic, strong) NSString *dbFilePath;
@property(nonatomic) sqlite3 *dbHandle;
@property(nonatomic) BOOL opened;
@property(nonatomic, strong) NSString *tableName;

#pragma mark INITIALIZATION

-(void)setDBFileName:(NSString *)dbFileName;
//-(NSString *)pathToDB;

#pragma mark DATABASE HANDLE METHODS

- (BOOL)open;
- (BOOL)close;
- (sqlite3_stmt *)prepare:(NSString *)sql;
- (id) lookupSingularSQL:(NSString *)sql forType:(NSString *)rettype;
- (BOOL)startTransaction:(sqlite3 *)pDBHandle;
- (BOOL)commitTransaction:(sqlite3 *)pDBHandle;
- (BOOL)rollbackTransaction:(sqlite3 *)pDBHandle;

#pragma mark DATABASE STATEMENT GET METHODS

- (int)getIntFromColumn:(sqlite3_stmt *)statement column:(int)column;
- (BOOL)getBoolFromColumn:(sqlite3_stmt *)statement column:(int)column;
- (NSString *)getStringFromColumn:(sqlite3_stmt *)statement column:(int)column;
- (NSDate *)getDateFromColumn:(sqlite3_stmt *)statement column:(int)column;
- (NSDate *)getDateFromColumn:(sqlite3_stmt *)statement column:(int)column dateFormatter:(NSDateFormatter *)dateFormatter;
- (UIImage *)getImageFromColumn:(sqlite3_stmt *)statement column:(int)column;

#pragma mark DATABASE STATEMENT SET METHODS

- (void)setIntToColumn:(sqlite3_stmt *)statement column:(int)column intValue:(int)intValue;
- (void)setBoolToColumn:(sqlite3_stmt *)statement column:(int)column boolValue:(BOOL)boolValue;
- (void)setStringToColumn:(sqlite3_stmt *)statement column:(int)column dataTransient:(BOOL)dataTransient string:(NSString *)string;
- (void)setDateToColumn:(sqlite3_stmt *)statement column:(int)column dataTransient:(BOOL)dataTransient date:(NSDate *)date;
- (void)setDateToColumn:(sqlite3_stmt *)statement column:(int)column dataTransient:(BOOL)dataTransient date:(NSDate *)date dateFormatter:(NSDateFormatter *)dateFormatter;
- (void)setImageToColumn:(sqlite3_stmt *)statement column:(int)column dataTransient:(BOOL)dataTransient image:(UIImage *)image;

#pragma mark CRUD methods

// CREATE METHODS

- (void)create:(id)object;
- (void)createList:(NSMutableArray *)objects;

// READ METHODS

- (int)totalNumberOfRecords;
- (int)totalNumberOfRecordsByIds:(NSArray *)guids;
- (int)totalNumberOfRecordsByNames:(NSArray *)names;
- (id)readById:(NSString *)guid;
- (NSMutableArray *)readByIds:(NSArray *)guids;
- (NSMutableDictionary *)readByIdsAsDictionary:(NSArray *)guids;
- (id)readByName:(NSString *)name;
- (NSMutableArray *)readByNames:(NSArray *)names;
- (NSMutableDictionary *)readByNamesAsDictionary:(NSArray *)names;
- (NSMutableArray *)readAll;
- (NSMutableDictionary *)readAllAsDictionary;

// UPDATE METHODS

- (void)update:(id)object;
- (void)updateList:(NSMutableArray *)objects;

// DELETE METHODS

- (void)deleteById:(NSString *)guid;
- (void)deleteByIds:(NSMutableArray *)guids;
- (void)deleteByName:(NSString *)name;
- (void)deleteByNames:(NSMutableArray *)names;
- (void)deleteAll;

@end
