//
//  VMSession.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMSession.h"
#import "VMConstants.h"

static VMSession *_sessionSharedInstance;

@implementation VMSession

#pragma mark -
#pragma mark Getters/Setters
/****************************************************************************
 * Getters/Setters
 ****************************************************************************/

@synthesize valid = _valid;
@synthesize user = _user;
@synthesize rfidReader = _rfidReader;
@synthesize barcodeReader = _barcodeReader;
//@synthesize bleShield = _bleShield;
@synthesize availableAutoIdModes = _availableAutoIdModes;
@synthesize autoIdMode = _autoIdMode;
@synthesize autoIdProcessStarted = _autoIdProcessStarted;

- (NSDictionary *)availableAutoIdModes {
    if (_availableAutoIdModes == nil)
        _availableAutoIdModes = [[NSMutableDictionary alloc] init];
    return _availableAutoIdModes;
}

#pragma mark -
#pragma mark Singleton
/****************************************************************************
 * Singleton
 ****************************************************************************/

+(VMSession *)sharedInstance {
    @synchronized (self) {
        if (_sessionSharedInstance == nil) {
            _sessionSharedInstance = [[self alloc] init];
        }
    }
    return _sessionSharedInstance;
}

#pragma mark -
#pragma mark Constructors
/****************************************************************************
 * Constructors
 ****************************************************************************/

- (id)initWithUser:(VMUser *)user {
    self = [super init];
    if (self) {
        self.user = user;
        if ([IS_SIMULATOR intValue] == 1)
            self.autoIdMode = [AUTO_ID_MODE_DEFAULT intValue];
        else {
            self.autoIdMode = [AUTO_ID_MODE_MIN_KEY_VALUE intValue];
            self.barcodeReader = [[VMBarcodeReader alloc] init];
            self.rfidReader = [[VMRFIDReader alloc] init];
            [self updateAvailableAutoIdModes];
        }
        [self updateCurrentAutoIdMode];
    }
    _sessionSharedInstance = self;
    return _sessionSharedInstance;
}

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

-(void)dispose {
    if (self.user) {
        [self.user dispose];
        self.user = nil;
    }
    if (self.rfidReader) {
        [self.rfidReader dispose];
        self.rfidReader = nil;
    }
    if (self.barcodeReader) {
        [self.barcodeReader dispose];
        self.barcodeReader = nil;
    }
    self.availableAutoIdModes = nil;
}

#pragma mark -
#pragma mark AIT Methods
/****************************************************************************
 * AIT Methods
 ****************************************************************************/

- (void)updateAvailableAutoIdModes {
    // If not simulator, instantiate hardware objects
    if ([IS_SIMULATOR intValue] == 0) {
        [self.availableAutoIdModes removeAllObjects];
        // BARCODE
        if (self.barcodeReader && self.barcodeReader.supportedAutoIdModes && self.barcodeReader.supportedAutoIdModes.count > 0) {
            [self.availableAutoIdModes addEntriesFromDictionary:self.barcodeReader.supportedAutoIdModes];
        }
        
        // RFID
        if (self.rfidReader && self.rfidReader.supportedAutoIdModes && self.rfidReader.supportedAutoIdModes.count > 0) {
            [self.availableAutoIdModes addEntriesFromDictionary:self.rfidReader.supportedAutoIdModes];
        }
    }
}

- (void)updateCurrentAutoIdMode {
    NSString *valueFoundInAvailableModes = [self.availableAutoIdModes objectForKey:[NSString stringWithFormat:@"%d", self.autoIdMode]];
    if (valueFoundInAvailableModes == nil)
        [self toggleAutoIdMode];
}

- (void)toggleAutoIdMode {
    // If current autoIdMode is less than max number supported, increment it to the next mode otherwise reset to the first (or zero)
    if ([IS_SIMULATOR intValue] == 0) { // Toggle only if not simulator
        int supportedAutoIdModesMaxValue = [AUTO_ID_MODE_MAX_KEY_VALUE intValue];
        BOOL foundNextAvailableAutoIdMode = NO;
        while (!foundNextAvailableAutoIdMode) {
            if (self.autoIdMode < supportedAutoIdModesMaxValue)
                self.autoIdMode++;
            else
                self.autoIdMode = [AUTO_ID_MODE_MIN_KEY_VALUE intValue];
            NSString *valueFoundInAvailableModes = [self.availableAutoIdModes objectForKey:[NSString stringWithFormat:@"%d", self.autoIdMode]];
            if (valueFoundInAvailableModes)
                foundNextAvailableAutoIdMode = YES;
        }
    }
}

- (NSString *)getAutoIdModeButtonImageName {
    if (self.autoIdMode == [AUTO_ID_MODE_MAGSTRIPE_KEY intValue])
        return AUTO_ID_MODE_MAGSTRIPE_IMAGE_NAME;
    else if (self.autoIdMode == [AUTO_ID_MODE_BARCODE_1D_KEY intValue])
        return AUTO_ID_MODE_BARCODE_1D_IMAGE_NAME;
    else if (self.autoIdMode == [AUTO_ID_MODE_BARCODE_2D_KEY intValue])
        return AUTO_ID_MODE_BARCODE_2D_IMAGE_NAME;
    else if (self.autoIdMode == [AUTO_ID_MODE_RFID_C1G2_KEY intValue])
        return AUTO_ID_MODE_RFID_C1G2_IMAGE_NAME;
    return AUTO_ID_MODE_NONE_IMAGE_NAME;
}

- (UIImage *)getAutoIdModeButtonImage {
    UIImage *buttonImage = nil;
    if (self.autoIdMode == [AUTO_ID_MODE_MAGSTRIPE_KEY intValue])
        buttonImage = [UIImage imageNamed:AUTO_ID_MODE_MAGSTRIPE_IMAGE_NAME];
    else if (self.autoIdMode == [AUTO_ID_MODE_BARCODE_1D_KEY intValue])
        buttonImage = [UIImage imageNamed:AUTO_ID_MODE_BARCODE_1D_IMAGE_NAME];
    else if (self.autoIdMode == [AUTO_ID_MODE_BARCODE_2D_KEY intValue])
        buttonImage = [UIImage imageNamed:AUTO_ID_MODE_BARCODE_2D_IMAGE_NAME];
    else if (self.autoIdMode == [AUTO_ID_MODE_RFID_C1G2_KEY intValue])
        buttonImage = [UIImage imageNamed:AUTO_ID_MODE_RFID_C1G2_IMAGE_NAME];
    else
        buttonImage = [UIImage imageNamed:AUTO_ID_MODE_NONE_IMAGE_NAME];
    return buttonImage;
}

- (NSString *)getAutoIdStartStopButtonImageName {
    if (self.autoIdMode == [AUTO_ID_MODE_MAGSTRIPE_KEY intValue] || self.autoIdMode == [AUTO_ID_MODE_BARCODE_1D_KEY intValue] || self.autoIdMode == [AUTO_ID_MODE_BARCODE_2D_KEY intValue])
        return AUTO_ID_PROCESS_SCAN_IMAGE_NAME;
    else if (self.autoIdMode == [AUTO_ID_MODE_RFID_C1G2_KEY intValue]) {
        if (!self.autoIdProcessStarted)
            return AUTO_ID_PROCESS_START_RFID_IMAGE_NAME;
        else
            return AUTO_ID_PROCESS_STOP_RFID_IMAGE_NAME;
    }
    return UNAVAILABLE_IMAGE_NAME;
}

- (UIImage *)getAutoIdStartStopButtonImage {
    UIImage *buttonImage = nil;
    if (self.autoIdMode == [AUTO_ID_MODE_MAGSTRIPE_KEY intValue] || self.autoIdMode == [AUTO_ID_MODE_BARCODE_1D_KEY intValue] || self.autoIdMode == [AUTO_ID_MODE_BARCODE_2D_KEY intValue])
        buttonImage = [UIImage imageNamed:AUTO_ID_PROCESS_SCAN_IMAGE_NAME];
    else if (self.autoIdMode == [AUTO_ID_MODE_RFID_C1G2_KEY intValue]) {
        if (!self.autoIdProcessStarted)
            buttonImage = [UIImage imageNamed:AUTO_ID_PROCESS_START_RFID_IMAGE_NAME];
        else
            buttonImage = [UIImage imageNamed:AUTO_ID_PROCESS_STOP_RFID_IMAGE_NAME];
    }
    else
        buttonImage = [UIImage imageNamed:UNAVAILABLE_IMAGE_NAME];
    return buttonImage;
}

@end
