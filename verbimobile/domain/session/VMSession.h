//
//  VMSession.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMUser.h"
#import "VMRFIDReader.h"
#import "VMBarcodeReader.h"

/*
 Singleton class or in Objective-C a SharedInstance
 */
@interface VMSession : NSObject

#pragma mark -
#pragma mark GETTERS/SETTERS

@property(nonatomic) BOOL valid;
@property(nonatomic, strong) VMUser *user;
@property(nonatomic, strong) VMRFIDReader *rfidReader;
@property(nonatomic, strong) VMBarcodeReader *barcodeReader;
//@property(nonatomic, strong) BLE *bleShield;
@property(nonatomic, strong) NSMutableDictionary *availableAutoIdModes;
@property(nonatomic) int autoIdMode;
@property(nonatomic) BOOL autoIdProcessStarted;

#pragma mark -
#pragma mark SINGLETON METHODS

+ (VMSession *)sharedInstance;

#pragma mark -
#pragma mark CONSTRUCTOR METHODS

- (id)initWithUser:(VMUser *)user;

#pragma mark -
#pragma mark DECONSTRUCTOR METHODS

- (void)dispose;

#pragma mark -
#pragma mark AUTO-ID METHODS

- (void)updateAvailableAutoIdModes;
- (void)updateCurrentAutoIdMode;
- (void)toggleAutoIdMode;
- (NSString *)getAutoIdModeButtonImageName;
- (UIImage *)getAutoIdModeButtonImage;
- (NSString *)getAutoIdStartStopButtonImageName;
- (UIImage *)getAutoIdStartStopButtonImage;

@end
