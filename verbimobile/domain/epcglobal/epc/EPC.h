//
//  EPC.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EPC : NSObject

// GETTERS/SETTERS
@property(nonatomic, strong) NSString *guid;
@property(nonatomic, strong) NSString *raw;
@property(nonatomic, strong) NSString *epcPureIdentity;     // EPC Pure Identity
@property(nonatomic, strong) NSString *epcTagUri;           // EPC Tag URI
@property(nonatomic, strong) NSString *epcBinary;           // EPC Binary Encoding
@property(nonatomic, strong) NSString *gs1Key;              // GS1 Key
@property(nonatomic, strong) NSString *scheme;
@property(nonatomic, strong) NSString *schemeName;
@property(nonatomic) int *encodingLength;
@property(nonatomic) BOOL enabled;
@property(nonatomic, strong) NSDate *createdDate;
@property(nonatomic, strong) NSDate *lastReadDate;
@property(nonatomic, strong) NSDate *lastWriteDate;

// METHODS
- (id)initWithRawTagId:(NSString *)rawTagId;

@end
