//
//  EPC.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

/*
 EPC Schemes: SGTIN, SSCC, SGLN, GRAI, GIAI, GDTI, GSRN, GID, USDOD, ADI
 
 EPC Scheme     Tag Encodings   ￼Corresponding GS1 Key           Typical Use
 ￼￼￼￼-------------  --------------  ------------------------------  ------------------------
 sgtin          sgtin-96        ￼GTIN key (plus added serial     Trade item
                sgtin-198       number)

 sscc           sscc-96         SSCC                            Pallet load or other
                                                                logistics unit load
 
 sgln           sgln-96         GLN key (with or without        Location
                sgln-195        additional extension)
 ￼￼
 grai           grai-96         GRAI (serial number             Returnable/reusable asset
                grai-170        mandatory)
 ￼
 ￼￼
 giai           giai-96         GIAI                            Fixed asset
                giai-202
 ￼
 ￼￼gdti           gdti-96         GDTI (serial number             Document
                gdti-113        mandatory)
 
 gsrn           gsrn-96         GSRN                            Service relation
                                                                (e.g., loyalty card)
 ￼￼
 gid            ￼gid-96         ￼ [none]                          Unspecified
 ￼￼
 ￼usdod          usdod-96        ￼[none]                          US Dept of Defense
                                                                supply chain
 ￼
 adi            ￼adi-var         [none]                          Aerospace and defense –
                                                                aircraft and other parts
                                                                and items
 
 Pure Identity EPC URI The primary representation of an Electronic Product Code is as an Internet Uniform Resource Identifier (URI) called the Pure Identity EPC URI. The Pure Identity EPC URI is the preferred way to denote a specific physical object within business applications. The pure identity URI may also be used at the data capture level when the EPC is to be read from an RFID tag or other data carrier, in a situation where the additional “control” information present on an RFID tag is not needed.
 Format: urn:epc:id:scheme:component1.component2....
 Example: urn:epc:id:sgtin:0614141.112345.400 (SGTIN)
 
 EPC Tag URI The EPC memory bank of a Gen 2 RFID Tag contains the EPC plus additional “control information” that is used to guide the process of data capture from RFID tags. The EPC Tag URI is a URI string that denotes a specific EPC together with specific settings for the control information found in the EPC memory bank. In other words, the EPC Tag URI is a text equivalent of the entire EPC memory bank contents. The EPC Tag URI is typically used at the data capture level when reading from an RFID tag in a situation where the control information is of interest to the capturing application. It is also used when writing the EPC memory bank of an RFID tag, in order to fully specify the contents to be written.
 
 Binary Encoding The EPC memory bank of a Gen 2 RFID Tag actually contains a compressed encoding of the EPC and additional “control information” in a compact binary form. There is a 1-to-1 translation between EPC Tag URIs and the binary contents of a Gen 2 RFID Tag. Normally, the binary encoding is only encountered at a very low level of software or hardware, and is translated to the EPC Tag URI or Pure Identity EPC URI form before being presented to application logic.
 
 Note that the Pure Identity EPC URI is independent of RFID, while the EPC Tag URI and the Binary Encoding are specific to Gen 2 RFID Tags because they include RFID-specific “control information” in addition to the unique EPC identifier.
 */

#import "EPC.h"

@implementation EPC

// GETTERS/SETTERS
@synthesize guid = _guid;
@synthesize raw = _raw;
@synthesize epcPureIdentity = _epcPureIdentity;     // urn:epc:id:sgtin:0614141.112345.400
@synthesize epcTagUri = _epcTagUri;                 // urn:epc:tag:sgtin-96:3.0614141.112345.400
@synthesize epcBinary = _epcBinary;                 // EPC Binary Encoding
@synthesize gs1Key = _gs1Key;                       // GS1 Key
@synthesize scheme = _scheme;
@synthesize schemeName = _schemeName;
@synthesize encodingLength = _encodingLength;
@synthesize enabled = _enabled;
@synthesize createdDate = _createdDate;
@synthesize lastReadDate = _lastReadDate;
@synthesize lastWriteDate = _lastWriteDate;

// METHODS
- (id)initWithRawTagId:(NSString *)rawTagId {
    self = [super init];
    if (self) {
        CFUUIDRef uuidObj = CFUUIDCreate(nil);//create a new UUID
        self.guid = (__bridge NSString*)CFUUIDCreateString(nil, uuidObj); //get string
        self.raw = rawTagId;
        self.enabled = YES;
        [self decode];
    }
    return self;
}

-(void)decode {
    if (_raw) {
        // TODO
    }
}

@end
