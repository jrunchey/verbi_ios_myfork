//
//  VMUser.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMUser.h"

@implementation VMUser

#pragma mark -
#pragma mark GETTERS/SETTERS
/****************************************************************************
 * GETTERS/SETTERS
 ****************************************************************************/

@synthesize ID = _ID;
@synthesize enabled = _enabled;
@synthesize archived = _archived;
@synthesize accessLevel = _accessLevel;
@synthesize accountId = _accountId;
@synthesize personId = _personId;
@synthesize name = _name;
@synthesize username = _username;
@synthesize email = _email;
@synthesize password = _password;
@synthesize pin = _pin;
@synthesize secretQuestion1 = _secretQuestion1;
@synthesize secretAnswer1 = _secretAnswer1;
@synthesize secretQuestion2 = _secretQuestion2;
@synthesize secretAnswer2 = _secretAnswer2;
@synthesize language = _language;
@synthesize timezone = _timezone;
@synthesize image = _image;
@synthesize lastLogin = _lastLogin;
@synthesize created = _created;
@synthesize updated = _updated;
@synthesize updatedBy = _updatedBy;
@synthesize hardwareProfile = _hardwareProfile;

#pragma mark -
#pragma mark CONSTRUCTOR METHODS
/****************************************************************************
 * CONSTRUCTOR METHODS
 ****************************************************************************/

- (id)init:name username:(NSString *)username email:(NSString *)email password:(NSString *)password language:(NSString *)language timezone:(NSString *)timezone image:(UIImage *)image {
    self = [super init];
    if (self) {
        CFUUIDRef uuidObj = CFUUIDCreate(nil);//create a new UUID
        self.ID = (__bridge NSString*)CFUUIDCreateString(nil, uuidObj); //get string
        self.name = name;
        self.username = username;
        self.email = email;
        self.password = password;
        self.language = language;
        self.timezone = timezone;
        self.image = image;
        self.enabled = YES;
        self.archived = NO;
        self.created = [NSDate date];
        self.updated = [NSDate date];
    }
    return self;
}

- (id)initWithUsername:(NSString *)username password:(NSString *)password email:(NSString *)email {
    self = [super init];
    if (self) {
        CFUUIDRef uuidObj = CFUUIDCreate(nil);//create a new UUID
        self.ID = (__bridge NSString*)CFUUIDCreateString(nil, uuidObj); //get string
        self.username = username;
        self.password = password;
        self.email = email;
        self.enabled = YES;
        self.archived = NO;
        self.created = [NSDate date];
        self.updated = [NSDate date];
    }
    return self;
}

#pragma mark -
#pragma mark DECONSTRUCTOR METHODS
/****************************************************************************
 * DECONSTRUCTOR METHODS
 ****************************************************************************/

-(void)dispose {
    self.accountId = nil;
    self.personId = nil;
    self.name = nil;
    self.username = nil;
    self.email = nil;
    self.password = nil;
    self.pin = nil;
    self.secretQuestion1 = nil;
    self.secretAnswer1 = nil;
    self.secretQuestion2 = nil;
    self.secretAnswer2 = nil;
    self.language = nil;
    self.timezone = nil;
    self.image = nil;
    self.lastLogin = nil;
    self.created = nil;
    self.updated = nil;
    self.updatedBy = nil;
    if (self.hardwareProfile) {
        [self.hardwareProfile dispose];
        _hardwareProfile = nil;
    }
}

@end
