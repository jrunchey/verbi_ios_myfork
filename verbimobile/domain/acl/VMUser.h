//
//  VMUser.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMHardwareProfile.h"

@interface VMUser : NSObject

#pragma mark -
#pragma mark GETTERS/SETTERS

@property(nonatomic, strong) NSString *ID;
@property(nonatomic) BOOL enabled;
@property(nonatomic) BOOL archived;
@property(nonatomic) int accessLevel;
@property(nonatomic, strong) NSString *accountId;
@property(nonatomic, strong) NSString *personId;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *username;
@property(nonatomic, strong) NSString *email;
@property(nonatomic, strong) NSString *password;
@property(nonatomic, strong) NSString *pin;
@property(nonatomic, strong) NSString *secretQuestion1;
@property(nonatomic, strong) NSString *secretAnswer1;
@property(nonatomic, strong) NSString *secretQuestion2;
@property(nonatomic, strong) NSString *secretAnswer2;
@property(nonatomic, strong) NSString *language;
@property(nonatomic, strong) NSString *timezone;
@property(nonatomic, strong) UIImage *image;
@property(nonatomic, strong) NSDate *lastLogin;
@property(nonatomic, strong) NSDate *created;
@property(nonatomic, strong) NSDate *updated;
@property(nonatomic, strong) NSString *updatedBy;
@property(nonatomic, strong) VMHardwareProfile *hardwareProfile;

#pragma mark -
#pragma mark CONSTRUCTOR METHODS

- (id)init:name username:(NSString *)username email:(NSString *)email password:(NSString *)password language:(NSString *)language timezone:(NSString *)timezone image:(UIImage *)image;

- (id)initWithUsername:(NSString *)username password:(NSString *)password email:(NSString *)email;

#pragma mark -
#pragma mark DECONSTRUCTOR METHODS

-(void)dispose;

@end
