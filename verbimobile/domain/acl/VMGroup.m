//
//  VMGroup.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMGroup.h"

@implementation VMGroup

#pragma mark -
#pragma mark GETTERS/SETTERS
/****************************************************************************
 * GETTERS/SETTERS
 ****************************************************************************/

@synthesize ID = _ID;
@synthesize enabled = _enabled;
@synthesize archived = _archived;
@synthesize accountId = _accountId;
@synthesize name = _name;
@synthesize lastLogin = _lastLogin;
@synthesize created = _created;
@synthesize updated = _updated;
@synthesize updatedBy = _updatedBy;

#pragma mark -
#pragma mark CONSTRUCTOR METHODS
/****************************************************************************
 * CONSTRUCTOR METHODS
 ****************************************************************************/

- (id)init:name {
    self = [super init];
    if (self) {
        CFUUIDRef uuidObj = CFUUIDCreate(nil);//create a new UUID
        self.ID = (__bridge NSString*)CFUUIDCreateString(nil, uuidObj); //get string
        self.name = name;
        self.enabled = YES;
        self.archived = NO;
        self.created = [NSDate date];
        self.updated = [NSDate date];
    }
    return self;
}

#pragma mark -
#pragma mark DECONSTRUCTOR METHODS
/****************************************************************************
 * DECONSTRUCTOR METHODS
 ****************************************************************************/

-(void)dispose {
    self.accountId = nil;
    self.name = nil;
    self.lastLogin = nil;
    self.created = nil;
    self.updated = nil;
    self.updatedBy = nil;
}

@end
