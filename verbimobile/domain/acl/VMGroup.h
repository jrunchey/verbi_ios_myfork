//
//  VMGroup.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VMGroup : NSObject

#pragma mark -
#pragma mark GETTERS/SETTERS

@property(nonatomic, strong) NSString *ID;
@property(nonatomic) BOOL enabled;
@property(nonatomic) BOOL archived;
@property(nonatomic, strong) NSString *accountId;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSDate *lastLogin;
@property(nonatomic, strong) NSDate *created;
@property(nonatomic, strong) NSDate *updated;
@property(nonatomic, strong) NSString *updatedBy;

#pragma mark -
#pragma mark CONSTRUCTOR METHODS

- (id)init:name;

#pragma mark -
#pragma mark DECONSTRUCTOR METHODS

-(void)dispose;

@end
