//
//  VMEvent.m
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMEvent.h"

@implementation VMEvent

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@synthesize type = _type;
@synthesize state = _state;
@synthesize details = _details;

#pragma mark -
#pragma mark Constructors
/****************************************************************************
 * Constructors
 ****************************************************************************/

- (id)init:(int)type details:(NSDictionary *)details {
    self = [super init];
    if (self) {
        self.type = type;
        self.details = details;
    }
    return self;
}

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose {
    self.state = nil;
    self.details = nil;
}

@end
