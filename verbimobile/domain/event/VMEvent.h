//
//  VMEvent.h
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
	EVENT_STATUS=0,
	EVENT_READ,
    EVENT_WRITE
}EVENT_TYPES;

@interface VMEvent : NSObject

#pragma mark -
#pragma mark GETTERS/SETTERS

@property(nonatomic) int type;
@property(nonatomic, weak) NSString *state;
@property(nonatomic, weak) NSDictionary *details;

#pragma mark -
#pragma mark CONSTRUCTOR METHODS

- (id)init:(int)type details:(NSDictionary *)details;

#pragma mark -
#pragma mark DECONSTRUCTOR METHODS

- (void)dispose;

@end
