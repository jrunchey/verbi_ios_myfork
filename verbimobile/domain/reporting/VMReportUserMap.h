//
//  VMReportUserMap.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VMReportUserMap : NSObject

// GETTERS/SETTERS
@property(nonatomic, strong) NSString *reportGUID;
@property(nonatomic, strong) NSString *userGUID;

// METHODS
- (id)init:(NSString *)reportGUID userGUID:(NSString *)userGUID;

@end
