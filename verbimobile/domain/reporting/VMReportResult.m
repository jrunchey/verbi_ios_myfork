//
//  VMReportResult.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMReportResult.h"

@implementation VMReportResult

// GETTERS/SETTERS
@synthesize value = _value;

// METHODS

- (id)init:(NSString *)value {
    self = [super init];
    if (self) {
        self.value = value;
    }
    return self;
}

@end
