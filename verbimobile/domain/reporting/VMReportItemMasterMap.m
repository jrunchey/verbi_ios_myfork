//
//  VMReportItemMasterMap.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMReportItemMasterMap.h"

@implementation VMReportItemMasterMap

// GETTERS/SETTERS
@synthesize reportGUID = _reportGUID;
@synthesize itemMasterGUID = _itemMasterGUID;

// METHODS

- (id)init:(NSString *)reportGUID itemMasterGUID:(NSString *)itemMasterGUID {
    self = [super init];
    if (self) {
        self.reportGUID = reportGUID;
        self.itemMasterGUID = itemMasterGUID;
    }
    return self;
}

@end
