//
//  VMReportResult.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VMReportResult : NSObject

// GETTERS/SETTERS
@property(nonatomic, strong) NSString *value;

// METHODS
- (id)init:(NSString *)value;

@end
