//
//  VMReportItemMasterMap.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VMReportItemMasterMap : NSObject

// GETTERS/SETTERS
@property(nonatomic, strong) NSString *reportGUID;
@property(nonatomic, strong) NSString *itemMasterGUID;

// METHODS
- (id)init:(NSString *)reportGUID itemMasterGUID:(NSString *)itemMasterGUID;

@end
