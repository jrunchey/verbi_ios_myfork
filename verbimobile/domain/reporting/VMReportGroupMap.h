//
//  VMReportGroupMap.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VMReportGroupMap : NSObject

// GETTERS/SETTERS
@property(nonatomic, strong) NSString *reportGUID;
@property(nonatomic, strong) NSString *groupGUID;

// METHODS
- (id)init:(NSString *)reportGUID groupGUID:(NSString *)groupGUID;

@end
