//
//  VMReport.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMUser.h"

@interface VMReport : NSObject

// GETTERS/SETTERS
@property(nonatomic, strong) NSString *guid;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *type;
// Auditing
@property(nonatomic, strong) NSDate *createdDate;
@property(nonatomic, strong) NSDate *lastUpdatedDate;
@property(nonatomic, strong) NSString *lastUpdatedUserId;
// Activation/Archiving
@property(nonatomic) BOOL archived;
@property(nonatomic) BOOL enabled;
// Mappings
@property(nonatomic, strong) NSMutableArray *groups;
@property(nonatomic, strong) NSMutableArray *users;
@property(nonatomic, strong) NSMutableArray *locations;
@property(nonatomic, strong) NSMutableArray *itemMasters;

// Transient
@property(nonatomic, strong) VMUser *lastUpdatedUser;

// METHODS
- (id)init:(NSString *)guid name:(NSString *)name type:(NSString *)type;

- (id)init:(NSString *)guid name:(NSString *)name type:(NSString *)type groups:(NSMutableArray *)groups users:(NSMutableArray *)users locations:(NSMutableArray *)locations itemMasters:(NSMutableArray *)itemMasters;

@end
