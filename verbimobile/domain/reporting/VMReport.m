//
//  VMReport.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMReport.h"

@implementation VMReport

// GETTERS/SETTERS
@synthesize guid = _guid;
@synthesize name = _name;
@synthesize type = _type;
@synthesize createdDate = _createdDate;
@synthesize lastUpdatedDate = _lastUpdatedDate;
@synthesize lastUpdatedUserId = _lastUpdatedUserId;
@synthesize enabled = _enabled;
@synthesize archived = _archived;
@synthesize groups = _groups;
@synthesize users = _users;
@synthesize locations = _locations;
@synthesize itemMasters = _itemMasters;
@synthesize lastUpdatedUser = _lastUpdatedUser;

// METHODS

- (id)init:(NSString *)guid name:(NSString *)name type:(NSString *)type {
    self = [super init];
    if (self) {
        if (guid)
            self.guid = guid;
        else {
            CFUUIDRef uuidObj = CFUUIDCreate(nil);//create a new UUID
            self.guid = (__bridge NSString*)CFUUIDCreateString(nil, uuidObj); //get string
        }
        self.name = name;
        self.type = type;
        self.enabled = YES;
        self.archived = NO;
        self.createdDate = [NSDate date];
        self.lastUpdatedDate = [NSDate date];
    }
    return self;
}

- (id)init:(NSString *)guid name:(NSString *)name type:(NSString *)type groups:(NSMutableArray *)groups users:(NSMutableArray *)users locations:(NSMutableArray *)locations itemMasters:(NSMutableArray *)itemMasters {
    self = [super init];
    if (self) {
        if (guid)
            self.guid = guid;
        else {
            CFUUIDRef uuidObj = CFUUIDCreate(nil);//create a new UUID
            self.guid = (__bridge NSString*)CFUUIDCreateString(nil, uuidObj); //get string
        }
        self.name = name;
        self.type = type;
        self.groups = groups;
        self.users = users;
        self.locations = locations;
        self.itemMasters = itemMasters;
        self.enabled = YES;
        self.archived = NO;
        self.createdDate = [NSDate date];
        self.lastUpdatedDate = [NSDate date];
    }
    return self;
}

@end
