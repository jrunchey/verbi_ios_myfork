//
//  VMReportLocationMap.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMReportLocationMap.h"

@implementation VMReportLocationMap

// GETTERS/SETTERS
@synthesize reportGUID = _reportGUID;
@synthesize locationGUID = _locationGUID;

// METHODS

- (id)init:(NSString *)reportGUID locationGUID:(NSString *)locationGUID {
    self = [super init];
    if (self) {
        self.reportGUID = reportGUID;
        self.locationGUID = locationGUID;
    }
    return self;
}

@end
