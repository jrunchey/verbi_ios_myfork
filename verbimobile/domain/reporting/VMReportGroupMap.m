//
//  VMReportGroupMap.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMReportGroupMap.h"

@implementation VMReportGroupMap

// GETTERS/SETTERS
@synthesize reportGUID = _reportGUID;
@synthesize groupGUID = _groupGUID;

// METHODS

- (id)init:(NSString *)reportGUID groupGUID:(NSString *)groupGUID {
    self = [super init];
    if (self) {
        self.reportGUID = reportGUID;
        self.groupGUID = groupGUID;
    }
    return self;
}

@end
