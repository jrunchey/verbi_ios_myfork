//
//  VMReportLocationMap.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VMReportLocationMap : NSObject

// GETTERS/SETTERS
@property(nonatomic, strong) NSString *reportGUID;
@property(nonatomic, strong) NSString *locationGUID;

// METHODS
- (id)init:(NSString *)reportGUID locationGUID:(NSString *)locationGUID;

@end
