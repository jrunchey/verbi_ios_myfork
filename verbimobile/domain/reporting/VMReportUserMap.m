//
//  VMReportUserMap.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMReportUserMap.h"

@implementation VMReportUserMap

// GETTERS/SETTERS
@synthesize reportGUID = _reportGUID;
@synthesize userGUID = _userGUID;

// METHODS

- (id)init:(NSString *)reportGUID userGUID:(NSString *)userGUID {
    self = [super init];
    if (self) {
        self.reportGUID = reportGUID;
        self.userGUID = userGUID;
    }
    return self;
}

@end
