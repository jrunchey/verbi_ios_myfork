//
//  VMTag.m
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMTag.h"

@implementation VMTag

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@synthesize value = _value;
@synthesize type = _type;

#pragma mark -
#pragma mark Constructors
/****************************************************************************
 * Constructors
 ****************************************************************************/

- (id)init:(NSString *)value {
    self = [super init];
    if (self) {
        self.value = value;
    }
    return self;
}

@end
