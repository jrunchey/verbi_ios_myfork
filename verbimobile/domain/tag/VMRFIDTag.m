//
//  VMRFIDTag.m
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMRFIDTag.h"

@implementation VMRFIDTag

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@synthesize length = _length;
@synthesize antenna = _antenna;
@synthesize rssi = _rssi;

#pragma mark -
#pragma mark Constructors
/****************************************************************************
 * Constructors
 ****************************************************************************/

- (id)init:(NSString *)value length:(NSNumber *)length antenna:(NSString *)antenna rssi:(NSNumber *)rssi {
    self = [super init:value];
    if (self) {
        self.type = TAG_RFID;
        self.length = length;
        self.antenna = antenna;
        self.rssi = rssi;
    }
    return self;
}

@end
