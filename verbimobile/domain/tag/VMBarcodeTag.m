//
//  VMBarcodeTag.m
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMBarcodeTag.h"

@implementation VMBarcodeTag

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@synthesize symbology = _symbology;

#pragma mark -
#pragma mark Constructors
/****************************************************************************
 * Constructors
 ****************************************************************************/

- (id)init:(NSString *)value symbology:(NSString *)symbology {
    self = [super init:value];
    if (self) {
        self.type = TAG_BARCODE;
        self.symbology = symbology;
    }
    return self;
}

@end
