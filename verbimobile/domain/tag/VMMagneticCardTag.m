//
//  VMMagneticCardTag.m
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMMagneticCardTag.h"

@implementation VMMagneticCardTag

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@synthesize cardName = _cardName;
@synthesize cardNumber = _cardNumber;
@synthesize expirationDate = _expirationDate;
@synthesize magneticCardTrack1 = _magneticCardTrack1;
@synthesize magneticCardTrack2 = _magneticCardTrack2;
@synthesize magneticCardTrack3 = _magneticCardTrack3;

#pragma mark -
#pragma mark Constructors
/****************************************************************************
 * Constructors
 ****************************************************************************/

- (id)init:(NSString *)value cardName:(NSString *)cardName cardNumber:(NSString *)cardNumber expirationDate:(NSString *)expirationDate {
    self = [super init:value];
    if (self) {
        self.type = TAG_MAGNETIC_CARD;
        self.cardName = cardName;
        self.cardNumber = cardNumber;
        self.expirationDate = expirationDate;
    }
    return self;
}

- (id)init:(NSString *)value cardName:(NSString *)cardName cardNumber:(NSString *)cardNumber expirationDate:(NSString *)expirationDate magneticCardTrack1:(NSString *)magneticCardTrack1 magneticCardTrack2:(NSString *)magneticCardTrack2 magneticCardTrack3:(NSString *)magneticCardTrack3 {
    self = [super init:value];
    if (self) {
        self.type = TAG_MAGNETIC_CARD;
        self.cardName = cardName;
        self.cardNumber = cardNumber;
        self.expirationDate = expirationDate;
        self.magneticCardTrack1 = magneticCardTrack1;
        self.magneticCardTrack2 = magneticCardTrack2;
        self.magneticCardTrack3 = magneticCardTrack3;
    }
    return self;
}

@end
