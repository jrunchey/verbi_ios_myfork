//
//  VMTag.h
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
	TAG_GENERIC=0,
	TAG_MAGNETIC_CARD,
    TAG_BARCODE,
    TAG_RFID
}TAG_TYPES;

@interface VMTag : NSObject

#pragma mark -
#pragma mark GETTERS/SETTERS

@property(nonatomic,weak) NSString *value;
@property(nonatomic) int type;

#pragma mark -
#pragma mark CONSTRUCTOR METHODS

- (id)init:(NSString *)value;

@end
