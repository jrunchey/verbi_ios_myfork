//
//  VMMagneticCardTag.h
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMTag.h"

@interface VMMagneticCardTag : VMTag

#pragma mark -
#pragma mark GETTERS/SETTERS

@property (nonatomic,weak) NSString *cardName;
@property (nonatomic,weak) NSString *cardNumber;
@property (nonatomic,weak) NSString *expirationDate;
@property (nonatomic,weak) NSString *magneticCardTrack1;
@property (nonatomic,weak) NSString *magneticCardTrack2;
@property (nonatomic,weak) NSString *magneticCardTrack3;

#pragma mark -
#pragma mark CONSTRUCTOR METHODS

- (id)init:(NSString *)value cardName:(NSString *)cardName cardNumber:(NSString *)cardNumber expirationDate:(NSString *)expirationDate;
- (id)init:(NSString *)value cardName:(NSString *)cardName cardNumber:(NSString *)cardNumber expirationDate:(NSString *)expirationDate magneticCardTrack1:(NSString *)magneticCardTrack1 magneticCardTrack2:(NSString *)magneticCardTrack2 magneticCardTrack3:(NSString *)magneticCardTrack3;

@end
