//
//  VMRFIDTag.h
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMTag.h"

@interface VMRFIDTag : VMTag

#pragma mark -
#pragma mark GETTERS/SETTERS

@property(nonatomic,weak) NSNumber *length;
@property(nonatomic,weak) NSString *antenna;
@property(nonatomic,weak) NSNumber *rssi;
@property(nonatomic,weak) NSString *userData;

#pragma mark -
#pragma mark CONSTRUCTOR METHODS

- (id)init:(NSString *)value length:(NSNumber *)length antenna:(NSString *)antenna rssi:(NSNumber *)rssi;

@end
