//
//  VMBarcodeTag.h
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMTag.h"

@interface VMBarcodeTag : VMTag

#pragma mark -
#pragma mark GETTERS/SETTERS

@property(nonatomic,weak) NSString *symbology;

#pragma mark -
#pragma mark CONSTRUCTOR METHODS

- (id)init:(NSString *)value symbology:(NSString *)symbology;

@end
