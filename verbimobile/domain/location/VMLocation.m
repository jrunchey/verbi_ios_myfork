//
//  VMLocation.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMLocation.h"
#import "EPC.h"

@implementation VMLocation

// GETTERS/SETTERS
@synthesize guid = _guid;
@synthesize name = _name;
@synthesize type = _type;
@synthesize image = _image;
@synthesize tagId = _tagId;
@synthesize epc = _epc;
@synthesize latitude = _latitude;
@synthesize longitude = _longitude;
@synthesize altitude = _altitude;
@synthesize address1 = _address1;
@synthesize address2 = _address2;
@synthesize townOrCity = _townOrCity;
@synthesize countyOrDistrict = _countyOrDistrict;
@synthesize stateOrRegion = _stateOrRegion;
@synthesize postal = _postal;
@synthesize country = _country;
@synthesize enabled = _enabled;

// METHODS
- (id)init:(NSString *)guid name:(NSString *)name type:(NSString *)type image:(UIImage *)image tagId:(NSString *)tagId epc:(NSString *)epc latitude:(NSString *)latitude longitude:(NSString *)longitude altitude:(NSString *)altitude address1:(NSString *)address1 address2:(NSString *)address2 townOrCity:(NSString *)townOrCity countyOrDistrict:(NSString *)countyOrDistrict stateOrRegion:(NSString *)stateOrRegion postal:(NSString *)postal country:(NSString *)country {
    self = [super init];
    if (self) {
        if (guid)
            self.guid = guid;
        else {
            CFUUIDRef uuidObj = CFUUIDCreate(nil);//create a new UUID
            self.guid = (__bridge NSString*)CFUUIDCreateString(nil, uuidObj); //get string
        }
        self.name = name;
        self.type = type;
        self.image = image;
        self.tagId = tagId;
        self.epc = epc;
        self.latitude = latitude;
        self.longitude = longitude;
        self.altitude = altitude;
        self.address1 = address1;
        self.address2 = address2;
        self.townOrCity = townOrCity;
        self.countyOrDistrict = countyOrDistrict;
        self.stateOrRegion = stateOrRegion;
        self.postal = postal;
        self.country = country;
        self.enabled = YES;
    }
    return self;
}

@end
