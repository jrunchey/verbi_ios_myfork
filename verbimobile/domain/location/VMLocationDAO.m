//
//  VMLocation.m
//  verbimobile
//
//  Copyright (c) 2012 Verbinden LLC. All rights reserved.
//

#import "VMLocation.h"
#import "EPC.h"

@implementation VMLocationDAO {
@private NSString *_guid;
@private NSString *_name;
@private NSString *_type;
@private EPC *_epc;
@private NSString *_latitude;
@private NSString *_longitude;
@private UIImage *_image;
}

// GETTERS/SETTERS
@synthesize guid = _guid;
@synthesize name = _name;
@synthesize type = _type;
@synthesize epc = _epc;
@synthesize latitude = _latitude;
@synthesize longitude = _longitude;
@synthesize image = _image;

// METHODS
- (id)init:(NSString *)guid name:(NSString *)name type:(NSString *)type epc:(EPC *)epc latitude:(NSString *)latitude longitude:(NSString *)longitude image:(UIImage *)image {
    self = [super init];
    if (self) {
        _guid = [guid copy];
        _name = [name copy];
        _type = [type copy];
        _epc = [epc copy];
        _latitude = [latitude copy];
        _longitude = [longitude copy];
        _image = [image copy];
    }
    return self;
}

@end
