//
//  VMLocation.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EPC.h"

@interface VMLocation : NSObject

// GETTERS/SETTERS
@property(nonatomic, strong) NSString *guid;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *type;        // fixed or movable
@property(nonatomic, strong) UIImage *image;
@property(nonatomic, strong) NSString *tagId;
@property(nonatomic, strong) NSString *epc;
@property(nonatomic, strong) NSString *latitude;
@property(nonatomic, strong) NSString *longitude;
@property(nonatomic, strong) NSString *altitude;
@property(nonatomic, strong) NSString *address1;
@property(nonatomic, strong) NSString *address2;
@property(nonatomic, strong) NSString *townOrCity;
@property(nonatomic, strong) NSString *countyOrDistrict;
@property(nonatomic, strong) NSString *stateOrRegion;
@property(nonatomic, strong) NSString *postal;
@property(nonatomic, strong) NSString *country;
@property(nonatomic) BOOL enabled;

// METHODS
- (id)init:(NSString *)guid name:(NSString *)name type:(NSString *)type image:(UIImage *)image tagId:(NSString *)tagId epc:(NSString *)epc latitude:(NSString *)latitude longitude:(NSString *)longitude altitude:(NSString *)altitude address1:(NSString *)address1 address2:(NSString *)address2 townOrCity:(NSString *)townOrCity countyOrDistrict:(NSString *)countyOrDistrict stateOrRegion:(NSString *)stateOrRegion postal:(NSString *)postal country:(NSString *)country;

@end
