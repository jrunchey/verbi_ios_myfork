//
//  VMWorkflowStep.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMWorkflowStep.h"

@implementation VMWorkflowStep

// GETTERS/SETTERS
@synthesize guid = _guid;
@synthesize category = _category;
@synthesize type = _type;
@synthesize workflow = _workflow;
@synthesize completed = _completed;
@synthesize createdDate = _createdDate;

#pragma mark -
#pragma mark CONSTRUCTOR METHODS
/****************************************************************************
 * CONSTRUCTOR METHODS
 ****************************************************************************/

- (id)init:(VMWorkflow *)workflow {
    self = [super init];
    if (self) {
        CFUUIDRef uuidObj = CFUUIDCreate(nil);//create a new UUID
        self.guid = (__bridge NSString*)CFUUIDCreateString(nil, uuidObj); //get string
        self.workflow = workflow;
        self.createdDate = [NSDate date];
    }
    return self;
}

#pragma mark -
#pragma mark DECONSTRUCTOR METHODS
/****************************************************************************
 * DECONSTRUCTOR METHODS
 ****************************************************************************/

-(void)dispose {
    self.guid = nil;
    self.category = nil;
    self.type = nil;
    self.workflow = nil;
    self.createdDate = nil;
}

@end
