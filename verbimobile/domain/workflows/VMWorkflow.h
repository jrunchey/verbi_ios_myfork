//
//  VMWorkflow.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMWorkflowStep.h"

@interface VMWorkflow : NSObject

// GETTERS/SETTERS
@property(nonatomic, strong) NSString *guid;
@property(nonatomic, strong) NSString *category;
@property(nonatomic, strong) NSString *type;
@property(nonatomic, strong) NSString *username;
@property(nonatomic) int expectedNumberOfSteps;
@property(nonatomic, strong) NSMutableDictionary *workflowSteps;
@property(nonatomic, strong) VMWorkflowStep *currentStep;
@property(nonatomic, strong) VMWorkflowStep *firstStep;
@property(nonatomic, strong) VMWorkflowStep *lastStep;
@property(nonatomic, strong) NSDate *createdDate;

#pragma mark -
#pragma mark CONSTRUCTOR METHODS

-(id)init:(NSString *)username;

#pragma mark -
#pragma mark DECONSTRUCTOR METHODS

-(void)dispose;

#pragma mark -
#pragma mark METHODS

-(void)addWorkflowStep:(VMWorkflowStep *)workflowStep;

@end
