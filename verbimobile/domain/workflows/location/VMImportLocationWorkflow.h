//
//  VMImportLocationWorkflow.h
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMWorkflow.h"

@interface VMImportLocationWorkflow : VMWorkflow

// METHODS
-(id)init:(NSString *)username;

@end
