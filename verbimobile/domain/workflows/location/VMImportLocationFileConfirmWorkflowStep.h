//
//  VMImportLocationFileConfirmWorkflowStep.h
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMWorkflowStep.h"

@interface VMImportLocationFileConfirmWorkflowStep : VMWorkflowStep

// GETTERS/SETTERS
@property(nonatomic, strong) NSMutableArray *locationsArray;
@property(nonatomic, strong) NSMutableArray *creatingLocationsArray;
@property(nonatomic, strong) NSMutableArray *existingLocationNamesArray;

// METHODS
- (id)init:(VMWorkflow *)workflow;
@end
