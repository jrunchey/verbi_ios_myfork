//
//  VMImportLocationFileWorkflowStep.h
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMWorkflowStep.h"

@interface VMImportLocationFileWorkflowStep : VMWorkflowStep

// METHODS
- (id)init:(VMWorkflow *)workflow;

@end
