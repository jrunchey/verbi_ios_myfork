//
//  VMImportLocationWorkflow.m
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMImportLocationWorkflow.h"
#import "VMConstants.h"

@implementation VMImportLocationWorkflow

// METHODS

-(id)init:(NSString *)username {
    self = [super init:username];
    if (self) {
        self.type = WKFL_LOC_IMPORT_LOCATION;
        self.expectedNumberOfSteps = 2;
    }
    return self;
}

- (void)dispose {
    [super dispose];
}

@end
