//
//  VMImportLocationFileConfirmWorkflowStep.m
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMImportLocationFileConfirmWorkflowStep.h"
#import "VMConstants.h"

@implementation VMImportLocationFileConfirmWorkflowStep

// GETTERS/SETTERS
@synthesize locationsArray = _locationsArray;
@synthesize creatingLocationsArray = _creatingLocationsArray;
@synthesize existingLocationNamesArray = _existingLocationNamesArray;

// METHODS

- (id)init:(VMWorkflow *)workflow {
    self = [super init:workflow];
    if (self) {
        self.type = WKFL_STEP_LOC_IMPORT_LOCATION_FILE_CONFIRM;
        self.locationsArray = [[NSMutableArray alloc] init];
        self.creatingLocationsArray = [[NSMutableArray alloc] init];
        self.existingLocationNamesArray = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dispose {
    if (self.locationsArray) {
        [self.locationsArray removeAllObjects];
        self.locationsArray = nil;
    }
    if (self.creatingLocationsArray) {
        [self.creatingLocationsArray removeAllObjects];
        self.creatingLocationsArray = nil;
    }
    if (self.existingLocationNamesArray) {
        [self.existingLocationNamesArray removeAllObjects];
        self.existingLocationNamesArray = nil;
    }
    [super dispose];
}


@end
