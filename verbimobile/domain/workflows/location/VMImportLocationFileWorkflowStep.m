//
//  VMImportLocationFileWorkflowStep.m
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMImportLocationFileWorkflowStep.h"
#import "VMConstants.h"

@implementation VMImportLocationFileWorkflowStep

// METHODS

- (id)init:(VMWorkflow *)workflow {
    self = [super init:workflow];
    if (self) {
        self.type = WKFL_STEP_LOC_IMPORT_LOCATION_FILE;
    }
    return self;
}

- (void)dispose {
    [super dispose];
}

@end
