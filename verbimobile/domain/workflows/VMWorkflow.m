//
//  VMWorkflow.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMWorkflow.h"

@implementation VMWorkflow

// GETTERS/SETTERS
@synthesize guid = _guid;
@synthesize category = _category;
@synthesize type = _type;
@synthesize username = _username;
@synthesize expectedNumberOfSteps = _expectedNumberOfSteps;
@synthesize workflowSteps = _workflowSteps;
@synthesize currentStep = _currentStep;
@synthesize firstStep = _firstStep;
@synthesize lastStep = _lastStep;
@synthesize createdDate = _createdDate;

#pragma mark -
#pragma mark CONSTRUCTOR METHODS
/****************************************************************************
 * CONSTRUCTOR METHODS
 ****************************************************************************/

-(id)init:(NSString *)username {
    self = [super init];
    if (self) {
        CFUUIDRef uuidObj = CFUUIDCreate(nil);//create a new UUID
        self.guid = (__bridge NSString*)CFUUIDCreateString(nil, uuidObj); //get string
        self.username = username;
        self.createdDate = [NSDate date];
    }
    return self;
}

#pragma mark -
#pragma mark DECONSTRUCTOR METHODS
/****************************************************************************
 * DECONSTRUCTOR METHODS
 ****************************************************************************/

-(void)dispose {
    self.guid = nil;
    self.category = nil;
    self.type = nil;
    self.createdDate = nil;
    self.username = nil;
    if (self.workflowSteps) {
        [self.workflowSteps removeAllObjects];
        self.workflowSteps = nil;
    }
    if (self.currentStep) {
        [self.currentStep dispose];
        self.currentStep = nil;
    }
    if (self.firstStep) {
        [self.firstStep dispose];
        self.firstStep = nil;
    }
    if (self.lastStep) {
        [self.lastStep dispose];
        self.lastStep = nil;
    }
}

#pragma mark -
#pragma mark METHODS
/****************************************************************************
 * METHODS
 ****************************************************************************/

-(void)addWorkflowStep:(VMWorkflowStep *)workflowStep {
    // Add new step to workflowSteps array
    if (self.workflowSteps == nil)
        self.workflowSteps = [[NSMutableDictionary alloc] init];
    //if (workflowSteps.)
    //[workflowSteps addObject:workflowStep];
    [self.workflowSteps setValue:workflowStep forKey:workflowStep.type];
    
    // Update first, current, and last steps
    if (self.firstStep == nil)
        self.firstStep = workflowStep;
    self.currentStep = workflowStep;
    if (self.lastStep == nil || (self.lastStep && self.workflowSteps.count < self.expectedNumberOfSteps))
        self.lastStep = workflowStep;
}

@end
