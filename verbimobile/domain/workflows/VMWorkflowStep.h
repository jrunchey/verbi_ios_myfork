//
//  VMWorkflowStep.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VMWorkflow;

@interface VMWorkflowStep : NSObject

// GETTERS/SETTERS
@property(nonatomic, strong) NSString *guid;
@property(nonatomic, strong) NSString *category;
@property(nonatomic, strong) NSString *type;
@property(nonatomic, strong) VMWorkflow *workflow;
@property(nonatomic) BOOL completed;
@property(nonatomic, strong) NSDate *createdDate;

#pragma mark -
#pragma mark CONSTRUCTOR METHODS

- (id)init:(VMWorkflow *)workflow;

#pragma mark -
#pragma mark DECONSTRUCTOR METHODS

-(void)dispose;

@end
