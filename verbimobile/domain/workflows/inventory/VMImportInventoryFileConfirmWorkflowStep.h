//
//  VMImportInventoryFileConfirmWorkflowStep.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMInventoryWorkflowStep.h"

@interface VMImportInventoryFileConfirmWorkflowStep : VMInventoryWorkflowStep

// GETTERS/SETTERS
@property(nonatomic, strong) NSMutableArray *itemsArray;
@property(nonatomic, strong) NSMutableArray *itemNamesArray;
@property(nonatomic, strong) NSMutableArray *itemVendorCodesArray;

// METHODS
- (id)init:(VMWorkflow *)workflow;

@end
