//
//  VMUpdateInventoryConfirmWorkflowStep.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMInventoryWorkflowStep.h"

@interface VMUpdateInventoryConfirmWorkflowStep : VMInventoryWorkflowStep

// GETTERS/SETTERS
@property(nonatomic, strong) NSMutableArray *itemsArray;        // expectedVsActualItemsArray;
@property(nonatomic, strong) NSMutableArray *itemsColorArray;   // expectedVsActualItemColorsArray;

// METHODS
- (id)init:(VMWorkflow *)workflow;

@end
