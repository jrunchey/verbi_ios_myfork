//
//  VMSelectInventoryLocationWorkflowStep.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMSelectInventoryLocationWorkflowStep.h"
#import "VMConstants.h"

@implementation VMSelectInventoryLocationWorkflowStep

// GETTERS/SETTERS
@synthesize location = _location;

// METHODS

- (id)init:(VMWorkflow *)workflow {
    self = [super init:workflow];
    if (self) {
        self.type = WKFL_STEP_INV_SELECT_INVENTORY_LOCATION;
    }
    return self;
}

- (void)dispose {
    [super dispose];
}

@end
