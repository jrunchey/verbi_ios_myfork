//
//  VMInventoryWorkflow.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMInventoryWorkflow.h"
#import "VMConstants.h"

@implementation VMInventoryWorkflow

// METHODS

- (id)init:(NSString *)username {
    self = [super init:username];
    if (self) {
        self.category = WKFL_INV_CATEGORY_NAME;
    }
    return self;
}

- (void)dispose {
    [super dispose];
}

@end
