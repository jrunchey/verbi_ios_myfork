//
//  VMImportInventoryWorkflow.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMWorkflow.h"
#import "VMInventoryWorkflow.h"

@interface VMImportInventoryWorkflow : VMInventoryWorkflow

// METHODS
-(id)init:(NSString *)username;

@end
