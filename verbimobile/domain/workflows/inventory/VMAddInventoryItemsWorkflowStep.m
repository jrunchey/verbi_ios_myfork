//
//  VMAddInventoryItemsWorkflowStep.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMAddInventoryItemsWorkflowStep.h"
#import "VMConstants.h"

@implementation VMAddInventoryItemsWorkflowStep

// GETTERS/SETTERS
@synthesize itemsArray = _itemsArray;
@synthesize itemGUIDsArray = _itemGUIDsArray;
@synthesize itemNamesArray = _itemNamesArray;
@synthesize itemUPCsArray = _itemUPCsArray;
@synthesize itemVendorCodesArray = _itemVendorCodesArray;

// METHODS

- (id)init:(VMWorkflow *)workflow {
    self = [super init:workflow];
    if (self) {
        self.type = WKFL_STEP_INV_ADD_INVENTORY_ITEMS;
        self.itemsArray = [[NSMutableArray alloc] init];
        self.itemGUIDsArray = [[NSMutableArray alloc] init];
        self.itemNamesArray = [[NSMutableArray alloc] init];
        self.itemUPCsArray = [[NSMutableArray alloc] init];
        self.itemVendorCodesArray = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dispose {
    if (self.itemsArray) {
        [self.itemsArray removeAllObjects];
        self.itemsArray = nil;
    }
    if (self.itemGUIDsArray) {
        [self.itemGUIDsArray removeAllObjects];
        self.itemGUIDsArray = nil;
    }
    if (self.itemNamesArray) {
        [self.itemNamesArray removeAllObjects];
        self.itemNamesArray = nil;
    }
    if (self.itemUPCsArray) {
        [self.itemUPCsArray removeAllObjects];
        self.itemUPCsArray = nil;
    }
    if (self.itemVendorCodesArray) {
        [self.itemVendorCodesArray removeAllObjects];
        self.itemVendorCodesArray = nil;
    }
    [super dispose];
}

@end
