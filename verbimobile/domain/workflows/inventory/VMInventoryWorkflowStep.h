//
//  VMInventoryWorkflowStep.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMWorkflowStep.h"

@interface VMInventoryWorkflowStep : VMWorkflowStep

// METHODS
- (id)init:(VMWorkflow *)workflow;

@end
