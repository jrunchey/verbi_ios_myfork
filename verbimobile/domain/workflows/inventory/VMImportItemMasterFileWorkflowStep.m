//
//  VMImportInventoryItemMasterFileWorkflowStep.m
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMImportItemMasterFileWorkflowStep.h"
#import "VMConstants.h"

@implementation VMImportItemMasterFileWorkflowStep

// METHODS

- (id)init:(VMWorkflow *)workflow {
    self = [super init:workflow];
    if (self) {
        self.type = WKFL_STEP_INV_IMPORT_ITEM_MASTER_FILE;
    }
    return self;
}

- (void)dispose {
    [super dispose];
}

@end
