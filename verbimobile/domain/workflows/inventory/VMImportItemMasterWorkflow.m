//
//  VMImportItemMasterWorkflow.m
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMImportItemMasterWorkflow.h"
#import "VMConstants.h"

@implementation VMImportItemMasterWorkflow

// METHODS

-(id)init:(NSString *)username {
    self = [super init:username];
    if (self) {
        self.type = WKFL_INV_IMPORT_ITEM_MASTER;
        self.expectedNumberOfSteps = 2;
    }
    return self;
}

- (void)dispose {
    [super dispose];
}

@end
