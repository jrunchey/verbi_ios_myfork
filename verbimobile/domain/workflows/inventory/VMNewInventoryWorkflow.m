//
//  VMNewInventoryWorkflow.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMNewInventoryWorkflow.h"
#import "VMConstants.h"

@implementation VMNewInventoryWorkflow

// METHODS

-(id)init:(NSString *)username {
    self = [super init:username];
    if (self) {
        self.type = WKFL_INV_NEW_INVENTORY;
        self.expectedNumberOfSteps = 4;
    }
    return self;
}

- (void)dispose {
    [super dispose];
}

@end
