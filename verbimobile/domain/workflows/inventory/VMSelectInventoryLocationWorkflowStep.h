//
//  VMSelectInventoryLocationWorkflowStep.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMInventoryWorkflowStep.h"
#import "VMLocation.h"

@interface VMSelectInventoryLocationWorkflowStep : VMInventoryWorkflowStep

// GETTERS/SETTERS
@property(strong, nonatomic) VMLocation *location;

// METHODS
- (id)init:(VMWorkflow *)workflow;

@end
