//
//  VMImportInventoryWorkflow.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMImportInventoryWorkflow.h"
#import "VMConstants.h"

@implementation VMImportInventoryWorkflow

// METHODS

-(id)init:(NSString *)username {
    self = [super init:username];
    if (self) {
        self.type = WKFL_INV_IMPORT_INVENTORY;
        self.expectedNumberOfSteps = 6;
    }
    return self;
}

- (void)dispose {
    [super dispose];
}

@end
