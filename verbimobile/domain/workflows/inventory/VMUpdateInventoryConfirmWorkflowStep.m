//
//  VMUpdateInventoryConfirmWorkflowStep.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMUpdateInventoryConfirmWorkflowStep.h"
#import "VMConstants.h"

@implementation VMUpdateInventoryConfirmWorkflowStep

// GETTERS/SETTERS
@synthesize itemsArray = _itemsArray;
@synthesize itemsColorArray = _itemsColorArray;

// METHODS

- (id)init:(VMWorkflow *)workflow {
    self = [super init:workflow];
    if (self) {
        self.type = WKFL_STEP_INV_UPDATE_INVENTORY_CONFIRM;
        self.itemsArray = [[NSMutableArray alloc] init];
        self.itemsColorArray = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dispose {
    if (self.itemsArray) {
        [self.itemsArray removeAllObjects];
        self.itemsArray = nil;
    }
    if (self.itemsColorArray) {
        [self.itemsColorArray removeAllObjects];
        self.itemsColorArray = nil;
    }
    [super dispose];
}

@end
