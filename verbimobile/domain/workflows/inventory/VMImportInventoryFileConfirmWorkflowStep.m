//
//  VMImportInventoryFileConfirmWorkflowStep.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMImportInventoryFileConfirmWorkflowStep.h"
#import "VMConstants.h"

@implementation VMImportInventoryFileConfirmWorkflowStep

// GETTERS/SETTERS
@synthesize itemsArray = _itemsArray;
@synthesize itemNamesArray = _itemNamesArray;
@synthesize itemVendorCodesArray = _itemVendorCodesArray;

// METHODS

- (id)init:(VMWorkflow *)workflow {
    self = [super init:workflow];
    if (self) {
        self.type = WKFL_STEP_INV_IMPORT_INVENTORY_FILE_CONFIRM;
        self.itemsArray = [[NSMutableArray alloc] init];
        self.itemNamesArray = [[NSMutableArray alloc] init];
        self.itemVendorCodesArray = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dispose {
    if (self.itemsArray) {
        [self.itemsArray removeAllObjects];
        self.itemsArray = nil;
    }
    if (self.itemNamesArray) {
        [self.itemNamesArray removeAllObjects];
        self.itemNamesArray = nil;
    }
    if (self.itemVendorCodesArray) {
        [self.itemVendorCodesArray removeAllObjects];
        self.itemVendorCodesArray = nil;
    }
    [super dispose];
}

@end
