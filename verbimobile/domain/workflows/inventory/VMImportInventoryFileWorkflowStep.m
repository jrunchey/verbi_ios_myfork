//
//  VMImportInventoryFileWorkflowStep.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMImportInventoryFileWorkflowStep.h"
#import "VMConstants.h"

@implementation VMImportInventoryFileWorkflowStep

// METHODS

- (id)init:(VMWorkflow *)workflow {
    self = [super init:workflow];
    if (self) {
        self.type = WKFL_STEP_INV_IMPORT_INVENTORY_FILE;
    }
    return self;
}

- (void)dispose {
    [super dispose];
}

@end
