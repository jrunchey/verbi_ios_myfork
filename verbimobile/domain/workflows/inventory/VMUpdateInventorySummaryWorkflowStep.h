//
//  VMUpdateInventorySummaryWorkflowStep.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMInventoryWorkflowStep.h"

@interface VMUpdateInventorySummaryWorkflowStep : VMInventoryWorkflowStep

// METHODS
- (id)init:(VMWorkflow *)workflow;

@end
