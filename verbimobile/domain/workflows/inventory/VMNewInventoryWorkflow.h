//
//  VMNewInventoryWorkflow.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMWorkflow.h"
#import "VMInventoryWorkflow.h"

@interface VMNewInventoryWorkflow : VMInventoryWorkflow

// METHODS
-(id)init:(NSString *)username;

@end
