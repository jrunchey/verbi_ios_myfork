//
//  VMImportInventoryItemMasterConfirmFileWorkflowStep.h
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMInventoryWorkflowStep.h"

@interface VMImportItemMasterFileConfirmWorkflowStep : VMInventoryWorkflowStep

// GETTERS/SETTERS
@property(nonatomic, strong) NSMutableArray *itemMastersArray;
@property(nonatomic, strong) NSMutableArray *creatingItemMastersArray;
@property(nonatomic, strong) NSMutableArray *existingItemMasterNamesArray;

// METHODS
- (id)init:(VMWorkflow *)workflow;

@end
