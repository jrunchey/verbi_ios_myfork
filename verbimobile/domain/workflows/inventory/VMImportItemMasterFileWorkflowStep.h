//
//  VMImportInventoryItemMasterFileWorkflowStep.h
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMInventoryWorkflowStep.h"

@interface VMImportItemMasterFileWorkflowStep : VMInventoryWorkflowStep

// METHODS
- (id)init:(VMWorkflow *)workflow;

@end
