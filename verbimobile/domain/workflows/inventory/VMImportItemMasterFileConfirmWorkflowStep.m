//
//  VMImportInventoryItemMasterConfirmFileWorkflowStep.m
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMImportItemMasterFileConfirmWorkflowStep.h"
#import "VMConstants.h"

@implementation VMImportItemMasterFileConfirmWorkflowStep

// GETTERS/SETTERS
@synthesize itemMastersArray = _itemMastersArray;
@synthesize creatingItemMastersArray = _creatingItemMastersArray;
@synthesize existingItemMasterNamesArray = _existingItemMasterNamesArray;

// METHODS

- (id)init:(VMWorkflow *)workflow {
    self = [super init:workflow];
    if (self) {
        self.type = WKFL_STEP_INV_IMPORT_ITEM_MASTER_FILE_CONFIRM;
        self.itemMastersArray = [[NSMutableArray alloc] init];
        self.creatingItemMastersArray = [[NSMutableArray alloc] init];
        self.existingItemMasterNamesArray = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dispose {
    if (self.itemMastersArray) {
        [self.itemMastersArray removeAllObjects];
        self.itemMastersArray = nil;
    }
    if (self.creatingItemMastersArray) {
        [self.creatingItemMastersArray removeAllObjects];
        self.creatingItemMastersArray = nil;
    }
    if (self.existingItemMasterNamesArray) {
        [self.existingItemMasterNamesArray removeAllObjects];
        self.existingItemMasterNamesArray = nil;
    }
    [super dispose];
}

@end
