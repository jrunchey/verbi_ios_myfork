//
//  VMImportItemMasterWorkflow.h
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMInventoryWorkflow.h"

@interface VMImportItemMasterWorkflow : VMInventoryWorkflow

// METHODS
-(id)init:(NSString *)username;

@end
