//
//  VMUpdateInventorySummaryWorkflowStep.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMUpdateInventorySummaryWorkflowStep.h"
#import "VMConstants.h"

@implementation VMUpdateInventorySummaryWorkflowStep

// METHODS

- (id)init:(VMWorkflow *)workflow {
    self = [super init:workflow];
    if (self) {
        self.type = WKFL_STEP_INV_UPDATE_INVENTORY_SUMMARY;
    }
    return self;
}

- (void)dispose {
    [super dispose];
}

@end
