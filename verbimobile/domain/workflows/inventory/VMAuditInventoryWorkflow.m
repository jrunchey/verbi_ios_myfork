//
//  VMAuditInventoryWorkflow.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMAuditInventoryWorkflow.h"
#import "VMConstants.h"

@implementation VMAuditInventoryWorkflow

// METHODS

-(id)init:(NSString *)username {
    self = [super init:username];
    if (self) {
        self.type = WKFL_INV_AUDIT_INVENTORY;
        self.expectedNumberOfSteps = 4;
    }
    return self;
}

- (void)dispose {
    [super dispose];
}

@end
