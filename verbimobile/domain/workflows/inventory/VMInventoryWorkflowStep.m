//
//  VMInventoryWorkflowStep.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMInventoryWorkflowStep.h"
#import "VMConstants.h"

@implementation VMInventoryWorkflowStep

// METHODS

- (id)init:(VMWorkflow *)workflow {
    self = [super init:workflow];
    if (self) {
        self.category = WKFL_STEP_INV_CATEGORY_NAME;
    }
    return self;
}

- (void)dispose {
    [super dispose];
}

@end
