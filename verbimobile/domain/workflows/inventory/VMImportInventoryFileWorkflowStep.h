//
//  VMImportInventoryFileWorkflowStep.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMInventoryWorkflowStep.h"

@interface VMImportInventoryFileWorkflowStep : VMInventoryWorkflowStep

// METHODS
- (id)init:(VMWorkflow *)workflow;

@end
