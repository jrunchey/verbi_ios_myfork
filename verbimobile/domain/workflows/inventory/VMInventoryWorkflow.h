//
//  VMInventoryWorkflow.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMWorkflow.h"

@interface VMInventoryWorkflow : VMWorkflow

// METHODS
- (id)init:(NSString *)username;

@end
