//
//  VMItemEvent.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMItem.h"

@interface VMItemEvent : NSObject

// GETTERS/SETTERS
@property(nonatomic, strong) NSString *guid;
@property(nonatomic, strong) NSString *itemGUID;
@property(nonatomic, strong) NSString *type;
@property(nonatomic, strong) NSString *locationGUID;
@property(nonatomic, strong) NSString *locationName;
// Date Attributes
@property(nonatomic, strong) NSDate *originationDate;
@property(nonatomic, strong) NSDate *maturityDate;
@property(nonatomic, strong) NSDate *bestByDate;
@property(nonatomic, strong) NSDate *changeDate;
@property(nonatomic, strong) NSDate *retestDate;
@property(nonatomic, strong) NSDate *expirationDate;
// Character Attributes
@property(nonatomic, strong) NSString *territoryCode;        // Country of Origin
@property(nonatomic, strong) NSString *placeOfOrigin;
@property(nonatomic, strong) NSString *lotNumber;
@property(nonatomic, strong) NSString *dateCode;
@property(nonatomic, strong) NSString *grade;
@property(nonatomic, strong) NSString *color;
@property(nonatomic, strong) NSString *status;
@property(nonatomic, strong) NSString *condition;
// Numeric Attributes
@property(nonatomic) float quantity;
@property(nonatomic) float value;
@property(nonatomic) int ageInDays;
@property(nonatomic) int length;
@property(nonatomic) int thickness;
@property(nonatomic) int volume;
@property(nonatomic) int width;
@property(nonatomic) int recycledContent;            // Percentage of Item that is recycled
// Auditing
@property(nonatomic, strong) NSDate *createdDate;
@property(nonatomic, strong) NSDate *lastUpdatedDate;
@property(nonatomic, strong) NSString *lastUpdatedUserId;

// Transient
@property(nonatomic, strong) VMItem *item;
@property(nonatomic, strong) VMLocation *location;
@property(nonatomic, strong) VMUser *lastUpdatedUser;

// METHODS
- (id)init:(NSString *)guid itemGUID:(NSString *)itemGUID locationGUID:(NSString *)locationGUID locationName:(NSString *)locationName quantity:(float)quantity;

/**
 * Creates a new item event by associating item and location references to this item event. If guid is null, autogenerates a new guid.
 */
- (id)init:(NSString *)guid item:(VMItem *)item location:(VMLocation *)location;

/**
 * Creates a new item event by associating item and location references and copying all individual item properties to this item event's properties. If guid is null, autogenerates a new guid.
 */
- (id)initWithCopy:(NSString *)guid item:(VMItem *)item location:(VMLocation *)location;

@end
