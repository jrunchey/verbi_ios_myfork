//
//  VMItem.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMItemMaster.h"
#import "VMLocation.h"

@interface VMItem : NSObject

// GETTERS/SETTERS
@property(nonatomic, strong) NSString *guid;
@property(nonatomic, strong) NSString *itemMasterGUID;
@property(nonatomic, strong) NSString *locationGUID;
// Date Attributes
@property(nonatomic, strong) NSDate *originationDate;
@property(nonatomic, strong) NSDate *maturityDate;
@property(nonatomic, strong) NSDate *bestByDate;
@property(nonatomic, strong) NSDate *changeDate;
@property(nonatomic, strong) NSDate *retestDate;
@property(nonatomic, strong) NSDate *expirationDate;
// Character Attributes
@property(nonatomic, strong) NSString *territoryCode;        // Country of Origin
@property(nonatomic, strong) NSString *placeOfOrigin;
@property(nonatomic, strong) NSString *lotNumber;
@property(nonatomic, strong) NSString *dateCode;
@property(nonatomic, strong) NSString *grade;
@property(nonatomic, strong) NSString *color;
@property(nonatomic, strong) NSString *status;
@property(nonatomic, strong) NSString *condition;
// Numeric Attributes
@property(nonatomic) float quantity;
@property(nonatomic) float value;
@property(nonatomic) int ageInDays;
@property(nonatomic) int length;
@property(nonatomic) int thickness;
@property(nonatomic) int volume;
@property(nonatomic) int width;
@property(nonatomic) int recycledContent;            // Percentage of Item that is recycled
// Auditing
@property(nonatomic, strong) NSDate *createdDate;
@property(nonatomic, strong) NSDate *lastUpdatedDate;
@property(nonatomic, strong) NSString *lastUpdatedUserId;
// Activation/Archiving
@property(nonatomic) BOOL archived;
@property(nonatomic) BOOL enabled;
// EPC
@property(nonatomic, strong) NSMutableArray *epcs;

// Transient
@property(nonatomic, strong) VMItemMaster *itemMaster;
@property(nonatomic, strong) VMLocation *location;
@property(nonatomic, strong) VMUser *lastUpdatedUser;
@property(nonatomic) float expectedQuantity;
@property(nonatomic) float lackQuantity;
@property(nonatomic) float excessQuantity;

// METHODS
- (id)init:(NSString *)guid itemMasterGUID:(NSString *)itemMasterGUID location:(NSString *)locationGUID quantity:(float)quantity;

- (id)init:(NSString *)guid itemMaster:(VMItemMaster *)itemMaster location:(VMLocation *)location quantity:(float)quantity;

@end
