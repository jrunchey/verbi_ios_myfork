//
//  VMImportItem.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VMImportItem : NSObject

@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *description;
@property(nonatomic, strong) NSString *vendorCode;
@property(nonatomic) float quantity;

// METHODS
- (id)init:(NSString *)name description:(NSString *)description vendorCode:(NSString *)vendorCode quantity:(float)quantity;

@end
