//
//  VMItem.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMItem.h"

@implementation VMItem

// GETTERS/SETTERS
@synthesize guid = _guid;
@synthesize itemMasterGUID = _itemMasterGUID;
@synthesize locationGUID = _locationGUID;
@synthesize originationDate = _originationDate;
@synthesize maturityDate = _maturityDate;
@synthesize bestByDate = _bestByDate;
@synthesize changeDate = _changeDate;
@synthesize retestDate = _retestDate;
@synthesize expirationDate = _expirationDate;
@synthesize territoryCode = _territoryCode;
@synthesize placeOfOrigin = _placeOfOrigin;
@synthesize lotNumber = _lotNumber;
@synthesize dateCode = _dateCode;
@synthesize grade = _grade;
@synthesize color = _color;
@synthesize status = _status;
@synthesize condition = _condition;
@synthesize quantity = _quantity;
@synthesize value = _value;
@synthesize ageInDays = _ageInDays;
@synthesize length = _length;
@synthesize thickness = _thickness;
@synthesize volume = _volume;
@synthesize width = _width;
@synthesize recycledContent = _recycledContent;
@synthesize createdDate = _createdDate;
@synthesize lastUpdatedDate = _lastUpdatedDate;
@synthesize lastUpdatedUserId = _lastUpdatedUserId;
@synthesize enabled = _enabled;
@synthesize archived = _archived;
@synthesize epcs = _epcs;
@synthesize itemMaster = _itemMaster;
@synthesize location = _location;
@synthesize lastUpdatedUser = _lastUpdatedUser;
@synthesize expectedQuantity = _expectedQuantity;
@synthesize lackQuantity = _lackQuantity;
@synthesize excessQuantity = _excessQuantity;

// METHODS

- (id)init:(NSString *)guid itemMasterGUID:(NSString *)itemMasterGUID location:(NSString *)locationGUID quantity:(float)quantity {
    self = [super init];
    if (self) {
        if (guid)
            self.guid = [guid copy];
        else {
            CFUUIDRef uuidObj = CFUUIDCreate(nil);//create a new UUID
            self.guid = ( NSString*)CFBridgingRelease(CFUUIDCreateString(nil, uuidObj)); //get string
        }
        self.itemMasterGUID = [itemMasterGUID copy];
        self.locationGUID = [locationGUID copy];
        self.quantity = quantity;
        self.expectedQuantity = quantity;
        self.enabled = YES;
        self.archived = NO;
        self.createdDate = [NSDate date];
        self.lastUpdatedDate = [NSDate date];
    }
    return self;
}

- (id)init:(NSString *)guid itemMaster:(VMItemMaster *)itemMaster location:(VMLocation *)location quantity:(float)quantity {
    self = [super init];
    if (self) {
        if (guid)
            self.guid = guid;
        else {
            CFUUIDRef uuidObj = CFUUIDCreate(nil);//create a new UUID
            self.guid = ( NSString*)CFBridgingRelease(CFUUIDCreateString(nil, uuidObj)); //get string
        }
        if (itemMaster) {
            self.itemMaster = itemMaster;
            self.itemMasterGUID = itemMaster.guid;
        }
        if (location) {
            self.location = location;
            self.locationGUID = location.guid;
        }
        self.quantity = quantity;
        self.expectedQuantity = quantity;
        self.enabled = YES;
        self.archived = NO;
        self.createdDate = [NSDate date];
        self.lastUpdatedDate = [NSDate date];
    }
    return self;
}

@end
