//
//  VMImportItem.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMImportItem.h"

@implementation VMImportItem

// GETTERS/SETTERS
@synthesize name = _name;
@synthesize description = _description;
@synthesize vendorCode = _vendorCode;
@synthesize quantity = _quantity;

// METHODS
- (id)init:(NSString *)name description:(NSString *)description vendorCode:(NSString *)vendorCode quantity:(float)quantity {
    self = [super init];
    if (self) {
        self.name = name;
        self.description = description;
        self.vendorCode = vendorCode;
        self.quantity = quantity;
    }
    return self;
}

@end
