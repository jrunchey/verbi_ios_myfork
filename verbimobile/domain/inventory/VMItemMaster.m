//
//  VMItemMaster.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMItemMaster.h"

@implementation VMItemMaster

// GETTERS/SETTERS
@synthesize guid = _guid;
@synthesize name = _name;
@synthesize type = _type;         // countable, weighable
@synthesize description = _description;
@synthesize upc = _upc;
@synthesize vendor = _vendor;
@synthesize vendorCode = _vendorCode;
@synthesize reorderPointQuantity = _reorderPointQuantity;
@synthesize leadTimeDays = _leadTimeDays;
@synthesize safetyStockQuantity = _safetyStockQuantity;
@synthesize optimumOrderingQuantity = _optimumOrderingQuantity;
@synthesize incrementQuantity = _incrementQuantity;
@synthesize price = _price;
@synthesize uom = _uom;          // unit of measure
@synthesize image = _image;
@synthesize expiration = _expiration;
@synthesize expirationTimePeriod = _expirationTimePeriod;
@synthesize expirationTimeUnit = _expirationTimeUnit;
@synthesize createdDate = _createdDate;
@synthesize lastUpdatedDate = _lastUpdatedDate;
@synthesize lastUpdatedUserId = _lastUpdatedUserId;
@synthesize enabled = _enabled;
@synthesize archived = _archived;
@synthesize lastUpdatedUser = _lastUpdatedUser;

// METHODS
- (id)init:(NSString *)guid name:(NSString *)name type:(NSString *)type description:(NSString *)description upc:(NSString *)upc vendor:(NSString *)vendor vendorCode:(NSString *)vendorCode incrementQuantity:(float)incrementQuantity price:(float)price uom:(NSString *)uom image:(UIImage *)image {
    self = [super init];
    if (self) {
        if (guid)
            self.guid = [guid copy];
        else {
            CFUUIDRef uuidObj = CFUUIDCreate(nil);//create a new UUID
            self.guid = (__bridge NSString*)CFUUIDCreateString(nil, uuidObj); //get string
        }
        self.name = name;
        self.type = type;
        self.description = description;
        self.upc = upc;
        self.vendor = vendor;
        self.vendorCode = vendorCode;
        self.incrementQuantity = incrementQuantity;
        self.price = price;
        self.uom = uom;
        self.image = image;
        self.enabled = YES;
        self.archived = NO;
        self.createdDate = [NSDate date];
        self.lastUpdatedDate = [NSDate date];
    }
    return self;
}

@end
