//
//  VMItemMaster.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMUser.h"

@interface VMItemMaster : NSObject

// GETTERS/SETTERS
@property(nonatomic, strong) NSString *guid;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *type;        // fixed or movable
@property(nonatomic, strong) NSString *description;
@property(nonatomic, strong) NSString *upc;
@property(nonatomic, strong) NSString *vendor;
@property(nonatomic, strong) NSString *vendorCode;
@property(nonatomic) float reorderPointQuantity;
@property(nonatomic) int leadTimeDays;
@property(nonatomic) float safetyStockQuantity;
@property(nonatomic) float optimumOrderingQuantity;
@property(nonatomic) float incrementQuantity;
@property(nonatomic) float price;
@property(nonatomic, strong) NSString *uom;
@property(nonatomic, strong) UIImage *image;
@property(nonatomic) BOOL expiration;
@property(nonatomic) int expirationTimePeriod;
@property(nonatomic, strong) NSString *expirationTimeUnit;
// Auditing
@property(nonatomic, strong) NSDate *createdDate;
@property(nonatomic, strong) NSDate *lastUpdatedDate;
@property(nonatomic, strong) NSString *lastUpdatedUserId;
// Activation/Archiving
@property(nonatomic) BOOL enabled;
@property(nonatomic) BOOL archived;

// Transient
@property(nonatomic, strong) VMUser *lastUpdatedUser;

// METHODS
- (id)init:(NSString *)guid name:(NSString *)name type:(NSString *)type description:(NSString *)description upc:(NSString *)upc vendor:(NSString *)vendor vendorCode:(NSString *)vendorCode incrementQuantity:(float)incrementQuantity price:(float)price uom:(NSString *)uom image:(UIImage *)image;

@end
