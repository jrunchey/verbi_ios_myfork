//
//  VMItemEvent.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMItemEvent.h"

@implementation VMItemEvent

// GETTERS/SETTERS
@synthesize guid = _guid;
@synthesize itemGUID = _itemGUID;
@synthesize type = _type;
@synthesize locationGUID = _locationGUID;
@synthesize locationName = _locationName;
@synthesize originationDate = _originationDate;
@synthesize maturityDate = _maturityDate;
@synthesize bestByDate = _bestByDate;
@synthesize changeDate = _changeDate;
@synthesize retestDate = _retestDate;
@synthesize expirationDate = _expirationDate;
@synthesize territoryCode = _territoryCode;
@synthesize placeOfOrigin = _placeOfOrigin;
@synthesize lotNumber = _lotNumber;
@synthesize dateCode = _dateCode;
@synthesize grade = _grade;
@synthesize color = _color;
@synthesize status = _status;
@synthesize condition = _condition;
@synthesize quantity = _quantity;
@synthesize value = _value;
@synthesize ageInDays = _ageInDays;
@synthesize length = _length;
@synthesize thickness = _thickness;
@synthesize volume = _volume;
@synthesize width = _width;
@synthesize recycledContent = _recycledContent;
@synthesize createdDate = _createdDate;
@synthesize lastUpdatedDate = _lastUpdatedDate;
@synthesize lastUpdatedUserId = _lastUpdatedUserId;
@synthesize item = _item;
@synthesize location = _location;
@synthesize lastUpdatedUser = _lastUpdatedUser;

// METHODS

- (id)init:(NSString *)guid itemGUID:(NSString *)itemGUID locationGUID:(NSString *)locationGUID locationName:(NSString *)locationName quantity:(float)quantity {
    self = [super init];
    if (self) {
        if (guid)
            self.guid = guid;
        else {
            CFUUIDRef uuidObj = CFUUIDCreate(nil);//create a new UUID
            self.guid = (__bridge NSString*)CFUUIDCreateString(nil, uuidObj); //get string
        }
        self.itemGUID = itemGUID;
        self.locationGUID = locationGUID;
        self.locationName = locationName;
        self.createdDate = [NSDate date];
        self.lastUpdatedDate = [NSDate date];
    }
    return self;
}

/**
 * Creates a new item event by associating item and location references to this item event. If guid is null, autogenerates a new guid.
 */
- (id)init:(NSString *)guid item:(VMItem *)item location:(VMLocation *)location {
    self = [super init];
    if (self) {
        if (guid)
            self.guid = guid;
        else {
            CFUUIDRef uuidObj = CFUUIDCreate(nil);//create a new UUID
            self.guid = (__bridge NSString*)CFUUIDCreateString(nil, uuidObj); //get string
        }
        if (item) {
            self.item = item;
            self.itemGUID = item.guid;
        }
        if (location) {
            self.location = location;
            self.locationGUID = location.guid;
            self.locationName = location.name;
        }
        self.createdDate = [NSDate date];
        self.lastUpdatedDate = [NSDate date];
    }
    return self;
}


/**
 * Creates a new item event by associating item and location references and copying all individual item properties to this item event's properties. If guid is null, autogenerates a new guid.
 */
- (id)initWithCopy:(NSString *)guid item:(VMItem *)item location:(VMLocation *)location {
    self = [super init];
    if (self) {
        if (guid)
            self.guid = guid;
        else {
            CFUUIDRef uuidObj = CFUUIDCreate(nil);//create a new UUID
            self.guid = (__bridge NSString*)CFUUIDCreateString(nil, uuidObj); //get string
        }
        if (item) {
            self.item = item;
            self.itemGUID = item.guid;
        }
        if (location) {
            self.location = location;
            self.locationGUID = location.guid;
            self.locationName = location.name;
        }
        if (item.originationDate) self.originationDate = item.originationDate;
        if (item.maturityDate) self.maturityDate = item.maturityDate;
        if (item.bestByDate) self.bestByDate = item.bestByDate;
        if (item.changeDate) self.changeDate = item.changeDate;
        if (item.retestDate) self.retestDate = item.retestDate;
        if (item.expirationDate) self.expirationDate = item.expirationDate;
        if (item.territoryCode) self.territoryCode = item.territoryCode;
        if (item.placeOfOrigin) self.placeOfOrigin = item.placeOfOrigin;
        if (item.lotNumber) self.lotNumber = item.lotNumber;
        if (item.dateCode) self.dateCode = item.dateCode;
        if (item.grade) self.grade = item.grade;
        if (item.color) self.color = item.color;
        if (item.status) self.status = item.status;
        if (item.condition) self.condition = item.condition;
        self.quantity = item.quantity;
        self.value = item.value;
        self.ageInDays = item.ageInDays;
        self.length = item.length;
        self.thickness = item.thickness;
        self.volume = item.volume;
        self.width = item.width;
        self.recycledContent = item.recycledContent;
        if (item.createdDate) self.createdDate = item.createdDate;
        if (item.lastUpdatedDate) self.lastUpdatedDate = item.lastUpdatedDate;
        if (item.lastUpdatedUserId) self.lastUpdatedUserId = item.lastUpdatedUserId;
        if (item.lastUpdatedUser) self.lastUpdatedUser = item.lastUpdatedUser;
        self.createdDate = [NSDate date];
        self.lastUpdatedDate = [NSDate date];
    }
    return self;
}

@end
