//
//  VMHardwareProfile.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMHardwareProfile.h"

@implementation VMHardwareProfile

#pragma mark -
#pragma mark GETTERS/SETTERS
/****************************************************************************
 * GETTERS/SETTERS
 ****************************************************************************/

@synthesize shortReadTxPower = _shortReadTxPower;
@synthesize shortReadRxPower = _shortReadRxPower;
@synthesize shortWriteTxPower = _shortWriteTxPower;
@synthesize shortWriteRxPower = _shortWriteRxPower;
@synthesize longReadTxPower = _longReadTxPower;
@synthesize longReadRxPower = _longReadRxPower;

#pragma mark -
#pragma mark CONSTRUCTOR METHODS
/****************************************************************************
 * CONSTRUCTOR METHODS
 ****************************************************************************/

- (id)init:(NSInteger)shortReadTxPower shortReadRxPower:(NSInteger)shortReadRxPower shortWriteTxPower:(NSInteger)shortWriteTxPower shortWriteRxPower:(NSInteger)shortWriteRxPower longReadTxPower:(NSInteger)longReadTxPower longReadRxPower:(NSInteger)longReadRxPower {
    self = [super init];
    if (self) {
        self.shortReadTxPower = shortReadTxPower;
        self.shortReadRxPower = shortReadRxPower;
        self.shortWriteTxPower = shortWriteTxPower;
        self.shortWriteRxPower = shortWriteRxPower;
        self.longReadTxPower = longReadTxPower;
        self.longReadRxPower = longReadRxPower;
    }
    return self;
}

#pragma mark -
#pragma mark DECONSTRUCTOR METHODS
/****************************************************************************
 * DECONSTRUCTOR METHODS
 ****************************************************************************/

-(void)dispose {
    self.shortReadTxPower = 0;
    self.shortReadRxPower = 0;
    self.shortWriteTxPower = 0;
    self.shortWriteRxPower = 0;
    self.longReadTxPower = 0;
    self.longReadRxPower = 0;
}

@end
