//
//  VMHardwareProfile.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VMHardwareProfile : NSObject

#pragma mark -
#pragma mark GETTERS/SETTERS

@property(nonatomic) int shortReadTxPower;
@property(nonatomic) int shortReadRxPower;
@property(nonatomic) int shortWriteTxPower;
@property(nonatomic) int shortWriteRxPower;
@property(nonatomic) int longReadTxPower;
@property(nonatomic) int longReadRxPower;

#pragma mark -
#pragma mark CONSTRUCTOR METHODS

- (id)init:(int)pShortReadTxPower shortReadRxPower:(int)pShortReadRxPower shortWriteTxPower:(int)pShortWriteTxPower shortWriteRxPower:(int)pShortWriteRxPower longReadTxPower:(int)pLongReadTxPower longReadRxPower:(int)pLongReadRxPower;

#pragma mark -
#pragma mark DECONSTRUCTOR METHODS

-(void)dispose;

@end
