//
//  LoginViewController.m
//  verbimobile
//
//  Created by Rob Hotaling on 3/10/13.
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "LoginViewController.h"
#import "ECSlidingViewController.h"
#import "LeftMenuViewController.h"
#import "RightMenuViewController.h"
#import "VMAccessControlService.h"
#import "VMSession.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/
@synthesize usernameTextField = _usernameTextField;
@synthesize passwordTextField = _passwordTextField;
@synthesize loginBtn = _loginBtn;

#pragma mark -
#pragma mark Lifecycle Methods
/****************************************************************************
 * Lifecycle Methods
 ****************************************************************************/
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // These 3 give effect that view controller is hovering slightly above menu
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    // Checks to see if we already have a Menu view controller from storyboard underneath our menu. If we don't, then create it
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[LeftMenuViewController class]]) {
        self.slidingViewController.underLeftViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LeftMenu"];
    }
    if (![self.slidingViewController.underRightViewController isKindOfClass:[RightMenuViewController class]]) {
        self.slidingViewController.underRightViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"RightMenu"];
    }
    
    // gives us the ability to swipe from left to right to reveal menu
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    // When the view has been loaded we register ourselves as the UITextFieldDelegate for both text fields
    self.usernameTextField.delegate = self;
    self.passwordTextField.delegate = self;
    
    // Focus usernameField with cursor
    [self.usernameTextField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)revealLeftMenu:(id)sender {
    [self.slidingViewController anchorTopViewTo:ECRight];
}

- (IBAction)revealRightMenu:(id)sender {
    [self.slidingViewController anchorTopViewTo:ECLeft];
}

- (void)viewDidUnload {
    [self setUsernameTextField:nil];
    [self setPasswordTextField:nil];
    [self setLoginBtn:nil];
    [super viewDidUnload];
}

- (IBAction)loginBtnPressed:(UIButton *)sender {
    // When login button has been pressed validate credentials with the Authenticator and if valid, get back session
    VMSession *session = [VMAccessControlService authenticate:self.usernameTextField.text password:self.passwordTextField.text];
    
    // If authentication fails or user didn't exist we message the user, else load main view controller
    if (session == nil) {
        NSString *alertViewTitle = @"Log In";
        NSString *alertMessage = @"Invalid username or password";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else {
        if (session.user.username)
            NSLog(@"Session authenticated for user [%@]", session.user.username);
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        [self dismissViewControllerAnimated:YES completion:nil];
        UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Main"];
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;    // moves it onto the top
        [self.slidingViewController resetTopView];  // slides it back
    }
}

// Hides keyboard when losing focus on a UITextView
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    
    if ([self.usernameTextField isFirstResponder] && [touch view] != self.usernameTextField) {
        [self.usernameTextField resignFirstResponder];
    }
    else if ([self.passwordTextField isFirstResponder] && [touch view] != self.passwordTextField) {
        [self.passwordTextField resignFirstResponder];
    }
    
    [super touchesBegan:touches withEvent:event];
}

// Handles when you press return in either text field
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    // If you pressed return in the password text field we will trigger loginPressed for you
    if (textField.tag == kPasswordFieldTag){
        [self loginBtnPressed:nil];
    }
    
    // this closed the keyboard if you press return in either text field
    return [textField resignFirstResponder];
}

// If you leave the text field we will close the keyboard
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
}

@end
