//
//  ConnectViewController.h
//  verbimobile
//
//  Created by Rob Hotaling on 3/8/13.
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMAutoIDCapableVC.h"

@interface ConnectViewController : VMAutoIDCapableVC <UITableViewDelegate,UITableViewDataSource, UIAlertViewDelegate, CBCentralManagerDelegate, CBPeripheralDelegate>

@property (strong, nonatomic) IBOutlet UIBarButtonItem *leftMenuBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *rightMenuBtn;
@property (strong, nonatomic) IBOutlet UILabel *connectLabel;
@property (strong, nonatomic) IBOutlet UITableView *devicesTableView;

@end
