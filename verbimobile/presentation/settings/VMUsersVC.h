//
//  VMUsersVC.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMParentVC.h"

@interface VMUsersVC : VMParentVC<UITableViewDelegate,UITableViewDataSource, UIAlertViewDelegate>

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@property (nonatomic, weak) IBOutlet UIButton *addButton;
@property (nonatomic, weak) IBOutlet UIButton *deleteButton;
@property (nonatomic, weak) IBOutlet UIButton *editButton;
@property (nonatomic, weak) IBOutlet UIButton *selectAllButton;
@property (nonatomic, weak) IBOutlet UITableView *usersTableView;

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO Methods
 ****************************************************************************/

- (IBAction)addPressed:(id)sender;
- (IBAction)deletePressed:(id)sender;
- (IBAction)editPressed:(id)sender;
- (IBAction)selectAllButtonPressed:(id)sender;

@end
