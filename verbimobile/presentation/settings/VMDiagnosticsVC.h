//
//  VMDiagnosticsVC.h
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMAutoIDCapableVC.h"

@interface VMDiagnosticsVC : VMAutoIDCapableVC <UITableViewDelegate,UITableViewDataSource, UIAlertViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tagsTableView;

@end
