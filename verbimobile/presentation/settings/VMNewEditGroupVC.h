//
//  VMNewEditGroupVC.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMAutoIDCapableVC.h"
#import "VMGroup.h"

@interface VMNewEditGroupVC : VMAutoIDCapableVC {
    // Edit
    VMGroup *__weak groupToEdit;
}

// Edit Properties
@property(weak, nonatomic) VMGroup *groupToEdit;

@end
