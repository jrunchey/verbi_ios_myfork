//
//  VMDevicesVC.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMDevicesVC.h"
//#import "VMViewController.h"
#import "VMSession.h"

@interface VMDevicesVC ()
@property(strong, nonatomic) NSMutableArray *devices;
@property(strong, nonatomic) NSMutableArray *deviceUUIDs;
@end

@implementation VMDevicesVC

// GETTERS/SETTERS

//@synthesize bleShield = _bleShield;
@synthesize devicesTableView = _devicesTableView;
@synthesize statusLabel = _statusLabel;
@synthesize connectButton = _connectButton;
@synthesize devices = _devices;
@synthesize deviceUUIDs = _deviceUUIDs;

#pragma mark -
#pragma mark View Lifecycle Public
/****************************************************************************
 * View Lifecycle Public
 ****************************************************************************/

// Called from viewDidLoad
- (void)initializeUIOnFirstLoad {
    @try {
        [super initializeUIOnFirstLoad];
        
        VMSession *session = [VMSession sharedInstance];
        if (session) {
            if (session.rfidReader.bleShield) {
                if (session.rfidReader.bleShield.activePeripheral) {
                    if (self.devices == nil)
                        self.devices = [[NSMutableArray alloc] init];
                    if (self.deviceUUIDs == nil)
                        self.deviceUUIDs = [[NSMutableArray alloc] init];
                    // Add active peripheral to devices array so it can be auto binded to devicesTableView
                    [self.devices addObject:session.rfidReader.bleShield.activePeripheral];
                    [self.deviceUUIDs addObject:(__bridge NSString*)CFUUIDCreateString(nil, session.rfidReader.bleShield.activePeripheral.UUID)];
                    [self.devicesTableView reloadData];
                    // If active peripheral is connected
                    if (session.rfidReader.bleShield.activePeripheral.isConnected) {
                        // Loop thru devicesTableView and set text color to blue to indicate it's already connected
                        int rowCount = [self.devicesTableView numberOfRowsInSection:0];
                        for (int i = 0; i < rowCount; i++) {
                            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
                            UITableViewCell *cell = [self.devicesTableView cellForRowAtIndexPath:indexPath];
                            cell.textLabel.textColor = [UIColor blueColor];
                            cell.accessoryType = UITableViewCellAccessoryNone;
                            break;
                        }
                        // Update button
                        [self.connectButton setTitle:@"Disconnect" forState:UIControlStateNormal];
                    }
                    // Update status label
                    NSString *status = [[NSString alloc] initWithFormat:@"%i devices found", self.devices.count];
                    self.statusLabel.text = status;
                }
            }
            else {
                NSLog(@"No RFID reader detected");
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

// Called from viewWillAppear
- (void)initializeUIBeforeRendering {
    @try {
        [super initializeUIBeforeRendering];
        self.activityIndicatorView.hidden = YES;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

- (void)dispose {
    @try {
        //[VMSession sharedInstance].rfidReader.bleShield = self.bleShield; // assign self's bleShield to the session
        
        if (self.statusLabel)
            self.statusLabel = nil;
        if (self.connectButton)
            self.connectButton = nil;
        if (self.devices) {
            [self.devices removeAllObjects];
            self.devices = nil;
        }
        if (self.deviceUUIDs) {
            [self.deviceUUIDs removeAllObjects];
            self.deviceUUIDs = nil;
        }
        
        [super dispose];
        
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO Methods
 ****************************************************************************/

- (void)backPressed:(id)sender{
    @try {
        [super backPressed:sender];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
        VMSession *session = [VMSession sharedInstance];
        if (session && session.rfidReader)
            session.rfidReader.delegate = nil;
    }
}

- (IBAction)scanButtonPressed:(id)sender {
    @try {
        VMSession *session = [VMSession sharedInstance];
        if (session.rfidReader.bleShield.activePeripheral) {
            if(session.rfidReader.bleShield.activePeripheral.isConnected) {
                [[session.rfidReader.bleShield CM] cancelPeripheralConnection:[session.rfidReader.bleShield activePeripheral]];
                return;
            }
        }

        if (session.rfidReader.bleShield.peripherals)
            session.rfidReader.bleShield.peripherals = nil;
        [self.devices removeAllObjects];
        self.devices = nil;
        [self.deviceUUIDs removeAllObjects];
        self.deviceUUIDs = nil;
        [self.devicesTableView reloadData];
        
        self.activityIndicatorView.hidden = NO;
        [self.activityIndicatorView startAnimating];
        self.statusLabel.text = @"Scanning...";
//        [session.rfidReader.bleShield findBLEPeripherals:[BLE_TIMEOUT intValue]];
//        [NSTimer scheduledTimerWithTimeInterval:(float)[BLE_TIMEOUT floatValue] target:self selector:@selector(connectionTimer:) userInfo:nil repeats:NO];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

- (IBAction)connectButtonPressed:(id)sender{
    @try {
        NSString *alertViewTitle = @"Connect";
        NSArray *indexes = [VMUtilityService getCheckedOrSelectedIndexes:self.devicesTableView];
        if (indexes.count == 0) {
            NSString *alertMessage = @"Select a device.";
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: alertViewTitle
                                  message: alertMessage
                                  delegate: self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
        else if (indexes.count == 1) {
            self.activityIndicatorView.hidden = NO;
            [self.activityIndicatorView startAnimating];
            
            for (NSIndexPath *path in indexes) {
                [self connectToDevice:path];
                break;
            }
        }
        else {
            NSString *alertMessage = @"Select a single device.";
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: alertViewTitle
                                  message: alertMessage
                                  delegate: self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

- (IBAction)viewButtonPressed:(id)sender {
    @try {
        NSString *alertViewTitle = @"View";
        NSArray *indexes = [VMUtilityService getCheckedOrSelectedIndexes:self.devicesTableView];
        if (indexes.count == 0) {
            NSString *alertMessage = @"Select a device.";
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: alertViewTitle
                                  message: alertMessage
                                  delegate: self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
        else if (indexes.count == 1) {
            for (NSIndexPath *path in indexes) {
                [self viewDevice:path];
                break;
            }
        }
        else {
            NSString *alertMessage = @"Select a single device.";
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: alertViewTitle
                                  message: alertMessage
                                  delegate: self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		// do nothing except close alertView
	}
	else {
	}
}

#pragma mark -
#pragma mark Table view methods
/****************************************************************************
 * Table view Methods
 ****************************************************************************/

// Defines the table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Defines the table - Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.devices.count;
}

// Fill the table - Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    if (self.devices && self.devices.count > 0) {
        CBPeripheral *peripheral = [self.devices objectAtIndex:indexPath.row];
        if (peripheral && peripheral.name) {

            // Cell Title Text
            NSString *uuid = (__bridge NSString*)CFUUIDCreateString(nil, peripheral.UUID);
            if (uuid == nil)
                uuid = @"Unknown";
            NSNumber *rssi = peripheral.RSSI;
            if (rssi == nil)
                rssi = [NSNumber numberWithFloat:0];
            cell.textLabel.text = [NSString stringWithFormat:@"[%@] %@", rssi, peripheral.name];
            
            // Cell Subtitle Text
            NSString *detailText = [NSString stringWithFormat:@"%@", uuid];
            cell.detailTextLabel.text = detailText;
        }
        
        // Convert all row selections to check marks
        NSIndexPath *selection = [tableView indexPathForSelectedRow];
        if (selection && selection.row == indexPath.row) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        // uncheck
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else {
        // check
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark -
#pragma mark Helper Methods
/****************************************************************************
 * Helper Methods
 ****************************************************************************/

// Handles connection to or disconnection from device
-(void)connectToDevice:(NSIndexPath *)indexPath {
    @try {
        VMSession *session = [VMSession sharedInstance];
        NSLog(@"Getting active BT device...");
        CBPeripheral *activeDevice = [session.rfidReader.bleShield activePeripheral];
        NSString *activeDeviceUUID = nil;
        if (activeDevice) {
            NSLog(@"Active BT device '%@' found.", activeDevice.name);
            if (activeDevice.UUID) {
                NSLog(@"Getting UUID of active BT device '%@'...", activeDevice.name);
                activeDeviceUUID = (__bridge NSString*)CFUUIDCreateString(nil, activeDevice.UUID);
                if (activeDeviceUUID)
                    NSLog(@"Active BT device '%@' UUID: %@", activeDevice.name, activeDeviceUUID);
            }
        }
        else {
            NSLog(@"Active BT device not found.");
        }
        
        NSLog(@"Getting selected BT device from devices array");
        NSUInteger index = [indexPath indexAtPosition:[indexPath length] - 1];
        CBPeripheral *selectedDevice = [self.devices objectAtIndex:index];
        NSString *selectedDeviceUUID = nil;
        if (selectedDevice) {
            NSLog(@"Selected BT device '%@' found", selectedDevice.name);
            if (selectedDevice.UUID) {
                NSLog(@"Getting UUID of selected BT device '%@'...", selectedDevice.name);
                selectedDeviceUUID = (__bridge NSString*)CFUUIDCreateString(nil, selectedDevice.UUID);
                if (selectedDeviceUUID)
                    NSLog(@"Selected BT device '%@' UUID: %@", selectedDevice.name, selectedDeviceUUID);
            }
            
            if (activeDevice == nil) {
                NSLog(@"No active BT device found. Connecting to selected BT device: %@", selectedDevice.name);
                [session.rfidReader.bleShield connectPeripheral:selectedDevice];
                NSLog(@"Now connected to selected BT device: %@", selectedDevice.name);
            }
            else if (activeDevice && (!activeDevice.isConnected || (activeDevice.isConnected && activeDeviceUUID && [[activeDeviceUUID lowercaseString] caseInsensitiveCompare:selectedDeviceUUID] != NSOrderedSame))) {
                NSLog(@"Active BT device is disconnected. Connecting to selected BT device: %@", selectedDevice.name);
                [session.rfidReader.bleShield connectPeripheral:selectedDevice];
                [session.rfidReader.bleShield setActivePeripheral:selectedDevice];
                NSLog(@"Now connected to selected BT device: %@", selectedDevice.name);
            }
            else {
                NSLog(@"Selected BT device found. Disconnecting from selected BT device: %@", selectedDevice.name);
                [session.rfidReader.bleShield setActivePeripheral:selectedDevice];
                if (session.rfidReader.bleShield.activePeripheral && session.rfidReader.bleShield.activePeripheral.isConnected) {
                    CBCentralManager *cbCentralManager = [session.rfidReader.bleShield CM];
                    if (cbCentralManager)
                        [cbCentralManager cancelPeripheralConnection:selectedDevice];
                    session.rfidReader.bleShield.activePeripheral = nil;
                }
                NSLog(@"Now disconnected to selected BT device: %@", selectedDevice.name);
            }
        }
        else {
            NSLog(@"Selected BT device not found in devices array");
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

-(void)viewDevice:(NSIndexPath *)indexPath {
    @try {
        VMSession *session = [VMSession sharedInstance];
        CBPeripheral *activeDevice = [session.rfidReader.bleShield activePeripheral];
        NSString *activeDeviceUUID = nil;
        if (activeDevice)
            activeDeviceUUID = (__bridge NSString*)CFUUIDCreateString(nil, activeDevice.UUID);
        
        NSUInteger index = [indexPath indexAtPosition:[indexPath length] - 1];
        CBPeripheral *selectedDevice = [self.devices objectAtIndex:index];
        NSString *selectedDeviceUUID = nil;
        if (selectedDevice) {
            selectedDeviceUUID = (__bridge NSString*)CFUUIDCreateString(nil, selectedDevice.UUID);
        }
        
        NSString *alertViewTitle = @"View";
        NSMutableString *alertMessage = [[NSMutableString alloc] initWithFormat:@"Name: %@\n\nUUID: %@\n\nRSSI: %@", selectedDevice.name, selectedDeviceUUID, selectedDevice.RSSI];
        
        if (activeDevice && activeDevice.isConnected && activeDeviceUUID && [[activeDeviceUUID lowercaseString] caseInsensitiveCompare:selectedDeviceUUID] == NSOrderedSame) {
            [alertMessage appendString:@"\n\nYou are connected to this device."];
        }
        
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

#pragma mark BLEShield Methods

// Called when scan period is over to connect to the first found peripheral
-(void) connectionTimer:(NSTimer *)timer {
    VMSession *session = [VMSession sharedInstance];
    int devicesFound = session.rfidReader.bleShield.peripherals.count;
    if(devicesFound > 0)
    {
        self.devices = [session.rfidReader.bleShield peripherals];
        if (self.devices && self.devices.count > 0) {
            [self.deviceUUIDs removeAllObjects];
            for (CBPeripheral *peripheral in self.devices) {
                [self.deviceUUIDs addObject:(__bridge NSString*)CFUUIDCreateString(nil, peripheral.UUID)];
            }
        }
        [self.devicesTableView reloadData];
        NSString *status = [[NSString alloc] initWithFormat:@"%i devices found", devicesFound];
        self.statusLabel.text = status;
    }
    else
    {
        self.statusLabel.text = @"No devices found";
    }
    
    [self.activityIndicatorView stopAnimating];
    self.activityIndicatorView.hidden = YES;
}

#pragma mark -
#pragma mark VMRFIDReaderDelegate Methods
/****************************************************************************
 * VMRFIDReaderDelegate Methods
 ****************************************************************************/

-(void)handleRFIDReaderStatusEvent:(NSString *)description event:(VMRFIDReaderEvent *)event {
    NSString *status = [event.details objectForKey:@"status"];
    NSString *name = [event.details objectForKey:@"name"];
    NSString *uuid = [event.details objectForKey:@"uuid"];
    NSNumber *rssi = [event.details objectForKey:@"rssi"];
    //NSLog(@"Received RFID reader status event: Status = %@; Name = %@; UUID = %@; RSSI = %@", status, name, uuid, rssi);
    
    if ([[status lowercaseString] caseInsensitiveCompare:@"connected"] == NSOrderedSame) {
        [self.activityIndicatorView stopAnimating];
        self.activityIndicatorView.hidden = YES;
        
        NSUInteger index = [self.deviceUUIDs indexOfObject:uuid];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];    // convert iterator to indexPath
        UITableViewCell *cell = [self.devicesTableView cellForRowAtIndexPath:indexPath];
        cell.textLabel.textColor = [UIColor blueColor];
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        [self.connectButton setTitle:@"Disconnect" forState:UIControlStateNormal];
        [VMSession sharedInstance].rfidReader.connected = YES;
        [self.activityIndicatorView stopAnimating];
        self.activityIndicatorView.hidden = YES;
    }
    else if ([[status lowercaseString] caseInsensitiveCompare:@"disconnected"] == NSOrderedSame) {
        NSUInteger index = [self.deviceUUIDs indexOfObject:uuid];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];    // convert iterator to indexPath
        UITableViewCell *cell = [self.devicesTableView cellForRowAtIndexPath:indexPath];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.accessoryType = UITableViewCellAccessoryNone;
        [self.connectButton setTitle:@"Connect" forState:UIControlStateNormal];
        [VMSession sharedInstance].rfidReader.connected = NO;
        
        [self.activityIndicatorView stopAnimating];
        self.activityIndicatorView.hidden = YES;
    }
    else if ([[status lowercaseString] caseInsensitiveCompare:@"rssi update"] == NSOrderedSame) {
        NSUInteger index = [self.deviceUUIDs indexOfObject:uuid];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];    // convert iterator to indexPath
        UITableViewCell *cell = [self.devicesTableView cellForRowAtIndexPath:indexPath];
        cell.textLabel.text = [NSString stringWithFormat:@"[%@] %@", rssi, name];
        
        [self.activityIndicatorView stopAnimating];
        self.activityIndicatorView.hidden = YES;
    }
    else {
    }
}

@end
