//
//  VMGroupsVC.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMParentVC.h"

@interface VMGroupsVC : VMParentVC<UITableViewDelegate,UITableViewDataSource, UIAlertViewDelegate> {
    // Buttons
    UIButton *addButton;
    UIButton *deleteButton;
    UIButton *editButton;
    UIButton *selectAllButton;
    
    // Table View
    UITableView *groupsTableView;
    NSMutableArray *groups;
}

// GETTER/SETTERS
@property (nonatomic, strong) IBOutlet UIButton *addButton;
@property (nonatomic, strong) IBOutlet UIButton *deleteButton;
@property (nonatomic, strong) IBOutlet UIButton *editButton;
@property (nonatomic, strong) IBOutlet UIButton *selectAllButton;
@property (nonatomic, strong) IBOutlet UITableView *groupsTableView;

// Button press methods
- (IBAction)addPressed:(id)sender;
- (IBAction)deletePressed:(id)sender;
- (IBAction)editPressed:(id)sender;
- (IBAction)selectAllButtonPressed:(id)sender;
@end
