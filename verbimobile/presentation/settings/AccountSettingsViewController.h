//
//  AccountSettingsViewController.h
//  verbimobile
//
//  Created by Rob Hotaling on 3/19/13.
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountSettingsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIBarButtonItem *leftMenuBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *rightMenuBtn;

@end
