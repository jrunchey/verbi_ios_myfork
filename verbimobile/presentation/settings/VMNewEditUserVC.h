//
//  VMNewEditUserVC.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMAutoIDCapableVC.h"
#import "VMUser.h"

@interface VMNewEditUserVC : VMAutoIDCapableVC <UIAlertViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate>

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

// Edit Properties
@property(nonatomic, strong) VMUser *userToEdit;

// Scroll View Properties
@property(nonatomic, weak) IBOutlet UIScrollView *scrollView;

// Picker Properties
@property(nonatomic, weak) IBOutlet UIPickerView *accessLevelPickerView;
//@property (strong, nonatomic) NSArray *accessLevelPickerArray;

// Image View
@property(nonatomic, weak) IBOutlet UIImageView *userImageView;
@property(nonatomic, strong) UIPopoverController *popoverController;

// Buttons
@property(nonatomic, weak) IBOutlet UIButton *cancelButton;
@property(nonatomic, weak) IBOutlet UIButton *okButton;

// Text Views
@property(nonatomic, weak) IBOutlet UITextField *nameTextField;
@property(nonatomic, weak) IBOutlet UITextField *usernameTextField;
@property(nonatomic, weak) IBOutlet UITextField *emailTextField;
@property(nonatomic, weak) IBOutlet UITextField *passwordTextField;
@property(nonatomic, weak) IBOutlet UISwitch *enabledSwitch;

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO Methods
 ****************************************************************************/

- (IBAction)cancelPressed:(id)sender;
- (IBAction)okPressed:(id)sender;
- (IBAction)chooseImagePressed:(id)sender;
- (IBAction)removeImagePressed:(id)sender;
- (IBAction)cameraPressed:(id)sender;

@end
