//
//  VMNewEditUserVC.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "MobileCoreServices/MobileCoreServices.h"
#import "VMNewEditUserVC.h"
#import "VMAccessControlService.h"

@interface VMNewEditUserVC ()

@end

@implementation VMNewEditUserVC {
    // Edit
    @private __strong VMUser *_userToEdit;
    
    // Scroll View objects
    @private __weak UIScrollView *_scrollView;
    
    // Picker objects
    @private __weak UIPickerView *_accessLevelPickerView;
    @private __strong NSArray *_accessLevelPickerArray;
    
    // Image View
    @private __strong UIPopoverController *_popoverController;
    @private __weak UIImageView *_userImageView;
    BOOL newMedia;
    
    // Buttons
    @private __weak UIButton *_cancelButton;
    @private __weak UIButton *_okButton;
    
    // Text Views
    @private __weak UITextField *_nameTextField;
    @private __weak UITextField *_usernameTextField;
    @private __weak UITextField *_emailTextField;
    @private __weak UITextField *_passwordTextField;
    @private __weak UISwitch *_enabledSwitch;
}

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

// Edit
@synthesize userToEdit = _userToEdit;

// Scroll View Properties
@synthesize scrollView = _scrollView;

// Picker Properties
@synthesize accessLevelPickerView = _accessLevelPickerView;

// Image View
@synthesize userImageView = _userImageView;
@synthesize popoverController = _popoverController;

// Buttons
@synthesize cancelButton = _cancelButton;
@synthesize okButton = _okButton;

// Text Views
@synthesize nameTextField = _nameTextField;
@synthesize usernameTextField = _usernameTextField;
@synthesize emailTextField = _emailTextField;
@synthesize passwordTextField = _passwordTextField;
@synthesize enabledSwitch = _enabledSwitch;

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose {
    @try {
        self.nameTextField = nil;
        self.usernameTextField = nil;
        self.emailTextField = nil;
        self.passwordTextField = nil;
        self.enabledSwitch = nil;
        
        self.cancelButton = nil;
        self.okButton = nil;
        
        self.userImageView = nil;
        self.popoverController = nil;
        
        if (_accessLevelPickerArray)
            _accessLevelPickerArray = nil;
        if (self.accessLevelPickerView)
            self.accessLevelPickerView = nil;
        
        if (self.scrollView)
            self.scrollView = nil;
        
        [super dispose];
        
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark Init Methods
/****************************************************************************
 * Init Methods
 ****************************************************************************/

-(void)initalizeFormFieldValues {
    if (self.userToEdit == nil) {
        if (self.titleLabel)
            self.titleLabel.text = @"New User";
        [self.nameTextField becomeFirstResponder];
    }
    else {
        if (self.titleLabel)
            self.titleLabel.text = @"Edit User";
        if (self.nameTextField)
            self.nameTextField.text = [self.userToEdit.name copy];
        if (self.usernameTextField)
            self.usernameTextField.text = [self.userToEdit.username copy];
        if (self.emailTextField)
            self.emailTextField.text = [self.userToEdit.email copy];
        if (self.passwordTextField) {
            self.passwordTextField.text = [self.userToEdit.password copy];
            self.passwordTextField.secureTextEntry = YES;
        }
        
        if (self.accessLevelPickerView) {
            // Assume normal user and change using case insensitive compare
            NSInteger selectedPickerViewRow = 0;
            if (self.userToEdit.accessLevel == 0)
                selectedPickerViewRow = 0;  // User
            else if (self.userToEdit.accessLevel == 1)
                selectedPickerViewRow = 1;  // Manager
            else if (self.userToEdit.accessLevel == 2)
                selectedPickerViewRow = 2;  // Administrator
            else if (self.userToEdit.accessLevel == 3)
                selectedPickerViewRow = 3;  // Super Administrator
            [self.accessLevelPickerView selectRow:selectedPickerViewRow inComponent:0 animated:NO];
        }
        
        if (self.enabledSwitch)
            self.enabledSwitch.on = self.userToEdit.enabled;
    }
}

#pragma mark -
#pragma mark View Lifecycle Public
/****************************************************************************
 * View Lifecycle Public
 ****************************************************************************/

// Called from viewWillAppear
- (void)initializeUIBeforeRendering {
    @try {
        [super initializeUIBeforeRendering];
        [self initalizeFormFieldValues];
        
        // Scroll view touch event
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
        [self.scrollView addGestureRecognizer:singleTap];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

// Called from viewDidLoad
- (void)initializeUIOnFirstLoad {
    @try {
        [super initializeUIOnFirstLoad];
        self.nameTextField.returnKeyType = UIReturnKeyDone;
        self.usernameTextField.returnKeyType = UIReturnKeyDone;
        self.emailTextField.returnKeyType = UIReturnKeyDone;
        self.passwordTextField.returnKeyType = UIReturnKeyDone;
        
        // Initialize scroll view
        self.scrollView.contentSize = self.scrollView.frame.size;
        [self.scrollView flashScrollIndicators];
        
        // Initialize picker arrays
        [self reloadAccessLevelPickerArray];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

// Called from viewDidAppear
- (void)initializeUIAfterRendering {
    @try {
        [super initializeUIAfterRendering];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO Methods
 ****************************************************************************/

// Hides keyboard when losing focus on a UITextView
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    
    if ([self.nameTextField isFirstResponder] && [touch view] != self.nameTextField) {
        [self.nameTextField resignFirstResponder];
    }
    else if ([self.usernameTextField isFirstResponder] && [touch view] != self.usernameTextField) {
        [self.usernameTextField resignFirstResponder];
    }
    else if ([self.emailTextField isFirstResponder] && [touch view] != self.emailTextField) {
        [self.emailTextField resignFirstResponder];
    }
    else if ([self.passwordTextField isFirstResponder] && [touch view] != self.passwordTextField) {
        [self.passwordTextField resignFirstResponder];
    }
    
    [super touchesBegan:touches withEvent:event];
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)recognizer
{
    CGPoint touchPoint = [recognizer locationInView:self.scrollView];
    
    // Hides keyboard when losing focus on a UITextField contained within a scroll view
    if ([self.nameTextField isFirstResponder]) {
        CGRect bounds = self.nameTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.nameTextField resignFirstResponder];
    }
    else if ([self.usernameTextField isFirstResponder]) {
        CGRect bounds = self.usernameTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.usernameTextField resignFirstResponder];
    }
    else if ([self.emailTextField isFirstResponder]) {
        CGRect bounds = self.emailTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.emailTextField resignFirstResponder];
    }
    else if ([self.passwordTextField isFirstResponder]) {
        CGRect bounds = self.passwordTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.passwordTextField resignFirstResponder];
    }
    else if ([self.accessLevelPickerView isFirstResponder]) {
        CGRect bounds = self.accessLevelPickerView.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.accessLevelPickerView resignFirstResponder];
    }
}

- (void)backPressed:(id)sender{
    @try {
        [super backPressed:sender];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
}

- (void)cancelPressed:(id)sender{
    [self loadLastViewController];
}

- (void)okPressed:(id)sender{
    NSString *alertViewTitle = @"Confirm Save";
    NSString *alertMessage = @"Are you sure you wish to save this user?";
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: alertViewTitle
                          message: alertMessage
                          delegate: self
                          cancelButtonTitle:@"No"
                          otherButtonTitles:@"Yes",nil];    // value passed to override of alertView method
    [alert show];
}

- (void)chooseImagePressed:(id)sender {
    @try {
        if ([self.popoverController isPopoverVisible]) {
            [self.popoverController dismissPopoverAnimated:YES];
        } else {
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]) {
                
                UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                imagePicker.delegate = self;
                imagePicker.allowsEditing = NO;
                imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeImage, nil];
                
                if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                    [self presentModalViewController:imagePicker animated:YES];
                }
                else {
                    // Apple requires showing image picker on iPad in a popover controller
                    self.popoverController = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
                    self.popoverController.delegate = self;
                    [self.popoverController presentPopoverFromRect:CGRectMake(0,0,320,480) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
                }
                
                newMedia = NO;
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"An exception has occured: %@", [exception reason]);
    }
}

- (void)removeImagePressed:(id)sender {
    self.userImageView.image = [UIImage imageNamed:IMAGE_NAME_NO_PHOTO_AVAILABLE];
}

- (void)cameraPressed:(id)sender{
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    [self presentModalViewController:imagePicker animated:YES];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeImage, nil];
        imagePicker.allowsEditing = NO;
        [self presentModalViewController:imagePicker animated:YES];
        newMedia = YES;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		// do nothing except close alertView
	}
	else {
        if ([[alertView.title lowercaseString] caseInsensitiveCompare:@"Confirm Save"] == NSOrderedSame)
            [self saveUser];
        
        [self loadLastViewController];
	}
}

-(void)setFocusedFieldText:(NSString *)text {
    // populate field that has focus with barcode value
    if ([self.nameTextField isFirstResponder]) {
        [self.nameTextField setText:text];
        [self.nameTextField resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.usernameTextField isFirstResponder]) {
        [self.usernameTextField setText:text];
        [self.usernameTextField resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.emailTextField isFirstResponder]) {
        [self.emailTextField setText:text];
        [self.emailTextField resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.passwordTextField isFirstResponder]) {
        [self.passwordTextField setText:text];
        [self.passwordTextField resignFirstResponder];    // automatically lose focus to hide keyboard
    }
}

#pragma mark -
#pragma mark PickerView DataSource
/****************************************************************************
 * PickerView DataSource
 ****************************************************************************/

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if ([pickerView isEqual: self.accessLevelPickerView]) {
        return [_accessLevelPickerArray count];
    }
    else
        return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if ([pickerView isEqual: self.accessLevelPickerView]) {
        return [_accessLevelPickerArray objectAtIndex:row];
    }
    else
        return 0;
}

-(void)reloadAccessLevelPickerArray {
    _accessLevelPickerArray = [[NSArray alloc] initWithObjects:
                               @"User", @"Manager", @"Administrator", @"Super Administrator", nil];
}

#pragma mark -
#pragma mark PickerView Delegate
/****************************************************************************
 * PickerView Delegate
 ****************************************************************************/

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if ([pickerView isEqual: self.accessLevelPickerView]) {
        //int typeIndex = [[accessLevelPickerArray objectAtIndex:row] intValue];
        // do something
    }
}

#pragma mark -
#pragma mark UIImagePickerController Delegate
/****************************************************************************
 * UIImagePickerController Delegate
 ****************************************************************************/

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissModalViewControllerAnimated:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    @try {
        [self.popoverController dismissPopoverAnimated:YES];
        
        NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
        [self dismissModalViewControllerAnimated:YES];
        if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
            UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
            self.userImageView.image = image;
            if (newMedia)
                UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:finishedSavingWithError:contextInfo:), nil);
        }
        else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie]) {
            // Code here to support video if enabled
        }
    }
    @catch (NSException *exception) {
        NSLog(@"An exception has occured: %@", [exception reason]);
    }
}

-(void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Save failed"
                              message: @"Failed to save image"\
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark -
#pragma mark CRUD Methods
/****************************************************************************
 * CRUD Methods
 ****************************************************************************/

-(void)saveUser {
    if (self.userToEdit == nil) {
        @try {
            // GUID
            NSString *name = self.nameTextField.text;
            NSString *username = self.usernameTextField.text;
            NSString *email = self.emailTextField.text;
            NSString *password = self.passwordTextField.text;
            NSString *language = nil;
            NSString *timezone = nil;
            UIImage *image = self.userImageView.image;
            
            // Access Level
            NSInteger row = [self.accessLevelPickerView selectedRowInComponent:0];
            NSString *accessLevelAsString = [[_accessLevelPickerArray objectAtIndex:row] lowercaseString];
            
            VMUser *newUser = [[VMUser alloc] init:name username:username email:email password:password language:language timezone:timezone image:image];
            if ([[accessLevelAsString lowercaseString] caseInsensitiveCompare:@"user"] == NSOrderedSame)
                newUser.accessLevel = 0;
            else if ([[accessLevelAsString lowercaseString] caseInsensitiveCompare:@"manager"] == NSOrderedSame)
                newUser.accessLevel = 1;
            else if ([[accessLevelAsString lowercaseString] caseInsensitiveCompare:@"administrator"] == NSOrderedSame)
                newUser.accessLevel = 2;
            else if ([[accessLevelAsString lowercaseString] caseInsensitiveCompare:@"super administrator"] == NSOrderedSame)
                newUser.accessLevel = 3;
            newUser.enabled = self.enabledSwitch.isOn;
            [VMAccessControlService createUser:newUser];
        }
        @catch (NSException *exception) {
            NSLog(@"An exception occurred: %@", exception.reason);
        }
    }
    else {
        @try {
            self.userToEdit.name = self.nameTextField.text;
            self.userToEdit.username = self.usernameTextField.text;
            self.userToEdit.email = self.emailTextField.text;
            self.userToEdit.password = self.passwordTextField.text;
            self.userToEdit.image = self.userImageView.image;
            
            // Type
            NSInteger row = [self.accessLevelPickerView selectedRowInComponent:0];
            NSString *accessLevelAsString = [[_accessLevelPickerArray objectAtIndex:row] lowercaseString];
            if ([[accessLevelAsString lowercaseString] caseInsensitiveCompare:@"user"] == NSOrderedSame)
                self.userToEdit.accessLevel = 0;
            else if ([[accessLevelAsString lowercaseString] caseInsensitiveCompare:@"manager"] == NSOrderedSame)
                self.userToEdit.accessLevel = 1;
            else if ([[accessLevelAsString lowercaseString] caseInsensitiveCompare:@"administrator"] == NSOrderedSame)
                self.userToEdit.accessLevel = 2;
            else if ([[accessLevelAsString lowercaseString] caseInsensitiveCompare:@"super administrator"] == NSOrderedSame)
                self.userToEdit.accessLevel = 3;
            self.userToEdit.enabled = self.enabledSwitch.isOn;
            
            [VMAccessControlService updateUser:self.userToEdit];
        }
        @catch (NSException *exception) {
            NSLog(@"An exception occurred: %@", exception.reason);
        }
    }
}

#pragma mark -
#pragma mark VerbiBarcodeReaderDelegate Methods
/****************************************************************************
 * VerbiBarcodeReaderDelegate Methods
 ****************************************************************************/

-(void)handleBarcodeReaderReadEvent:(NSString *)barcode symbology:(NSString *)symbology event:(VMBarcodeReaderEvent *)event {
    [self setFocusedFieldText:barcode];
}

@end
