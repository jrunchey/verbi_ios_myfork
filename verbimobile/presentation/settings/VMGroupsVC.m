//
//  VMGroupsVC.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMGroupsVC.h"
#import "VMAccessControlService.h"
#import "VMNewEditGroupVC.h"

@interface VMGroupsVC ()

@end

@implementation VMGroupsVC

// GETTERS/SETTERS

// Buttons
@synthesize addButton;
@synthesize deleteButton;
@synthesize editButton;
@synthesize selectAllButton;

// Table View
@synthesize groupsTableView;

BOOL allSelected;

// METHODS

#pragma mark -
#pragma mark View Lifecycle Public
/****************************************************************************
 * View Lifecycle Public
 ****************************************************************************/

// Called from viewWillAppear
- (void)initializeUIBeforeRendering {
    @try {
        [super initializeUIBeforeRendering];
        [self reloadTableView];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

// Called from viewDidLoad
- (void)initializeUIOnFirstLoad {
    @try {
        [super initializeUIOnFirstLoad];
        // Initialize table view properties
        [self reloadGroupsArray];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

// Called from viewDidAppear
- (void)initializeUIAfterRendering {
    @try {
        [super initializeUIAfterRendering];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)dispose {
    @try {
        if (groups) {
            [groups removeAllObjects];
            groups = nil;
        }
        if (groupsTableView)
            groupsTableView = nil;
        
        [super dispose];
        
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark Table view methods

// Defines the table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Defines the table - Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return groups.count;
}

// Fill the table - Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    VMGroup *group = [groups objectAtIndex:indexPath.row];
    cell.textLabel.text = group.name;
    
    // Convert all row selections to check marks
    NSIndexPath *selection = [tableView indexPathForSelectedRow];
    if (selection && selection.row == indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        // uncheck
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else {
        // check
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark -
#pragma mark IO Methods

- (void)backPressed:(id)sender{
    @try {
        [super backPressed:sender];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
}

- (void)addPressed:(id)sender{
    [self loadNewEditGroupScreen:nil];
}

- (void)deletePressed:(id)sender{
    NSMutableArray *groupNames;
    NSString *alertViewTitle;
    
    NSArray *indexes = [self getCheckedOrSelectedIndexes:groupsTableView];
    if (indexes.count == 0) {
        alertViewTitle = @"Delete";
        NSString *alertMessage = @"Select a group.";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else {
        groupNames = [[NSMutableArray alloc] init];
        for (NSIndexPath *path in indexes) {
            NSUInteger index = [path indexAtPosition:[path length] - 1];
            VMGroup *group = [groups objectAtIndex:index];
            [groupNames addObject:[group.name copy]];
        }
        
        alertViewTitle = @"Confirm Delete";
        NSString *alertMessage = [NSString stringWithFormat:@"Are you sure you wish to delete these %i groups?", groupNames.count];
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"No"
                              otherButtonTitles:@"Yes",nil];    // value passed to override of alertView method
        [alert show];
    }
}

- (void)editPressed:(id)sender{
    VMGroup *group;
    NSString *alertViewTitle = @"Edit";
    
    NSArray *indexes = [self getCheckedOrSelectedIndexes:groupsTableView];
    if (indexes.count == 0) {
        NSString *alertMessage = @"Select a group.";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if (indexes.count == 1) {
        for (NSIndexPath *path in indexes) {
            NSUInteger index = [path indexAtPosition:[path length] - 1];
            group = [groups objectAtIndex:index];
        }
        if (group) {
            [self loadNewEditGroupScreen:group];
        }
    }
    else {
        NSString *alertMessage = @"Select a single group.";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void)selectAllButtonPressed:(id)sender {
    if (allSelected) {
        int rowCount = [groupsTableView numberOfRowsInSection:0];
        for (int i = 0; i < rowCount; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];    // convert iterator to indexPath
            UITableViewCell *cell = [groupsTableView cellForRowAtIndexPath:indexPath];    // get cell using indexPath
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        [selectAllButton setTitle:@"Select All" forState:UIControlStateNormal];
        allSelected = NO;
    }
    else {
        int rowCount = [groupsTableView numberOfRowsInSection:0];
        for (int i = 0; i < rowCount; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];    // convert iterator to indexPath
            UITableViewCell *cell = [groupsTableView cellForRowAtIndexPath:indexPath];    // get cell using indexPath
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        [selectAllButton setTitle:@"Deselect All" forState:UIControlStateNormal];
        allSelected = YES;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		// do nothing except close alertView
	}
	else {
        if ([[alertView.title lowercaseString] caseInsensitiveCompare:@"Confirm Delete"] == NSOrderedSame) {
            NSMutableArray *deletedGroups = [self deleteGroups];
            //[self reloadTableViewWithDeletedGroups:deletedGroups];
            [self reloadTableView];
            
            NSString *alertViewTitle = @"Delete Completed";
            NSString *alertMessage = [[NSString alloc] initWithFormat:@"%i groups deleted.", deletedGroups.count];
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: alertViewTitle
                                  message: alertMessage
                                  delegate: self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
	}
}

#pragma mark -
#pragma mark Helper Methods

-(void)loadNewEditGroupScreen:(VMGroup *)group {
    VMNewEditGroupVC *newEditGroupViewController = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        newEditGroupViewController = [[VMNewEditGroupVC alloc] initWithNibName:@"VMNewEditGroupVC_iPhone" bundle:nil];
    } else {
        newEditGroupViewController = [[VMNewEditGroupVC alloc] initWithNibName:@"VMNewEditGroupVC_iPad" bundle:nil];
    }
//    newEditGroupViewController.mainViewController = self.mainViewController;
    newEditGroupViewController.lastViewController = self;
    if (group)
        newEditGroupViewController.groupToEdit = group;
    [self presentModalViewController:newEditGroupViewController animated:NO];
}

-(NSMutableArray *)deleteGroups {
    NSMutableArray *deletedGroups;
    @try {
        //NSArray *indexes = [groupsTableView indexPathsForSelectedRows];
        NSArray *indexes = [self getCheckedOrSelectedIndexes:groupsTableView];
        if (indexes.count > 0) {
            deletedGroups = [[NSMutableArray alloc] init];
            for (NSIndexPath *path in indexes) {
                NSUInteger index = [path indexAtPosition:[path length] - 1];
                VMGroup *group = [groups objectAtIndex:index];
                [deletedGroups addObject:group.name];
            }
        }
        //[(VMAccessControlService *)[VMAccessControlService sharedInstance] deleteGroupByNames:deletedGroups];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", e.reason);
    }
    return deletedGroups;
}

-(void)reloadGroupsArray {
    /*
    int count = [(VMAccessControlService *)[VMAccessControlService sharedInstance] totalNumberOfGroupRecords];
    if (count > 0) {
        groups = [(VMAccessControlService *)[VMAccessControlService sharedInstance] readAllGroups];
    }
     */
}

-(void)reloadTableView {
    [self reloadGroupsArray];
    [groupsTableView reloadData];
}

-(NSMutableArray *)getCheckedOrSelectedIndexes:(UITableView *)tableView {
    NSMutableArray *indexes = [[NSMutableArray alloc] init];
    
    // Add selected indexes
    NSArray *selectedIndexes = [tableView indexPathsForSelectedRows];
    if (selectedIndexes.count > 0)
        [indexes addObjectsFromArray:selectedIndexes];
    
    // Add checked indexes
    NSMutableArray *checkedIndexes = [[NSMutableArray alloc] init];
    int rowCount = [tableView numberOfRowsInSection:0];
    for (int i = 0; i < rowCount; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];    // convert iterator to indexPath
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];    // get cell using indexPath
        if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
            [checkedIndexes addObject:indexPath];
        }
    }
    if (checkedIndexes.count > 0)
        [indexes addObjectsFromArray:checkedIndexes];
    
    return indexes;
}

@end
