//
//  VMGeneralSettingsVC.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMGeneralSettingsVC.h"
#import "ECSlidingViewController.h"
#import "LeftMenuViewController.h"
#import "RightMenuViewController.h"
//#import "VMViewController.h"
#import "VMSession.h"
#import "VMAccessControlService.h"

@interface VMGeneralSettingsVC ()

@end

@implementation VMGeneralSettingsVC

@synthesize scrollView = _scrollView;
@synthesize nameTextField = _nameTextField;
@synthesize usernameTextField = _usernameTextField;
@synthesize emailTextField = _emailTextField;
@synthesize passwordTextField = _passwordTextField;
@synthesize confirmPasswordTextField = _confirmPasswordTextField;

// METHODS

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

/*
 When the view has appeared on the page, we check to see if there is a session object (VMSession) stored
 on the view controller. If not then we display a new controller to authenticate the session.
 
 FYI, this has to be done in the viewDidAppear and not viewDidLoad since the UI isn't fully setup
 the call to presentModalViewController will not work in viewDidLoad.
 
 */
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    // Initialize session
    VMSession *session = [VMSession sharedInstance];
    if (session == nil || (session && (!session.valid || session.user == nil))){
        NSLog(@"No session, authenticating");
        /*
        // Load the authentication controller since we do not have a session
        VMLoginVC *loginViewController = nil;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            loginViewController = [[VMLoginVC alloc] initWithNibName:@"VMLoginVC_iPhone" bundle:nil];
        } else {
            loginViewController = [[VMLoginVC alloc] initWithNibName:@"VMLoginVC_iPad" bundle:nil];
        }
//        loginViewController.mainViewController = self.mainViewController;
        
        [self presentModalViewController:loginViewController animated:NO];
        */
    }else{
        // Initilialize header and fields elements to whether New or Edit
        [self initalizeFormFieldValues];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Initialize scroll view
    self.scrollView.contentSize = self.scrollView.frame.size;
    [self.scrollView flashScrollIndicators];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Scroll view touch event
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    [self.scrollView addGestureRecognizer:singleTap];
    
    if (self.interfaceOrientation == UIInterfaceOrientationLandscapeLeft
        || self.interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        // TODO - landscape
    } else {
        // TODO - landscape
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    // Scroll View Properties
    self.scrollView = nil;
    
    // Text Fields
    self.usernameTextField = nil;
    self.passwordTextField = nil;
    self.confirmPasswordTextField = nil;
    self.emailTextField = nil;
}

#pragma mark -
#pragma mark IO Methods

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    
    // Hides keyboard when losing focus on a UITextField
    if ([self.usernameTextField isFirstResponder] && [touch view] != self.usernameTextField) {
        [self.usernameTextField resignFirstResponder];
    }
    else if ([self.passwordTextField isFirstResponder] && [touch view] != self.passwordTextField) {
        [self.passwordTextField resignFirstResponder];
    }
    else if ([self.confirmPasswordTextField isFirstResponder] && [touch view] != self.confirmPasswordTextField) {
        [self.confirmPasswordTextField resignFirstResponder];
    }
    else if ([self.emailTextField isFirstResponder] && [touch view] != self.emailTextField) {
        [self.emailTextField resignFirstResponder];
    }
    
    [super touchesBegan:touches withEvent:event];
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)recognizer
{
    CGPoint touchPoint = [recognizer locationInView:self.scrollView];
    
    // Hides keyboard when losing focus on a UITextField contained within a scroll view
    if ([self.usernameTextField isFirstResponder]) {
        CGRect bounds = self.usernameTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.usernameTextField resignFirstResponder];
    }
    else if ([self.passwordTextField isFirstResponder]) {
        CGRect bounds = self.passwordTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.passwordTextField resignFirstResponder];
    }
    else if ([self.confirmPasswordTextField isFirstResponder]) {
        CGRect bounds = self.confirmPasswordTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.confirmPasswordTextField resignFirstResponder];
    }
    else if ([self.emailTextField isFirstResponder]) {
        CGRect bounds = self.emailTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.emailTextField resignFirstResponder];
    }
}

- (void)backPressed:(id)sender{
//    [self.mainViewController dismissModalViewControllerAnimated:NO];
}

- (IBAction)cancelPressed:(id)sender{
    [self loadLastViewController];
}

- (IBAction)okPressed:(id)sender{
    NSString *alertViewTitle = @"Confirm Save";
    NSString *alertMessage = @"Are you sure you wish to save?";
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: alertViewTitle
                          message: alertMessage
                          delegate: self
                          cancelButtonTitle:@"No"
                          otherButtonTitles:@"Yes",nil];    // value passed to override of alertView method
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		// do nothing except close alertView
	}
	else {
        BOOL saveSuccessful = NO;
        if ([[alertView.title lowercaseString] caseInsensitiveCompare:@"Confirm Save"] == NSOrderedSame) {
            if ([self validate])
                saveSuccessful = [self save];
        }
        
        if (saveSuccessful)
            [self loadLastViewController];
	}
}

#pragma mark -
#pragma mark Helper methods

-(void)initalizeFormFieldValues {
    if (self.usernameTextField == nil) {
        [self.usernameTextField becomeFirstResponder];
    }
    else {
        VMSession *session = [VMSession sharedInstance];
        if (session) {
            VMUser *user = session.user;
            if (user) {
                if (self.nameTextField)
                    self.nameTextField.text = [user.name copy];
                if (self.usernameTextField)
                    self.usernameTextField.text = [user.username copy];
                if (self.emailTextField)
                    self.emailTextField.text = [user.email copy];
                // leave password text field blank, user must enter something to show intended change
            }
        }
    }
}

-(void)setFocusedFieldText:(NSString *)text {
    if ([self.usernameTextField isFirstResponder]) {
        [self.usernameTextField setText:text];
        [self.usernameTextField resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.passwordTextField isFirstResponder]) {
        [self.passwordTextField setText:text];
        [self.passwordTextField resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.confirmPasswordTextField isFirstResponder]) {
        [self.confirmPasswordTextField setText:text];
        [self.confirmPasswordTextField resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.emailTextField isFirstResponder]) {
        [self.emailTextField setText:text];
        [self.emailTextField resignFirstResponder];    // automatically lose focus to hide keyboard
    }
}

-(BOOL)save {
    BOOL success = NO;
    // Edit
    @try {
        VMSession *session = [VMSession sharedInstance];
        if (session) {
            VMUser *user = session.user;
            if (user) {
                user.username = self.usernameTextField.text;
                if ([self.passwordTextField.text length] > 0)
                    user.password = self.passwordTextField.text;
                user.email = self.emailTextField.text;
                user.updated = [NSDate date];
                
                [VMAccessControlService updateUser:user];
                success = YES;
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occurred: %@", exception.reason);
    }
    return success;
}

-(BOOL)validate {
    [self resetFieldColors];
    
    if ([self.usernameTextField.text length] == 0) {
        NSString *alertViewTitle = @"Validation Error";
        NSString *alertMessage = @"Username cannot be blank";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];    // value passed to override of alertView method
        [alert show];
        self.usernameTextField.backgroundColor = [UIColor redColor];
        [self.usernameTextField becomeFirstResponder];
        [self.usernameTextField resignFirstResponder];
        return NO;
    }
    else if ([self.passwordTextField.text length] == 0 && [self.confirmPasswordTextField.text length] > 0) {
        NSString *alertViewTitle = @"Validation Error";
        NSString *alertMessage = @"Password cannot be blank";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];    // value passed to override of alertView method
        [alert show];
        self.passwordTextField.backgroundColor = [UIColor redColor];
        [self.passwordTextField becomeFirstResponder];
        [self.passwordTextField resignFirstResponder];
        return NO;
    }
    else if ([self.passwordTextField.text length] > 0 && ([self.confirmPasswordTextField.text length] == 0 || ![self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text])) {
        NSString *alertViewTitle = @"Validation Error";
        NSString *alertMessage = @"Confirm Password does not match Password";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];    // value passed to override of alertView method
        [alert show];
        self.confirmPasswordTextField.backgroundColor = [UIColor redColor];
        [self.confirmPasswordTextField becomeFirstResponder];
        [self.confirmPasswordTextField resignFirstResponder];
        return NO;
    }
    else if ([self.emailTextField.text length] == 0) {
        NSString *alertViewTitle = @"Validation Error";
        NSString *alertMessage = @"Email cannot be blank";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];    // value passed to override of alertView method
        [alert show];
        self.emailTextField.backgroundColor = [UIColor redColor];
        [self.emailTextField becomeFirstResponder];
        [self.emailTextField resignFirstResponder];
        return NO;
    }
    
    [self resetFieldColors];
    return YES;
}

-(void)resetFieldColors {
    self.usernameTextField.backgroundColor = [UIColor whiteColor];
    self.passwordTextField.backgroundColor = [UIColor whiteColor];
    self.confirmPasswordTextField.backgroundColor = [UIColor whiteColor];
    self.emailTextField.backgroundColor = [UIColor whiteColor];
}

@end
