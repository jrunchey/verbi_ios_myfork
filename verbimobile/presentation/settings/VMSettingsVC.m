//
//  VMSettingsVC.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMSettingsVC.h"
#import "VMGeneralSettingsVC.h"
#import "VMDevicesVC.h"
#import "VMDiagnosticsVC.h"
#import "VMUsersVC.h"
#import "VMGroupsVC.h"

@interface VMSettingsVC ()

@end

@implementation VMSettingsVC

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO Methods
 ****************************************************************************/

- (IBAction)screenButtonPressed:(UIButton *)sender {
    if ([[sender.currentTitle lowercaseString] caseInsensitiveCompare:@"General"] == NSOrderedSame)
        [self loadGeneralScreen];
    else if ([[sender.currentTitle lowercaseString] caseInsensitiveCompare:@"Devices"] == NSOrderedSame)
        [self loadDevicesScreen];
    else if ([[sender.currentTitle lowercaseString] caseInsensitiveCompare:@"Diagnostics"] == NSOrderedSame)
        [self loadDiagnosticsScreen];
    else if ([[sender.currentTitle lowercaseString] caseInsensitiveCompare:@"Users"] == NSOrderedSame)
        [self loadUsersScreen];
    else if ([[sender.currentTitle lowercaseString] caseInsensitiveCompare:@"Groups"] == NSOrderedSame)
        [self loadGroupsScreen];
    
}

#pragma mark -
#pragma mark Navigation Methods
/****************************************************************************
 * Navigation Methods
 ****************************************************************************/

- (void)loadGeneralScreen{
    @try {
        // Load the general settings controller
        VMGeneralSettingsVC *generalSettingsViewController = nil;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            generalSettingsViewController = [[VMGeneralSettingsVC alloc] initWithNibName:@"VMGeneralSettingsVC_iPhone" bundle:nil];
        } else {
            generalSettingsViewController = [[VMGeneralSettingsVC alloc] initWithNibName:@"VMGeneralSettingsVC_iPad" bundle:nil];
        }
//        generalSettingsViewController.mainViewController = self.mainViewController;
        generalSettingsViewController.lastViewController = self;
        [self presentModalViewController:generalSettingsViewController animated:NO];
        [self dispose];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

- (void)loadDevicesScreen{
    @try {
        // Load the devices controller
        VMDevicesVC *devicesViewController = nil;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            devicesViewController = [[VMDevicesVC alloc] initWithNibName:@"VMDevicesVC_iPhone" bundle:nil];
        } else {
            devicesViewController = [[VMDevicesVC alloc] initWithNibName:@"VMDevicesVC_iPad" bundle:nil];
        }
//        devicesViewController.mainViewController = self.mainViewController;
        devicesViewController.lastViewController = self;
        [self presentModalViewController:devicesViewController animated:NO];
        [self dispose];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

- (void)loadDiagnosticsScreen{
    @try {
        // Load the devices controller
        VMDiagnosticsVC *diagnosticsViewController = nil;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            diagnosticsViewController = [[VMDiagnosticsVC alloc] initWithNibName:@"VMDiagnosticsVC_iPhone" bundle:nil];
        } else {
            diagnosticsViewController = [[VMDiagnosticsVC alloc] initWithNibName:@"VMDiagnosticsVC_iPad" bundle:nil];
        }
//        diagnosticsViewController.mainViewController = self.mainViewController;
        diagnosticsViewController.lastViewController = self;
        [self presentModalViewController:diagnosticsViewController animated:NO];
        [self dispose];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

- (void)loadUsersScreen{
    @try {
        // Load the users controller
        VMUsersVC *usersViewController = nil;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            usersViewController = [[VMUsersVC alloc] initWithNibName:@"VMUsersVC_iPhone" bundle:nil];
        } else {
            usersViewController = [[VMUsersVC alloc] initWithNibName:@"VMUsersVC_iPad" bundle:nil];
        }
//        usersViewController.mainViewController = self.mainViewController;
        usersViewController.lastViewController = self;
        [self presentModalViewController:usersViewController animated:NO];
        [self dispose];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

- (void)loadGroupsScreen{
    @try {
        // Load the groups controller
        VMGroupsVC *groupsViewController = nil;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            groupsViewController = [[VMGroupsVC alloc] initWithNibName:@"VMGroupsVC_iPhone" bundle:nil];
        } else {
            groupsViewController = [[VMGroupsVC alloc] initWithNibName:@"VMGroupsVC_iPad" bundle:nil];
        }
//        groupsViewController.mainViewController = self.mainViewController;
        groupsViewController.lastViewController = self;
        [self presentModalViewController:groupsViewController animated:NO];
        [self dispose];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

@end
