//
//  VMUsersVC.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMUsersVC.h"
#import "VMAccessControlService.h"
#import "VMNewEditUserVC.h"

@interface VMUsersVC ()

@end

@implementation VMUsersVC {
    // Buttons
    @private __weak UIButton *_addButton;
    @private __weak UIButton *_deleteButton;
    @private __weak UIButton *_editButton;
    @private __weak UIButton *_selectAllButton;
    
    // Table View
    @private __weak UITableView *_usersTableView;
    @private __strong NSMutableArray *users;
}

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

// Buttons
@synthesize addButton = _addButton;
@synthesize deleteButton = _deleteButton;
@synthesize editButton = _editButton;
@synthesize selectAllButton = _selectAllButton;

// Table View
@synthesize usersTableView = _usersTableView;

BOOL allSelected;

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose {
    @try {
        self.addButton = nil;
        self.deleteButton = nil;
        self.editButton = nil;
        self.selectAllButton = nil;
        self.usersTableView = nil;
        
        if (users) {
            [users removeAllObjects];
            users = nil;
        }
        
        [super dispose];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark View Lifecycle Public
/****************************************************************************
 * View Lifecycle Public
 ****************************************************************************/

- (void)initializeUIOnFirstLoad {
    @try {
        [super initializeUIOnFirstLoad];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIBeforeRendering {
    @try {
        [super initializeUIBeforeRendering];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

- (void)initializeUIAfterRendering {
    @try {
        [super initializeUIAfterRendering];
        self.activityIndicatorView.hidden = NO;
        [self.activityIndicatorView startAnimating];
        [self reloadTableView];
        [self.activityIndicatorView stopAnimating];
        self.activityIndicatorView.hidden = YES;
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO Methods
 ****************************************************************************/

- (void)backPressed:(id)sender{
    @try {
        [super backPressed:sender];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
}

- (void)addPressed:(id)sender{
    [self loadNewEditUserScreen:nil];
}

- (void)deletePressed:(id)sender{
    NSMutableArray *userNames;
    NSString *alertViewTitle;
    
    NSArray *indexes = [VMUtilityService getCheckedOrSelectedIndexes:self.usersTableView];
    if (indexes.count == 0) {
        alertViewTitle = @"Delete";
        NSString *alertMessage = @"Select a user.";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else {
        userNames = [[NSMutableArray alloc] init];
        for (NSIndexPath *path in indexes) {
            NSUInteger index = [path indexAtPosition:[path length] - 1];
            VMUser *user = [users objectAtIndex:index];
            [userNames addObject:[user.username copy]];
        }
        
        alertViewTitle = @"Confirm Delete";
        NSString *alertMessage = [NSString stringWithFormat:@"Are you sure you wish to delete these %i users?", userNames.count];
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"No"
                              otherButtonTitles:@"Yes",nil];    // value passed to override of alertView method
        [alert show];
    }
}

- (void)editPressed:(id)sender{
    VMUser *user;
    NSString *alertViewTitle = @"Edit";
    
    NSArray *indexes = [VMUtilityService getCheckedOrSelectedIndexes:self.usersTableView];
    if (indexes.count == 0) {
        NSString *alertMessage = @"Select a user.";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if (indexes.count == 1) {
        for (NSIndexPath *path in indexes) {
            NSUInteger index = [path indexAtPosition:[path length] - 1];
            user = [users objectAtIndex:index];
        }
        if (user) {
            [self loadNewEditUserScreen:user];
        }
    }
    else {
        NSString *alertMessage = @"Select a single user.";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void)selectAllButtonPressed:(id)sender {
    if (allSelected) {
        int rowCount = [self.usersTableView numberOfRowsInSection:0];
        for (int i = 0; i < rowCount; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];    // convert iterator to indexPath
            UITableViewCell *cell = [self.usersTableView cellForRowAtIndexPath:indexPath];    // get cell using indexPath
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        [self.selectAllButton setTitle:@"Select All" forState:UIControlStateNormal];
        allSelected = NO;
    }
    else {
        int rowCount = [self.usersTableView numberOfRowsInSection:0];
        for (int i = 0; i < rowCount; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];    // convert iterator to indexPath
            UITableViewCell *cell = [self.usersTableView cellForRowAtIndexPath:indexPath];    // get cell using indexPath
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        [self.selectAllButton setTitle:@"Deselect All" forState:UIControlStateNormal];
        allSelected = YES;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		// do nothing except close alertView
	}
	else {
        if ([[alertView.title lowercaseString] caseInsensitiveCompare:@"Confirm Delete"] == NSOrderedSame) {
            NSMutableArray *deletedUsers = [self deleteUsers];
            //[self reloadTableViewWithDeletedUsers:deletedUsers];
            [self reloadTableView];
            
            NSString *alertViewTitle = @"Delete Completed";
            NSString *alertMessage = [[NSString alloc] initWithFormat:@"%i users deleted.", deletedUsers.count];
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: alertViewTitle
                                  message: alertMessage
                                  delegate: self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
	}
}

#pragma mark -
#pragma mark Table View Methods
/****************************************************************************
 * Table View Methods
 ****************************************************************************/

// Defines the table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Defines the table - Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return users.count;
}

// Fill the table - Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    VMUser *user = [users objectAtIndex:indexPath.row];
    cell.textLabel.text = user.username;
    
    // Convert all row selections to check marks
    NSIndexPath *selection = [tableView indexPathForSelectedRow];
    if (selection && selection.row == indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        // uncheck
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else {
        // check
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
}

-(void)reloadTableView {
    [self reloadUsersArray];
    [self.usersTableView reloadData];
}

-(void)reloadUsersArray {
    @autoreleasepool {
        int count = [VMAccessControlService totalNumberOfUserRecords];
        if (count > 0) {
            users = [VMAccessControlService readAllUsers];
        }
    }
}

#pragma mark -
#pragma mark Navigation Methods
/****************************************************************************
 * Navigation Methods
 ****************************************************************************/

-(void)loadNewEditUserScreen:(VMUser *)user {
    @try {
        VMNewEditUserVC *newEditUserViewController = nil;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            newEditUserViewController = [[VMNewEditUserVC alloc] initWithNibName:@"VMNewEditUserVC_iPhone" bundle:nil];
        } else {
            newEditUserViewController = [[VMNewEditUserVC alloc] initWithNibName:@"VMNewEditUserVC_iPad" bundle:nil];
        }
//        newEditUserViewController.mainViewController = self.mainViewController;
        newEditUserViewController.lastViewController = self;
        if (user)
            newEditUserViewController.userToEdit = user;
        [self presentModalViewController:newEditUserViewController animated:NO];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", e.reason);
    }
}

#pragma mark -
#pragma mark CRUD Methods
/****************************************************************************
 * CRUD Methods
 ****************************************************************************/

-(NSMutableArray *)deleteUsers {
    NSMutableArray *deletedUsers;
    @autoreleasepool {
        @try {
            //NSArray *indexes = [usersTableView indexPathsForSelectedRows];
            NSArray *indexes = [VMUtilityService getCheckedOrSelectedIndexes:self.usersTableView];
            if (indexes.count > 0) {
                deletedUsers = [[NSMutableArray alloc] init];
                for (NSIndexPath *path in indexes) {
                    NSUInteger index = [path indexAtPosition:[path length] - 1];
                    VMUser *user = [users objectAtIndex:index];
                    [deletedUsers addObject:user.username];
                }
            }
            [VMAccessControlService deleteUserByNames:deletedUsers];
        }
        @catch (NSException *e) {
            NSLog(@"An exception has occurred: %@", e.reason);
        }
    }
    return deletedUsers;
}

@end
