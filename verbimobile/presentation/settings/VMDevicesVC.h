//
//  VMDevicesVC.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMAutoIDCapableVC.h"

@interface VMDevicesVC : VMAutoIDCapableVC <UITableViewDelegate,UITableViewDataSource, UIAlertViewDelegate>

//@property (nonatomic, strong) BLE *bleShield;
@property (nonatomic, weak) IBOutlet UITableView *devicesTableView;
@property (nonatomic, weak) IBOutlet UILabel *statusLabel;
@property (nonatomic, weak) IBOutlet UIButton *connectButton;

@end
