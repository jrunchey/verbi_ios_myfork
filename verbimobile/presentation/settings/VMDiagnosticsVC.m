//
//  VMDiagnosticsVC.m
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMDiagnosticsVC.h"
#import "ECSlidingViewController.h"
#import "LeftMenuViewController.h"
#import "RightMenuViewController.h"

@interface VMDiagnosticsVC ()
@property(strong, nonatomic) NSMutableDictionary *tagDictionary;
@property(strong, nonatomic) NSMutableDictionary *tagCountsDictionary;
@end

@implementation VMDiagnosticsVC

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@synthesize tagsTableView = _tagsTableView;
@synthesize tagDictionary = _tagDictionary;
@synthesize tagCountsDictionary = _tagCountsDictionary;

- (NSMutableDictionary *)tagDictionary {
    if (_tagDictionary == nil)
        _tagDictionary = [[NSMutableDictionary alloc] init];
    return _tagDictionary;
}

- (NSMutableDictionary *)tagCountsDictionary {
    if (_tagCountsDictionary == nil)
        _tagCountsDictionary = [[NSMutableDictionary alloc] init];
    return _tagCountsDictionary;
}

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose {
    @try {
        self.tagsTableView = nil;
        [self disposeTagSets];
        [super dispose];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)disposeTagSets {
    if (self.tagDictionary) {
        [self.tagDictionary removeAllObjects];
        self.tagDictionary = nil;
    }
    if (self.tagCountsDictionary) {
        [self.tagCountsDictionary removeAllObjects];
        self.tagCountsDictionary = nil;
    }
}

#pragma mark -
#pragma mark View Lifecycle Public
/****************************************************************************
 * View Lifecycle Public
 ****************************************************************************/

- (void)initializeUIOnFirstLoad {
    @try {
        [super initializeUIOnFirstLoad];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIBeforeRendering {
    @try {
        [super initializeUIBeforeRendering];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

- (void)initializeUIAfterRendering {
    @try {
        [super initializeUIAfterRendering];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO Methods
 ****************************************************************************/

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTagsTableView:nil];
    [super viewDidUnload];
}

- (IBAction)clearPressed:(UIButton *)sender {
    [self disposeTagSets];
    if (self.tagDictionary)
        [self.tagDictionary removeAllObjects];
    if (self.tagDictionary)
        [self.tagCountsDictionary removeAllObjects];
    [self.tagsTableView reloadData];
}

- (void)addTag:(VMTag *)tag {
    if (tag) {
        VMTag *tagFound = [self.tagDictionary objectForKey:tag.value];
        NSNumber *tagCount = [self.tagCountsDictionary objectForKey:tag.value];
        if (tagFound && tagCount) {
            int tagCountIntVal = [tagCount intValue];
            tagCountIntVal++;
            tagCount = [NSNumber numberWithInt:tagCountIntVal];
        }
        else {
            tagCount = [NSNumber numberWithInt:1];
            tagFound = tag;
        }
        if (tagFound) {
            [self.tagDictionary setObject:tagFound forKey:tag.value];
            [self.tagCountsDictionary setObject:tagCount forKey:tag.value];
            [self.tagsTableView reloadData];
        }
    }
}

#pragma mark -
#pragma mark Table View Methods
/****************************************************************************
 * Table View Methods
 ****************************************************************************/

// Defines the table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Defines the table - Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tagCountsDictionary.count;
}

// Fill the table - Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    if (self.tagCountsDictionary && self.tagCountsDictionary.count > 0) {
        NSString *tagId = [[self.tagCountsDictionary allKeys] objectAtIndex:indexPath.row];
        VMTag *tag = [self.tagDictionary objectForKey:tagId];
        NSNumber *tagCount = [self.tagCountsDictionary objectForKey:tagId];
        NSString *detailText = nil;
        if (tagId) {
            // Cell Title Text
            if (tag.type == TAG_MAGNETIC_CARD) {
                VMMagneticCardTag *magstripeTag = (VMMagneticCardTag *)tag;
                cell.textLabel.text = [NSString stringWithFormat:@"%@ [%d]", magstripeTag.value, [tagCount intValue]];
                detailText = [NSString stringWithFormat:@"Magstripe | Count: %d", [tagCount intValue]];
            }
            else if (tag.type == TAG_BARCODE) {
                VMBarcodeTag *barcodeTag = (VMBarcodeTag *)tag;
                cell.textLabel.text = [NSString stringWithFormat:@"%@", barcodeTag.value];
                detailText = [NSString stringWithFormat:@"Barcode (%@) | Count: %d", barcodeTag.symbology, [tagCount intValue]];
            }
            else if (tag.type == TAG_RFID) {
                VMRFIDTag *rfidTag = (VMRFIDTag *)tag;
                cell.textLabel.text = [NSString stringWithFormat:@"%@", rfidTag.value];
                detailText = [NSString stringWithFormat:@"RFID | Count: %d", [tagCount intValue]];
                //detailText = [NSString stringWithFormat:@"Count: (%d) | RSSI: %f", [tagCount intValue], 0.0];
            }
            
            // Cell Subtitle Text
            cell.detailTextLabel.text = detailText; // set RSSI Info
        }
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Tag Ids";
}

#pragma mark -
#pragma mark VMRFIDReaderDelegate Methods
/****************************************************************************
 * VMRFIDReaderDelegate Methods
 ****************************************************************************/

-(void)handleRFIDReaderStatusEvent:(NSString *)description event:(VMRFIDReaderEvent *)event {
    
}

-(void)handleRFIDReaderReadEvent:(NSArray *)tagIds event:(VMRFIDReaderEvent *)event {
    if (event && event.tags) {
        for (VMRFIDTag *tag in event.tags) {
            [self addTag:tag];
        }
    }
}

#pragma mark -
#pragma mark VMBarcodeReaderDelegate Methods
/****************************************************************************
 * VMBarcodeReaderDelegate Methods
 ****************************************************************************/

-(void)handleBarcodeReaderReadEvent:(NSString *)barcode symbology:(NSString *)symbology event:(VMBarcodeReaderEvent *)event {
    if (event && event.tag)
        [self addTag:event.tag];
}

-(void)handleMagneticCardReaderReadEvent:(NSString *)data event:(VMMagneticCardReaderEvent *)event {
    if (event && event.tag)
        [self addTag:event.tag];
}

@end
