//
//  VMParentVC.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMParentVC.h"
#import "ECSlidingViewController.h"
#import "VMSession.h"

@interface VMParentVC ()
@end

@implementation VMParentVC

#pragma mark -
#pragma mark Getter/Setters
/****************************************************************************
 * Getter/Setters
 ****************************************************************************/

//@synthesize mainViewController = _mainViewController;
@synthesize lastViewController = _lastViewController;
@synthesize workflow = _workflow;
@synthesize workflowStep = _workflowStep;
@synthesize homeBackButton = _homeBackButton;
@synthesize activityIndicatorView = _activityIndicatorView;
@synthesize titleLabel = _titleLabel;

#pragma mark -
#pragma mark Constructors
/****************************************************************************
 * Constructors
 ****************************************************************************/

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose {
    if (self.workflow) {
        [self.workflow dispose];
        self.workflow = nil;
    }
    if (self.workflowStep) {
        [self.workflowStep dispose];
        self.workflowStep = nil;
    }
}

#pragma mark -
#pragma mark View Lifecycle
/****************************************************************************
 * View Lifecycle
 ****************************************************************************/

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupHomeBackButtonLongPressGestureRecognizer];
    [self initializeUIOnFirstLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self initializeUIBeforeRendering];
    
    if (self.activityIndicatorView) {
        self.activityIndicatorView.hidden = YES;
    }
    
    /*
    if (self.interfaceOrientation == UIInterfaceOrientationLandscapeLeft
        || self.interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        // TODO - landscape
    } else {
        // TODO - landscape
    }
    */
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    /*
     When the view has appeared on the page, we check to see if there is a user object (LSUser) stored
     on the view controller. If not then we display a new controller to authenticate the user.
     
     FYI, this has to be done in the viewDidAppear and not viewDidLoad since the UI isn't fully setup
     the call to presentModalViewController will not work in viewDidLoad.
     
     */
    
    // Initialize session
    VMSession *session = [VMSession sharedInstance];
    if (session == nil || (session && (!session.valid || session.user == nil))){
        NSLog(@"No session, authenticating...");
        
        UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Login"];
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;    // moves it onto the top
        [self.slidingViewController resetTopView];  // slides it back
        
        /*
        VMLoginVC *loginViewController = nil;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            loginViewController = [[VMLoginVC alloc] initWithNibName:@"VMLoginVC_iPhone" bundle:nil];
        } else {
            loginViewController = [[VMLoginVC alloc] initWithNibName:@"VMLoginVC_iPad" bundle:nil];
        }
//        loginViewController.mainViewController = self.mainViewController;
        loginViewController.lastViewController = self.lastViewController;
        
        [self presentModalViewController:loginViewController animated:NO];
         */
    }
    else {
        [self initializeUIAfterRendering];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self cleanupUIBeforeDerendering];
}

/**
 * Cleanup when view is unloaded from memory. The controller may still exist but UI elements have been discarded
 */
- (void)viewDidUnload {
    [super viewDidUnload];
    [self cleanupUIOnDispose];  // Release any retained subviews of the main view.
}

- (void)didReceiveMemoryWarning {
    [self dispose];
    [super didReceiveMemoryWarning];    // Releases the view if it doesn't have a superview.
}

#pragma mark -
#pragma mark View Lifecycle Public
/****************************************************************************
 * View Lifecycle Public
 ****************************************************************************/

/**
 * Called from viewDidLoad which is only called when the view is first loaded from the Nib file. If the view was already loaded and you push the view again it will not fire again. viewDidLoad is things you have to do once.
 */
- (void)initializeUIOnFirstLoad {
}

/**
 * Called from viewWillAppear which is called each time the view is about to be rendered or displayed to the user. Creation of UIViews is fairly expensive, and you should avoid as much as possible doing that on the ViewWillAppear method, becuase when this gets called, it means that the iPhone is already ready to show the UIView to the user, and anything heavy you do here will impact performance in a very visible manner (like animations being delayed, etc).
 when you are loading things from a server, you also have to think about latency. If you pack all of your network communication into viewDidLoad or viewWillAppear, they will be executed before the user gets to see the view - possibly resulting a short freeze of your app. It may be good idea to first show the user an unpopulated view with an activity indicator of some sort.
 */
- (void)initializeUIBeforeRendering {
    self.activityIndicatorView.hidden = YES;
}

/**
 * Called from viewDidAppear which is called each time after the view is rendered or displayed to the user. Use the ViewDidAppear to start off new threads to things that would take a long time to execute, like for example doing a webservice call to get extra data for the form above.The good thing is that because the view already exists and is being displayed to the user, you can show a nice "Waiting" message to the user while you get the data.
ViewLoad: loadView is the method in UIViewController that will actually load up the view and assign it to the "view" property. This is also the location that a subclass of UIViewController would override if you wanted to programatically set up the "view" property.
*/
- (void)initializeUIAfterRendering {
}

/**
 * Called from viewWillDisappear
 */
- (void)cleanupUIBeforeDerendering {
}

/**
 * Called from viewDidUnload
 */
- (void)cleanupUIOnDispose {
    [self dispose];
}

#pragma mark -
#pragma mark Navigation Methods
/****************************************************************************
 * Navigation Methods
 ****************************************************************************/

// Loads the main view controller
- (void)loadMainViewController {
//    if (self.mainViewController) {
//        [self dispose];
//        [self.mainViewController dismissModalViewControllerAnimated:NO];
//    }
}

// Loads the last view controller
- (void)loadLastViewController {
    @try {
        if (self.lastViewController) {
            [self dispose];
            [self.lastViewController dismissModalViewControllerAnimated:NO];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Failed to load the last view controller. An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark IO Methods
/*****************************************************************************
 * IO Methods
 *****************************************************************************/

// Standard Boilerplate code - locks orientation to portrait
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    //return YES;
}

- (void)longPressHandler:(UILongPressGestureRecognizer *)gestureRecognizer {
}

- (void)homePressed:(id)sender{
    [self loadMainViewController];
}

- (void)backPressed:(id)sender{
    [self loadLastViewController];
}

#pragma mark -
#pragma mark Helper Methods
/*****************************************************************************
 * Helper Methods
 *****************************************************************************/

- (void)uncheckAllItems:(UITableView *)tableView {
    int rowCount = [tableView numberOfRowsInSection:0];
    for (int i = 0; i < rowCount; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];    // convert iterator to indexPath
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];    // get cell using indexPath
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}

/**
 * This method creates a gesture recognizer for a long press on the homeBackButton to call homePressed
 * after 1 finger is held on button for 0.5 seconds.
 */
- (void)setupHomeBackButtonLongPressGestureRecognizer {
    UILongPressGestureRecognizer *longpressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(homePressed:)];
    longpressGesture.numberOfTouchesRequired = 1;   // 1 finger required for gesture recongnization
    longpressGesture.minimumPressDuration = 0.5;  // 0.5 seconds required for gesture recognization
    [longpressGesture setDelegate:self];
    [self.homeBackButton addGestureRecognizer:longpressGesture];
}

- (void)startActivityIndicator {
    // Activity indicator
    if (self.activityIndicatorView == nil) {
        self.activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.activityIndicatorView.frame = CGRectMake(10.0, 0.0, 40.0, 40.0);
        self.activityIndicatorView.center = self.view.center;
        [self.view addSubview:self.activityIndicatorView];
    }
    if (self.activityIndicatorView) {
        self.activityIndicatorView.hidden = NO;
        [self.activityIndicatorView startAnimating];
    }
}

- (void)stopActivityIndicator {
    if (self.activityIndicatorView) {
        [self.activityIndicatorView stopAnimating];
        self.activityIndicatorView.hidden = YES;
        [self setActivityIndicatorView:nil];
    }
}

@end
