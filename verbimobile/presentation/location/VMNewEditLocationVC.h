//
//  VMNewLocationViewController.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMAutoIDCapableVC.h"
#import "VMLocation.h"
#import "VMSelectInventoryLocationVC.h"

@interface VMNewEditLocationVC : VMAutoIDCapableVC <UIAlertViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate>

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@property(nonatomic, strong) VMSelectInventoryLocationVC *selectInventoryLocationController;

// Edit Properties
@property(nonatomic, strong) VMLocation *locationToEdit;

// Scroll View Properties
@property(nonatomic, weak) IBOutlet UIScrollView *scrollView;

// Picker Properties
@property(nonatomic, weak) IBOutlet UIPickerView *typePickerView;
//@property (strong, nonatomic) NSArray *typePickerArray;

// Image View
@property(nonatomic, weak) IBOutlet UIImageView *locationImageView;
@property(nonatomic, strong) UIPopoverController *popoverController;

// Buttons
@property(nonatomic, weak) IBOutlet UIButton *cancelButton;
@property(nonatomic, weak) IBOutlet UIButton *okButton;

// Text Views
@property(nonatomic, weak) IBOutlet UITextField *nameTextView;
@property(nonatomic, weak) IBOutlet UITextField *tagIdTextView;
@property(nonatomic, weak) IBOutlet UITextField *epcTextView;
@property(nonatomic, weak) IBOutlet UITextField *latitudeTextView;
@property(nonatomic, weak) IBOutlet UITextField *longitudeTextView;
@property(nonatomic, weak) IBOutlet UITextField *altitudeTextView;
@property(nonatomic, weak) IBOutlet UITextField *address1TextView;
@property(nonatomic, weak) IBOutlet UITextField *address2TextView;
@property(nonatomic, weak) IBOutlet UITextField *townOrCityTextView;
@property(nonatomic, weak) IBOutlet UITextField *countyOrDistrictTextView;
@property(nonatomic, weak) IBOutlet UITextField *stateOrRegionTextView;
@property(nonatomic, weak) IBOutlet UITextField *postalTextView;
@property(nonatomic, weak) IBOutlet UITextField *countryTextView;

@end
