//
//  VMImportLocationConfirmVC.m
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMImportLocationConfirmVC.h"
#import "VMLocationsVC.h"
#import "VMImportLocationFileWorkflowStep.h"
#import "VMImportLocationFileConfirmWorkflowStep.h"
#import "VMLocationService.h"

@interface VMImportLocationConfirmVC ()

@end

@implementation VMImportLocationConfirmVC

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@synthesize importFileURL = _importFileURL;
@synthesize locationsTableView = _locationsTableView;

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose {
    @try {
        self.locationsTableView = nil;
        self.importFileURL = nil;
        [super dispose];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark View Lifecycle Public
/****************************************************************************
 * View Lifecycle Public
 ****************************************************************************/

- (void)initializeUIOnFirstLoad {
    @try {
        [super initializeUIOnFirstLoad];
        
        // Set workflow step
        if (self.workflow) {
            VMWorkflowStep *workflowStep = [[VMImportLocationFileConfirmWorkflowStep alloc] init:self.workflow];
            if (workflowStep) {
                self.workflowStep = workflowStep;
                [self.workflow addWorkflowStep:self.workflowStep];
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIBeforeRendering {
    @try {
        [super initializeUIBeforeRendering];
        self.activityIndicatorView.hidden = YES;
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIAfterRendering {
    @try {
        [super initializeUIAfterRendering];
        self.activityIndicatorView.hidden = NO;
        [self.activityIndicatorView startAnimating];
        
        @autoreleasepool {
            @try {
                NSError *outError;
                NSString *importFileContents = nil;
                if (_importFileURL) {
                    importFileContents = [NSString stringWithContentsOfURL:_importFileURL encoding:NSUTF8StringEncoding error:&outError];
                    //NSLog(importFileContents);
                    
                    if (importFileContents && self.workflowStep) {
                        [self loadLocationsFromCSV:[importFileContents csvRows]];
                        [self reloadTableView];
                    }
                    else {
                        NSString *alertViewTitle;
                        alertViewTitle = @"Import - Error";
                        NSString *alertMessage = @"Error importing file";
                        UIAlertView *alert = [[UIAlertView alloc]
                                              initWithTitle: alertViewTitle
                                              message: alertMessage
                                              delegate: self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
                        [alert show];
                    }
                }
                else {
                    NSString *alertViewTitle;
                    alertViewTitle = @"Import - Error";
                    NSString *alertMessage = @"Import failed. A file was not specified.";
                    UIAlertView *alert = [[UIAlertView alloc]
                                          initWithTitle: alertViewTitle
                                          message: alertMessage
                                          delegate: self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
                    [alert show];
                }
            }
            @catch (NSException *exception) {
                NSLog(@"An exception occured: %@", [exception reason]);
            }
        }
        
        [self.activityIndicatorView stopAnimating];
        self.activityIndicatorView.hidden = YES;
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO methods
 ****************************************************************************/

- (IBAction)cancelPressed:(id)sender{
    [self loadMainViewController];
}

- (IBAction)verifyPressed:(id)sender{
    @autoreleasepool {
        @try {
            NSMutableArray *newLocations = ((VMImportLocationFileConfirmWorkflowStep *)self.workflowStep).creatingLocationsArray;
            [VMLocationService createLocations:newLocations];
            [self loadMainViewController];
        }
        @catch (NSException *exception) {
            NSLog(@"An exception has occurred: %@", [exception reason]);
            NSString *alertViewTitle;
            alertViewTitle = @"Import - Error";
            NSString *alertMessage = [[NSString alloc] initWithFormat:@"Import failed: %@", [exception reason]];
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: alertViewTitle
                                  message: alertMessage
                                  delegate: self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
            [self loadMainViewController];
        }
    }
}

-(void) loadLocationsFromCSV:(NSArray *)csvArray {
    NSMutableArray *allLocations = ((VMImportLocationFileConfirmWorkflowStep *)self.workflowStep).locationsArray;
    NSMutableArray *newLocations = ((VMImportLocationFileConfirmWorkflowStep *)self.workflowStep).creatingLocationsArray;
    NSMutableArray *existingLocationNames = ((VMImportLocationFileConfirmWorkflowStep *)self.workflowStep).existingLocationNamesArray;
    BOOL validFile = NO;
    BOOL loadByName = NO;
    BOOL loadByTagId = NO;
    BOOL loadByEPC = NO;
    int indexForNameHeader = -1;
    int indexForTypeHeader = -1;
    int indexForTagIdHeader = -1;
    int indexForEPCHeader = -1;
    int indexForLatitudeHeader = -1;
    int indexForLongitudeHeader = -1;
    int indexForAltitudeHeader = -1;
    int indexForAddress1Header = -1;
    int indexForAddress2Header = -1;
    int indexForTownOrCityHeader = -1;
    int indexForCountyOrDistrictHeader = -1;
    int indexForStateOrRegionHeader = -1;
    int indexForPostalHeader = -1;
    int indexForCountryHeader = -1;
    int indexForEnabledHeader = -1;
    
    if (csvArray && csvArray.count > 0) {
        @autoreleasepool {
            int i = 0;
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
            [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
            
            for (NSArray *row in csvArray) {
                //1st row is a header - skip
                if (i == 0) {
                    for (int h = 0; h < [row count]; h++) {
                        NSString *header = [row objectAtIndex:h];
                        if ([[header lowercaseString] caseInsensitiveCompare:@"Name"] == NSOrderedSame) {
                            loadByName = YES;
                            indexForNameHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Type"] == NSOrderedSame) {
                            indexForTypeHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"TagId"] == NSOrderedSame || [[header lowercaseString] caseInsensitiveCompare:@"Tag Id"] == NSOrderedSame) {
                            loadByTagId = YES;
                            indexForTagIdHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"EPC"] == NSOrderedSame) {
                            loadByEPC = YES;
                            indexForEPCHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Latitude"] == NSOrderedSame) {
                            indexForLatitudeHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Longitude"] == NSOrderedSame) {
                            indexForLongitudeHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Altitude"] == NSOrderedSame) {
                            indexForAltitudeHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Address1"] == NSOrderedSame) {
                            indexForAddress1Header = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Address2"] == NSOrderedSame) {
                            indexForAddress2Header = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"TownOrCity"] == NSOrderedSame || [[header lowercaseString] caseInsensitiveCompare:@"Town Or City"] == NSOrderedSame) {
                            indexForTownOrCityHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"CountyOrDistrict"] == NSOrderedSame || [[header lowercaseString] caseInsensitiveCompare:@"County Or District"] == NSOrderedSame) {
                            indexForCountyOrDistrictHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"StateOrRegion"] == NSOrderedSame || [[header lowercaseString] caseInsensitiveCompare:@"State Or Region"] == NSOrderedSame) {
                            indexForStateOrRegionHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Postal"] == NSOrderedSame) {
                            indexForPostalHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Country"] == NSOrderedSame) {
                            indexForCountryHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Enabled"] == NSOrderedSame) {
                            indexForEnabledHeader = h;
                        }
                        // File is valid if there are name anbd type fields
                        if (indexForNameHeader >= 0 && indexForTypeHeader >= 0)
                            validFile = YES;
                    }
                }
                
                if (validFile && i > 0) {
                    VMLocation *lookupLocation = nil;
                    NSString *lookupField = nil;
                    if (lookupLocation == nil && loadByName) {
                        lookupField = [row objectAtIndex:indexForNameHeader];
                        lookupLocation = [VMLocationService readByName:lookupField];
                    }
                    if (lookupLocation == nil && loadByTagId) {
                        lookupField = [row objectAtIndex:indexForTagIdHeader];
                        lookupLocation = [VMLocationService readByTagId:lookupField];
                    }
                    if (lookupLocation == nil && indexForEPCHeader) {
                        lookupField = [row objectAtIndex:indexForEPCHeader];
                        lookupLocation = [VMLocationService readByEPC:lookupField];
                    }
                    
                    NSString *name;
                    if (indexForNameHeader > -1)
                        name = [row objectAtIndex:indexForNameHeader];
                    NSString *type;
                    if (indexForTypeHeader > -1)
                        type = [row objectAtIndex:indexForTypeHeader];
                    BOOL enabled = [[row objectAtIndex:indexForEnabledHeader] boolValue];
                                        
                    VMLocation *location = [[VMLocation alloc] init:nil name:name type:type image:nil tagId:nil epc:nil latitude:nil longitude:nil altitude:nil address1:nil address2:nil townOrCity:nil countyOrDistrict:nil stateOrRegion:nil postal:nil country:nil];
                    if (location) {
                        if (indexForTagIdHeader > -1)
                            location.tagId = [row objectAtIndex:indexForTagIdHeader];
                        if (indexForEPCHeader > -1)
                            location.epc = [row objectAtIndex:indexForEPCHeader];
                        if (indexForLatitudeHeader > -1)
                            location.latitude = [row objectAtIndex:indexForLatitudeHeader];
                        if (indexForLongitudeHeader > -1)
                            location.longitude = [row objectAtIndex:indexForLongitudeHeader];
                        if (indexForAltitudeHeader > -1)
                            location.altitude = [row objectAtIndex:indexForAltitudeHeader];
                        if (indexForAddress1Header > -1)
                            location.address1 = [row objectAtIndex:indexForAddress1Header];
                        if (indexForAddress2Header > -1)
                            location.address2 = [row objectAtIndex:indexForAddress2Header];
                        if (indexForTownOrCityHeader > -1)
                            location.townOrCity = [row objectAtIndex:indexForTownOrCityHeader];
                        if (indexForCountyOrDistrictHeader > -1)
                            location.countyOrDistrict = [row objectAtIndex:indexForCountyOrDistrictHeader];
                        if (indexForStateOrRegionHeader > -1)
                            location.stateOrRegion = [row objectAtIndex:indexForStateOrRegionHeader];
                        if (indexForPostalHeader > -1)
                            location.postal = [row objectAtIndex:indexForPostalHeader];
                        if (indexForCountryHeader > -1)
                            location.country = [row objectAtIndex:indexForCountryHeader];
                        location.enabled = enabled;
                        
                        [allLocations addObject:location];
                        // If location already exists in database, add to existingLocations array for notifying user, else add to newLocations
                        if (lookupLocation) {
                            [existingLocationNames addObject:name];
                        }
                        else {
                            [newLocations addObject:location];
                        }
                    }
                }
                i++;
            }
        }
    }
}

#pragma mark -
#pragma mark Table view methods
/****************************************************************************
 * Table view methods
 ****************************************************************************/

// Defines the table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Defines the table - Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int count = 0;
    VMImportLocationFileConfirmWorkflowStep *workflowStep = [self.workflow.workflowSteps objectForKey:WKFL_STEP_LOC_IMPORT_LOCATION_FILE_CONFIRM];
    if (workflowStep && workflowStep.locationsArray) {
        count = workflowStep.locationsArray.count;
    }
    return count;
}

// Fill the table - Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    @autoreleasepool {
        VMImportLocationFileConfirmWorkflowStep *workflowStep = [self.workflow.workflowSteps objectForKey:WKFL_STEP_LOC_IMPORT_LOCATION_FILE_CONFIRM];
        if (workflowStep && workflowStep.locationsArray && workflowStep.locationsArray.count > 0) {
            VMLocation *location = [workflowStep.locationsArray objectAtIndex:indexPath.row];
            if (location) {
                BOOL locationExists = NO;
                NSMutableArray *existingLocationNames = ((VMImportLocationFileConfirmWorkflowStep *)self.workflowStep).existingLocationNamesArray;
                if ([existingLocationNames containsObject:location.name])
                    locationExists = YES;
                
                // Cell Title Text
                
                // define the range you're interested in
                NSRange stringRange = {0, MIN([location.type length], 1)};
                // adjust the range to include dependent chars
                stringRange = [location.type rangeOfComposedCharacterSequencesForRange:stringRange];
                // Now you can create the short string
                NSString *typeSubstring = [[location.type uppercaseString] substringWithRange:stringRange];
                
                cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@", typeSubstring, location.name];
                
                // Cell Subtitle Text
                
                NSMutableString *detailText = [[NSMutableString alloc] init];
                if (location.latitude && [location.latitude length] > 0 && location.longitude && [location.longitude length] > 0) {
                    [detailText appendFormat:@"GEO: %@, %@ ", location.latitude, location.longitude];
                }
                else if (location.address1 && [location.address1 length] > 0 && location.townOrCity && [location.townOrCity length] > 0 && location.stateOrRegion && [location.stateOrRegion length] > 0 && location.postal && [location.postal length] > 0) {
                    [detailText appendFormat:@"%@, ", location.address1];
                    if (location.address2)
                        [detailText appendFormat:@"%@, ", location.address2];
                    [detailText appendFormat:@"%@, ", location.townOrCity];
                    [detailText appendFormat:@"%@ ", location.stateOrRegion];
                    [detailText appendFormat:@"%@, ", location.postal];
                    if (location.country)
                        [detailText appendFormat:@"%@", location.country];
                }
                cell.detailTextLabel.text = detailText;
                if (locationExists)
                    cell.detailTextLabel.textColor = [UIColor redColor];
            }
        }
    }
    
    return cell;
}

/*
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
return @"Locations";
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger) section
{
//section text as a label
UILabel *lbl = [[UILabel alloc] init];
lbl.textAlignment = UITextAlignmentLeft;

lbl.text = @"Locations";
[lbl setBackgroundColor:[UIColor clearColor]];

return lbl;
}
*/

-(void)reloadTableView {
    [self reloadLocationsArray];
    [_locationsTableView reloadData];
}

-(void)reloadLocationsArray {
    @try {
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark Navigation Methods
/****************************************************************************
 * Navigation Methods
 ****************************************************************************/

- (void)loadLocationsScreen{
    @autoreleasepool {
        // Load the locations controller
        VMLocationsVC *locationsViewController = nil;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            locationsViewController = [[VMLocationsVC alloc] initWithNibName:@"VMLocationsVC_iPhone" bundle:nil];
        } else {
            locationsViewController = [[VMLocationsVC alloc] initWithNibName:@"VMLocationsVC_iPad" bundle:nil];
        }
//        locationsViewController.mainViewController = self.mainViewController;
        locationsViewController.lastViewController = self;
        [self presentModalViewController:locationsViewController animated:NO];
    }
}

@end
