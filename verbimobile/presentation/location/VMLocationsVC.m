//
//  VMLocationsVC.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMLocationsVC.h"
#import "VMLocationService.h"
#import "VMNewEditLocationVC.h"
#import "VMSession.h"

@interface VMLocationsVC ()
@property(nonatomic,strong) NSMutableArray *locationsArray;
@property(nonatomic,strong) NSMutableArray *locationNamesArray;
@property(nonatomic,strong) NSMutableArray *locationTagIdsArray;
@property(nonatomic,strong) NSMutableArray *locationEPCsArray;
@property(nonatomic) BOOL allSelected;
@property(nonatomic) BOOL searchAll;
@end

@implementation VMLocationsVC

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

// Buttons
@synthesize addButton = _addButton;
@synthesize deleteButton = _deleteButton;
@synthesize editButton = _editButton;
@synthesize selectAllButton = _selectAllButton;
@synthesize searchTextField = _searchTextField;
@synthesize clearButton = _clearButton;

// Table View
@synthesize locationsTableView = _locationsTableView;
@synthesize locationsArray = _locationsArray;
@synthesize locationNamesArray = _locationNamesArray;
@synthesize locationTagIdsArray = _locationTagIdsArray;
@synthesize locationEPCsArray = _locationEPCsArray;
@synthesize allSelected = _allSelected;
@synthesize searchAll = _searchAll;

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose {
    @try {
        self.addButton = nil;
        self.deleteButton = nil;
        self.editButton = nil;
        self.selectAllButton = nil;
        self.searchTextField = nil;
        self.clearButton = nil;
        self.locationsTableView = nil;
        
        if (self.locationsArray) {
            [self.locationsArray removeAllObjects];
            self.locationsArray = nil;
        }
        if (self.locationNamesArray) {
            [self.locationNamesArray removeAllObjects];
            self.locationNamesArray = nil;
        }
        if (self.locationTagIdsArray) {
            [self.locationTagIdsArray removeAllObjects];
            self.locationTagIdsArray = nil;
        }
        if (self.locationEPCsArray) {
            [self.locationEPCsArray removeAllObjects];
            self.locationEPCsArray = nil;
        }
        
        [super dispose];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark View Lifecycle Public
/****************************************************************************
 * View Lifecycle Public
 ****************************************************************************/

- (void)initializeUIOnFirstLoad {
    @try {
        [super initializeUIOnFirstLoad];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIBeforeRendering {
    @try {
        [super initializeUIBeforeRendering];
        self.searchTextField.delegate = self;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

- (void)initializeUIAfterRendering {
    @try {
        [super initializeUIAfterRendering];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO Methods
 ****************************************************************************/

// Hides keyboard when losing focus on a UITextView
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    
    if ([self.searchTextField isFirstResponder] && [touch view] != self.searchTextField) {
        [self.searchTextField resignFirstResponder];
    }
    else if ([self.locationsTableView isFirstResponder] && [touch view] != self.locationsTableView) {
        [self.locationsTableView resignFirstResponder];
    }
    
    [super touchesBegan:touches withEvent:event];
}

- (IBAction)backPressed:(id)sender{
    @try {
        [super backPressed:sender];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
}

- (IBAction)addButtonPressed:(id)sender{
    [self loadNewEditLocationScreen:nil];
}

- (IBAction)deleteButtonPressed:(id)sender{
    NSMutableArray *locationNames;
    NSString *alertViewTitle;
    
    NSArray *indexes = [VMUtilityService getCheckedOrSelectedIndexes:self.locationsTableView];
    if (indexes.count == 0) {
        alertViewTitle = @"Delete";
        NSString *alertMessage = @"Select a location.";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else {
        locationNames = [[NSMutableArray alloc] init];
        for (NSIndexPath *path in indexes) {
            NSUInteger index = [path indexAtPosition:[path length] - 1];
            NSString *locationName = [self.locationsArray objectAtIndex:index];
            [locationNames addObject:locationName];
        }
        
        alertViewTitle = @"Confirm Delete";
        NSString *alertMessage = [NSString stringWithFormat:@"Are you sure you wish to delete these %i location(s)?", locationNames.count];
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"No"
                              otherButtonTitles:@"Yes",nil];    // value passed to override of alertView method
        [alert show];
    }
}

- (IBAction)editButtonPressed:(id)sender{
    VMLocation *location;
    NSString *alertViewTitle = @"Edit";
    
    NSArray *indexes = [VMUtilityService getCheckedOrSelectedIndexes:self.locationsTableView];
    if (indexes.count == 0) {
        NSString *alertMessage = @"Select a location.";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if (indexes.count == 1) {
        for (NSIndexPath *path in indexes) {
            NSUInteger index = [path indexAtPosition:[path length] - 1];
            location = [self.locationsArray objectAtIndex:index];
        }
        if (location) {
            [self loadNewEditLocationScreen:location];
        }
    }
    else {
        NSString *alertMessage = @"Select a single location.";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)selectAllButtonPressed:(id)sender {
    if (self.allSelected) {
        int rowCount = [self.locationsTableView numberOfRowsInSection:0];
        for (int i = 0; i < rowCount; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];    // convert iterator to indexPath
            UITableViewCell *cell = [self.locationsTableView cellForRowAtIndexPath:indexPath];    // get cell using indexPath
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        [self.selectAllButton setTitle:@"Select All" forState:UIControlStateNormal];
        self.allSelected = NO;
    }
    else {
        int rowCount = [self.locationsTableView numberOfRowsInSection:0];
        for (int i = 0; i < rowCount; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];    // convert iterator to indexPath
            UITableViewCell *cell = [self.locationsTableView cellForRowAtIndexPath:indexPath];    // get cell using indexPath
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        [self.selectAllButton setTitle:@"Deselect All" forState:UIControlStateNormal];
        self.allSelected = YES;
    }
}

- (void)resetSearch {
    self.activityIndicatorView.hidden = NO;
    [self.activityIndicatorView startAnimating];
    if (self.locationsArray) {
        [self.locationsArray removeAllObjects];
    }
    if (self.locationNamesArray) {
        [self.locationNamesArray removeAllObjects];
    }
    if (self.locationTagIdsArray) {
        [self.locationTagIdsArray removeAllObjects];
    }
    if (self.locationEPCsArray) {
        [self.locationEPCsArray removeAllObjects];
    }
    [self.locationsTableView reloadData];
    [self.activityIndicatorView stopAnimating];
    self.activityIndicatorView.hidden = YES;
}

- (IBAction)search:(id)sender{
    if (self.searchTextField) {
        [self resetSearch];
        
        self.activityIndicatorView.hidden = NO;
        [self.activityIndicatorView startAnimating];
        @autoreleasepool {
            if ([self.searchTextField.text length] == 0 && self.searchAll) {
                self.locationsArray = [VMLocationService readAll];
                self.searchAll = NO;
            }
            else if ([self.searchTextField.text length] > 0) {
                self.locationsArray = [VMLocationService readLikeName:self.searchTextField.text];
            }
            
            if (self.locationsArray && self.locationsArray.count > 0) {
                self.locationNamesArray = [[NSMutableArray alloc] init];
                self.locationTagIdsArray = [[NSMutableArray alloc] init];
                self.locationEPCsArray = [[NSMutableArray alloc] init];
                for (VMLocation *location in self.locationsArray) {
                    [self.locationNamesArray addObject:location.name];
                    if (location.tagId && [location.tagId length] > 0)
                        [self.locationTagIdsArray addObject:location.tagId];
                    else
                        [self.locationTagIdsArray addObject:@""];   // add blank to keep index matched to self.locationNamesArray
                    if (location.epc && [location.epc length] > 0)
                        [self.locationEPCsArray addObject:location.epc];
                    else
                        [self.locationEPCsArray addObject:@""];   // add blank to keep index matched to self.locationNamesArray
                }
            }
        }
        
        [self.locationsTableView reloadData];
        [self.activityIndicatorView stopAnimating];
        self.activityIndicatorView.hidden = YES;
    }
}

- (IBAction)clearPressed:(id)sender{
    if (self.searchTextField)
        self.searchTextField.text = nil;
    [self resetSearch];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		// do nothing except close alertView
	}
	else {
        if ([[alertView.title lowercaseString] caseInsensitiveCompare:@"Confirm Delete"] == NSOrderedSame) {
            NSMutableArray *deletedLocations = [self deleteLocations];
            [self reloadTableView];
            
            NSString *alertViewTitle = @"Delete Completed";
            NSString *alertMessage = [[NSString alloc] initWithFormat:@"%i locations deleted.", deletedLocations.count];
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: alertViewTitle
                                  message: alertMessage
                                  delegate: self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
        else if ([[alertView.title lowercaseString] caseInsensitiveCompare:@"Selection Not Found"] == NSOrderedSame) {
            // If location does not exist, then navigate to New Location screen
            VMLocation *location = [[VMLocation alloc] init:nil name:self.getLastScanValue type:nil image:nil tagId:nil epc:nil latitude:nil longitude:nil altitude:nil address1:nil address2:nil townOrCity:nil countyOrDistrict:nil stateOrRegion:nil postal:nil country:nil];
            [self loadNewEditLocationScreen:location];
        }
	}
}

// Handles when you press return in either text field
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    // If you pressed return in the password text field we will trigger loginPressed for you
    if (textField.tag == kSearchFieldTag){
        if ([self.searchTextField.text length] == 0) {
            self.searchAll = YES;
            [self search:nil];
        }
    }
    
    // this closed the keyboard if you press return in either text field
    return [textField resignFirstResponder];
}

// If you leave the text field we will close the keyboard
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
}

#pragma mark -
#pragma mark Table View Methods
/****************************************************************************
 * Table View Methods
 ****************************************************************************/

// Defines the table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Defines the table - Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.locationsArray.count;
}

// Fill the table - Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    if (self.locationsArray && self.locationsArray.count > 0) {
        VMLocation *location = [self.locationsArray objectAtIndex:indexPath.row];
        if (location) {
            // Cell Title Text
            
            // define the range you're interested in
            NSRange stringRange = {0, MIN([location.type length], 1)};
            // adjust the range to include dependent chars
            stringRange = [location.type rangeOfComposedCharacterSequencesForRange:stringRange];
            // Now you can create the short string
            NSString *typeSubstring = [[location.type uppercaseString] substringWithRange:stringRange];
            
            cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@", typeSubstring, location.name];
            
            // Cell Subtitle Text
            
            NSMutableString *detailText = [[NSMutableString alloc] init];
            if (location.latitude && [location.latitude length] > 0 && location.longitude && [location.longitude length] > 0) {
                [detailText appendFormat:@"GEO: %@, %@ ", location.latitude, location.longitude];
            }
            else if (location.address1 && [location.address1 length] > 0 && location.townOrCity && [location.townOrCity length] > 0 && location.stateOrRegion && [location.stateOrRegion length] > 0 && location.postal && [location.postal length] > 0) {
                [detailText appendFormat:@"%@, ", location.address1];
                if (location.address2)
                    [detailText appendFormat:@"%@, ", location.address2];
                [detailText appendFormat:@"%@, ", location.townOrCity];
                [detailText appendFormat:@"%@ ", location.stateOrRegion];
                [detailText appendFormat:@"%@, ", location.postal];
                if (location.country)
                    [detailText appendFormat:@"%@", location.country];
            }
            cell.detailTextLabel.text = detailText;
        }
    }
    
    // Convert all row selections to check marks
    NSIndexPath *selection = [tableView indexPathForSelectedRow];
    if (selection && selection.row == indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        // uncheck
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else {
        // check
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Scan or Search Locations";
}

-(void)reloadTableView {
    [self reloadLocationsArray];
    [self.locationsTableView reloadData];
}

-(void)reloadLocationsArray {
    int count = [VMLocationService totalNumberOfRecords];
    if (count > 0) {
        @autoreleasepool {
            self.locationsArray = [VMLocationService readAll];
            if (self.locationsArray && self.locationsArray.count > 0) {
                self.locationNamesArray = [[NSMutableArray alloc] init];
                self.locationTagIdsArray = [[NSMutableArray alloc] init];
                self.locationEPCsArray = [[NSMutableArray alloc] init];
                for (VMLocation *location in self.locationsArray) {
                    [self.locationNamesArray addObject:location.name];
                    if (location.tagId && [location.tagId length] > 0)
                        [self.locationTagIdsArray addObject:location.tagId];
                    else
                        [self.locationTagIdsArray addObject:@""];   // add blank to keep index matched to self.locationNamesArray
                    if (location.epc && [location.epc length] > 0)
                        [self.locationEPCsArray addObject:location.epc];
                    else
                        [self.locationEPCsArray addObject:@""];   // add blank to keep index matched to self.locationNamesArray
                }
            }
        }
    }
}

#pragma mark -
#pragma mark Navigation Methods
/****************************************************************************
 * Navigation Methods
 ****************************************************************************/

-(void)loadNewEditLocationScreen:(VMLocation *)location {
    @try {
        // Load the new location controller
        VMNewEditLocationVC *newEditLocationViewController = nil;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            newEditLocationViewController = [[VMNewEditLocationVC alloc] initWithNibName:@"VMNewEditLocationVC_iPhone" bundle:nil];
        } else {
            newEditLocationViewController = [[VMNewEditLocationVC alloc] initWithNibName:@"VMNewEditLocationVC_iPad" bundle:nil];
        }
//        newEditLocationViewController.mainViewController = self.mainViewController;
        newEditLocationViewController.lastViewController = self;
        if (location)
            newEditLocationViewController.locationToEdit = location;
        [self presentModalViewController:newEditLocationViewController animated:NO];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", e.reason);
    }
}

-(void)lookupLocation:(NSString *)locationName {
    VMLocation *location;
    @try {
        // Lookup location from locationsArray
        NSUInteger index = [self.locationNamesArray indexOfObject:locationName];
        location = [self.locationsArray objectAtIndex:index];
        if (location)
            [self loadNewEditLocationScreen:location];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
    @finally {
        if (location == nil) {
            // If location does not exist, then popup if they'd like to add a new location
            NSString *alertMessage = [NSString stringWithFormat:@"Location not found. Would like to add '%@' as a new location?", self.getLastScanValue];
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: @"Selection Not Found"
                                  message: alertMessage
                                  delegate: self
                                  cancelButtonTitle:@"No"
                                  otherButtonTitles:@"Yes",nil];    // value passed to override of alertView method
            [alert show];
        }
    }
}

#pragma mark -
#pragma mark CRUD Methods
/****************************************************************************
 * CRUD Methods
 ****************************************************************************/

-(NSMutableArray *)deleteLocations {
    NSMutableArray *deletedLocations;
    @autoreleasepool {
        @try {
            //NSArray *indexes = [locationsTableView indexPathsForSelectedRows];
            NSArray *indexes = [VMUtilityService getCheckedOrSelectedIndexes:self.locationsTableView];
            if (indexes.count > 0) {
                deletedLocations = [[NSMutableArray alloc] init];
                for (NSIndexPath *path in indexes) {
                    NSUInteger index = [path indexAtPosition:[path length] - 1];
                    VMLocation *location = [self.locationsArray objectAtIndex:index];
                    [deletedLocations addObject:location.name];
                }
            }
            [VMLocationService deleteByNames:deletedLocations];
        }
        @catch (NSException *e) {
            NSLog(@"An exception has occurred: %@", e.reason);
        }
    }
    return deletedLocations;
}

#pragma mark -
#pragma mark VerbiBarcodeReaderDelegate Methods
/****************************************************************************
 * VerbiBarcodeReaderDelegate Methods
 ****************************************************************************/

-(void)handleBarcodeReaderReadEvent:(NSString *)barcode symbology:(NSString *)symbology event:(VMBarcodeReaderEvent *)event {
    [self lookupLocation:barcode];
}

@end
