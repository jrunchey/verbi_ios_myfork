//
//  VMImportLocationVC.h
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMParentVC.h"
#import "NSString+ParsingExtensions_.h"

@interface VMImportLocationVC : VMParentVC

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@property (nonatomic, strong) NSURL *importFileURL;
@property (nonatomic, weak) IBOutlet UITextField *importFileField;

@end
