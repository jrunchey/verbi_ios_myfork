//
//  VMNewLocationViewController.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "MobileCoreServices/MobileCoreServices.h"
#import "VMNewEditLocationVC.h"
#import "VMAppDelegate.h"
#import "VMSelectInventoryLocationVC.h"
#import "VMLocationService.h"

@interface VMNewEditLocationVC ()
@property(nonatomic,strong) NSArray *typePickerArray;
@property(nonatomic) BOOL newMedia;
@end

@implementation VMNewEditLocationVC

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@synthesize selectInventoryLocationController = _selectInventoryLocationController;

// Edit
@synthesize locationToEdit = _locationToEdit;

// Scroll View Properties
@synthesize scrollView = _scrollView;

// Picker Properties
@synthesize typePickerView = _typePickerView;
@synthesize typePickerArray = _typePickerArray;

// Image View
@synthesize locationImageView = _locationImageView;
@synthesize popoverController = _mypopoverController;
@synthesize newMedia = _newMedia;

// Buttons
@synthesize cancelButton = _cancelButton;
@synthesize okButton = _okButton;

// Text Views
@synthesize nameTextView = _nameTextView;
@synthesize tagIdTextView = _tagIdTextView;
@synthesize epcTextView = _epcTextView;
@synthesize latitudeTextView = _latitudeTextView;
@synthesize longitudeTextView = _longitudeTextView;
@synthesize altitudeTextView = _altitudeTextView;
@synthesize address1TextView = _address1TextView;
@synthesize address2TextView = _address2TextView;
@synthesize townOrCityTextView = _townOrCityTextView;
@synthesize countyOrDistrictTextView = _countyOrDistrictTextView;
@synthesize stateOrRegionTextView = _stateOrRegionTextView;
@synthesize postalTextView = _postalTextView;
@synthesize countryTextView = _countryTextView;

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose {
    @try {
        self.nameTextView = nil;
        self.tagIdTextView = nil;
        self.epcTextView = nil;
        self.latitudeTextView = nil;
        self.longitudeTextView = nil;
        self.altitudeTextView = nil;
        self.address1TextView = nil;
        self.address2TextView = nil;
        self.townOrCityTextView = nil;
        self.countyOrDistrictTextView = nil;
        self.stateOrRegionTextView = nil;
        self.postalTextView = nil;
        self.countryTextView = nil;
        self.titleLabel = nil;
        self.cancelButton = nil;
        self.okButton = nil;
        self.locationImageView = nil;
        self.popoverController = nil;
        self.typePickerArray = nil;
        self.typePickerView = nil;
        self.selectInventoryLocationController = nil;
        self.scrollView = nil;
        self.locationToEdit = nil;
        
        [super dispose];
        
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark Init Methods
/****************************************************************************
 * Init Methods
 ****************************************************************************/

-(void)initalizeFormFieldValues {
    if (self.locationToEdit == nil) {
        if (self.titleLabel)
            self.titleLabel.text = @"New Location";
        [self.nameTextView becomeFirstResponder];
    }
    else {
        if (self.titleLabel)
            self.titleLabel.text = @"Edit Location";
        if (self.nameTextView)
            self.nameTextView.text = [self.locationToEdit.name copy];
        
        if (self.typePickerArray == nil || self.typePickerArray.count == 0)
            [self reloadTypePickerArray];
        
        if (self.typePickerView) {
            // Assume fixed location and change using case insensitive compare
            NSInteger selectedPickerViewRow = 0;
            if ([[self.locationToEdit.type lowercaseString] caseInsensitiveCompare:@"movable"] == NSOrderedSame) {
                selectedPickerViewRow = 1;
            }
            [self.typePickerView selectRow:selectedPickerViewRow inComponent:0 animated:YES];
        }
        
        if (self.tagIdTextView && self.locationToEdit.tagId && [self.locationToEdit.tagId length] > 0)
            self.tagIdTextView.text = [self.locationToEdit.tagId copy];
        if (self.epcTextView && self.locationToEdit.epc && [self.locationToEdit.epc length] > 0)
            self.epcTextView.text = [self.locationToEdit.epc copy];
        if (self.latitudeTextView && self.locationToEdit.latitude && [self.locationToEdit.latitude length] > 0)
            self.latitudeTextView.text = [self.locationToEdit.latitude copy];
        if (self.longitudeTextView && self.locationToEdit.longitude && [self.locationToEdit.longitude length] > 0)
            self.longitudeTextView.text = [self.locationToEdit.longitude copy];
        if (self.altitudeTextView && self.locationToEdit.altitude && [self.locationToEdit.altitude length] > 0)
            self.altitudeTextView.text = [self.locationToEdit.altitude copy];
        if (self.address1TextView && self.locationToEdit.address1 && [self.locationToEdit.address1 length] > 0)
            self.address1TextView.text = [self.locationToEdit.address1 copy];
        if (self.address2TextView && self.locationToEdit.address2 && [self.locationToEdit.address2 length] > 0)
            self.address2TextView.text = [self.locationToEdit.address2 copy];
        if (self.townOrCityTextView && self.locationToEdit.townOrCity && [self.locationToEdit.townOrCity length] > 0)
            self.townOrCityTextView.text = [self.locationToEdit.townOrCity copy];
        if (self.countyOrDistrictTextView && self.locationToEdit.countyOrDistrict && [self.locationToEdit.countyOrDistrict length] > 0)
            self.countyOrDistrictTextView.text = [self.locationToEdit.countyOrDistrict copy];
        if (self.stateOrRegionTextView && self.locationToEdit.stateOrRegion && [self.locationToEdit.stateOrRegion length] > 0)
            self.stateOrRegionTextView.text = [self.locationToEdit.stateOrRegion copy];
        if (self.postalTextView && self.locationToEdit.postal && [self.locationToEdit.postal length] > 0)
            self.postalTextView.text = [self.locationToEdit.postal copy];
        if (self.countryTextView && self.locationToEdit.country && [self.locationToEdit.country length] > 0)
            self.countryTextView.text = [self.locationToEdit.country copy];
    }
}

#pragma mark -
#pragma mark View Lifecycle Public
/****************************************************************************
 * View Lifecycle Public
 ****************************************************************************/

- (void)initializeUIOnFirstLoad {
    @try {
        [super initializeUIOnFirstLoad];
        self.nameTextView.returnKeyType = UIReturnKeyDone;
        self.tagIdTextView.returnKeyType = UIReturnKeyDone;
        self.epcTextView.returnKeyType = UIReturnKeyDone;
        self.latitudeTextView.returnKeyType = UIReturnKeyDone;
        self.longitudeTextView.returnKeyType = UIReturnKeyDone;
        self.altitudeTextView.returnKeyType = UIReturnKeyDone;
        self.address1TextView.returnKeyType = UIReturnKeyDone;
        self.address2TextView.returnKeyType = UIReturnKeyDone;
        self.townOrCityTextView.returnKeyType = UIReturnKeyDone;
        self.countyOrDistrictTextView.returnKeyType = UIReturnKeyDone;
        self.stateOrRegionTextView.returnKeyType = UIReturnKeyDone;
        self.postalTextView.returnKeyType = UIReturnKeyDone;
        self.countryTextView.returnKeyType = UIReturnKeyDone;
        
        // Initialize scroll view
        self.scrollView.contentSize = self.scrollView.frame.size;
        [self.scrollView flashScrollIndicators];
        
        // Scroll view touch event
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
        [self.scrollView addGestureRecognizer:singleTap];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIBeforeRendering {
    @try {
        [super initializeUIBeforeRendering];
        [self initalizeFormFieldValues];
        
        // Initialize picker arrays
        [self reloadTypePickerArray];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

- (void)initializeUIAfterRendering {
    @try {
        [super initializeUIAfterRendering];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO Methods
 ****************************************************************************/

// Hides keyboard when losing focus on a UITextView
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    
    if ([self.nameTextView isFirstResponder] && [touch view] != self.nameTextView) {
        [self.nameTextView resignFirstResponder];
    }
    else if ([self.tagIdTextView isFirstResponder] && [touch view] != self.tagIdTextView) {
        [self.tagIdTextView resignFirstResponder];
    }
    else if ([self.epcTextView isFirstResponder] && [touch view] != self.epcTextView) {
        [self.epcTextView resignFirstResponder];
    }
    else if ([self.latitudeTextView isFirstResponder] && [touch view] != self.latitudeTextView) {
        [self.latitudeTextView resignFirstResponder];
    }
    else if ([self.longitudeTextView isFirstResponder] && [touch view] != self.longitudeTextView) {
        [self.longitudeTextView resignFirstResponder];
    }
    else if ([self.altitudeTextView isFirstResponder] && [touch view] != self.altitudeTextView) {
        [self.altitudeTextView resignFirstResponder];
    }
    else if ([self.address1TextView isFirstResponder] && [touch view] != self.address1TextView) {
        [self.address1TextView resignFirstResponder];
    }
    else if ([self.address2TextView isFirstResponder] && [touch view] != self.address2TextView) {
        [self.address2TextView resignFirstResponder];
    }
    else if ([self.townOrCityTextView isFirstResponder] && [touch view] != self.townOrCityTextView) {
        [self.townOrCityTextView resignFirstResponder];
    }
    else if ([self.countyOrDistrictTextView isFirstResponder] && [touch view] != self.countyOrDistrictTextView) {
        [self.countyOrDistrictTextView resignFirstResponder];
    }
    else if ([self.stateOrRegionTextView isFirstResponder] && [touch view] != self.stateOrRegionTextView) {
        [self.stateOrRegionTextView resignFirstResponder];
    }
    else if ([self.postalTextView isFirstResponder] && [touch view] != self.postalTextView) {
        [self.postalTextView resignFirstResponder];
    }
    else if ([self.countryTextView isFirstResponder] && [touch view] != self.countryTextView) {
        [self.countryTextView resignFirstResponder];
    }
    
    [super touchesBegan:touches withEvent:event];
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)recognizer
{
    CGPoint touchPoint = [recognizer locationInView:self.scrollView];
    
    // Hides keyboard when losing focus on a UITextField contained within a scroll view
    if ([self.nameTextView isFirstResponder]) {
        CGRect bounds = self.nameTextView.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.nameTextView resignFirstResponder];
    }
    else if ([self.typePickerView isFirstResponder]) {
        CGRect bounds = self.typePickerView.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.typePickerView resignFirstResponder];
    }
    else if ([self.tagIdTextView isFirstResponder]) {
        CGRect bounds = self.tagIdTextView.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.tagIdTextView resignFirstResponder];
    }
    else if ([self.epcTextView isFirstResponder]) {
        CGRect bounds = self.epcTextView.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.epcTextView resignFirstResponder];
    }
    else if ([self.latitudeTextView isFirstResponder]) {
        CGRect bounds = self.latitudeTextView.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.latitudeTextView resignFirstResponder];
    }
    else if ([self.longitudeTextView isFirstResponder]) {
        CGRect bounds = self.longitudeTextView.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.longitudeTextView resignFirstResponder];
    }
    else if ([self.altitudeTextView isFirstResponder]) {
        CGRect bounds = self.altitudeTextView.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.altitudeTextView resignFirstResponder];
    }
    else if ([self.address1TextView isFirstResponder]) {
        CGRect bounds = self.address1TextView.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.address1TextView resignFirstResponder];
    }
    else if ([self.address2TextView isFirstResponder]) {
        CGRect bounds = self.address2TextView.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.address2TextView resignFirstResponder];
    }
    else if ([self.townOrCityTextView isFirstResponder]) {
        CGRect bounds = self.townOrCityTextView.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.townOrCityTextView resignFirstResponder];
    }
    else if ([self.countyOrDistrictTextView isFirstResponder]) {
        CGRect bounds = self.countyOrDistrictTextView.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.countyOrDistrictTextView resignFirstResponder];
    }
    else if ([self.stateOrRegionTextView isFirstResponder]) {
        CGRect bounds = self.stateOrRegionTextView.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.stateOrRegionTextView resignFirstResponder];
    }
    else if ([self.postalTextView isFirstResponder]) {
        CGRect bounds = self.postalTextView.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.postalTextView resignFirstResponder];
    }
    else if ([self.countryTextView isFirstResponder]) {
        CGRect bounds = self.countryTextView.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.countryTextView resignFirstResponder];
    }
}

- (IBAction)backPressed:(id)sender{
    @try {
        [super backPressed:sender];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
}

- (IBAction)cancelPressed:(id)sender{
    [self loadLastViewController];
}

- (IBAction)okPressed:(id)sender{
    NSString *alertViewTitle = @"Confirm Save";
    NSString *alertMessage = @"Are you sure you wish to save this location?";
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: alertViewTitle
                          message: alertMessage
                          delegate: self
                          cancelButtonTitle:@"No"
                          otherButtonTitles:@"Yes",nil];    // value passed to override of alertView method
    [alert show];
}

- (IBAction)chooseImagePressed:(id)sender {
    @try {
        if ([self.popoverController isPopoverVisible]) {
            [self.popoverController dismissPopoverAnimated:YES];
        } else {
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]) {
                
                UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                imagePicker.delegate = self;
                imagePicker.allowsEditing = NO;
                imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeImage, nil];
                
                if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                    [self presentModalViewController:imagePicker animated:YES];
                }
                else {
                    // Apple requires showing image picker on iPad in a popover controller
                    self.popoverController = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
                    self.popoverController.delegate = self;
                    [self.popoverController presentPopoverFromRect:CGRectMake(0,0,320,480) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
                }
                
                self.newMedia = NO;
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"An exception has occured: %@", [exception reason]);
    }
}

- (IBAction)removeImagePressed:(id)sender {
    self.locationImageView.image = [UIImage imageNamed:IMAGE_NAME_NO_PHOTO_AVAILABLE];
}

- (IBAction)cameraPressed:(id)sender{
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    [self presentModalViewController:imagePicker animated:YES];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeImage, nil];
        imagePicker.allowsEditing = NO;
        [self presentModalViewController:imagePicker animated:YES];
        self.newMedia = YES;
    }
}

- (IBAction)mapPressed:(id)sender {
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		// do nothing except close alertView
	}
	else {
        BOOL success = NO;
        if ([[alertView.title lowercaseString] caseInsensitiveCompare:@"Confirm Save"] == NSOrderedSame)
            success = [self saveLocation];
        
        if (success)
            [self loadLastViewController];
        else {
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: @"Save failed"
                                  message: @"Location already exists"\
                                  delegate: nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
	}
}

-(void)setFocusedFieldText:(NSString *)text {
    // populate field that has focus with tagId value
    if ([self.nameTextView isFirstResponder]) {
        [self.nameTextView setText:text];
        [self.nameTextView resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.tagIdTextView isFirstResponder]) {
        [self.tagIdTextView setText:text];
        [self.tagIdTextView resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.epcTextView isFirstResponder]) {
        [self.epcTextView setText:text];
        [self.epcTextView resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.latitudeTextView isFirstResponder]) {
        [self.latitudeTextView setText:text];
        [self.latitudeTextView resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.longitudeTextView isFirstResponder]) {
        [self.longitudeTextView setText:text];
        [self.longitudeTextView resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.altitudeTextView isFirstResponder]) {
        [self.altitudeTextView setText:text];
        [self.altitudeTextView resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.address1TextView isFirstResponder]) {
        [self.address1TextView setText:text];
        [self.address1TextView resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.address2TextView isFirstResponder]) {
        [self.address2TextView setText:text];
        [self.address2TextView resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.townOrCityTextView isFirstResponder]) {
        [self.townOrCityTextView setText:text];
        [self.townOrCityTextView resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.countyOrDistrictTextView isFirstResponder]) {
        [self.countyOrDistrictTextView setText:text];
        [self.countyOrDistrictTextView resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.stateOrRegionTextView isFirstResponder]) {
        [self.stateOrRegionTextView setText:text];
        [self.stateOrRegionTextView resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.postalTextView isFirstResponder]) {
        [self.postalTextView setText:text];
        [self.postalTextView resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.countryTextView isFirstResponder]) {
        [self.countryTextView setText:text];
        [self.countryTextView resignFirstResponder];    // automatically lose focus to hide keyboard
    }
}

#pragma mark -
#pragma mark PickerView DataSource
/****************************************************************************
 * PickerView DataSource
 ****************************************************************************/

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if ([pickerView isEqual: self.typePickerView]) {
        return [self.typePickerArray count];
    }
    else
        return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if ([pickerView isEqual: self.typePickerView]) {
        return [self.typePickerArray objectAtIndex:row];
    }
    else
        return 0;
}

-(void)reloadTypePickerArray {
    self.typePickerArray = [[NSArray alloc] initWithObjects:
                        @"Fixed", @"Movable", nil];
}

#pragma mark -
#pragma mark PickerView Delegate
/****************************************************************************
 * PickerView Delegate
 ****************************************************************************/

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if ([pickerView isEqual: self.typePickerView]) {
        //int typeIndex = [[typePickerArray objectAtIndex:row] intValue];
        // do something
    }
}

#pragma mark -
#pragma mark UIImagePickerController Delegate
/****************************************************************************
 * UIImagePickerController Delegate
 ****************************************************************************/

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissModalViewControllerAnimated:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    @try {
        [self.popoverController dismissPopoverAnimated:YES];
        
        NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
        [self dismissModalViewControllerAnimated:YES];
        if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
            UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
            self.locationImageView.image = image;
            if (self.newMedia)
                UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:finishedSavingWithError:contextInfo:), nil);
        }
        else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie]) {
            // Code here to support video if enabled
        }
    }
    @catch (NSException *exception) {
        NSLog(@"An exception has occured: %@", [exception reason]);
    }
}

-(void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Save failed"
                              message: @"Failed to save image"\
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark -
#pragma mark CRUD Methods
/****************************************************************************
 * CRUD Methods
 ****************************************************************************/

-(BOOL)saveLocation {
    @autoreleasepool {
        NSString *name = self.nameTextView.text;
        
        if (self.locationToEdit == nil) {
            @try {
                VMLocation *locationFound = [VMLocationService readByName:name];
                if (locationFound)
                    return NO;
                
                UIImage *image = self.locationImageView.image;
                
                // Type
                NSInteger row = [self.typePickerView selectedRowInComponent:0];
                NSString *type = [[self.typePickerArray objectAtIndex:row] lowercaseString];
                
                VMLocation *newLocation = [[VMLocation alloc] init:nil name:name type:type image:image tagId:self.tagIdTextView.text epc:self.epcTextView.text latitude:self.latitudeTextView.text longitude:self.longitudeTextView.text altitude:self.altitudeTextView.text address1:self.address1TextView.text address2:self.address2TextView.text townOrCity:self.townOrCityTextView.text countyOrDistrict:self.countyOrDistrictTextView.text stateOrRegion:self.stateOrRegionTextView.text postal:self.postalTextView.text country:self.countryTextView.text];
                [VMLocationService create:newLocation];
            }
            @catch (NSException *exception) {
                NSLog(@"An exception occurred: %@", exception.reason);
            }
        }
        else {
            @try {
                self.locationToEdit.name = name;
                self.locationToEdit.image = self.locationImageView.image;
                
                // Type
                NSInteger row = [self.typePickerView selectedRowInComponent:0];
                NSString *type = [[self.typePickerArray objectAtIndex:row] lowercaseString];
                self.locationToEdit.type = type;
                
                self.locationToEdit.tagId = self.tagIdTextView.text;
                self.locationToEdit.epc = self.epcTextView.text;
                self.locationToEdit.latitude = self.latitudeTextView.text;
                self.locationToEdit.longitude = self.longitudeTextView.text;
                self.locationToEdit.altitude = self.altitudeTextView.text;
                self.locationToEdit.address1 = self.address1TextView.text;
                self.locationToEdit.address2 = self.address2TextView.text;
                self.locationToEdit.townOrCity = self.townOrCityTextView.text;
                self.locationToEdit.countyOrDistrict = self.countyOrDistrictTextView.text;
                self.locationToEdit.stateOrRegion = self.stateOrRegionTextView.text;
                self.locationToEdit.postal = self.postalTextView.text;
                self.locationToEdit.country = self.countryTextView.text;
                [VMLocationService update:self.locationToEdit];
            }
            @catch (NSException *exception) {
                NSLog(@"An exception occurred: %@", exception.reason);
            }
        }
    }
    return YES;
}

#pragma mark -
#pragma mark VerbiBarcodeReaderDelegate Methods
/****************************************************************************
 * VerbiBarcodeReaderDelegate Methods
 ****************************************************************************/

-(void)handleBarcodeReaderReadEvent:(NSString *)barcode symbology:(NSString *)symbology event:(VMBarcodeReaderEvent *)event {
    [self setFocusedFieldText:barcode];
}

@end
