//
//  VMImportLocationVC.m
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMImportLocationVC.h"
#import "VMImportLocationConfirmVC.h"
#import "VMImportLocationFileWorkflowStep.h"
#import "VMLocationService.h"

@interface VMImportLocationVC ()

@end

@implementation VMImportLocationVC

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@synthesize importFileURL = _importFileURL;
@synthesize importFileField = _importFileField;

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose {
    @try {
        self.importFileField = nil;
        self.importFileURL = nil;
        [super dispose];
        
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark View Lifecycle Public
/****************************************************************************
 * View Lifecycle Public
 ****************************************************************************/

- (void)initializeUIOnFirstLoad {
    @try {
        [super initializeUIOnFirstLoad];
        
        // Set workflow step
        if (self.workflow) {
            VMWorkflowStep *workflowStep = [[VMImportLocationFileWorkflowStep alloc] init:self.workflow];
            if (workflowStep) {
                self.workflowStep = workflowStep;
                [self.workflow addWorkflowStep:self.workflowStep];
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIBeforeRendering {
    @try {
        [super initializeUIBeforeRendering];
        self.activityIndicatorView.hidden = YES;
        if (_importFileURL && _importFileField) {
            _importFileField.text = _importFileURL.absoluteString;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIAfterRendering {
    @try {
        [super initializeUIAfterRendering];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO methods
 ****************************************************************************/

- (IBAction)cancelPressed:(id)sender{
    [self loadMainViewController];
}

- (IBAction)nextPressed:(id)sender{
    /*
     self.activityIndicatorView.hidden = NO;
     [self.activityIndicatorView startAnimating];
     @autoreleasepool {
     @try {
     NSError *outError;
     NSString *importFileContents = nil;
     NSURL *url = nil;
     if (_importFileURL)
     url = _importFileURL;
     if (url == nil && _importFileField.text)
     url = [NSURL fileURLWithPath:_importFileField.text];
     if (url) {
     importFileContents = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&outError];
     //NSLog(importFileContents);
     
     if (importFileContents && self.workflowStep) {
     ((VMImportLocationFileWorkflowStep *)self.workflowStep).itemsArray = [self loadLocationItemsFromCSV:[importFileContents csvRows]];
     [self loadImportLocationConfirmScreen];
     }
     else {
     NSString *alertViewTitle;
     alertViewTitle = @"Import - Error";
     NSString *alertMessage = @"Error importing file";
     UIAlertView *alert = [[UIAlertView alloc]
     initWithTitle: alertViewTitle
     message: alertMessage
     delegate: self
     cancelButtonTitle:@"OK"
     otherButtonTitles:nil];
     [alert show];
     }
     }
     else {
     NSString *alertViewTitle;
     alertViewTitle = @"Import";
     NSString *alertMessage = @"Browse for a file.";
     UIAlertView *alert = [[UIAlertView alloc]
     initWithTitle: alertViewTitle
     message: alertMessage
     delegate: self
     cancelButtonTitle:@"OK"
     otherButtonTitles:nil];
     [alert show];
     }
     }
     @catch (NSException *exception) {
     NSLog(@"An exception occured: %@", [exception reason]);
     }
     }
     */
}

- (IBAction)browsePressed:(UITextView *)dataStr{
    @autoreleasepool {
        /***
         
         //CREATE DIRECTORY
         NSFileManager *fileManager;
         
         fileManager = [NSFileManager defaultManager];
         
         NSURL *newDir = [NSURL fileURLWithPath:@"/Users/mhotaling/Desktop/VerbiMobile/Data"];
         [fileManager createDirectoryAtURL: newDir withIntermediateDirectories:YES attributes: nil error:nil];
         
         
         //WRITE FILE
         NSString *filePath = @"/Users/mhotaling/Desktop/VerbiMobile/Data/test123456.txt";
         //Uncomment the next three lines to save file to iPhone simulator directory
         //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
         //NSString *documentsDirectory = [paths objectAtIndex:0];
         //NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"verbiData.data"];
         NSLog(@"filePath %@", filePath);
         
         if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) { // if file is not exist, create it.
         //NSString *dataStr = @"Pepperoni, Cheese, Mushroom";
         NSString *dataStr = _importFileField.text;
         NSError *error;
         [dataStr writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
         
         //[importFileField setText:dataStr];
         }
         else {
         //NSString *dataStr = importFileField.text;
         //[importFileField setText:dataStr];
         }
         
         if ([[NSFileManager defaultManager] isWritableFileAtPath:filePath]) {
         NSLog(@"Writing to verbiData.data...");
         }else {
         NSLog(@"verbiData.data not found!");
         }
         
         
         //Files in the "Resource folder" are actually the contents of your application bundle
         NSString* path = [[NSBundle mainBundle] pathForResource:@"filename"
         ofType:@"txt"];
         //Load the Content into a string
         NSString* content = [NSString stringWithContentsOfFile:path
         encoding:NSUTF8StringEncoding
         error:NULL];
         
         
         ****/
        
        /*
         NSString *theText = @"Pepperoni, Cheese, Mushroom";
         [[NSUserDefaults standardUserDefaults] setObject:theText forKey:@"/Users/mhotaling/Desktop/VerbiMobile/Data/v123.txt"];
         //return [[NSUserDefaults standardUserDefaults] stringForKey:@"/Users/mhotaling/Desktop/VerbiMobile/Data/test123456.txt"];
         */
        
        
        //-- Open File Dialog Issue: NSOpenPanel Doesn't work with IOS
        /*
         int i; // Loop counter.
         
         // Create the File Open Dialog class.
         NSOpenPanel* openDlg = [NSOpenPanel openPanel];
         
         // Enable the selection of files in the dialog.
         [openDlg setCanChooseFiles:YES];
         
         // Enable the selection of directories in the dialog.
         [openDlg setCanChooseDirectories:YES];
         
         // Display the dialog.  If the OK button was pressed,
         // process the files.
         if ( [openDlg runModalForDirectory:nil file:nil] == NSOKButton )
         {
         // Get an array containing the full filenames of all
         // files and directories selected.
         NSArray* files = [openDlg filenames];
         
         // Loop through all the files and process them.
         for( i = 0; i < [files count]; i++ )
         {
         NSString* fileName = [files objectAtIndex:i];
         
         // Do something with the filename.
         }
         }
         */
        
    }
}

-(NSMutableArray *) loadLocationItemsFromCSV:(NSArray *)csvArray {
    NSMutableArray *locations = nil;
    BOOL validFile = NO;
    BOOL loadByName = NO;
    BOOL loadByTagId = NO;
    BOOL loadByEPC = NO;
    int indexForNameHeader = -1;
    int indexForTypeHeader = -1;
    int indexForTagIdHeader = -1;
    int indexForEPCHeader = -1;
    int indexForLatitudeHeader = -1;
    int indexForLongitudeHeader = -1;
    int indexForAltitudeHeader = -1;
    int indexForAddress1Header = -1;
    int indexForAddress2Header = -1;
    int indexForTownOrCityHeader = -1;
    int indexForCountyOrDistrictHeader = -1;
    int indexForStateOrRegionHeader = -1;
    int indexForPostalHeader = -1;
    int indexForCountryHeader = -1;
    int indexForEnabledHeader = -1;
    
    if (csvArray && csvArray.count > 0) {
        @autoreleasepool {
            int i = 0;
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
            [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
            
            locations = [[NSMutableArray alloc] init];
            for (NSArray *row in csvArray) {
                //1st row is a header - skip
                if (i == 0) {
                    for (int h = 0; h < [row count]; h++) {
                        NSString *header = [row objectAtIndex:h];
                        if ([[header lowercaseString] caseInsensitiveCompare:@"Name"] == NSOrderedSame || [[header lowercaseString] caseInsensitiveCompare:@"Name"] == NSOrderedSame) {
                            loadByName = YES;
                            indexForNameHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Type"] == NSOrderedSame) {
                            indexForTypeHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"TagId"] == NSOrderedSame || [[header lowercaseString] caseInsensitiveCompare:@"Tag Id"] == NSOrderedSame) {
                            loadByTagId = YES;
                            indexForTagIdHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"EPC"] == NSOrderedSame) {
                            loadByEPC = YES;
                            indexForEPCHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Latitude"] == NSOrderedSame) {
                            indexForLatitudeHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Longitude"] == NSOrderedSame) {
                            indexForLongitudeHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Altitude"] == NSOrderedSame) {
                            indexForAltitudeHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Address1"] == NSOrderedSame) {
                            indexForAddress1Header = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Address2"] == NSOrderedSame) {
                            indexForAddress2Header = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"TownOrCity"] == NSOrderedSame || [[header lowercaseString] caseInsensitiveCompare:@"Town Or City"] == NSOrderedSame) {
                            indexForTownOrCityHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"CountyOrDistrict"] == NSOrderedSame || [[header lowercaseString] caseInsensitiveCompare:@"County Or District"] == NSOrderedSame) {
                            indexForCountyOrDistrictHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"StateOrRegion"] == NSOrderedSame || [[header lowercaseString] caseInsensitiveCompare:@"State Or Region"] == NSOrderedSame) {
                            indexForStateOrRegionHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Postal"] == NSOrderedSame) {
                            indexForPostalHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Country"] == NSOrderedSame) {
                            indexForCountryHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Enabled"] == NSOrderedSame) {
                            indexForEnabledHeader = h;
                        }
                        // File is valid if there are name anbd type fields
                        if (indexForNameHeader >= 0 && indexForTypeHeader >= 0)
                            validFile = YES;
                    }
                }
                
                if (validFile && i > 0) {
                    VMLocation *lookupLocation = nil;
                    NSString *lookupField = nil;
                    if (lookupLocation == nil && loadByName) {
                        lookupField = [row objectAtIndex:indexForNameHeader];
                        lookupLocation = [VMLocationService readByName:lookupField];
                    }
                    if (lookupLocation == nil && loadByTagId) {
                        lookupField = [row objectAtIndex:indexForTagIdHeader];
                        lookupLocation = [VMLocationService readByTagId:lookupField];
                    }
                    if (lookupLocation == nil && indexForEPCHeader) {
                        lookupField = [row objectAtIndex:indexForEPCHeader];
                        lookupLocation = [VMLocationService readByEPC:lookupField];
                    }
                    
                    NSString *name;
                    if (indexForNameHeader > -1)
                        name = [row objectAtIndex:indexForNameHeader];
                    NSString *type;
                    if (indexForTypeHeader > -1)
                        type = [row objectAtIndex:indexForTypeHeader];
                    BOOL enabled = [[row objectAtIndex:indexForEnabledHeader] boolValue];
                    
                    if (lookupLocation == nil) {
                        VMLocation *location = [[VMLocation alloc] init:nil name:name type:type image:nil tagId:nil epc:nil latitude:nil longitude:nil altitude:nil address1:nil address2:nil townOrCity:nil countyOrDistrict:nil stateOrRegion:nil postal:nil country:nil];
                        if (location) {
                            if (indexForTagIdHeader > -1)
                                location.tagId = [row objectAtIndex:indexForTagIdHeader];
                            if (indexForEPCHeader > -1)
                                location.epc = [row objectAtIndex:indexForEPCHeader];
                            if (indexForLatitudeHeader > -1)
                                location.latitude = [row objectAtIndex:indexForLatitudeHeader];
                            if (indexForLongitudeHeader > -1)
                                location.longitude = [row objectAtIndex:indexForLongitudeHeader];
                            if (indexForAltitudeHeader > -1)
                                location.altitude = [row objectAtIndex:indexForAltitudeHeader];
                            if (indexForAddress1Header > -1)
                                location.address1 = [row objectAtIndex:indexForAddress1Header];
                            if (indexForAddress2Header > -1)
                                location.address2 = [row objectAtIndex:indexForAddress2Header];
                            if (indexForTownOrCityHeader > -1)
                                location.townOrCity = [row objectAtIndex:indexForTownOrCityHeader];
                            if (indexForCountyOrDistrictHeader > -1)
                                location.countyOrDistrict = [row objectAtIndex:indexForCountyOrDistrictHeader];
                            if (indexForStateOrRegionHeader > -1)
                                location.stateOrRegion = [row objectAtIndex:indexForStateOrRegionHeader];
                            if (indexForPostalHeader > -1)
                                location.postal = [row objectAtIndex:indexForPostalHeader];
                            if (indexForCountryHeader > -1)
                                location.country = [row objectAtIndex:indexForCountryHeader];
                            location.enabled = enabled;
                            [locations addObject:location];
                        }
                    }
                }
                i++;
            }
        }
    }
    
    return locations;
}

#pragma mark -
#pragma mark Navigation Methods
/****************************************************************************
 * Navigation Methods
 ****************************************************************************/

-(void)loadImportInventoryConfirmScreen {
    // Load the import inventory confirm controller
    VMImportLocationConfirmVC *importLocationConfirmViewController = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        importLocationConfirmViewController = [[VMImportLocationConfirmVC alloc] initWithNibName:@"VMImportLocationConfirmVC_iPhone" bundle:nil];
    } else {
        importLocationConfirmViewController = [[VMImportLocationConfirmVC alloc] initWithNibName:@"VMImportLocationConfirmVC_iPad" bundle:nil];
    }
//    importLocationConfirmViewController.mainViewController = self.mainViewController;
    importLocationConfirmViewController.lastViewController = self;
    importLocationConfirmViewController.workflow = self.workflow;
    [self presentModalViewController:importLocationConfirmViewController animated:NO];
    [self dispose];
}

@end
