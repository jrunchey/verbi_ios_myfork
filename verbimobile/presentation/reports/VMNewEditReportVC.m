//
//  VMNewEditReportVC.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMNewEditReportVC.h"
#import "VMAppDelegate.h"
#import "VMReportService.h"

@interface VMNewEditReportVC ()
@property(nonatomic, strong) NSArray *typePickerArray;
@property(nonatomic, strong) NSMutableArray *itemMastersArray;
@property(nonatomic, strong) NSMutableArray *itemMasterNamesArray;
@property(nonatomic) BOOL itemMastersAllSelected;
@property(nonatomic, strong) NSMutableArray *locationsArray;
@property(nonatomic, strong) NSMutableArray *locationNamesArray;
@property(nonatomic) BOOL locationsAllSelected;
@end

@implementation VMNewEditReportVC

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

// Edit
@synthesize reportToEdit = _reportToEdit;

// Scroll View Properties
@synthesize scrollView = _scrollView;

// Picker Properties
@synthesize typePickerView = _typePickerView;

// Buttons
@synthesize cancelButton = _cancelButton;
@synthesize okButton = _okButton;

// Text Views
@synthesize nameTextView = _nameTextView;

// Item Masters
@synthesize itemMastersAddButton = _itemMastersAddButton;
@synthesize itemMastersDeleteButton = _itemMastersDeleteButton;
@synthesize itemMastersSelectAllButton = _itemMastersSelectAllButton;
@synthesize itemMastersTableView = _itemMastersTableView;
@synthesize typePickerArray = _typePickerArray;
@synthesize itemMastersArray = _itemMastersArray;
@synthesize itemMasterNamesArray = _itemMasterNamesArray;
@synthesize itemMastersAllSelected = _itemMastersAllSelected;

// Locations
@synthesize locationsAddButton = _locationsAddButton;
@synthesize locationsDeleteButton = _locationsDeleteButton;
@synthesize locationsSelectAllButton = _locationsSelectAllButton;
@synthesize locationsTableView = _locationsTableView;
@synthesize locationsArray = _locationsArray;
@synthesize locationNamesArray = _locationNamesArray;
@synthesize locationsAllSelected = _locationsAllSelected;

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose {
    @try {
        self.nameTextView = nil;
        self.titleLabel = nil;
        
        self.cancelButton = nil;
        self.okButton = nil;
        
        if (self.typePickerArray)
            self.typePickerArray = nil;
        if (self.typePickerView)
            self.typePickerView = nil;
        
        if (self.scrollView)
            self.scrollView = nil;
        
        [super dispose];
        
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark Init Methods
/****************************************************************************
 * Init Methods
 ****************************************************************************/

-(void)initalizeFormFieldValues {
    if (self.reportToEdit == nil) {
        if (self.titleLabel)
            self.titleLabel.text = @"New Report";
        [self.nameTextView becomeFirstResponder];
    }
    else {
        if (self.titleLabel)
            self.titleLabel.text = @"Edit Report";
        if (self.nameTextView)
            self.nameTextView.text = [self.reportToEdit.name copy];
        
        if (self.typePickerArray == nil || self.typePickerArray.count == 0)
            [self reloadTypePickerArray];
        
        if (self.typePickerView) {
            // Assume fixed location and change using case insensitive compare
            NSInteger selectedPickerViewRow = 0;
            if ([[self.reportToEdit.type lowercaseString] caseInsensitiveCompare:INVENTORY_ITEM] == NSOrderedSame) {
                selectedPickerViewRow = 0;
            }
            else if ([[self.reportToEdit.type lowercaseString] caseInsensitiveCompare:INVENTORY_EVENT] == NSOrderedSame) {
                selectedPickerViewRow = 1;
            }
            [self.typePickerView selectRow:selectedPickerViewRow inComponent:0 animated:YES];
        }
    }
}

#pragma mark -
#pragma mark View Lifecycle Public
/****************************************************************************
 * View Lifecycle Public
 ****************************************************************************/

- (void)initializeUIOnFirstLoad {
    @try {
        [super initializeUIOnFirstLoad];
        self.nameTextView.returnKeyType = UIReturnKeyDone;
        
        // Initialize scroll view
        self.scrollView.contentSize = self.scrollView.frame.size;
        [self.scrollView flashScrollIndicators];
        
        // Scroll view touch event
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
        [self.scrollView addGestureRecognizer:singleTap];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIBeforeRendering {
    @try {
        [super initializeUIBeforeRendering];
        [self initalizeFormFieldValues];
        
        // Initialize picker arrays
        [self reloadTypePickerArray];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

- (void)initializeUIAfterRendering {
    @try {
        [super initializeUIAfterRendering];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO Methods
 ****************************************************************************/

// Hides keyboard when losing focus on a UITextView
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    
    if ([self.nameTextView isFirstResponder] && [touch view] != self.nameTextView) {
        [self.nameTextView resignFirstResponder];
    }
        
    [super touchesBegan:touches withEvent:event];
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)recognizer
{
    CGPoint touchPoint = [recognizer locationInView:self.scrollView];
    
    // Hides keyboard when losing focus on a UITextField contained within a scroll view
    if ([self.nameTextView isFirstResponder]) {
        CGRect bounds = self.nameTextView.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.nameTextView resignFirstResponder];
    }
    else if ([self.typePickerView isFirstResponder]) {
        CGRect bounds = self.typePickerView.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.typePickerView resignFirstResponder];
    }
}

- (IBAction)backPressed:(id)sender{
    @try {
        [super backPressed:sender];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
}

- (IBAction)cancelPressed:(id)sender{
    [self loadLastViewController];
}

- (IBAction)okPressed:(id)sender{
    NSString *alertViewTitle = @"Confirm Save";
    NSString *alertMessage = @"Are you sure you wish to save this report?";
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: alertViewTitle
                          message: alertMessage
                          delegate: self
                          cancelButtonTitle:@"No"
                          otherButtonTitles:@"Yes",nil];    // value passed to override of alertView method
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		// do nothing except close alertView
	}
	else {
        BOOL success = NO;
        if ([[alertView.title lowercaseString] caseInsensitiveCompare:@"Confirm Save"] == NSOrderedSame)
            success = [self saveReport];
        
        if (success)
            [self loadLastViewController];
        else {
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: @"Save failed"
                                  message: @"Report already exists"\
                                  delegate: nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
	}
}

-(void)setFocusedFieldText:(NSString *)text {
    // populate field that has focus with barcode value
    if ([self.nameTextView isFirstResponder]) {
        [self.nameTextView setText:text];
        [self.nameTextView resignFirstResponder];    // automatically lose focus to hide keyboard
    }
}

#pragma mark -
#pragma mark PickerView DataSource
/****************************************************************************
 * PickerView DataSource
 ****************************************************************************/

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if ([pickerView isEqual: self.typePickerView]) {
        return [self.typePickerArray count];
    }
    else
        return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if ([pickerView isEqual: self.typePickerView]) {
        return [self.typePickerArray objectAtIndex:row];
    }
    else
        return 0;
}

-(void)reloadTypePickerArray {
    self.typePickerArray = [[NSArray alloc] initWithObjects:
                        @"Inventory Item", @"Inventory Event", nil];
}

#pragma mark -
#pragma mark PickerView Delegate
/****************************************************************************
 * PickerView Delegate
 ****************************************************************************/

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if ([pickerView isEqual: self.typePickerView]) {
        //int typeIndex = [[typePickerArray objectAtIndex:row] intValue];
        // do something
    }
}

#pragma mark -
#pragma mark CRUD Methods
/****************************************************************************
 * CRUD Methods
 ****************************************************************************/

-(BOOL)saveReport {
    @autoreleasepool {
        NSString *name = self.nameTextView.text;
        
        // Type
        NSInteger typeRow = [self.typePickerView selectedRowInComponent:0];
        NSString *type = [[self.typePickerArray objectAtIndex:typeRow] lowercaseString];
        
        if (self.reportToEdit == nil) {
            @try {
                VMReport *reportFound = [VMReportService readByName:name];
                if (reportFound)
                    return NO;
                
                VMReport *newReport = [[VMReport alloc] init:nil name:name type:type];
                newReport.createdDate = [NSDate date];
                newReport.lastUpdatedDate = [NSDate date];
                [VMReportService create:newReport];
            }
            @catch (NSException *exception) {
                NSLog(@"An exception occurred: %@", exception.reason);
            }
        }
        else {
            @try {
                self.reportToEdit.name = name;
                self.reportToEdit.type = type;
                self.reportToEdit.lastUpdatedDate = [NSDate date];
                
                [VMReportService update:self.reportToEdit];
            }
            @catch (NSException *exception) {
                NSLog(@"An exception occurred: %@", exception.reason);
            }
        }
    }
    return YES;
}

@end
