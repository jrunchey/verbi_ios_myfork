//
//  VMReportResultVC.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMReportResultVC.h"

@interface VMReportResultVC ()
@property(nonatomic, strong) NSMutableArray *reportResultsArray;
@end

@implementation VMReportResultVC

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

// Buttons
@synthesize runButton = _runButton;

// Run
@synthesize reportToRun = _reportToRun;

// Table View
@synthesize reportResultsTableView = _reportResultsTableView;
@synthesize reportResultsArray = _reportResultsArray;

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose {
    @try {
        self.reportToRun = nil;
        self.runButton = nil;
        self.reportResultsTableView = nil;
        
        if (self.reportResultsArray) {
            [self.reportResultsArray removeAllObjects];
            self.reportResultsArray = nil;
        }
        
        [super dispose];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark View Lifecycle Public
/****************************************************************************
 * View Lifecycle Public
 ****************************************************************************/

- (void)initializeUIOnFirstLoad {
    @try {
        [super initializeUIOnFirstLoad];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIBeforeRendering {
    @try {
        [super initializeUIBeforeRendering];
        self.titleLabel.text = [NSString stringWithFormat:@"%@ - %@", self.titleLabel.text, self.reportToRun.name];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

- (void)initializeUIAfterRendering {
    @try {
        [super initializeUIAfterRendering];
        [self reloadTableView];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO Methods
 ****************************************************************************/

// Hides keyboard when losing focus on a UITextView
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    
    if ([self.reportResultsTableView isFirstResponder] && [touch view] != self.reportResultsTableView) {
        [self.reportResultsTableView resignFirstResponder];
    }
    
    [super touchesBegan:touches withEvent:event];
}

- (void)backPressed:(id)sender{
    @try {
        [super backPressed:sender];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark Table View Methods
/****************************************************************************
 * Table View Methods
 ****************************************************************************/

// Defines the table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Defines the table - Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.reportResultsArray.count;
}

// Fill the table - Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (self.reportResultsArray && self.reportResultsArray.count > 0) {
        VMReportResult *reportResult = [self.reportResultsArray objectAtIndex:indexPath.row];
        cell.textLabel.text = [reportResult.value copy];
    }
    
    /*
    // Convert all row selections to check marks
    NSIndexPath *selection = [tableView indexPathForSelectedRow];
    if (selection && selection.row == indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
     */
    
    return cell;
}

/*
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        // uncheck
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else {
        // check
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
}
 */

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Search Reports";
}

-(void)reloadTableView {
    [self reloadReportResultsArray];
    [self.reportResultsTableView reloadData];
}

-(void)reloadReportResultsArray {
    self.reportResultsArray = [VMReportService processReport:self.reportToRun];
}

@end
