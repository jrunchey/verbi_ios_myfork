//
//  VMReportsVC.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMReportsVC.h"
#import "VMReportService.h"
#import "VMNewEditReportVC.h"
#import "VMReportResultVC.h"
#import "VMSession.h"

@interface VMReportsVC ()
@property(nonatomic, strong) NSMutableArray *reportsArray;
@property(nonatomic, strong) NSMutableArray *reportNamesArray;
@property(nonatomic) BOOL allSelected;
@property(nonatomic) BOOL searchAll;
@end

@implementation VMReportsVC

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

// Buttons
@synthesize addButton = _addButton;
@synthesize deleteButton = _deleteButton;
@synthesize editButton = _editButton;
@synthesize selectAllButton = _selectAllButton;
@synthesize runButton = _runButton;
@synthesize searchTextField = _searchTextField;
@synthesize clearButton = _clearButton;

// Table View
@synthesize reportsTableView = _reportsTableView;
@synthesize reportsArray = _reportsArray;
@synthesize reportNamesArray = _reportNamesArray;
@synthesize allSelected = _allSelected;
@synthesize searchAll = _searchAll;

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose {
    @try {
        self.addButton = nil;
        self.deleteButton = nil;
        self.editButton = nil;
        self.selectAllButton = nil;
        self.runButton = nil;
        self.searchTextField = nil;
        self.clearButton = nil;
        self.reportsTableView = nil;
        
        if (self.reportsArray) {
            [self.reportsArray removeAllObjects];
            self.reportsArray = nil;
        }
        if (self.reportNamesArray) {
            [self.reportNamesArray removeAllObjects];
            self.reportNamesArray = nil;
        }
        
        [super dispose];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark View Lifecycle Public
/****************************************************************************
 * View Lifecycle Public
 ****************************************************************************/

- (void)initializeUIOnFirstLoad {
    @try {
        [super initializeUIOnFirstLoad];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIBeforeRendering {
    @try {
        [super initializeUIBeforeRendering];
        self.searchTextField.delegate = self;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

- (void)initializeUIAfterRendering {
    @try {
        [super initializeUIAfterRendering];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO Methods
 ****************************************************************************/

// Hides keyboard when losing focus on a UITextView
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    
    if ([self.searchTextField isFirstResponder] && [touch view] != self.searchTextField) {
        [self.searchTextField resignFirstResponder];
    }
    else if ([self.reportsTableView isFirstResponder] && [touch view] != self.reportsTableView) {
        [self.reportsTableView resignFirstResponder];
    }
    
    [super touchesBegan:touches withEvent:event];
}

- (IBAction)backPressed:(id)sender{
    @try {
        [super backPressed:sender];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
}

- (IBAction)addButtonPressed:(id)sender{
    [self loadNewEditReportScreen:nil];
}

- (IBAction)deleteButtonPressed:(id)sender{
    NSMutableArray *reportNames;
    NSString *alertViewTitle;
    
    NSArray *indexes = [VMUtilityService getCheckedOrSelectedIndexes:self.reportsTableView];
    if (indexes.count == 0) {
        alertViewTitle = @"Delete";
        NSString *alertMessage = @"Select a report.";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else {
        reportNames = [[NSMutableArray alloc] init];
        for (NSIndexPath *path in indexes) {
            NSUInteger index = [path indexAtPosition:[path length] - 1];
            NSString *reportName = [self.reportsArray objectAtIndex:index];
            [reportNames addObject:reportName];
        }
        
        alertViewTitle = @"Confirm Delete";
        NSString *alertMessage = [NSString stringWithFormat:@"Are you sure you wish to delete these %i report(s)?", reportNames.count];
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"No"
                              otherButtonTitles:@"Yes",nil];    // value passed to override of alertView method
        [alert show];
    }
}

- (IBAction)editButtonPressed:(id)sender{
    VMReport *report;
    NSString *alertViewTitle = @"Edit";
    
    NSArray *indexes = [VMUtilityService getCheckedOrSelectedIndexes:self.reportsTableView];
    if (indexes.count == 0) {
        NSString *alertMessage = @"Select a report.";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if (indexes.count == 1) {
        for (NSIndexPath *path in indexes) {
            NSUInteger index = [path indexAtPosition:[path length] - 1];
            report = [self.reportsArray objectAtIndex:index];
        }
        if (report) {
            [self loadNewEditReportScreen:report];
        }
    }
    else {
        NSString *alertMessage = @"Select a single report.";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)selectAllButtonPressed:(id)sender {
    if (self.allSelected) {
        int rowCount = [self.reportsTableView numberOfRowsInSection:0];
        for (int i = 0; i < rowCount; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];    // convert iterator to indexPath
            UITableViewCell *cell = [self.reportsTableView cellForRowAtIndexPath:indexPath];    // get cell using indexPath
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        [self.selectAllButton setTitle:@"Select All" forState:UIControlStateNormal];
        self.allSelected = NO;
    }
    else {
        int rowCount = [self.reportsTableView numberOfRowsInSection:0];
        for (int i = 0; i < rowCount; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];    // convert iterator to indexPath
            UITableViewCell *cell = [self.reportsTableView cellForRowAtIndexPath:indexPath];    // get cell using indexPath
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        [self.selectAllButton setTitle:@"Deselect All" forState:UIControlStateNormal];
        self.allSelected = YES;
    }
}

- (IBAction)runButtonPressed:(id)sender {
    VMReport *report;
    NSString *alertViewTitle = @"Edit";
    
    NSArray *indexes = [VMUtilityService getCheckedOrSelectedIndexes:self.reportsTableView];
    if (indexes.count == 0) {
        NSString *alertMessage = @"Select a report.";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if (indexes.count == 1) {
        for (NSIndexPath *path in indexes) {
            NSUInteger index = [path indexAtPosition:[path length] - 1];
            report = [self.reportsArray objectAtIndex:index];
        }
        if (report) {
            [self loadReportResultScreen:report];
        }
    }
    else {
        NSString *alertMessage = @"Select a single report.";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void)resetSearch {
    self.activityIndicatorView.hidden = NO;
    [self.activityIndicatorView startAnimating];
    if (self.reportsArray) {
        [self.reportsArray removeAllObjects];
    }
    if (self.reportNamesArray) {
        [self.reportNamesArray removeAllObjects];
    }
    [self.reportsTableView reloadData];
    [self.activityIndicatorView stopAnimating];
    self.activityIndicatorView.hidden = YES;
}

- (IBAction)search:(id)sender{
    if (self.searchTextField) {
        [self resetSearch];
        
        self.activityIndicatorView.hidden = NO;
        [self.activityIndicatorView startAnimating];
        @autoreleasepool {
            if ([self.searchTextField.text length] == 0 && self.searchAll) {
                self.reportsArray = [VMReportService readAll];
                self.searchAll = NO;
            }
            else if ([self.searchTextField.text length] > 0) {
                self.reportsArray = [VMReportService readLikeName:self.searchTextField.text];
            }
            
            if (self.reportsArray && self.reportsArray.count > 0) {
                self.reportNamesArray = [[NSMutableArray alloc] init];
                for (VMReport *report in self.reportsArray) {
                    [self.reportNamesArray addObject:report.name];
                }
            }
        }
        
        [self.reportsTableView reloadData];
        [self.activityIndicatorView stopAnimating];
        self.activityIndicatorView.hidden = YES;
    }
}

- (IBAction)clearPressed:(id)sender{
    if (self.searchTextField)
        self.searchTextField.text = nil;
    [self resetSearch];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		// do nothing except close alertView
	}
	else {
        if ([[alertView.title lowercaseString] caseInsensitiveCompare:@"Confirm Delete"] == NSOrderedSame) {
            NSMutableArray *deletedReports = [self deleteReports];
            [self reloadTableView];
            
            NSString *alertViewTitle = @"Delete Completed";
            NSString *alertMessage = [[NSString alloc] initWithFormat:@"%i reports deleted.", deletedReports.count];
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: alertViewTitle
                                  message: alertMessage
                                  delegate: self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
        else if ([[alertView.title lowercaseString] caseInsensitiveCompare:@"Selection Not Found"] == NSOrderedSame) {
            // If report does not exist, then navigate to New Report screen
            VMReport *report = [[VMReport alloc] init:nil name:nil type:nil];
            [self loadNewEditReportScreen:report];
        }
	}
}

// Handles when you press return in either text field
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    // If you pressed return in the password text field we will trigger loginPressed for you
    if (textField.tag == kSearchFieldTag){
        if ([self.searchTextField.text length] == 0) {
            self.searchAll = YES;
            [self search:nil];
        }
    }
    
    // this closed the keyboard if you press return in either text field
    return [textField resignFirstResponder];
}

// If you leave the text field we will close the keyboard
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
}

#pragma mark -
#pragma mark Table View Methods
/****************************************************************************
 * Table View Methods
 ****************************************************************************/

// Defines the table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Defines the table - Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.reportsArray.count;
}

// Fill the table - Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (self.reportsArray && self.reportsArray.count > 0) {
        VMReport *report = [self.reportsArray objectAtIndex:indexPath.row];
        cell.textLabel.text = [report.name copy];
    }
    
    // Convert all row selections to check marks
    NSIndexPath *selection = [tableView indexPathForSelectedRow];
    if (selection && selection.row == indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        // uncheck
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else {
        // check
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Search Reports";
}

-(void)reloadTableView {
    [self reloadReportsArray];
    [self.reportsTableView reloadData];
}

-(void)reloadReportsArray {
    int count = [VMReportService totalNumberOfRecords];
    if (count > 0) {
        self.reportsArray = [VMReportService readAll];
        if (self.reportsArray && self.reportsArray.count > 0) {
            self.reportNamesArray = [[NSMutableArray alloc] init];
            for (VMReport *report in self.reportsArray) {
                [self.reportNamesArray addObject:report.name];
            }
        }
    }
}

#pragma mark -
#pragma mark Navigation Methods
/****************************************************************************
 * Navigation Methods
 ****************************************************************************/

-(void)loadNewEditReportScreen:(VMReport *)report {
    @try {
        // Load the new report controller
        VMNewEditReportVC *newEditReportViewController = nil;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            newEditReportViewController = [[VMNewEditReportVC alloc] initWithNibName:@"VMNewEditReportVC_iPhone" bundle:nil];
        } else {
            newEditReportViewController = [[VMNewEditReportVC alloc] initWithNibName:@"VMNewEditReportVC_iPad" bundle:nil];
        }
//        newEditReportViewController.mainViewController = self.mainViewController;
        newEditReportViewController.lastViewController = self;
        if (report)
            newEditReportViewController.reportToEdit = report;
        [self presentModalViewController:newEditReportViewController animated:NO];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", e.reason);
    }
}

-(void)loadReportResultScreen:(VMReport *)report {
    @try {
        // Load the new report controller
        VMReportResultVC *reportResultViewController = nil;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            reportResultViewController = [[VMReportResultVC alloc] initWithNibName:@"VMReportResultVC_iPhone" bundle:nil];
        } else {
            reportResultViewController = [[VMReportResultVC alloc] initWithNibName:@"VMReportResultVC_iPad" bundle:nil];
        }
//        reportResultViewController.mainViewController = self.mainViewController;
        reportResultViewController.lastViewController = self;
        if (report)
            reportResultViewController.reportToRun = report;
        [self presentModalViewController:reportResultViewController animated:NO];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", e.reason);
    }
}

-(void)lookupReport:(NSString *)reportName {
    VMReport *report;
    @try {
        // Lookup report from reportsArray
        NSUInteger index = [self.reportNamesArray indexOfObject:reportName];
        report = [self.reportsArray objectAtIndex:index];
        if (report)
            [self loadNewEditReportScreen:report];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark CRUD Methods
/****************************************************************************
 * CRUD Methods
 ****************************************************************************/

-(NSMutableArray *)deleteReports {
    NSMutableArray *deletedReports;
    @autoreleasepool {
        @try {
            //NSArray *indexes = [reportsTableView indexPathsForSelectedRows];
            NSArray *indexes = [VMUtilityService getCheckedOrSelectedIndexes:self.reportsTableView];
            if (indexes.count > 0) {
                deletedReports = [[NSMutableArray alloc] init];
                for (NSIndexPath *path in indexes) {
                    NSUInteger index = [path indexAtPosition:[path length] - 1];
                    VMReport *report = [self.reportsArray objectAtIndex:index];
                    [deletedReports addObject:report.name];
                }
            }
            [VMReportService deleteByNames:deletedReports];
        }
        @catch (NSException *e) {
            NSLog(@"An exception has occurred: %@", e.reason);
        }
    }
    return deletedReports;
}


@end
