//
//  VMNewEditReportVC.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMParentVC.h"
#import "VMReport.h"

@interface VMNewEditReportVC : VMParentVC <UIAlertViewDelegate>

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

// Edit Properties
@property(nonatomic, strong) VMReport *reportToEdit;

// Scroll View Properties
@property(nonatomic, weak) IBOutlet UIScrollView *scrollView;

// Picker Properties
@property(nonatomic, weak) IBOutlet UIPickerView *typePickerView;

@property(nonatomic, weak) IBOutlet UIButton *cancelButton;
@property(nonatomic, weak) IBOutlet UIButton *okButton;
@property(nonatomic, weak) IBOutlet UITextField *nameTextView;
@property(nonatomic, weak) IBOutlet UIButton *itemMastersAddButton;
@property(nonatomic, weak) IBOutlet UIButton *itemMastersDeleteButton;
@property(nonatomic, weak) IBOutlet UIButton *itemMastersSelectAllButton;
@property(nonatomic, weak) IBOutlet UITableView *itemMastersTableView;
@property(nonatomic, weak) IBOutlet UIButton *locationsAddButton;
@property(nonatomic, weak) IBOutlet UIButton *locationsDeleteButton;
@property(nonatomic, weak) IBOutlet UIButton *locationsSelectAllButton;
@property(nonatomic, weak) IBOutlet UITableView *locationsTableView;

@end
