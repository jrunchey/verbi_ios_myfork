//
//  VMReportResultVC.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMParentVC.h"
#import "VMReportService.h"

@interface VMReportResultVC : VMParentVC <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate, UITextFieldDelegate>

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@property(nonatomic, weak) IBOutlet UIButton *runButton;
@property(nonatomic, weak) IBOutlet UITableView *reportResultsTableView;
@property(nonatomic, strong) VMReport *reportToRun;

@end
