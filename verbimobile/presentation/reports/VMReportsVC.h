//
//  VMReportsVC.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMParentVC.h"

// Identifier so we know what UITextField the user pressed return in
#define kSearchFieldTag 1

@interface VMReportsVC : VMParentVC <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate, UITextFieldDelegate>

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@property(nonatomic, weak) IBOutlet UIButton *addButton;
@property(nonatomic, weak) IBOutlet UIButton *deleteButton;
@property(nonatomic, weak) IBOutlet UIButton *editButton;
@property(nonatomic, weak) IBOutlet UIButton *selectAllButton;
@property(nonatomic, weak) IBOutlet UIButton *runButton;
@property(nonatomic, weak) IBOutlet UITextField *searchTextField;
@property(nonatomic, weak) IBOutlet UIButton *clearButton;
@property(nonatomic, weak) IBOutlet UITableView *reportsTableView;

@end
