//
//  ImportCategoriesSummaryViewController.h
//  verbimobile
//
//  Created by Rob Hotaling on 3/18/13.
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImportCategoriesSummaryViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIBarButtonItem *okMenuBtn;


@end
