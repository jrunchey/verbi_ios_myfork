//
//  LoginViewController.h
//  verbimobile
//
//  Created by Rob Hotaling on 3/10/13.
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

// Identifier so we know what UITextField the user pressed return in
#define kPasswordFieldTag 1

@interface LoginViewController : UIViewController <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *usernameTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UIButton *loginBtn;

@end
