//
//  ImportACLSummaryViewController.m
//  verbimobile
//
//  Created by Rob Hotaling on 3/18/13.
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "ImportACLSummaryViewController.h"
#import "ECSlidingViewController.h"
#import "LeftMenuViewController.h"
#import "RightMenuViewController.h"

@interface ImportACLSummaryViewController ()

@end

@implementation ImportACLSummaryViewController

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/
@synthesize okMenuBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // These 3 give effect that view controller is hovering slightly above menu
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    // Checks to see if we already have a Menu view controller from storyboard underneath our menu. If we don't, then create it
    if (![self.slidingViewController.underRightViewController isKindOfClass:[RightMenuViewController class]]) {
        self.slidingViewController.underRightViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"RightMenu"];
    }
    
    // gives us the ability to swipe from left to right to reveal menu
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    [self.okMenuBtn setTarget:self];
    [self.okMenuBtn setAction:@selector(revealRightMenu:)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)revealRightMenu:(id)sender {
    [self.slidingViewController anchorTopViewTo:ECLeft];
}

- (void)viewDidUnload {
    [self setOkMenuBtn:nil];
    [super viewDidUnload];
}

@end
