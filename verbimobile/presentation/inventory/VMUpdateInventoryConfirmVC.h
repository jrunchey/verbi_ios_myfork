//
//  VMUpdateInventoryConfirmViewController.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMParentVC.h"

@interface VMUpdateInventoryConfirmVC : VMParentVC <UITableViewDelegate, UITableViewDataSource,UIAlertViewDelegate>

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@property(nonatomic, weak) IBOutlet UITableView *itemsTableView;

@end
