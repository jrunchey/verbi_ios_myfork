//
//  VMImportInventoryViewController.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMParentVC.h"
#import "NSString+ParsingExtensions_.h"
#import "VMInventoryService.h"
#import "VMItemMaster.h"
#import "VMItem.h"

@interface VMImportInventoryVC : VMParentVC

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@property (nonatomic, strong) NSURL *importFileURL;
@property (nonatomic, weak) IBOutlet UITextField *importFileField;

@end
