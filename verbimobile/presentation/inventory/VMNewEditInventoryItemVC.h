//
//  VMNewEditInventoryItemViewController.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMAutoIDCapableVC.h"
#import "VMItemMaster.h"
#import "VMItem.h"
#import "VMAddInventoryItemsVC.h"

@interface VMNewEditInventoryItemVC : VMAutoIDCapableVC <UITableViewDelegate,UITableViewDataSource, UIAlertViewDelegate>

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@property(nonatomic, strong) VMAddInventoryItemsVC *addInventoryItemsController;

// Edit
@property(nonatomic, strong) VMItem *itemToEdit;

// Scroll View objects
@property(nonatomic, weak) IBOutlet UIScrollView *scrollView;

// Buttons
@property(nonatomic, weak) IBOutlet UIButton *cancelButton;
@property(nonatomic, weak) IBOutlet UIButton *lookupButton;
@property(nonatomic, weak) IBOutlet UIButton *okButton;
@property(nonatomic, weak) IBOutlet UIButton *processModeButton;
@property(nonatomic, weak) IBOutlet UIButton *selectAllButton;
@property(nonatomic, weak) IBOutlet UIButton *addButton;
@property(nonatomic, weak) IBOutlet UIButton *removeButton;

// Text Views
@property(nonatomic, weak) IBOutlet UITextField *nameTextField;
@property(nonatomic, weak) IBOutlet UITextField *quantityTextField;
@property(nonatomic, weak) IBOutlet UITextField *valueTextField;
@property(nonatomic, weak) IBOutlet UITextField *originTextField;
@property(nonatomic, weak) IBOutlet UITextField *lotTextField;

// Table Views
@property(nonatomic, weak) IBOutlet UITableView *tagIdTableView;

@end
