//
//  VMImportInventoryItemMasterConfirmVC.m
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMImportInventoryItemMasterConfirmVC.h"
#import "VMInventoryItemMasterListVC.h"
#import "VMImportItemMasterFileWorkflowStep.h"
#import "VMImportItemMasterFileConfirmWorkflowStep.h"
#import "VMInventoryService.h"

@interface VMImportInventoryItemMasterConfirmVC ()

@end

@implementation VMImportInventoryItemMasterConfirmVC

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@synthesize importFileURL = _importFileURL;
@synthesize itemMastersTableView = _itemMastersTableView;

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose {
    @try {
        self.itemMastersTableView = nil;
        [super dispose];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark View Lifecycle Public
/****************************************************************************
 * View Lifecycle Public
 ****************************************************************************/

- (void)initializeUIOnFirstLoad {
    @try {
        [super initializeUIOnFirstLoad];
        
        // Set workflow step
        if (self.workflow) {
            VMWorkflowStep *workflowStep = [[VMImportItemMasterFileConfirmWorkflowStep alloc] init:self.workflow];
            if (workflowStep) {
                self.workflowStep = workflowStep;
                [self.workflow addWorkflowStep:self.workflowStep];
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIBeforeRendering {
    @try {
        [super initializeUIBeforeRendering];
        self.activityIndicatorView.hidden = YES;
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIAfterRendering {
    @try {
        [super initializeUIAfterRendering];
        self.activityIndicatorView.hidden = NO;
        [self.activityIndicatorView startAnimating];
        
        @autoreleasepool {
            @try {
                NSError *outError;
                NSString *importFileContents = nil;
                if (self.importFileURL) {
                    importFileContents = [NSString stringWithContentsOfURL:self.importFileURL encoding:NSUTF8StringEncoding error:&outError];
                    //NSLog(importFileContents);
                    
                    if (importFileContents && self.workflowStep) {
                        [self loadItemMastersFromCSV:[importFileContents csvRows]];
                        [self reloadTableView];
                    }
                    else {
                        NSString *alertViewTitle;
                        alertViewTitle = @"Import - Error";
                        NSString *alertMessage = @"Error importing file";
                        UIAlertView *alert = [[UIAlertView alloc]
                                              initWithTitle: alertViewTitle
                                              message: alertMessage
                                              delegate: self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
                        [alert show];
                    }
                }
                else {
                    NSString *alertViewTitle;
                    alertViewTitle = @"Import - Error";
                    NSString *alertMessage = @"Import failed. A file was not specified.";
                    UIAlertView *alert = [[UIAlertView alloc]
                                          initWithTitle: alertViewTitle
                                          message: alertMessage
                                          delegate: self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
                    [alert show];
                }
            }
            @catch (NSException *exception) {
                NSLog(@"An exception occured: %@", [exception reason]);
            }
        }
        
        [self.activityIndicatorView stopAnimating];
        self.activityIndicatorView.hidden = YES;
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO methods
 ****************************************************************************/

- (IBAction)cancelPressed:(id)sender{
    [self loadMainViewController];
}

- (IBAction)verifyPressed:(id)sender{
    @autoreleasepool {
        @try {
            NSMutableArray *newItemMasters = ((VMImportItemMasterFileConfirmWorkflowStep *)self.workflowStep).creatingItemMastersArray;
            [VMInventoryService createItemMasters:newItemMasters];
            [self loadMainViewController];
        }
        @catch (NSException *exception) {
            NSLog(@"An exception has occurred: %@", [exception reason]);
            NSString *alertViewTitle;
            alertViewTitle = @"Import - Error";
            NSString *alertMessage = [[NSString alloc] initWithFormat:@"Import failed: %@", [exception reason]];
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: alertViewTitle
                                  message: alertMessage
                                  delegate: self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
            [self loadMainViewController];
        }
    }
}

-(void) loadItemMastersFromCSV:(NSArray *)csvArray {
    NSMutableArray *allItemMasters = ((VMImportItemMasterFileConfirmWorkflowStep *)self.workflowStep).itemMastersArray;
    NSMutableArray *newItemMasters = ((VMImportItemMasterFileConfirmWorkflowStep *)self.workflowStep).creatingItemMastersArray;
    NSMutableArray *existingItemMasterNames = ((VMImportItemMasterFileConfirmWorkflowStep *)self.workflowStep).existingItemMasterNamesArray;
    BOOL validFile = NO;
    BOOL loadByName = NO;
    BOOL loadByUPC = NO;
    int indexForNameHeader = -1;
    int indexForTypeHeader = -1;
    int indexForDescriptionHeader = -1;
    int indexForUPCHeader = -1;
    int indexForVendorHeader = -1;
    int indexForVendorCodeHeader = -1;
    int indexForIncrementQtyHeader = -1;
    int indexForPriceHeader = -1;
    int indexForUOMHeader = -1;
    int indexForExpirationHeader = -1;
    int indexForExpirationTimePeriodHeader = -1;
    int indexForExpirationTimeUnitHeader = -1;
    int indexForEnabledHeader = -1;
    
    if (csvArray && csvArray.count > 0) {
        @autoreleasepool {
            int i = 0;
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
            [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
            
            for (NSArray *row in csvArray) {
                //1st row is a header - skip
                if (i == 0) {
                    for (int h = 0; h < [row count]; h++) {
                        NSString *header = [row objectAtIndex:h];
                        if ([[header lowercaseString] caseInsensitiveCompare:@"Name"] == NSOrderedSame) {
                            loadByName = YES;
                            indexForNameHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Type"] == NSOrderedSame) {
                            indexForTypeHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Description"] == NSOrderedSame) {
                            indexForDescriptionHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"UPC"] == NSOrderedSame|| [[header lowercaseString] caseInsensitiveCompare:@"EAN"] == NSOrderedSame) {
                            loadByUPC = YES;
                            indexForUPCHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Vendor"] == NSOrderedSame) {
                            indexForVendorHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"VendorCode"] == NSOrderedSame || [[header lowercaseString] caseInsensitiveCompare:@"Vendor Code"] == NSOrderedSame) {
                            indexForVendorCodeHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"IncrementQty"] == NSOrderedSame || [[header lowercaseString] caseInsensitiveCompare:@"Increment Qty"] == NSOrderedSame) {
                            indexForIncrementQtyHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Price"] == NSOrderedSame) {
                            indexForPriceHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"UOM"] == NSOrderedSame) {
                            indexForUOMHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Expiration"] == NSOrderedSame) {
                            indexForExpirationHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"ExpirationTimePeriod"] == NSOrderedSame || [[header lowercaseString] caseInsensitiveCompare:@"Expiration Time Period"] == NSOrderedSame) {
                            indexForExpirationTimePeriodHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"ExpirationTimeUnit"] == NSOrderedSame || [[header lowercaseString] caseInsensitiveCompare:@"Expiration Time Unit"] == NSOrderedSame) {
                            indexForExpirationTimeUnitHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Enabled"] == NSOrderedSame) {
                            indexForEnabledHeader = h;
                        }
                        // File is valid if there are name anbd type fields
                        if (indexForNameHeader >= 0 && indexForTypeHeader >= 0)
                            validFile = YES;
                    }
                }
                
                if (validFile && i > 0) {
                    VMItemMaster *lookupItemMaster = nil;
                    NSString *lookupField = nil;
                    if (lookupItemMaster == nil && loadByName) {
                        lookupField = [row objectAtIndex:indexForNameHeader];
                        lookupItemMaster = [VMInventoryService readItemMasterByName:lookupField];
                    }
                    if (lookupItemMaster == nil && indexForUPCHeader) {
                        lookupField = [row objectAtIndex:indexForUPCHeader];
                        lookupItemMaster = [VMInventoryService readItemMasterByUPC:lookupField];
                    }
                    
                    NSString *name;
                    if (indexForNameHeader > -1)
                        name = [row objectAtIndex:indexForNameHeader];
                    NSString *type;
                    if (indexForTypeHeader > -1)
                        type = [row objectAtIndex:indexForTypeHeader];
                    NSNumber *incrementQty = [numberFormatter numberFromString:[row objectAtIndex:indexForIncrementQtyHeader]];
                    NSNumber *price = [numberFormatter numberFromString:[row objectAtIndex:indexForPriceHeader]];
                    BOOL expiration = [[row objectAtIndex:indexForExpirationHeader] boolValue];
                    NSNumber *expirationTimeUnit = [numberFormatter numberFromString:[row objectAtIndex:indexForExpirationTimeUnitHeader]];
                    BOOL enabled = [[row objectAtIndex:indexForEnabledHeader] boolValue];
                    
                    VMItemMaster *itemMaster = [[VMItemMaster alloc] init:nil name:name type:type description:nil upc:nil vendor:nil vendorCode:nil incrementQuantity:[incrementQty floatValue] price:[price floatValue] uom:nil image:nil];
                    if (itemMaster) {
                        if (indexForUPCHeader > -1)
                            itemMaster.upc = [row objectAtIndex:indexForUPCHeader];
                        if (indexForVendorHeader > -1)
                            itemMaster.vendor = [row objectAtIndex:indexForVendorHeader];
                        if (indexForVendorCodeHeader > -1)
                            itemMaster.vendorCode = [row objectAtIndex:indexForVendorCodeHeader];
                        if (indexForUOMHeader > -1)
                            itemMaster.uom = [row objectAtIndex:indexForUOMHeader];
                        if (indexForExpirationHeader > -1)
                            itemMaster.expiration = expiration;
                        if (indexForExpirationTimePeriodHeader > -1)
                            itemMaster.expirationTimePeriod = [expirationTimeUnit intValue];
                        if (indexForExpirationTimeUnitHeader > -1)
                            itemMaster.expirationTimeUnit = [row objectAtIndex:indexForExpirationTimeUnitHeader];
                        itemMaster.enabled = enabled;
                        
                        [allItemMasters addObject:itemMaster];
                        // If itemMaster already exists in database, add to existingItemMasters array for notifying user, else add to newItemMasters
                        if (lookupItemMaster) {
                            [existingItemMasterNames addObject:name];
                        }
                        else {
                            [newItemMasters addObject:itemMaster];
                        }
                    }
                }
                i++;
            }
        }
    }
}

#pragma mark -
#pragma mark Table view methods
/****************************************************************************
 * Table view methods
 ****************************************************************************/

// Defines the table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Defines the table - Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int count = 0;
    VMImportItemMasterFileConfirmWorkflowStep *workflowStep = [self.workflow.workflowSteps objectForKey:WKFL_STEP_INV_IMPORT_ITEM_MASTER_FILE_CONFIRM];
    if (workflowStep && workflowStep.itemMastersArray) {
        count = workflowStep.itemMastersArray.count;
    }
    return count;
}

// Fill the table - Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    @autoreleasepool {
        VMImportItemMasterFileConfirmWorkflowStep *workflowStep = [self.workflow.workflowSteps objectForKey:WKFL_STEP_INV_IMPORT_ITEM_MASTER_FILE_CONFIRM];
        if (workflowStep && workflowStep.itemMastersArray && workflowStep.itemMastersArray.count > 0) {
            VMItemMaster *itemMaster = [workflowStep.itemMastersArray objectAtIndex:indexPath.row];
            if (itemMaster) {
                BOOL itemMasterExists = NO;
                NSMutableArray *existingItemMasterNames = ((VMImportItemMasterFileConfirmWorkflowStep *)self.workflowStep).existingItemMasterNamesArray;
                if ([existingItemMasterNames containsObject:itemMaster.name])
                    itemMasterExists = YES;
                
                // Cell Title Text
                
                // define the range you're interested in
                NSRange stringRange = {0, MIN([itemMaster.type length], 1)};
                // adjust the range to include dependent chars
                stringRange = [itemMaster.type rangeOfComposedCharacterSequencesForRange:stringRange];
                // Now you can create the short string
                NSString *typeSubstring = [[itemMaster.type uppercaseString] substringWithRange:stringRange];
                
                cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@", typeSubstring, itemMaster.name];
                
                // Cell Subtitle Text
                
                NSMutableString *detailText = [[NSMutableString alloc] init];
                if (itemMaster.vendor && [itemMaster.vendor length] > 0 && itemMaster.vendorCode && [itemMaster.vendorCode length] > 0) {
                    [detailText appendFormat:@"%@ | %@", itemMaster.vendor, itemMaster.vendorCode];
                }
                else if (itemMaster.vendor && [itemMaster.vendor length] > 0) {
                    [detailText appendFormat:@"%@", itemMaster.vendor];
                }
                else if (itemMaster.vendorCode && [itemMaster.vendorCode length] > 0) {
                    [detailText appendFormat:@"%@", itemMaster.vendorCode];
                }
                if (itemMaster.description && [itemMaster.description length] > 0)
                    [detailText appendFormat:@" | %@", itemMaster.description];
                cell.detailTextLabel.text = detailText;
                if (itemMasterExists)
                    cell.detailTextLabel.textColor = [UIColor redColor];
            }
        }
    }
    
    return cell;
}

/*
 - (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
 {
 return @"Item Masters";
 }
 
 - (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger) section
 {
 //section text as a label
 UILabel *lbl = [[UILabel alloc] init];
 lbl.textAlignment = UITextAlignmentLeft;
 
 lbl.text = @"Item Masters";
 [lbl setBackgroundColor:[UIColor clearColor]];
 
 return lbl;
 }
 */

-(void)reloadTableView {
    [self reloadItemMastersArray];
    [self.itemMastersTableView reloadData];
}

-(void)reloadItemMastersArray {
    @try {
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark Navigation Methods
/****************************************************************************
 * Navigation Methods
 ****************************************************************************/

- (void)loadItemMastersScreen{
    @autoreleasepool {
        // Load the itemMasters controller
        VMInventoryItemMasterListVC *itemMastersViewController = nil;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            itemMastersViewController = [[VMInventoryItemMasterListVC alloc] initWithNibName:@"VMInventoryItemMasterListVC_iPhone" bundle:nil];
        } else {
            itemMastersViewController = [[VMInventoryItemMasterListVC alloc] initWithNibName:@"VMInventoryItemMasterListVC_iPad" bundle:nil];
        }
//        itemMastersViewController.mainViewController = self.mainViewController;
        itemMastersViewController.lastViewController = self;
        [self presentModalViewController:itemMastersViewController animated:NO];
    }
}

@end
