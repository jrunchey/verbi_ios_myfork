//
//  VMImportInventoryItemMasterConfirmVC.h
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMParentVC.h"
#import "NSString+ParsingExtensions_.h"

@interface VMImportInventoryItemMasterConfirmVC : VMParentVC <UITableViewDelegate, UITableViewDataSource,UIAlertViewDelegate>

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@property (nonatomic, strong) NSURL *importFileURL;
@property(nonatomic, weak) IBOutlet UITableView *itemMastersTableView;

@end
