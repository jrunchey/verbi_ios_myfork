//
//  VMSelectInventoryLocationViewController.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMSelectInventoryLocationVC.h"
#import "VMSession.h"
#import "VMAddInventoryItemsVC.h"
#import "VMNewEditLocationVC.h"
#import "VMLocationService.h"
#import "VMSelectInventoryLocationWorkflowStep.h"

@interface VMSelectInventoryLocationVC ()
@property(nonatomic,strong)NSMutableArray *locationsArray;
@property(nonatomic,strong)NSMutableArray *locationNamesArray;
@property(nonatomic,strong)NSMutableArray *locationTagIdsArray;
@property(nonatomic,strong)NSMutableArray *locationEPCsArray;
@property(nonatomic) BOOL searchAll;
@end

@implementation VMSelectInventoryLocationVC

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@synthesize cancelButton = _cancelButton;
@synthesize nextButton = _nextButton;
@synthesize searchTextField = _searchTextField;
@synthesize clearButton = _clearButton;
@synthesize locationsTableView = _locationsTableView;
@synthesize locationsArray = _locationsArray;
@synthesize locationNamesArray = _locationNamesArray;
@synthesize locationTagIdsArray = _locationTagIdsArray;
@synthesize locationEPCsArray = _locationEPCsArray;
@synthesize searchAll = _searchAll;

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose {
    @try {
        self.cancelButton = nil;
        self.nextButton = nil;
        self.searchTextField = nil;
        self.clearButton = nil;
        self.locationsTableView = nil;
        
        if (self.locationsArray) {
            [self.locationsArray removeAllObjects];
            self.locationsArray = nil;
        }
        if (self.locationNamesArray) {
            [self.locationNamesArray removeAllObjects];
            self.locationNamesArray = nil;
        }
        if (self.locationTagIdsArray) {
            [self.locationTagIdsArray removeAllObjects];
            self.locationTagIdsArray = nil;
        }
        if (self.locationEPCsArray) {
            [self.locationEPCsArray removeAllObjects];
            self.locationEPCsArray = nil;
        }
        
        [super dispose];
        
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark View Lifecycle Public
/****************************************************************************
 * View Lifecycle Public
 ****************************************************************************/

- (void)initializeUIOnFirstLoad {
    @try {
        [super initializeUIOnFirstLoad];
        
        // Set workflow step
        if (self.workflow) {
            VMWorkflowStep *workflowStep = [[VMSelectInventoryLocationWorkflowStep alloc] init:self.workflow];
            if (workflowStep) {
                self.workflowStep = workflowStep;
                [self.workflow addWorkflowStep:self.workflowStep];
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIBeforeRendering {
    @try {
        [super initializeUIBeforeRendering];
        self.searchTextField.delegate = self;
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIAfterRendering {
    @try {
        [super initializeUIAfterRendering];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO methods
 ****************************************************************************/

// Hides keyboard when losing focus on a UITextView
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    
    if ([self.searchTextField isFirstResponder] && [touch view] != self.searchTextField) {
        [self.searchTextField resignFirstResponder];
    }
    else if ([self.locationsTableView isFirstResponder] && [touch view] != self.locationsTableView) {
        [self.locationsTableView resignFirstResponder];
    }
    
    [super touchesBegan:touches withEvent:event];
}

- (IBAction)cancelPressed:(id)sender{
    [self loadMainViewController];
}

- (IBAction)nextPressed:(id)sender{
    @autoreleasepool {
        VMLocation *location;
        NSString *alertViewTitle = @"Edit";
        
        NSArray *indexes = [VMUtilityService getCheckedOrSelectedIndexes:self.locationsTableView];
        if (indexes.count == 0) {
            NSString *alertMessage = @"Select a location.";
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: alertViewTitle
                                  message: alertMessage
                                  delegate: self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
        else if (indexes.count == 1) {
            for (NSIndexPath *path in indexes) {
                NSUInteger index = [path indexAtPosition:[path length] - 1];
                location = [self.locationsArray objectAtIndex:index];
            }
            if (location) {
                ((VMSelectInventoryLocationWorkflowStep *)self.workflowStep).location = location; // assign location pointer so Add Items screen has access to selection
                [self loadAddItemsScreen];
            }
        }
        else {
            NSString *alertMessage = @"Select a single location.";
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: alertViewTitle
                                  message: alertMessage
                                  delegate: self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
    }
}

- (void)resetSearch {
    self.activityIndicatorView.hidden = NO;
    [self.activityIndicatorView startAnimating];
    if (self.searchTextField)
        self.searchTextField.text = nil;
    if (self.locationsArray) {
        [self.locationsArray removeAllObjects];
    }
    if (self.locationNamesArray) {
        [self.locationNamesArray removeAllObjects];
    }
    [self.locationsTableView reloadData];
    [self.activityIndicatorView stopAnimating];
    self.activityIndicatorView.hidden = YES;
}

- (IBAction)search:(id)sender{    
    if (self.searchTextField) {
        [self resetSearch];
        
        self.activityIndicatorView.hidden = NO;
        [self.activityIndicatorView startAnimating];
        @autoreleasepool {
            if ([self.searchTextField.text length] == 0 && self.searchAll) {
                self.locationsArray = [VMLocationService readAll];
                self.searchAll = NO;
            }
            else if ([self.searchTextField.text length] > 0) {
                self.locationsArray = [VMLocationService readLikeName:self.searchTextField.text];
            }
            
            if (self.locationsArray && self.locationsArray.count > 0) {
                self.locationNamesArray = [[NSMutableArray alloc] init];
                self.locationTagIdsArray = [[NSMutableArray alloc] init];
                self.locationEPCsArray = [[NSMutableArray alloc] init];
                for (VMLocation *location in self.locationsArray) {
                    [self.locationNamesArray addObject:location.name];
                    if (location.tagId && [location.tagId length] > 0)
                        [self.locationTagIdsArray addObject:location.tagId];
                    else
                        [self.locationTagIdsArray addObject:@""];   // add blank to keep index matched to self.locationNamesArray
                    if (location.epc && [location.epc length] > 0)
                        [self.locationEPCsArray addObject:location.epc];
                    else
                        [self.locationEPCsArray addObject:@""];   // add blank to keep index matched to self.locationNamesArray
                }
            }
        }
        
        [self.locationsTableView reloadData];
        [self.activityIndicatorView stopAnimating];
        self.activityIndicatorView.hidden = YES;
    }
}

- (IBAction)clearPressed:(id)sender{
    [self resetSearch];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		// do nothing except close alertView
        ((VMSelectInventoryLocationWorkflowStep *)self.workflowStep).location = nil;
	}
	else {
        if ([[alertView.title lowercaseString] caseInsensitiveCompare:@"Selection Not Found"] == NSOrderedSame) {
            @autoreleasepool {
                // If location does not exist, then navigate to New Location screen
                VMLocation *location = [[VMLocation alloc] init:nil name:self.getLastScanValue type:nil image:nil tagId:nil epc:nil latitude:nil longitude:nil altitude:nil address1:nil address2:nil townOrCity:nil countyOrDistrict:nil stateOrRegion:nil postal:nil country:nil];
                [self loadNewLocationScreen:location];
            }
        }
	}
}

// Handles when you press return in either text field
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    // If you pressed return in the password text field we will trigger loginPressed for you
    if (textField.tag == kSearchFieldTag){
        if ([self.searchTextField.text length] == 0) {
            self.searchAll = YES;
            [self search:nil];
        }
    }
    
    // this closed the keyboard if you press return in either text field
    return [textField resignFirstResponder];
}

// If you leave the text field we will close the keyboard
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
}

#pragma mark -
#pragma mark Table view methods
/****************************************************************************
 * Table view methods
 ****************************************************************************/

// Defines the table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Defines the table - Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.locationsArray.count;
}

// Fill the table - Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (self.locationsArray && self.locationsArray.count > 0) {
        VMLocation *location = [self.locationsArray objectAtIndex:indexPath.row];
        cell.textLabel.text = [location.name copy];
    }
    
    // Convert all row selections to check marks
    NSIndexPath *selection = [tableView indexPathForSelectedRow];
    if (selection && selection.row == indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        // uncheck
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else {
        // check
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Scan or Search a Location";
}

-(void)reloadTableView {
    [self reloadLocationsArray];
    [self.locationsTableView reloadData];
}

-(void)reloadLocationsArray {
    int count = [VMLocationService totalNumberOfRecords];
    if (count > 0) {
        self.locationsArray = [VMLocationService readAll];
        if (self.locationsArray && self.locationsArray.count > 0) {
            self.locationNamesArray = [[NSMutableArray alloc] init];
            self.locationTagIdsArray = [[NSMutableArray alloc] init];
            self.locationEPCsArray = [[NSMutableArray alloc] init];
            for (VMLocation *location in self.locationsArray) {
                [self.locationNamesArray addObject:location.name];
                if (location.tagId && [location.tagId length] > 0)
                    [self.locationTagIdsArray addObject:location.tagId];
                else
                    [self.locationTagIdsArray addObject:@""];   // add blank to keep index matched to self.locationNamesArray
                if (location.epc && [location.epc length] > 0)
                    [self.locationEPCsArray addObject:location.epc];
                else
                    [self.locationEPCsArray addObject:@""];   // add blank to keep index matched to self.locationNamesArray
            }
        }
    }
}

#pragma mark -
#pragma mark Navigation Methods
/****************************************************************************
 * Navigation Methods
 ****************************************************************************/
 
-(void)loadAddItemsScreen {
    @autoreleasepool {
        @try {
            // Load the add inventory items controller
            VMAddInventoryItemsVC *addInventoryItemsViewController = nil;
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                addInventoryItemsViewController = [[VMAddInventoryItemsVC alloc] initWithNibName:@"VMAddInventoryItemsVC_iPhone" bundle:nil];
            } else {
                addInventoryItemsViewController = [[VMAddInventoryItemsVC alloc] initWithNibName:@"VMAddInventoryItemsVC_iPad" bundle:nil];
            }
//            addInventoryItemsViewController.mainViewController = self.mainViewController;
            addInventoryItemsViewController.lastViewController = self;
            addInventoryItemsViewController.workflow = self.workflow;
            [self presentModalViewController:addInventoryItemsViewController animated:NO];
        }
        @catch (NSException *exception) {
            NSLog(@"An exception occured: %@", [exception reason]);
        }
    }
}

-(void)loadNewLocationScreen:(VMLocation *)location {
    @autoreleasepool {
        @try {
            // Load the new location controller
            VMNewEditLocationVC *newEditLocationViewController = nil;
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                newEditLocationViewController = [[VMNewEditLocationVC alloc] initWithNibName:@"VMNewEditLocationVC_iPhone" bundle:nil];
            } else {
                newEditLocationViewController = [[VMNewEditLocationVC alloc] initWithNibName:@"VMNewEditLocationVC_iPad" bundle:nil];
            }
//            newEditLocationViewController.mainViewController = self.mainViewController;
            newEditLocationViewController.lastViewController = self;
            newEditLocationViewController.selectInventoryLocationController = self;
            if (location)
                newEditLocationViewController.locationToEdit = location;
            [self presentModalViewController:newEditLocationViewController animated:NO];
        }
        @catch (NSException *exception) {
            NSLog(@"An exception occured: %@", [exception reason]);
        }
    }
}

-(void)lookupLocationByTag:(VMTag *)tag {
    if (tag.type == TAG_BARCODE) {
        [self lookupLocationByName:tag.value];
    }
    else if (tag.type == TAG_RFID) {
        NSUInteger index = [self.locationEPCsArray indexOfObject:tag.value];
        VMLocation *location = [self.locationsArray objectAtIndex:index];
        if (location != nil)
            [self lookupLocationByName:location.name];
    }
}

-(void)lookupLocationByName:(NSString *)locationName {
    VMLocation *location;
    @autoreleasepool {
        @try {
            
            // Lookup location from locationsArray
            NSUInteger index = [self.locationNamesArray indexOfObject:locationName];
            location = [self.locationsArray objectAtIndex:index];
            if (location) {
                // Uncheck all items in table view then check only the found item in the table view
                [self uncheckAllItems:self.locationsTableView];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];    // use index to get indexPath
                UITableViewCell *cell = [self.locationsTableView cellForRowAtIndexPath:indexPath];    // get cell using indexPath
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            
            // If location not found in table, then lookup from database
            if (location == nil) {
                location = [VMLocationService readByName:locationName];
            }
            
            if (location) {
                // Be assumptive and set location to workflow now and then popup confirm where on 'No' can remove location later
                ((VMSelectInventoryLocationWorkflowStep *)self.workflowStep).location = location;
                
                // Load the add inventory items controller
                [self loadAddItemsScreen];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"An exception occured: %@", [exception reason]);
        }
        @finally {
            if (location == nil) {
                // If location does not exist, then popup if they'd like to add a new location
                NSString *alertMessage = [NSString stringWithFormat:@"Location not found. Would like to add '%@' as a new location?", self.getLastScanValue];
                UIAlertView *alert = [[UIAlertView alloc]
                                      initWithTitle: @"Selection Not Found"
                                      message: alertMessage
                                      delegate: self
                                      cancelButtonTitle:@"No"
                                      otherButtonTitles:@"Yes",nil];    // value passed to override of alertView method
                [alert show];
            }
        }
    }
}

#pragma mark -
#pragma mark VMRFIDReaderDelegate Methods
/****************************************************************************
 * VMRFIDReaderDelegate Methods
 ****************************************************************************/

-(void)handleRFIDReaderStatusEvent:(NSString *)description event:(VMRFIDReaderEvent *)event {
    
}

-(void)handleRFIDReaderReadEvent:(NSArray *)tagIds event:(VMRFIDReaderEvent *)event {
    if (event && event.tags) {
        for (VMRFIDTag *tag in event.tags) {
            [self lookupLocationByTag:tag];
        }
    }
}

#pragma mark -
#pragma mark VMBarcodeReaderDelegate Methods
/****************************************************************************
 * VMBarcodeReaderDelegate Methods
 ****************************************************************************/

-(void)handleBarcodeReaderReadEvent:(NSString *)barcode symbology:(NSString *)symbology event:(VMBarcodeReaderEvent *)event {
    if (event && event.tag)
        [self lookupLocationByTag:event.tag];
}

-(void)handleMagneticCardReaderReadEvent:(NSString *)data event:(VMMagneticCardReaderEvent *)event {
    if (event && event.tag)
        [self lookupLocationByTag:event.tag];
}

@end
