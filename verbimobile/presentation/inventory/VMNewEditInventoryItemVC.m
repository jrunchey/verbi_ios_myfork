//
//  VMNewEditInventoryItemViewController.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMNewEditInventoryItemVC.h"
#import "VMAddInventoryItemsVC.h"
#import "VMAddInventoryItemsWorkflowStep.h"
#import "VMInventoryItemMasterListVC.h"
#import "VMSession.h"

@interface VMNewEditInventoryItemVC ()
@property(nonatomic,strong) NSMutableArray *tagIdArray;
// PROCESS MODES
// 0 - Quantity Incrementing, 1 - Tag Reading
@property(nonatomic,strong) NSDictionary *processModes;
@property(nonatomic) NSInteger processMode;
@property(nonatomic) int PROCESS_MODE_DEFAULT;
@end

@implementation VMNewEditInventoryItemVC

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@synthesize addInventoryItemsController = _addInventoryItemsController;
@synthesize itemToEdit = _itemToEdit;
@synthesize scrollView = _scrollView;
@synthesize cancelButton = _cancelButton;
@synthesize lookupButton = _lookupButton;
@synthesize okButton = _okButton;
@synthesize processModeButton = _processModeButton;
@synthesize selectAllButton = _selectAllButton;
@synthesize addButton = _addButton;
@synthesize removeButton = _removeButton;
@synthesize nameTextField = _nameTextField;
@synthesize quantityTextField = _quantityTextField;
@synthesize valueTextField = _valueTextField;
@synthesize originTextField = _originTextField;
@synthesize lotTextField = _lotTextField;
@synthesize tagIdTableView = _tagIdTableView;
@synthesize tagIdArray = _tagIdArray;
@synthesize processModes = _processModes;
@synthesize processMode = _processMode;
@synthesize PROCESS_MODE_DEFAULT = _PROCESS_MODE_DEFAULT;

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose {
    @try {
        self.addInventoryItemsController = nil;
        self.itemToEdit = nil;
        self.scrollView = nil;
        self.cancelButton = nil;
        self.lookupButton = nil;
        self.okButton = nil;
        self.processModeButton = nil;
        self.selectAllButton = nil;
        self.addButton = nil;
        self.removeButton = nil;
        self.nameTextField = nil;
        self.quantityTextField = nil;
        self.valueTextField = nil;
        self.originTextField = nil;
        self.lotTextField = nil;
        self.tagIdTableView = nil;
        if (self.tagIdArray) {
            [self.tagIdArray removeAllObjects];
            self.tagIdArray = nil;
        }
        self.processModes = nil;
        
        [super dispose];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark Init Methods
/****************************************************************************
 * Init Methods
 ****************************************************************************/

-(void)initalizeFormFieldValues {
    if (self.itemToEdit == nil) {
        if (self.titleLabel)
            self.titleLabel.text = @"New Item";
        [self.nameTextField becomeFirstResponder];
    }
    else {
        if (self.titleLabel)
            self.titleLabel.text = @"Edit Item";
        if (self.nameTextField)
            self.nameTextField.text = [self.itemToEdit.itemMaster.name copy];
        if (self.quantityTextField)
            self.quantityTextField.text = [NSString stringWithFormat:@"%1.2f",self.itemToEdit.quantity];
        if (self.valueTextField)
            self.valueTextField.text = [NSString stringWithFormat:@"%1.2f",self.itemToEdit.value];
        if (self.originTextField)
            self.originTextField.text = [self.itemToEdit.placeOfOrigin copy];
        if (self.lotTextField)
            self.lotTextField.text = [self.itemToEdit.lotNumber copy];
    }
}

#pragma mark -
#pragma mark View Lifecycle Public
/****************************************************************************
 * View Lifecycle Public
 ****************************************************************************/

// Called from viewDidLoad
- (void)initializeUIOnFirstLoad {
    @try {
        [super initializeUIOnFirstLoad];
        
        self.nameTextField.returnKeyType = UIReturnKeyDone;
        self.quantityTextField.returnKeyType = UIReturnKeyDone;
        self.valueTextField.returnKeyType = UIReturnKeyDone;
        self.originTextField.returnKeyType = UIReturnKeyDone;
        self.lotTextField.returnKeyType = UIReturnKeyDone;
        
        // Default process modes and button
        self.processModes = [[NSDictionary alloc] initWithObjectsAndKeys:
                        @"Qty", @"0",
                        @"Tag", @"1",
                        nil];
        self.processMode = self.PROCESS_MODE_DEFAULT;
        
        // Initialize scroll view
        self.scrollView.contentSize = self.scrollView.frame.size;
        [self.scrollView flashScrollIndicators];
        
        // Scroll view touch event
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
        [self.scrollView addGestureRecognizer:singleTap];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIBeforeRendering {
    @try {
        [super initializeUIBeforeRendering];
        
        // Initilialize header and fields elements to whether New or Edit
        [self initalizeFormFieldValues];
        
        [self updateProcessModeButton];
    }
    @catch (NSException *exception) {
        NSLog(@"Location could not be acquired from workflow. An exception has occurred: %@", [exception reason]);
    }
}

- (void)initializeUIAfterRendering {
    @try {
        [super initializeUIAfterRendering];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO Methods
 ****************************************************************************/

// Hides keyboard when losing focus on a UITextView
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    
    if ([self.nameTextField isFirstResponder] && [touch view] != self.nameTextField) {
        [self.nameTextField resignFirstResponder];
    }
    else if ([self.quantityTextField isFirstResponder] && [touch view] != self.quantityTextField) {
        [self.quantityTextField resignFirstResponder];
    }
    else if ([self.valueTextField isFirstResponder] && [touch view] != self.valueTextField) {
        [self.valueTextField resignFirstResponder];
    }
    else if ([self.originTextField isFirstResponder] && [touch view] != self.originTextField) {
        [self.originTextField resignFirstResponder];
    }
    else if ([self.lotTextField isFirstResponder] && [touch view] != self.lotTextField) {
        [self.lotTextField resignFirstResponder];
    }
    else if ([self.tagIdTableView isFirstResponder] && [touch view] != self.tagIdTableView) {
        [self.tagIdTableView resignFirstResponder];
    }
    
    [super touchesBegan:touches withEvent:event];
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)recognizer
{
    CGPoint touchPoint = [recognizer locationInView:self.scrollView];
    
    // Hides keyboard when losing focus on a UITextField contained within a scroll view
    if ([self.nameTextField isFirstResponder]) {
        CGRect bounds = self.nameTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.nameTextField resignFirstResponder];
    }
    else if ([self.quantityTextField isFirstResponder]) {
        CGRect bounds = self.quantityTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.quantityTextField resignFirstResponder];
    }
    else if ([self.valueTextField isFirstResponder]) {
        CGRect bounds = self.valueTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.valueTextField resignFirstResponder];
    }
    else if ([self.originTextField isFirstResponder]) {
        CGRect bounds = self.originTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.originTextField resignFirstResponder];
    }
    else if ([self.lotTextField isFirstResponder]) {
        CGRect bounds = self.lotTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.lotTextField resignFirstResponder];
    }
    else if ([self.tagIdTableView isFirstResponder]) {
        CGRect bounds = self.tagIdTableView.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.tagIdTableView resignFirstResponder];
    }
}
            
- (IBAction)cancelPressed:(id)sender{
    [self loadLastViewController];
}

- (IBAction)okPressed:(id)sender{
    if (self.addInventoryItemsController) {
        NSString *alertViewTitle = @"Confirm Save";
        NSString *alertMessage = @"Are you sure you wish to add or update this item to your item list?";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"No"
                              otherButtonTitles:@"Yes",nil];    // value passed to override of alertView method
        [alert show];
    }
    else {
        [self loadLastViewController];
    }
}

- (IBAction)lookupPressed:(id)sender{
    [self loadInventoryItemMasterListScreen];
}

- (IBAction)processModePressed:(id)sender{
    // The process mode can only be changed if not MAG /RFID or if MAG/RFID then process must not be started
    int autoIdMode = [VMSession sharedInstance].autoIdMode;
    BOOL autoIdProcessStarted = [VMSession sharedInstance].autoIdProcessStarted;
    if (autoIdMode == [AUTO_ID_MODE_BARCODE_1D_KEY intValue] || autoIdMode == [AUTO_ID_MODE_BARCODE_2D_KEY intValue] || ((autoIdMode == [AUTO_ID_MODE_MAGSTRIPE_KEY intValue] || autoIdMode == [AUTO_ID_MODE_RFID_C1G2_KEY intValue]) && !autoIdProcessStarted)) {
        if (self.processMode < (self.processModes.count - 1)) {
            self.processMode++;
        }
        else {
            self.processMode = 0;
        }
        [self updateProcessModeButton];
    }
}

- (IBAction)updateValueTextFieldOnQtyChange:(id)sender{
    if (self.valueTextField && self.quantityTextField && self.itemToEdit) {
        float quantity = [self.quantityTextField.text floatValue];
        float newValue = quantity * self.itemToEdit.itemMaster.price;
        self.valueTextField.text = [NSString stringWithFormat:@"%1.2f",newValue];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		// do nothing except close alertView
	}
	else {
        if ([[alertView.title lowercaseString] caseInsensitiveCompare:@"Confirm Save"] == NSOrderedSame) {
            [self addItemToWorkflowStep];
        }
        
        [self loadLastViewController];
	}
}

- (void)updateProcessModeButton{
    // Update processModeButton
    NSString *processModeString = [NSString stringWithFormat:@"%d", self.processMode];
    NSString *processModeDesc = (NSString *)[self.processModes objectForKey:processModeString];
    [self.processModeButton setTitle:processModeDesc forState:UIControlStateNormal];
}

-(void)setFocusedFieldText:(NSString *)text {
    // populate field that has focus with barcode value
    if ([self.quantityTextField isFirstResponder]) {
        [self incrementQuantityWithMatch:text];
        [self.quantityTextField resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.tagIdTableView isFirstResponder]) {
        //[tagIdTableView setText:text];
        [self addItemToTagIdTableView:text];
        [self.tagIdTableView resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.originTextField isFirstResponder]) {
        self.originTextField.text = [text copy];
        [self.originTextField resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.lotTextField isFirstResponder]) {
        self.lotTextField.text = [text copy];
        [self.lotTextField resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else {
        if (self.processMode == 0) {
            // If process mode is Quantity Incrementing, make quantityTextField first responder
            [self.quantityTextField becomeFirstResponder];
            [self incrementQuantityWithMatch:text];
            [self.quantityTextField resignFirstResponder];    // automatically lose focus to hide keyboard
        }
        else if (self.processMode == 1) {
            // If process mode is Tag Reading, make tagIdTableView first responder
            [self.tagIdTableView becomeFirstResponder];
            [self addItemToTagIdTableView:text];
            [self.tagIdTableView resignFirstResponder];    // automatically lose focus to hide keyboard
        }
    }
}

-(void)incrementQuantityWithMatch:(NSString *)text {
    // If process mode is Quantity Incrementing and new scan value matches last scan value, convert quantityTextField to an integer, increment it and write it back
    if (self.processMode == 0 && [[self.getLastScanValue lowercaseString] caseInsensitiveCompare:text] == NSOrderedSame) {
        NSString *quantityStr = [self.quantityTextField text];
        NSInteger quantity = [quantityStr integerValue];
        quantity++;
        quantityStr = [NSString stringWithFormat:@"%d", quantity];
        [self.quantityTextField setText:quantityStr];
    }
    else {
        // do nothing
    }
}

-(void)addItemToWorkflowStep {
    if (self.itemToEdit == nil) {
        // New
        @try {
            // todo
        }
        @catch (NSException *exception) {
            NSLog(@"An exception occurred: %@", exception.reason);
        }
    }
    else {
        // Edit
        @try {
            self.itemToEdit.status = @"AVAILABLE";
            self.itemToEdit.quantity = [self.quantityTextField.text floatValue];
            [self updateValueTextFieldOnQtyChange:nil];
            self.itemToEdit.value = [self.valueTextField.text floatValue];
            self.itemToEdit.placeOfOrigin = [self.originTextField.text copy];
            self.itemToEdit.lotNumber = [self.lotTextField.text copy];
            self.itemToEdit.lastUpdatedDate = [NSDate date];
            self.itemToEdit.lastUpdatedUserId = [[VMSession sharedInstance].user.ID copy];
            self.itemToEdit.lastUpdatedUser = [VMSession sharedInstance].user;
            
            NSMutableArray *addInventoryItemsWorkflowStepItems = ((VMAddInventoryItemsWorkflowStep *)self.workflowStep).itemsArray;
            NSMutableArray *addInventoryItemsWorkflowStepItemGUIDs = ((VMAddInventoryItemsWorkflowStep *)self.workflowStep).itemGUIDsArray;
            NSMutableArray *addInventoryItemsWorkflowStepItemNames = ((VMAddInventoryItemsWorkflowStep *)self.workflowStep).itemNamesArray;
            NSMutableArray *addInventoryItemsWorkflowStepItemUPCs = ((VMAddInventoryItemsWorkflowStep *)self.workflowStep).itemUPCsArray;
            
            if (addInventoryItemsWorkflowStepItems) {
                //@autoreleasepool {
                    BOOL itemFound = NO;
                    NSUInteger index = 0;
                    itemFound = [addInventoryItemsWorkflowStepItemGUIDs containsObject:self.itemToEdit.guid];
                    if (itemFound)
                        index = [addInventoryItemsWorkflowStepItemGUIDs indexOfObject:self.itemToEdit.guid];
                    else {
                        itemFound = [addInventoryItemsWorkflowStepItemNames containsObject:self.itemToEdit.itemMaster.name];
                        if (itemFound)
                            index = [addInventoryItemsWorkflowStepItemNames indexOfObject:self.itemToEdit.itemMaster.name];
                        else if (!itemFound && self.itemToEdit.itemMaster.upc && [self.itemToEdit.itemMaster.upc length] > 0) {
                            itemFound = [addInventoryItemsWorkflowStepItemUPCs containsObject:self.itemToEdit.itemMaster.upc];
                            if (itemFound)
                                index = [addInventoryItemsWorkflowStepItemUPCs indexOfObject:self.itemToEdit.itemMaster.upc];
                        }
                    }
                    if (itemFound) {
                        VMItem *itemInList = [addInventoryItemsWorkflowStepItems objectAtIndex:index];
                        if (itemInList) {
                            itemInList.quantity = self.itemToEdit.quantity;
                            itemInList.value = self.itemToEdit.quantity * self.itemToEdit.itemMaster.price;
                        }
                    }
                    else {
                        [addInventoryItemsWorkflowStepItems addObject:self.itemToEdit];
                        NSString *itemGUID = @"";
                        if (self.itemToEdit.guid)
                            itemGUID = self.itemToEdit.guid;
                        [addInventoryItemsWorkflowStepItemGUIDs addObject:itemGUID];
                        NSString *itemMasterName = @"";
                        if (self.itemToEdit.itemMaster.name)
                            itemMasterName = self.itemToEdit.itemMaster.name;
                        [addInventoryItemsWorkflowStepItemNames addObject:itemMasterName];
                        NSString *itemMasterUPC = @"";
                        if (self.itemToEdit.itemMaster.upc)
                            itemMasterUPC = self.itemToEdit.itemMaster.upc;
                        [addInventoryItemsWorkflowStepItemUPCs addObject:itemMasterUPC];
                    }
                //}
            }
        }
        @catch (NSException *exception) {
            NSLog(@"An exception occurred: %@", exception.reason);
        }
    }
}

#pragma mark -
#pragma mark Table view Methods
/****************************************************************************
 * Table view Methods
 ****************************************************************************/

// Defines the table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Defines the table - Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tagIdArray.count;
}

// Fill the table - Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = [self.tagIdArray objectAtIndex:indexPath.row];
    //cell.textLabel.text = [[itemDictionariesArray objectAtIndex:indexPath.row] objectForKey:@"name"];
    
    // Convert all row selections to check marks
    NSIndexPath *selection = [tableView indexPathForSelectedRow];
    if (selection && selection.row == indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        // uncheck
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else {
        // check
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
}

-(void)addItemToTagIdTableView:(NSString *)tagId {
    //UIView *tagIdTableHeaderView = tagIdTableView.tableHeaderView;
    //NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    //[tagIdTableView insertRowsAtIndexPaths:<#(NSArray *)#> withRowAnimation:<#(UITableViewRowAnimation)#>
    //[tagIdTableView insertSections:<#(NSIndexSet *)#> withRowAnimation:<#(UITableViewRowAnimation)#>
    
    //[tagIdTableView beginUpdates];
    // do all row insertion/delete here
    
    //NSArray *tagIds = [NSArray arrayWithObjects:@"111" ,@"222", @"333", @"444", nil];
    //[tagIdTableView insertRowsAtIndexPaths:tagIds withRowAnimation:UITableViewRowAnimationTop];
    
    //[tagIdTableView endUpdates];
    
    if (self.tagIdArray == nil)
        self.tagIdArray = [[NSMutableArray alloc] init];
    
    [self.tagIdArray addObject:tagId];
    [self.tagIdTableView reloadData];
}

#pragma mark -
#pragma mark Navigation Methods
/****************************************************************************
 * Navigation Methods
 ****************************************************************************/

-(void)loadInventoryItemMasterListScreen {
    // Load the inventory item master list controller
    VMInventoryItemMasterListVC *inventoryItemMasterListViewController = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        inventoryItemMasterListViewController = [[VMInventoryItemMasterListVC alloc] initWithNibName:@"VMInventoryItemMasterListVC_iPhone" bundle:nil];
    } else {
        inventoryItemMasterListViewController = [[VMInventoryItemMasterListVC alloc] initWithNibName:@"VMInventoryItemMasterListVC_iPad" bundle:nil];
    }
//    inventoryItemMasterListViewController.mainViewController = self.mainViewController;
    inventoryItemMasterListViewController.lastViewController = self;
    inventoryItemMasterListViewController.inventoryItemController = self;
    inventoryItemMasterListViewController.workflow = self.workflow;
    inventoryItemMasterListViewController.workflowStep = self.workflowStep;
    [self presentModalViewController:inventoryItemMasterListViewController animated:NO];
}

#pragma mark -
#pragma mark VerbiBarcodeReaderDelegate Methods
/****************************************************************************
 * VerbiBarcodeReaderDelegate Methods
 ****************************************************************************/

-(void)handleBarcodeReaderReadEvent:(NSString *)barcode symbology:(NSString *)symbology event:(VMBarcodeReaderEvent *)event {
    if (self.processMode == 0)
        [self incrementQuantityWithMatch:barcode];
    else
        [self addItemToTagIdTableView:barcode];
}

@end
