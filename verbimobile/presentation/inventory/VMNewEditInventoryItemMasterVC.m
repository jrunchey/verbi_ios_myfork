//
//  VMNewEditInventoryItemMasterVC.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "MobileCoreServices/MobileCoreServices.h"
#import "VMNewEditInventoryItemMasterVC.h"
//#import "VMViewController.h"
#import "VMConstants.h"
#import "VMAppDelegate.h"
#import "VMSession.h"
#import "VMAddInventoryItemsVC.h"
#import "VMInventoryService.h"

@interface VMNewEditInventoryItemMasterVC ()
@property(nonatomic, strong) NSArray *typePickerArray;
@property(nonatomic, strong) NSArray *uomPickerArray;
@property(nonatomic, strong) NSArray *timeUnitPickerArray;
@property(nonatomic) BOOL newMedia;
@end

@implementation VMNewEditInventoryItemMasterVC

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@synthesize addInventoryItemsController = _addInventoryItemsController;

// Edit
@synthesize itemMasterToEdit = _itemMasterToEdit;

// Scroll View Properties
@synthesize scrollView = _scrollView;

// Picker Properties
@synthesize typePickerView = _typePickerView;
@synthesize uomPickerView = _uomPickerView;
@synthesize timeUnitPickerView = _timeUnitPickerView;

// Buttons
@synthesize cancelButton = _cancelButton;
@synthesize okButton = _okButton;
@synthesize cameraButton = _cameraButton;

// Image View
@synthesize itemMasterImageView = _itemMasterImageView;
@synthesize popoverController = _mypopoverController;
@synthesize typePickerArray = _typePickerArray;
@synthesize uomPickerArray = _uomPickerArray;
@synthesize timeUnitPickerArray = _timeUnitPickerArray;
@synthesize newMedia = _newMedia;

// Text Field objects
@synthesize nameTextField = _nameTextField;
@synthesize descriptionTextField = _descriptionTextField;
@synthesize upcTextField = _upcTextField;
@synthesize vendorTextField = _vendorTextField;
@synthesize vendorCodeTextField = _vendorCodeTextField;
@synthesize reorderPointQuantityTextField = _reorderPointQuantityTextField;
@synthesize leadTimeDaysTextField = _leadTimeDaysTextField;
@synthesize safetyStockQuantityTextField = _safetyStockQuantityTextField;
@synthesize optimumOrderingQuantityTextField = _optimumOrderingQuantityTextField;
@synthesize incrementQuantityTextField = _incrementQuantityTextField;
@synthesize priceTextField = _priceTextField;
@synthesize expiresSwitch = _expiresSwitch;
@synthesize timePeriodTextField = _timePeriodTextField;
@synthesize enabledSwitch = _enabledSwitch;

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose {
    @try {
        self.nameTextField = nil;
        self.descriptionTextField = nil;
        self.upcTextField = nil;
        self.vendorTextField = nil;
        self.vendorCodeTextField = nil;
        self.reorderPointQuantityTextField = nil;
        self.leadTimeDaysTextField = nil;
        self.safetyStockQuantityTextField = nil;
        self.optimumOrderingQuantityTextField = nil;
        self.incrementQuantityTextField = nil;
        self.priceTextField = nil;
        self.expiresSwitch = nil;
        self.timePeriodTextField = nil;
        self.enabledSwitch = nil;
        
        self.cancelButton = nil;
        self.okButton = nil;
        self.cameraButton = nil;
        
        self.itemMasterImageView = nil;
        self.popoverController = nil;
        
        if (self.itemMasterToEdit)
            self.itemMasterToEdit = nil;
        
        if (self.typePickerArray)
            self.typePickerArray = nil;
        if (self.uomPickerArray)
            self.uomPickerArray = nil;
        if (self.timeUnitPickerArray)
            self.timeUnitPickerArray = nil;
        if (self.typePickerView)
            self.typePickerView = nil;
        if (self.uomPickerView)
            self.uomPickerView = nil;
        if (self.timeUnitPickerView)
            self.timeUnitPickerView = nil;
        
        if (self.scrollView)
            self.scrollView = nil;
        
        [super dispose];
        
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark Init Methods
/****************************************************************************
 * Init Methods
 ****************************************************************************/

-(void)initalizeFormFieldValues {
    if (self.itemMasterToEdit == nil) {
        if (self.titleLabel)
            self.titleLabel.text = @"New Item Master";
        [self.nameTextField becomeFirstResponder];
        self.reorderPointQuantityTextField.text = @"1";
        self.leadTimeDaysTextField.text = @"30";
        self.safetyStockQuantityTextField.text = @"1";
        self.optimumOrderingQuantityTextField.text = @"1";
        self.incrementQuantityTextField.text = @"1";
        self.priceTextField.text = @"0.00";
    }
    else {
        if (self.titleLabel)
            self.titleLabel.text = @"Edit Item Master";
        if (self.nameTextField)
            self.nameTextField.text = [self.itemMasterToEdit.name copy];
        
        if (self.descriptionTextField)
            self.descriptionTextField.text = [self.itemMasterToEdit.description copy];
        
        if (self.typePickerView) {
            // Assume fixed location and change using case insensitive compare
            NSInteger selectedPickerViewRow = 0;    // default to countable
            if ([[self.itemMasterToEdit.type lowercaseString] caseInsensitiveCompare:@"countable"] == NSOrderedSame) {
                selectedPickerViewRow = 0;
            }
            else if ([[self.itemMasterToEdit.type lowercaseString] caseInsensitiveCompare:@"weighable"] == NSOrderedSame) {
                selectedPickerViewRow = 1;
            }
            [self.typePickerView selectRow:selectedPickerViewRow inComponent:0 animated:YES];
        }
        
        if (self.upcTextField)
            self.upcTextField.text = [self.itemMasterToEdit.upc copy];
        if (self.vendorTextField)
            self.vendorTextField.text = [self.itemMasterToEdit.vendor copy];
        if (self.vendorCodeTextField)
            self.vendorCodeTextField.text = [self.itemMasterToEdit.vendorCode copy];
        if (self.reorderPointQuantityTextField)
            self.reorderPointQuantityTextField.text = [NSString stringWithFormat:@"%1.2f",self.itemMasterToEdit.reorderPointQuantity];
        if (self.leadTimeDaysTextField)
            self.leadTimeDaysTextField.text = [NSString stringWithFormat:@"%1.0d",self.itemMasterToEdit.leadTimeDays];
        if (self.safetyStockQuantityTextField)
            self.safetyStockQuantityTextField.text = [NSString stringWithFormat:@"%1.2f",self.itemMasterToEdit.safetyStockQuantity];
        if (self.optimumOrderingQuantityTextField)
            self.optimumOrderingQuantityTextField.text = [NSString stringWithFormat:@"%1.2f",self.itemMasterToEdit.optimumOrderingQuantity];
        if (self.incrementQuantityTextField)
            self.incrementQuantityTextField.text = [NSString stringWithFormat:@"%1.2f",self.itemMasterToEdit.incrementQuantity];
        if (self.priceTextField)
            self.priceTextField.text = [NSString stringWithFormat:@"%1.2f",self.itemMasterToEdit.price];
        
        if (self.uomPickerView) {
            // Assume fixed location and change using case insensitive compare
            NSInteger selectedPickerViewRow = 0;
            if ([[self.itemMasterToEdit.uom lowercaseString] caseInsensitiveCompare:@"none"] == NSOrderedSame) {
                selectedPickerViewRow = 1;
            }
            [self.uomPickerView selectRow:selectedPickerViewRow inComponent:0 animated:YES];
        }
        
        if (self.expiresSwitch)
            self.expiresSwitch.on = self.itemMasterToEdit.expiration;
        if (self.timePeriodTextField)
            self.timePeriodTextField.text = [NSString stringWithFormat:@"%i",self.itemMasterToEdit.expirationTimePeriod];
        
        if (self.timeUnitPickerView) {
            // Assume fixed location and change using case insensitive compare
            NSInteger selectedPickerViewRow = 0;
            if ([[self.itemMasterToEdit.expirationTimeUnit lowercaseString] caseInsensitiveCompare:@"second(s)"] == NSOrderedSame) {
                selectedPickerViewRow = 1;
            }
            else if ([[self.itemMasterToEdit.expirationTimeUnit lowercaseString] caseInsensitiveCompare:@"minute(s)"] == NSOrderedSame) {
                selectedPickerViewRow = 2;
            }
            else if ([[self.itemMasterToEdit.expirationTimeUnit lowercaseString] caseInsensitiveCompare:@"hour(s)"] == NSOrderedSame) {
                selectedPickerViewRow = 3;
            }
            else if ([[self.itemMasterToEdit.expirationTimeUnit lowercaseString] caseInsensitiveCompare:@"day(s)"] == NSOrderedSame) {
                selectedPickerViewRow = 4;
            }
            else if ([[self.itemMasterToEdit.expirationTimeUnit lowercaseString] caseInsensitiveCompare:@"week(s)"] == NSOrderedSame) {
                selectedPickerViewRow = 5;
            }
            else if ([[self.itemMasterToEdit.expirationTimeUnit lowercaseString] caseInsensitiveCompare:@"month(s)"] == NSOrderedSame) {
                selectedPickerViewRow = 6;
            }
            else if ([[self.itemMasterToEdit.expirationTimeUnit lowercaseString] caseInsensitiveCompare:@"year(s)"] == NSOrderedSame) {
                selectedPickerViewRow = 7;
            }
            [self.timeUnitPickerView selectRow:selectedPickerViewRow inComponent:0 animated:YES];
        }
        
        if (self.enabledSwitch)
            self.enabledSwitch.on = self.itemMasterToEdit.enabled;
    }
}

#pragma mark -
#pragma mark View Lifecycle Public
/****************************************************************************
 * View Lifecycle Public
 ****************************************************************************/

- (void)initializeUIOnFirstLoad {
    @try {
        [super initializeUIOnFirstLoad];
        self.nameTextField.returnKeyType = UIReturnKeyDone;
        self.descriptionTextField.returnKeyType = UIReturnKeyDone;
        self.upcTextField.returnKeyType = UIReturnKeyDone;
        self.vendorTextField.returnKeyType = UIReturnKeyDone;
        self.vendorCodeTextField.returnKeyType = UIReturnKeyDone;
        self.reorderPointQuantityTextField.returnKeyType = UIReturnKeyDone;
        self.leadTimeDaysTextField.returnKeyType = UIReturnKeyDone;
        self.safetyStockQuantityTextField.returnKeyType = UIReturnKeyDone;
        self.optimumOrderingQuantityTextField.returnKeyType = UIReturnKeyDone;
        self.incrementQuantityTextField.returnKeyType = UIReturnKeyDone;
        self.priceTextField.returnKeyType = UIReturnKeyDone;
        
        // Initialize scroll view
        self.scrollView.contentSize = self.scrollView.frame.size;
        [self.scrollView flashScrollIndicators];
        
        // Scroll view touch event
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
        [self.scrollView addGestureRecognizer:singleTap];
        
        // Initialize picker arrays
        [self loadTypePickerArray];
        [self loadUomPickerArray];
        [self loadTimeUnitPickerArray];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIBeforeRendering {
    @try {
        [super initializeUIBeforeRendering];
        [self initalizeFormFieldValues];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

- (void)initializeUIAfterRendering {
    @try {
        [super initializeUIAfterRendering];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO Methods
 ****************************************************************************/

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    
    // Hides keyboard when losing focus on a UITextField
    if ([self.nameTextField isFirstResponder] && [touch view] != self.nameTextField) {
        [self.nameTextField resignFirstResponder];
    }
    else if ([self.descriptionTextField isFirstResponder] && [touch view] != self.descriptionTextField) {
        [self.descriptionTextField resignFirstResponder];
    }
    else if ([self.typePickerView isFirstResponder] && [touch view] != self.typePickerView) {
        [self.typePickerView resignFirstResponder];
    }
    else if ([self.upcTextField isFirstResponder] && [touch view] != self.upcTextField) {
        [self.upcTextField resignFirstResponder];
    }
    else if ([self.vendorTextField isFirstResponder] && [touch view] != self.vendorTextField) {
        [self.vendorTextField resignFirstResponder];
    }
    else if ([self.vendorCodeTextField isFirstResponder] && [touch view] != self.vendorCodeTextField) {
        [self.vendorCodeTextField resignFirstResponder];
    }
    else if ([self.reorderPointQuantityTextField isFirstResponder] && [touch view] != self.reorderPointQuantityTextField) {
        [self.reorderPointQuantityTextField resignFirstResponder];
    }
    else if ([self.leadTimeDaysTextField isFirstResponder] && [touch view] != self.leadTimeDaysTextField) {
        [self.leadTimeDaysTextField resignFirstResponder];
    }
    else if ([self.safetyStockQuantityTextField isFirstResponder] && [touch view] != self.safetyStockQuantityTextField) {
        [self.safetyStockQuantityTextField resignFirstResponder];
    }
    else if ([self.optimumOrderingQuantityTextField isFirstResponder] && [touch view] != self.optimumOrderingQuantityTextField) {
        [self.optimumOrderingQuantityTextField resignFirstResponder];
    }
    else if ([self.incrementQuantityTextField isFirstResponder] && [touch view] != self.incrementQuantityTextField) {
        [self.incrementQuantityTextField resignFirstResponder];
    }
    else if ([self.priceTextField isFirstResponder] && [touch view] != self.priceTextField) {
        [self.priceTextField resignFirstResponder];
    }
    else if ([self.uomPickerView isFirstResponder] && [touch view] != self.uomPickerView) {
        [self.uomPickerView resignFirstResponder];
    }
    else if ([self.timePeriodTextField isFirstResponder] && [touch view] != self.timePeriodTextField) {
        [self.timePeriodTextField resignFirstResponder];
    }
    else if ([self.timeUnitPickerView isFirstResponder] && [touch view] != self.timeUnitPickerView) {
        [self.timeUnitPickerView resignFirstResponder];
    }
    
    [super touchesBegan:touches withEvent:event];
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)recognizer
{
    CGPoint touchPoint = [recognizer locationInView:self.scrollView];
    
    // Hides keyboard when losing focus on a UITextField contained within a scroll view
    if ([self.nameTextField isFirstResponder]) {
        CGRect bounds = self.nameTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.nameTextField resignFirstResponder];
    }
    else if ([self.descriptionTextField isFirstResponder]) {
        CGRect bounds = self.descriptionTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.descriptionTextField resignFirstResponder];
    }
    else if ([self.typePickerView isFirstResponder]) {
        CGRect bounds = self.typePickerView.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.typePickerView resignFirstResponder];
    }
    else if ([self.upcTextField isFirstResponder]) {
        CGRect bounds = self.upcTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.upcTextField resignFirstResponder];
    }
    else if ([self.vendorTextField isFirstResponder]) {
        CGRect bounds = self.vendorTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.vendorTextField resignFirstResponder];
    }
    else if ([self.vendorCodeTextField isFirstResponder]) {
        CGRect bounds = self.vendorCodeTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.vendorCodeTextField resignFirstResponder];
    }
    else if ([self.reorderPointQuantityTextField isFirstResponder]) {
        CGRect bounds = self.reorderPointQuantityTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.reorderPointQuantityTextField resignFirstResponder];
    }
    else if ([self.leadTimeDaysTextField isFirstResponder]) {
        CGRect bounds = self.leadTimeDaysTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.leadTimeDaysTextField resignFirstResponder];
    }
    else if ([self.safetyStockQuantityTextField isFirstResponder]) {
        CGRect bounds = self.safetyStockQuantityTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.safetyStockQuantityTextField resignFirstResponder];
    }
    else if ([self.optimumOrderingQuantityTextField isFirstResponder]) {
        CGRect bounds = self.optimumOrderingQuantityTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.optimumOrderingQuantityTextField resignFirstResponder];
    }
    else if ([self.incrementQuantityTextField isFirstResponder]) {
        CGRect bounds = self.incrementQuantityTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.incrementQuantityTextField resignFirstResponder];
    }
    else if ([self.priceTextField isFirstResponder]) {
        CGRect bounds = self.priceTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.priceTextField resignFirstResponder];
    }
    else if ([self.uomPickerView isFirstResponder]) {
        CGRect bounds = self.uomPickerView.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.uomPickerView resignFirstResponder];
    }
    else if ([self.timePeriodTextField isFirstResponder]) {
        CGRect bounds = self.timePeriodTextField.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.timePeriodTextField resignFirstResponder];
    }
    else if ([self.timeUnitPickerView isFirstResponder]) {
        CGRect bounds = self.timeUnitPickerView.bounds;
        BOOL touchWithinBounds = [VMUtilityService isTouchWithinBounds:&touchPoint bounds:&bounds];
        if (touchWithinBounds)
            [self.timeUnitPickerView resignFirstResponder];
    }
}

- (IBAction)cancelPressed:(id)sender{
    [self loadLastViewController];
}

- (IBAction)okPressed:(id)sender{
    NSString *alertViewTitle = @"Confirm Save";
    NSString *alertMessage = @"Are you sure you wish to save this item master?";
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: alertViewTitle
                          message: alertMessage
                          delegate: self
                          cancelButtonTitle:@"No"
                          otherButtonTitles:@"Yes",nil];    // value passed to override of alertView method
    [alert show];
}

- (IBAction)chooseImagePressed:(id)sender {
    @try {
        if ([self.popoverController isPopoverVisible]) {
            [self.popoverController dismissPopoverAnimated:YES];
        } else {
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]) {
                
                UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                imagePicker.delegate = self;
                imagePicker.allowsEditing = NO;
                imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeImage, nil];
                
                if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                    [self presentModalViewController:imagePicker animated:YES];
                }
                else {
                    // Apple requires showing image picker on iPad in a popover controller
                    self.popoverController = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
                    self.popoverController.delegate = self;
                    [self.popoverController presentPopoverFromRect:CGRectMake(0,0,320,480) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
                }
                
                self.newMedia = NO;
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"An exception has occured: %@", [exception reason]);
    }
}

- (IBAction)removeImagePressed:(id)sender {
    self.itemMasterImageView.image = [UIImage imageNamed:IMAGE_NAME_NO_PHOTO_AVAILABLE];
}

- (IBAction)cameraPressed:(id)sender{
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    [self presentModalViewController:imagePicker animated:YES];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeImage, nil];
        imagePicker.allowsEditing = NO;
        [self presentModalViewController:imagePicker animated:YES];
        self.newMedia = YES;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		// do nothing except close alertView
	}
	else {
        BOOL success = NO;
        if ([[alertView.title lowercaseString] caseInsensitiveCompare:@"Confirm Save"] == NSOrderedSame)
            success = [self saveItemMaster];
        
        if (success)
            [self loadLastViewController];
        else {
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: @"Save failed"
                                  message: @"Item master already exists"\
                                  delegate: nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
	}
}

-(void)setFocusedFieldText:(NSString *)text {
    // populate field that has focus with barcode value
    if ([self.nameTextField isFirstResponder]) {
        [self.nameTextField setText:text];
        [self.nameTextField resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.descriptionTextField isFirstResponder]) {
        [self.descriptionTextField setText:text];
        [self.descriptionTextField resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.upcTextField isFirstResponder]) {
        [self.upcTextField setText:text];
        [self.upcTextField resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.vendorTextField isFirstResponder]) {
        [self.vendorTextField setText:text];
        [self.vendorTextField resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.vendorCodeTextField isFirstResponder]) {
        [self.vendorCodeTextField setText:text];
        [self.vendorCodeTextField resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.reorderPointQuantityTextField isFirstResponder]) {
        [self.reorderPointQuantityTextField setText:text];
        [self.reorderPointQuantityTextField resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.leadTimeDaysTextField isFirstResponder]) {
        [self.leadTimeDaysTextField setText:text];
        [self.leadTimeDaysTextField resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.safetyStockQuantityTextField isFirstResponder]) {
        [self.safetyStockQuantityTextField setText:text];
        [self.safetyStockQuantityTextField resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.optimumOrderingQuantityTextField isFirstResponder]) {
        [self.optimumOrderingQuantityTextField setText:text];
        [self.optimumOrderingQuantityTextField resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.incrementQuantityTextField isFirstResponder]) {
        [self.incrementQuantityTextField setText:text];
        [self.incrementQuantityTextField resignFirstResponder];    // automatically lose focus to hide keyboard
    }
    else if ([self.priceTextField isFirstResponder]) {
        [self.priceTextField setText:text];
        [self.priceTextField resignFirstResponder];    // automatically lose focus to hide keyboard
    }
}

#pragma mark -
#pragma mark PickerView DataSource
/****************************************************************************
 * PickerView DataSource
 ****************************************************************************/

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if ([pickerView isEqual: self.typePickerView]) {
        return [self.typePickerArray count];
    }
    else if([pickerView isEqual: self.uomPickerView]) {
        return [self.uomPickerArray count];
    }
    else if([pickerView isEqual: self.timeUnitPickerView]) {
        return [self.timeUnitPickerArray count];
    }
    else
        return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if ([pickerView isEqual: self.typePickerView]) {
        return [self.typePickerArray objectAtIndex:row];
    }
    else if([pickerView isEqual: self.uomPickerView]) {
        return [self.uomPickerArray objectAtIndex:row];
    }
    else if([pickerView isEqual: self.timeUnitPickerView]) {
        return [self.timeUnitPickerArray objectAtIndex:row];
    }
    else
        return 0;
}

-(void)loadTypePickerArray {
    self.typePickerArray = [[NSArray alloc] initWithObjects:
                        @"Countable", @"Weighable", nil];
}

-(void)loadUomPickerArray {
    self.uomPickerArray = [[NSArray alloc] initWithObjects:
                       @"None", @"Bag", @"Box", @"Container", @"Package", @"Pallet", @"Piece", nil];
}

-(void)loadTimeUnitPickerArray {
    self.timeUnitPickerArray = [[NSArray alloc] initWithObjects:
                            @"Second(s)", @"Minute(s)", @"Hour(s)", @"Day(s)", @"Week(s)", @"Month(s)", @"Year(s)", nil];
}

#pragma mark -
#pragma mark PickerView Delegate
/****************************************************************************
 * PickerView Delegate
 ****************************************************************************/

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if ([pickerView isEqual: self.typePickerView]) {
        //int typeIndex = [[typePickerArray objectAtIndex:row] intValue];
        // do something
    }
    else if([pickerView isEqual: self.uomPickerView]) {
        //int uomIndex = [[uomPickerArray objectAtIndex:row] intValue];
        // do something
    }
    else if([pickerView isEqual: self.timeUnitPickerView]) {
        //int timeUnitIndex = [[timeUnitPickerArray objectAtIndex:row] intValue];
        // do something
    }
}

#pragma mark -
#pragma mark UIImagePickerController Delegate
/****************************************************************************
 * UIImagePickerController Delegate
 ****************************************************************************/

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissModalViewControllerAnimated:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    @try {
        [self.popoverController dismissPopoverAnimated:YES];
        
        NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
        [self dismissModalViewControllerAnimated:YES];
        if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
            UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
            self.itemMasterImageView.image = image;
            if (self.newMedia)
                UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:finishedSavingWithError:contextInfo:), nil);
        }
        else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie]) {
            // Code here to support video if enabled
        }
    }
    @catch (NSException *exception) {
        NSLog(@"An exception has occured: %@", [exception reason]);
    }
}

-(void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Save failed"
                              message: @"Failed to save image"\
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark -
#pragma mark CRUD Methods
/****************************************************************************
 * CRUD Methods
 ****************************************************************************/

-(BOOL)saveItemMaster {
    @autoreleasepool {
        NSString *name = self.nameTextField.text;
        
        if (self.itemMasterToEdit == nil) {
            // New
            @try {
                VMItemMaster *itemMasterFound = [VMInventoryService readItemMasterByName:name];
                if (itemMasterFound)
                    return NO;
                
                NSString *description = self.descriptionTextField.text;
                UIImage *image = self.itemMasterImageView.image;
                
                // Type
                NSInteger typeRow = [self.typePickerView selectedRowInComponent:0];
                NSString *type = [[self.typePickerArray objectAtIndex:typeRow] lowercaseString];
                
                NSString *upc = self.upcTextField.text;
                NSString *vendor = self.vendorTextField.text;
                NSString *vendorCode = self.vendorCodeTextField.text;
                float reorderPointQuantity = [self.reorderPointQuantityTextField.text floatValue];
                int leadTimeDays = [self.leadTimeDaysTextField.text integerValue];
                float safetyStockQuantity = [self.safetyStockQuantityTextField.text floatValue];
                float optimumOrderingQuantity = [self.optimumOrderingQuantityTextField.text floatValue];
                float incrementQuantity = [self.incrementQuantityTextField.text floatValue];
                float price = [self.priceTextField.text floatValue];
                
                // UOM
                NSInteger uomRow = [self.uomPickerView selectedRowInComponent:0];
                NSString *uom = [[self.uomPickerArray objectAtIndex:uomRow] lowercaseString];

                VMItemMaster *newItemMaster = [[VMItemMaster alloc] init:nil name:name type:type description:description upc:upc vendor:vendor vendorCode:vendorCode incrementQuantity:incrementQuantity price:price uom:uom image:image];
                newItemMaster.reorderPointQuantity = reorderPointQuantity;
                newItemMaster.leadTimeDays = leadTimeDays;
                newItemMaster.safetyStockQuantity = safetyStockQuantity;
                newItemMaster.optimumOrderingQuantity = optimumOrderingQuantity;
                newItemMaster.expiration = self.expiresSwitch.isOn;
                newItemMaster.expirationTimePeriod = [self.timePeriodTextField.text integerValue];
                newItemMaster.enabled = self.enabledSwitch.isOn;
                newItemMaster.createdDate = [NSDate date];
                newItemMaster.lastUpdatedDate = [NSDate date];
                
                [VMInventoryService createItemMaster:newItemMaster];
            }
            @catch (NSException *exception) {
                NSLog(@"An exception occurred: %@", exception.reason);
            }
        }
        else {
            // Edit
            @try {
                self.itemMasterToEdit.name = name;
                self.itemMasterToEdit.image = self.itemMasterImageView.image;
                
                // Type
                NSInteger typeRow = [self.typePickerView selectedRowInComponent:0];
                NSString *type = [[self.typePickerArray objectAtIndex:typeRow] lowercaseString];
                self.itemMasterToEdit.type = type;
                
                self.itemMasterToEdit.upc = self.upcTextField.text;
                self.itemMasterToEdit.vendor = self.vendorTextField.text;
                self.itemMasterToEdit.vendorCode = self.vendorCodeTextField.text;
                self.itemMasterToEdit.reorderPointQuantity = [self.reorderPointQuantityTextField.text floatValue];
                self.itemMasterToEdit.leadTimeDays = [self.leadTimeDaysTextField.text integerValue];
                self.itemMasterToEdit.safetyStockQuantity = [self.safetyStockQuantityTextField.text floatValue];
                self.itemMasterToEdit.optimumOrderingQuantity = [self.optimumOrderingQuantityTextField.text floatValue];
                self.itemMasterToEdit.incrementQuantity = [self.incrementQuantityTextField.text floatValue];
                self.itemMasterToEdit.price = [self.priceTextField.text floatValue];
                
                // UOM
                NSInteger uomRow = [self.uomPickerView selectedRowInComponent:0];
                NSString *uom = [[self.uomPickerArray objectAtIndex:uomRow] lowercaseString];
                self.itemMasterToEdit.uom = uom;
                
                self.itemMasterToEdit.expiration = self.expiresSwitch.isOn;
                self.itemMasterToEdit.expirationTimePeriod = [self.timePeriodTextField.text integerValue];
                self.itemMasterToEdit.enabled = self.enabledSwitch.isOn;
                self.itemMasterToEdit.lastUpdatedDate = [NSDate date];
                
                [VMInventoryService updateItemMaster:self.itemMasterToEdit];
            }
            @catch (NSException *exception) {
                NSLog(@"An exception occurred: %@", exception.reason);
            }
        }
    }
    return YES;
}

#pragma mark -
#pragma mark VerbiBarcodeReaderDelegate Methods
/****************************************************************************
 * VerbiBarcodeReaderDelegate Methods
 ****************************************************************************/

-(void)handleBarcodeReaderReadEvent:(NSString *)barcode symbology:(NSString *)symbology event:(VMBarcodeReaderEvent *)event {
    [self setFocusedFieldText:barcode];
}

@end
