//
//  VMAddInventoryItemsViewController.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMAddInventoryItemsVC.h"
#import "VMUpdateInventoryConfirmVC.h"
#import "VMNewEditInventoryItemVC.h"
#import "VMAddInventoryItemsWorkflowStep.h"
#import "VMSelectInventoryLocationWorkflowStep.h"
#import "VMInventoryService.h"

@interface VMAddInventoryItemsVC ()

@end

@implementation VMAddInventoryItemsVC

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@synthesize cancelButton = _cancelButton;
@synthesize finishButton = _finishButton;
@synthesize selectAllButton = _selectAllButton;
@synthesize editButton = _editButton;
@synthesize removeButton = _removeButton;
@synthesize addButton = _addButton;
@synthesize locationTextField = _locationTextField;
@synthesize itemsTableView = _itemsTableView;

BOOL allSelected;

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose {
    @try {
        _cancelButton = nil;
        _finishButton = nil;
        _selectAllButton = nil;
        _editButton = nil;
        _removeButton = nil;
        _addButton = nil;
        _locationTextField = nil;
        _itemsTableView = nil;
        
        [super dispose];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark View Lifecycle Public
/****************************************************************************
 * View Lifecycle Public
 ****************************************************************************/

- (void)initializeUIOnFirstLoad {
    @try {
        [super initializeUIOnFirstLoad];
        
        // Set workflow step
        if (self.workflow) {
            // If this is a reload of form, then get existing workflowStep from workflow
            if (self.workflowStep == nil) {
                self.workflowStep = [self.workflow.workflowSteps valueForKey:WKFL_STEP_INV_ADD_INVENTORY_ITEMS];
            }
            // If still nil, then this is first load of form during workflow so create new step
            if (self.workflowStep == nil) {
                self.workflowStep = [[VMAddInventoryItemsWorkflowStep alloc] init:self.workflow];
                [self.workflow addWorkflowStep:self.workflowStep];
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIBeforeRendering {
    @try {
        [super initializeUIBeforeRendering];
        
        @autoreleasepool {
            // Populate location text field with selected one from last step
            VMSelectInventoryLocationWorkflowStep *selectInventoryLocationWorkflowStep = [self.workflow.workflowSteps objectForKey:WKFL_STEP_INV_SELECT_INVENTORY_LOCATION];
            if (selectInventoryLocationWorkflowStep && selectInventoryLocationWorkflowStep.location) {
                VMLocation *location = selectInventoryLocationWorkflowStep.location;
                if (location && location.name) {
                    NSString *locationName = location.name;
                    if (_locationTextField)
                        _locationTextField.text = [locationName copy];
                }
            }
        }
        
        [self reloadTableView];
    }
    @catch (NSException *exception) {
        NSLog(@"Location could not be acquired from workflow. An exception has occurred: %@", [exception reason]);
    }
}

- (void)initializeUIAfterRendering {
    @try {
        [super initializeUIAfterRendering];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO methods
 ****************************************************************************/

- (IBAction)cancelPressed:(id)sender{
    [self loadMainViewController];
}

- (IBAction)finishPressed:(id)sender{
    @autoreleasepool {
        if (((VMAddInventoryItemsWorkflowStep *)self.workflowStep).itemsArray && ((VMAddInventoryItemsWorkflowStep *)self.workflowStep).itemsArray.count == 0) {
            NSString *alertViewTitle = @"Add Items";
            NSString *alertMessage = @"A minimum of one item must added before moving to the next step.";
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: alertViewTitle
                                  message: alertMessage
                                  delegate: self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
        else {
            // Load the update inventory confirm controller
            VMUpdateInventoryConfirmVC *updateInventoryConfirmViewController = nil;
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                updateInventoryConfirmViewController = [[VMUpdateInventoryConfirmVC alloc] initWithNibName:@"VMUpdateInventoryConfirmVC_iPhone" bundle:nil];
            } else {
                updateInventoryConfirmViewController = [[VMUpdateInventoryConfirmVC alloc] initWithNibName:@"VMUpdateInventoryConfirmVC_iPad" bundle:nil];
            }
//            updateInventoryConfirmViewController.mainViewController = self.mainViewController;
            updateInventoryConfirmViewController.lastViewController = self;
            updateInventoryConfirmViewController.workflow = self.workflow;
            [self presentModalViewController:updateInventoryConfirmViewController animated:NO];
        }
    }
}

- (IBAction)selectAllPressed:(id)sender{
    @autoreleasepool {
        if (allSelected) {
            int rowCount = [self.itemsTableView numberOfRowsInSection:0];
            for (int i = 0; i < rowCount; i++) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];    // convert iterator to indexPath
                UITableViewCell *cell = [self.itemsTableView cellForRowAtIndexPath:indexPath];    // get cell using indexPath
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
            [self.selectAllButton setTitle:@"Select All" forState:UIControlStateNormal];
            allSelected = NO;
        }
        else {
            int rowCount = [self.itemsTableView numberOfRowsInSection:0];
            for (int i = 0; i < rowCount; i++) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];    // convert iterator to indexPath
                UITableViewCell *cell = [self.itemsTableView cellForRowAtIndexPath:indexPath];    // get cell using indexPath
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            [self.selectAllButton setTitle:@"Deselect All" forState:UIControlStateNormal];
            allSelected = YES;
        }
    }
}

- (IBAction)addPressed:(id)sender{
    [self loadNewEditInventoryItemScreen:nil];
}

- (IBAction)editPressed:(id)sender{
    VMItem *item;
    @autoreleasepool {
        NSString *alertViewTitle = @"Edit";
        
        NSArray *indexes = [VMUtilityService getCheckedOrSelectedIndexes:self.itemsTableView];
        if (indexes.count == 0) {
            NSString *alertMessage = @"Select an item.";
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: alertViewTitle
                                  message: alertMessage
                                  delegate: self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
        else if (indexes.count == 1) {
            for (NSIndexPath *path in indexes) {
                NSUInteger index = [path indexAtPosition:[path length] - 1];
                item = [((VMAddInventoryItemsWorkflowStep *)self.workflowStep).itemsArray objectAtIndex:index];
            }
            if (item) {
                [self loadNewEditInventoryItemScreen:item];
            }
        }
        else {
            NSString *alertMessage = @"Select a single item.";
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: alertViewTitle
                                  message: alertMessage
                                  delegate: self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
    }
}

- (IBAction)removePressed:(id)sender{
    NSMutableArray *itemNames;
    NSString *alertViewTitle;
    
    @autoreleasepool {
        NSArray *indexes = [VMUtilityService getCheckedOrSelectedIndexes:self.itemsTableView];
        if (indexes.count == 0) {
            alertViewTitle = @"Delete";
            NSString *alertMessage = @"Select an item master.";
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: alertViewTitle
                                  message: alertMessage
                                  delegate: self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
        else {
            itemNames = [[NSMutableArray alloc] init];
            for (NSIndexPath *path in indexes) {
                NSUInteger index = [path indexAtPosition:[path length] - 1];
                VMItem *item = [((VMAddInventoryItemsWorkflowStep *)self.workflowStep).itemsArray objectAtIndex:index];
                [itemNames addObject:item.itemMaster.name];
            }
            
            alertViewTitle = @"Confirm Delete";
            NSString *alertMessage = [NSString stringWithFormat:@"Are you sure you wish to delete these %i item(s)?", itemNames.count];
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: alertViewTitle
                                  message: alertMessage
                                  delegate: self
                                  cancelButtonTitle:@"No"
                                  otherButtonTitles:@"Yes",nil];    // value passed to override of alertView method
            [alert show];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		// do nothing except close alertView
	}
	else {
        @autoreleasepool {
            if ([[alertView.title lowercaseString] caseInsensitiveCompare:@"Confirm Selection"] == NSOrderedSame) {
                // Load the add inventory items controller
                //[self loadAddItemsScreen];
            }
            else if ([[alertView.title lowercaseString] caseInsensitiveCompare:@"Confirm Delete"] == NSOrderedSame) {
                NSMutableArray *deletedItems = [self deleteItems];
                [self reloadTableView];
                
                NSString *alertViewTitle = @"Delete Completed";
                NSString *alertMessage = [[NSString alloc] initWithFormat:@"%i items deleted.", deletedItems.count];
                UIAlertView *alert = [[UIAlertView alloc]
                                      initWithTitle: alertViewTitle
                                      message: alertMessage
                                      delegate: self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
                [alert show];
            }
            else if ([[alertView.title lowercaseString] caseInsensitiveCompare:@"Selection Not Found"] == NSOrderedSame) {
                // If location does not exist, then navigate to New Location screen
                // Perform item master lookup and create new instance using defaults provided by item master
                NSString *itemUPC = [self getLastScanValue];
                VMItemMaster *itemMaster = [VMInventoryService readItemMasterByUPC:itemUPC];
                if (itemMaster) {
                    VMSelectInventoryLocationWorkflowStep *selectInventoryLocationWorkflowStep = [self.workflow.workflowSteps objectForKey:WKFL_STEP_INV_SELECT_INVENTORY_LOCATION];
                    if (selectInventoryLocationWorkflowStep && selectInventoryLocationWorkflowStep.location) {
                        VMLocation *location = selectInventoryLocationWorkflowStep.location;
                        VMItem *item = [[VMItem alloc] init:nil itemMaster:itemMaster location:location quantity:itemMaster.incrementQuantity];
                        if (item) {
                            item.value = itemMaster.price;
                            [self loadNewEditInventoryItemScreen:item];
                        }
                    }
                }
            }
        }
	}
}

#pragma mark -
#pragma mark Table view methods
/****************************************************************************
 * Table view methods
 ****************************************************************************/

// Defines the table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Defines the table - Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ((VMAddInventoryItemsWorkflowStep *)self.workflowStep).itemsArray.count;
}

// Fill the table - Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    @autoreleasepool {
        NSMutableArray *itemsArray = ((VMAddInventoryItemsWorkflowStep *)self.workflowStep).itemsArray;
        if (itemsArray && itemsArray.count > 0) {
            VMItem *item = [itemsArray objectAtIndex:indexPath.row];
            if (item && item.itemMaster) {
                // Cell Title Text
                
                // define the range you're interested in
                NSRange stringRange = {0, MIN([item.itemMaster.type length], 1)};
                // adjust the range to include dependent chars
                stringRange = [item.itemMaster.type rangeOfComposedCharacterSequencesForRange:stringRange];
                // Now you can create the short string
                NSString *typeSubstring = [[item.itemMaster.type uppercaseString] substringWithRange:stringRange];
                
                cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@", typeSubstring, item.itemMaster.name];
                
                // Cell Subtitle Text
                
                NSMutableString *detailText = [[NSMutableString alloc] init];
                
                // Add quantity and value
                [detailText appendFormat:@"%1.2f | $%1.2f ", item.quantity, item.value];
                
                // Add vendor and/or vendor code
                if (item.itemMaster.vendor && [item.itemMaster.vendor length] > 0 && item.itemMaster.vendorCode && [item.itemMaster.vendorCode length] > 0) {
                    [detailText appendFormat:@"| %@ | %@ ", item.itemMaster.vendor, item.itemMaster.vendorCode];
                }
                else if (item.itemMaster.vendor && [item.itemMaster.vendor length] > 0) {
                    [detailText appendFormat:@"| %@ ", item.itemMaster.vendor];
                }
                else if (item.itemMaster.vendorCode && [item.itemMaster.vendorCode length] > 0) {
                    [detailText appendFormat:@"| %@ ", item.itemMaster.vendorCode];
                }
                
                // Add description
                if (item.itemMaster.description && [item.itemMaster.description length] > 0) {
                    [detailText appendFormat:@"| %@ ", item.itemMaster.description];
                }
                
                cell.detailTextLabel.text = detailText;
            }
        }
    }
    
    // Convert all row selections to check marks
    NSIndexPath *selection = [tableView indexPathForSelectedRow];
    if (selection && selection.row == indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        // uncheck
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else {
        // check
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
}

/*
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Items";
}
 */

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger) section
{
    //section text as a label
    UILabel *lbl = [[UILabel alloc] init];
    lbl.textAlignment = UITextAlignmentLeft;
    
    lbl.text = @"Items";
    [lbl setBackgroundColor:[UIColor clearColor]];
    
    return lbl;
}

-(void)reloadItemsArray {
    @try {
        if (((VMAddInventoryItemsWorkflowStep *)self.workflowStep).itemsArray == nil)
            ((VMAddInventoryItemsWorkflowStep *)self.workflowStep).itemsArray = [[NSMutableArray alloc] init];
        if (((VMAddInventoryItemsWorkflowStep *)self.workflowStep).itemNamesArray == nil)
            ((VMAddInventoryItemsWorkflowStep *)self.workflowStep).itemNamesArray = [[NSMutableArray alloc] init];
        if (((VMAddInventoryItemsWorkflowStep *)self.workflowStep).itemUPCsArray == nil)
            ((VMAddInventoryItemsWorkflowStep *)self.workflowStep).itemUPCsArray = [[NSMutableArray alloc] init];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

-(void)reloadTableView {
    [self reloadItemsArray];
    [_itemsTableView reloadData];
}

-(NSMutableArray *)deleteItems {
    NSMutableArray *deletedItems;
    @autoreleasepool {
        @try {
            NSArray *indexes = [VMUtilityService getCheckedOrSelectedIndexes:self.itemsTableView];
            if (indexes.count > 0) {
                deletedItems = [[NSMutableArray alloc] init];
                for (NSIndexPath *path in indexes) {
                    NSUInteger index = [path indexAtPosition:[path length] - 1];
                    VMItem *item = [((VMAddInventoryItemsWorkflowStep *)self.workflowStep).itemsArray objectAtIndex:index];
                    [deletedItems addObject:item];
                }
                if (deletedItems.count > 0) {
                    [((VMAddInventoryItemsWorkflowStep *)self.workflowStep).itemUPCsArray removeObjectsInArray:deletedItems];
                    [((VMAddInventoryItemsWorkflowStep *)self.workflowStep).itemNamesArray removeObjectsInArray:deletedItems];
                    [((VMAddInventoryItemsWorkflowStep *)self.workflowStep).itemsArray removeObjectsInArray:deletedItems];
                }
            }
        }
        @catch (NSException *e) {
            NSLog(@"An exception has occurred: %@", e.reason);
        }
    }
    return deletedItems;
}

#pragma mark -
#pragma mark Navigation methods
/****************************************************************************
 * Navigation methods
 ****************************************************************************/

-(void)loadNewEditInventoryItemScreen:(VMItem *)item {
    // Load the new/edit inventory item controller
    VMNewEditInventoryItemVC *newEditInventoryItemViewController = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        newEditInventoryItemViewController = [[VMNewEditInventoryItemVC alloc] initWithNibName:@"VMNewEditInventoryItemVC_iPhone" bundle:nil];
    } else {
        newEditInventoryItemViewController = [[VMNewEditInventoryItemVC alloc] initWithNibName:@"VMNewEditInventoryItemVC_iPad" bundle:nil];
    }
//    newEditInventoryItemViewController.mainViewController = self.mainViewController;
    newEditInventoryItemViewController.lastViewController = self;
    newEditInventoryItemViewController.addInventoryItemsController = self;
    newEditInventoryItemViewController.workflow = self.workflow;
    newEditInventoryItemViewController.workflowStep = self.workflowStep;
    if (item) {
        newEditInventoryItemViewController.itemToEdit = item;
    }
    [self presentModalViewController:newEditInventoryItemViewController animated:NO];
}

-(void)lookupItem:(NSString *)scanValue {
    VMItem *item = nil;
    
    // Step 1 - Try lookup in table by name
    item = [self lookupItemInTableByName:scanValue];
    
    // Step 2 - Try lookup in table by UPC
    if (item == nil)
        item = [self lookupItemInTableByUPC:scanValue];
    
    // Step 3 - Try lookup from DB by name
    if (item == nil)
        item = [self lookupItemFromDBByName:scanValue];
    
    // Step 4 - Try lookup from DB by UPC
    if (item == nil)
        item = [self lookupItemFromDBByUPC:scanValue];
    
    // Step 5 - If item master exists, load New/Edit screen; else ask user if they like to add a new one using scan value
    if (item) {
        [self loadNewEditInventoryItemScreen:item];
    }
    else {
        NSString *alertMessage = [NSString stringWithFormat:@"Item not found. Would like to add '%@' as a new item?", scanValue];
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Selection Not Found"
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"No"
                              otherButtonTitles:@"Yes",nil];    // value passed to override of alertView method
        [alert show];
    }
}

-(VMItem *)lookupItemInTableByName:(NSString *)itemName {
    VMItem *item;
    @autoreleasepool {
        @try {
            NSUInteger index = [((VMAddInventoryItemsWorkflowStep *)self.workflowStep).itemNamesArray indexOfObject:itemName];
            item = [((VMAddInventoryItemsWorkflowStep *)self.workflowStep).itemsArray objectAtIndex:index];
            if (item && item.itemMaster) {
                item.quantity += item.itemMaster.incrementQuantity;
                item.value = item.quantity * item.itemMaster.price;
            }
        }
        @catch (NSException *exception) {
        }
    }
    return item;
}

-(VMItem *)lookupItemInTableByUPC:(NSString *)itemUPC {
    VMItem *item;
    @autoreleasepool {
        @try {
            NSUInteger index = [((VMAddInventoryItemsWorkflowStep *)self.workflowStep).itemUPCsArray indexOfObject:itemUPC];
            item = [((VMAddInventoryItemsWorkflowStep *)self.workflowStep).itemsArray objectAtIndex:index];
            if (item && item.itemMaster) {
                item.quantity += item.itemMaster.incrementQuantity;
                item.value = item.quantity * item.itemMaster.price;
            }
        }
        @catch (NSException *exception) {
        }
    }
    return item;
}

-(VMItem *)lookupItemFromDBByName:(NSString *)itemName {
    VMItem *item;
    @autoreleasepool {
        @try {
            VMItemMaster *itemMaster = [VMInventoryService readItemMasterByName:itemName];
            if (itemMaster) {
                VMSelectInventoryLocationWorkflowStep *selectInventoryLocationWorkflowStep = [self.workflow.workflowSteps objectForKey:WKFL_STEP_INV_SELECT_INVENTORY_LOCATION];
                if (selectInventoryLocationWorkflowStep && selectInventoryLocationWorkflowStep.location) {
                    VMLocation *location = selectInventoryLocationWorkflowStep.location;
                    if (location && location.name) {
                        item = [[VMItem alloc] init:nil itemMaster:itemMaster location:location quantity:itemMaster.incrementQuantity];
                    }
                }
            }
        }
        @catch (NSException *exception) {
        }
    }
    return item;
}

-(VMItem *)lookupItemFromDBByUPC:(NSString *)itemUPC {
    VMItem *item;
    @autoreleasepool {
        @try {
            VMItemMaster *itemMaster = [VMInventoryService readItemMasterByUPC:itemUPC];
            if (itemMaster) {
                VMSelectInventoryLocationWorkflowStep *selectInventoryLocationWorkflowStep = [self.workflow.workflowSteps objectForKey:WKFL_STEP_INV_SELECT_INVENTORY_LOCATION];
                if (selectInventoryLocationWorkflowStep && selectInventoryLocationWorkflowStep.location) {
                    VMLocation *location = selectInventoryLocationWorkflowStep.location;
                    if (location && location.name) {
                        item = [[VMItem alloc] init:nil itemMaster:itemMaster location:location quantity:itemMaster.incrementQuantity];
                    }
                }
            }
        }
        @catch (NSException *exception) {
        }
    }
    return item;
}

#pragma mark -
#pragma mark VerbiBarcodeReaderDelegate Methods
/****************************************************************************
 * VerbiBarcodeReaderDelegate Methods
 ****************************************************************************/

-(void)handleBarcodeReaderReadEvent:(NSString *)barcode symbology:(NSString *)symbology event:(VMBarcodeReaderEvent *)event {
    [self lookupItem:barcode];
}

@end
