//
//  VMInventoryItemMasterListVC.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMInventoryItemMasterListVC.h"
#import "VMInventoryService.h"
#import "VMNewEditInventoryItemMasterVC.h"
#import "VMImportInventoryItemMasterListVC.h"
#import "VMSelectInventoryLocationWorkflowStep.h"

@interface VMInventoryItemMasterListVC ()
@property(nonatomic, strong) NSMutableArray *itemMastersArray;
@property(nonatomic, strong) NSMutableArray *itemMasterNamesArray;
@property(nonatomic, strong) NSMutableArray *itemMasterUPCsArray;
@property(nonatomic, strong) NSMutableArray *deletedItemMasterNamesArray;
@property(nonatomic) BOOL allSelected;
@property(nonatomic) BOOL searchAll;
@end

@implementation VMInventoryItemMasterListVC

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@synthesize inventoryItemController = _inventoryItemController;

// Buttons
@synthesize addButton = _addButton;
@synthesize deleteButton = _deleteButton;
@synthesize editButton = _editButton;
@synthesize selectAllButton = _selectAllButton;
@synthesize searchTextField = _searchTextField;
@synthesize clearButton = _clearButton;

// Table View
@synthesize itemMastersTableView = _itemMastersTableView;
@synthesize itemMastersArray = _itemMastersArray;
@synthesize itemMasterNamesArray = _itemMasterNamesArray;
@synthesize itemMasterUPCsArray = _itemMasterUPCsArray;
@synthesize deletedItemMasterNamesArray = _deletedItemMasterNamesArray;
@synthesize allSelected = _allSelected;
@synthesize searchAll = _searchAll;

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose {
    @try {
        self.addButton = nil;
        self.deleteButton = nil;
        self.editButton = nil;
        self.selectAllButton = nil;
        self.searchTextField = nil;
        self.clearButton = nil;
        self.itemMastersTableView = nil;

        if (self.itemMastersArray) {
            [self.itemMastersArray removeAllObjects];
            self.itemMastersArray = nil;
        }
        if (self.itemMasterNamesArray) {
            [self.itemMasterNamesArray removeAllObjects];
            self.itemMasterNamesArray = nil;
        }
        if (self.itemMasterUPCsArray) {
            [self.itemMasterUPCsArray removeAllObjects];
            self.itemMasterUPCsArray = nil;
        }
        if (self.deletedItemMasterNamesArray) {
            [self.deletedItemMasterNamesArray removeAllObjects];
            self.deletedItemMasterNamesArray = nil;
        }
        
        [super dispose];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark View Lifecycle Public
/****************************************************************************
 * View Lifecycle Public
 ****************************************************************************/

- (void)initializeUIOnFirstLoad {
    @try {
        [super initializeUIOnFirstLoad];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIBeforeRendering {
    @try {
        [super initializeUIBeforeRendering];
        
        if (self.inventoryItemController) {
            self.addButton.enabled = NO;
            self.deleteButton.enabled = NO;
            self.editButton.enabled = NO;
            self.selectAllButton.enabled = NO;
        }
        self.searchTextField.delegate = self;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

- (void)initializeUIAfterRendering {
    @try {
        [super initializeUIAfterRendering];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO Methods
 ****************************************************************************/

// Hides keyboard when losing focus on a UITextView
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    
    if ([self.searchTextField isFirstResponder] && [touch view] != self.searchTextField) {
        [self.searchTextField resignFirstResponder];
    }
    else if ([self.itemMastersTableView isFirstResponder] && [touch view] != self.itemMastersTableView) {
        [self.itemMastersTableView resignFirstResponder];
    }
    
    [super touchesBegan:touches withEvent:event];
}

- (IBAction)backPressed:(id)sender{
    @try {
        [super backPressed:sender];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
}

- (IBAction)addButtonPressed:(id)sender{
    NSString *alertViewTitle = @"New or Import";
    NSString *alertMessage = @"Would you like to add a new item or import?";
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: alertViewTitle
                          message: alertMessage
                          delegate: self
                          cancelButtonTitle:@"No"
                          otherButtonTitles:@"New",@"Import",nil];    // value passed to override of alertView method
    [alert show];
}

- (IBAction)deleteButtonPressed:(id)sender{
    NSString *alertViewTitle;
    
    NSArray *indexes = [VMUtilityService getCheckedOrSelectedIndexes:self.itemMastersTableView];
    if (indexes.count == 0) {
        alertViewTitle = @"Delete";
        NSString *alertMessage = @"Select an item master.";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else {
        [self.deletedItemMasterNamesArray removeAllObjects];
        self.deletedItemMasterNamesArray = [[NSMutableArray alloc] init];
        for (NSIndexPath *path in indexes) {
            NSUInteger index = [path indexAtPosition:[path length] - 1];
            VMItemMaster *itemMaster = [self.itemMastersArray objectAtIndex:index];
            [self.deletedItemMasterNamesArray addObject:itemMaster.name];
        }
        
        alertViewTitle = @"Confirm Delete";
        NSString *alertMessage = [NSString stringWithFormat:@"Are you sure you wish to delete these %i item master(s)?", self.deletedItemMasterNamesArray.count];
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"No"
                              otherButtonTitles:@"Yes",nil];    // value passed to override of alertView method
        [alert show];
    }
}

- (IBAction)editButtonPressed:(id)sender{
    VMItemMaster *itemMaster;
    NSString *alertViewTitle = @"Edit";
    
    NSArray *indexes = [VMUtilityService getCheckedOrSelectedIndexes:self.itemMastersTableView];
    if (indexes.count == 0) {
        NSString *alertMessage = @"Select an item master.";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if (indexes.count == 1) {
        for (NSIndexPath *path in indexes) {
            NSUInteger index = [path indexAtPosition:[path length] - 1];
            itemMaster = [self.itemMastersArray objectAtIndex:index];
        }
        if (itemMaster) {
            [self loadNewEditInventoryItemMasterScreen:itemMaster];
        }
    }
    else {
        NSString *alertMessage = @"Select a single item master.";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)selectAllButtonPressed:(id)sender {
    if (self.allSelected) {
        int rowCount = [self.itemMastersTableView numberOfRowsInSection:0];
        for (int i = 0; i < rowCount; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];    // convert iterator to indexPath
            UITableViewCell *cell = [self.itemMastersTableView cellForRowAtIndexPath:indexPath];    // get cell using indexPath
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        [self.selectAllButton setTitle:@"Select All" forState:UIControlStateNormal];
        self.allSelected = NO;
    }
    else {
        int rowCount = [self.itemMastersTableView numberOfRowsInSection:0];
        for (int i = 0; i < rowCount; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];    // convert iterator to indexPath
            UITableViewCell *cell = [self.itemMastersTableView cellForRowAtIndexPath:indexPath];    // get cell using indexPath
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        [self.selectAllButton setTitle:@"Deselect All" forState:UIControlStateNormal];
        self.allSelected = YES;
    }
}

- (void)resetSearch {
    self.activityIndicatorView.hidden = NO;
    [self.activityIndicatorView startAnimating];
    if (self.itemMastersArray) {
        [self.itemMastersArray removeAllObjects];
    }
    if (self.itemMasterNamesArray) {
        [self.itemMasterNamesArray removeAllObjects];
    }
    [self.itemMastersTableView reloadData];
    [self.activityIndicatorView stopAnimating];
    self.activityIndicatorView.hidden = YES;
}

- (IBAction)search:(id)sender{
    if (self.searchTextField) {
        [self resetSearch];
        
        self.activityIndicatorView.hidden = NO;
        [self.activityIndicatorView startAnimating];
        @autoreleasepool {
            if ([self.searchTextField.text length] == 0 && self.searchAll) {
                self.itemMastersArray = [VMInventoryService readAllItemMasters];
                self.searchAll = NO;
            }
            else if ([self.searchTextField.text length] > 0) {
                self.itemMastersArray = [VMInventoryService readLikeName:self.searchTextField.text];
            }
            
            if (self.itemMastersArray && self.itemMastersArray.count > 0) {
                self.itemMasterNamesArray = [[NSMutableArray alloc] init];
                for (VMItemMaster *itemMaster in self.itemMastersArray) {
                    [self.itemMasterNamesArray addObject:itemMaster.name];
                }
            }
        }
        [self.itemMastersTableView reloadData];
        [self.activityIndicatorView stopAnimating];
        self.activityIndicatorView.hidden = YES;
    }
}

- (IBAction)clearPressed:(id)sender{
    if (self.searchTextField)
        self.searchTextField.text = nil;
    [self resetSearch];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		// do nothing except close alertView
        [self.deletedItemMasterNamesArray removeAllObjects];
	}
    else {
        if ([[alertView.title lowercaseString] caseInsensitiveCompare:@"Confirm Delete"] == NSOrderedSame) {
            NSMutableArray *deletedItemMasters = [self deleteItemMasters];
            //[self reloadTableViewWithDeletedItemMasters:deletedItemMasters];
            [self reloadTableView];
            
            NSString *alertViewTitle = @"Delete Completed";
            NSString *alertMessage = [[NSString alloc] initWithFormat:@"%i item masters deleted.", deletedItemMasters.count];
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: alertViewTitle
                                  message: alertMessage
                                  delegate: self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
        else if ([[alertView.title lowercaseString] caseInsensitiveCompare:@"Selection Not Found"] == NSOrderedSame) {
            // If location does not exist, then navigate to New Location screen
            VMItemMaster *itemMaster = [[VMItemMaster alloc] init:nil name:self.getLastScanValue type:nil description:nil upc:nil vendor:nil vendorCode:nil incrementQuantity:0 price:0 uom:nil image:nil];
            [self loadNewEditInventoryItemMasterScreen:itemMaster];
        }
        else if ([[alertView.title lowercaseString] caseInsensitiveCompare:@"New or Import"] == NSOrderedSame) {
            NSString *buttonTitle = [alertView buttonTitleAtIndex:buttonIndex];
            if ([[buttonTitle lowercaseString] caseInsensitiveCompare:@"New"] == NSOrderedSame) {
                [self loadNewEditInventoryItemMasterScreen:nil];
            }
            else {
                [self loadImportInventoryItemMasterListScreen];
            }
        }
	}
}

// Handles when you press return in either text field
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    // If you pressed return in the password text field we will trigger loginPressed for you
    if (textField.tag == kSearchFieldTag){
        if ([self.searchTextField.text length] == 0) {
            self.searchAll = YES;
            [self search:nil];
        }
    }
    
    // this closed the keyboard if you press return in either text field
    return [textField resignFirstResponder];
}

// If you leave the text field we will close the keyboard
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
}

#pragma mark -
#pragma mark Table View Methods
/****************************************************************************
 * Table View Methods
 ****************************************************************************/

// Defines the table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Defines the table - Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.itemMastersArray.count;
}

// Fill the table - Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    VMItemMaster *itemMaster = [self.itemMastersArray objectAtIndex:indexPath.row];
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // define the range you're interested in
    NSRange stringRange = {0, MIN([itemMaster.type length], 1)};
    // adjust the range to include dependent chars
    stringRange = [itemMaster.type rangeOfComposedCharacterSequencesForRange:stringRange];
    // Now you can create the short string
    NSString *typeSubstring = [[itemMaster.type uppercaseString] substringWithRange:stringRange];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@", typeSubstring, itemMaster.name];
    
    // Cell Subtitle Text
    
    NSMutableString *detailText = [[NSMutableString alloc] init];
    if (itemMaster.vendor && [itemMaster.vendor length] > 0 && itemMaster.vendorCode && [itemMaster.vendorCode length] > 0) {
        [detailText appendFormat:@"%@ | %@", itemMaster.vendor, itemMaster.vendorCode];
    }
    else if (itemMaster.vendor && [itemMaster.vendor length] > 0) {
        [detailText appendFormat:@"%@", itemMaster.vendor];
    }
    else if (itemMaster.vendorCode && [itemMaster.vendorCode length] > 0) {
        [detailText appendFormat:@"%@", itemMaster.vendorCode];
    }
    if (itemMaster.description && [itemMaster.description length] > 0)
        [detailText appendFormat:@" | %@", itemMaster.description];
    cell.detailTextLabel.text = detailText;
    
    // Convert all row selections to check marks
    NSIndexPath *selection = [tableView indexPathForSelectedRow];
    if (selection && selection.row == indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        // uncheck
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else {
        // check
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        
        // If lastViewController is VMNewEditInventoryItemVC, then check creates item and assigns it then jumps back
        if (self.inventoryItemController) {
            NSUInteger index = [indexPath indexAtPosition:[indexPath length] - 1];
            VMItemMaster *itemMaster = [self.itemMastersArray objectAtIndex:index];
            if (itemMaster) {
                VMSelectInventoryLocationWorkflowStep *selectInventoryLocationWorkflowStep = [self.workflow.workflowSteps objectForKey:WKFL_STEP_INV_SELECT_INVENTORY_LOCATION];
                if (selectInventoryLocationWorkflowStep && selectInventoryLocationWorkflowStep.location) {
                    VMLocation *location = selectInventoryLocationWorkflowStep.location;
                    VMItem *item = [[VMItem alloc] init:nil itemMaster:itemMaster location:location quantity:itemMaster.incrementQuantity];
                    if (item) {
                        item.value = itemMaster.price;
                        self.inventoryItemController.itemToEdit = item;
                        [self loadLastViewController];
                    }
                }
            }
        }
    }
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Scan or Search Item Masters";
}

-(void)reloadTableView {
    [self reloadItemMastersArray];
    [self.itemMastersTableView reloadData];
}

-(void)reloadItemMastersArray {
    int count = [VMInventoryService itemMasterCount];
    if (count > 0) {
        self.itemMastersArray = [VMInventoryService readAllItemMasters];
        if (self.itemMastersArray && self.itemMastersArray.count > 0) {
            self.itemMasterNamesArray = [[NSMutableArray alloc] init];
            self.itemMasterUPCsArray = [[NSMutableArray alloc] init];
            for (VMItemMaster *itemMaster in self.itemMastersArray) {
                [self.itemMasterNamesArray addObject:itemMaster.name];
                if (itemMaster.upc)
                    [self.itemMasterUPCsArray addObject:itemMaster.upc];
                else
                    [self.itemMasterUPCsArray addObject:@""];   // add blank to keep index matched to self.itemMasterNamesArray
            }
        }
    }
}

#pragma mark -
#pragma mark Navigation Methods
/****************************************************************************
 * Navigation Methods
 ****************************************************************************/

-(void)loadNewEditInventoryItemMasterScreen:(VMItemMaster *)itemMaster {
    @try {
        // Load the new edit inventory item master controller
        VMNewEditInventoryItemMasterVC *newEditInventoryItemMasterViewController = nil;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            newEditInventoryItemMasterViewController = [[VMNewEditInventoryItemMasterVC alloc] initWithNibName:@"VMNewEditInventoryItemMasterVC_iPhone" bundle:nil];
        } else {
            newEditInventoryItemMasterViewController = [[VMNewEditInventoryItemMasterVC alloc] initWithNibName:@"VMNewEditInventoryItemMasterVC_iPad" bundle:nil];
        }
//        newEditInventoryItemMasterViewController.mainViewController = self.mainViewController;
        newEditInventoryItemMasterViewController.lastViewController = self;
        newEditInventoryItemMasterViewController.itemMasterToEdit = itemMaster;
        [self presentModalViewController:newEditInventoryItemMasterViewController animated:NO];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", e.reason);
    }
}

-(void)loadImportInventoryItemMasterListScreen {
    @try {
        // Load the new edit inventory item master controller
        VMImportInventoryItemMasterListVC *importInventoryItemMasterListViewController = nil;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            importInventoryItemMasterListViewController = [[VMImportInventoryItemMasterListVC alloc] initWithNibName:@"VMImportInventoryItemMasterListVC_iPhone" bundle:nil];
        } else {
            importInventoryItemMasterListViewController = [[VMImportInventoryItemMasterListVC alloc] initWithNibName:@"VMImportInventoryItemMasterListVC_iPad" bundle:nil];
        }
//        importInventoryItemMasterListViewController.mainViewController = self.mainViewController;
        importInventoryItemMasterListViewController.lastViewController = self;
        [self presentModalViewController:importInventoryItemMasterListViewController animated:NO];
    }
    @catch (NSException *e) {
        NSLog(@"An exception has occurred: %@", e.reason);
    }
}

-(void)lookupItem:(NSString *)scanValue {
    VMItemMaster *itemMaster = nil;
    
    // Step 1 - Try lookup by name
    itemMaster = [self lookupItemMasterByName:scanValue];
    
    // Step 2 - Try lookup by UPC/EAN
    if (itemMaster == nil)
        itemMaster = [self lookupItemMasterByUPC:scanValue];
    
    // Step 3 - If item master exists, load New/Edit screen; else ask user if they like to add a new one using scan value
    if (itemMaster)
        [self loadNewEditInventoryItemMasterScreen:itemMaster];
    else {
        NSString *alertMessage = [NSString stringWithFormat:@"Item Master not found. Would like to add '%@' as a new item master?", scanValue];
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Selection Not Found"
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"No"
                              otherButtonTitles:@"Yes",nil];    // value passed to override of alertView method
        [alert show];
    }
}

-(VMItemMaster *)lookupItemMasterByName:(NSString *)itemMasterName {
    VMItemMaster *itemMaster;
    @try {
        if ([self.itemMasterNamesArray containsObject:itemMasterName]) {
            NSUInteger index = [self.itemMasterNamesArray indexOfObject:itemMasterName];
            itemMaster = [self.itemMastersArray objectAtIndex:index];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
    return itemMaster;
}

-(VMItemMaster *)lookupItemMasterByUPC:(NSString *)itemMasterUPC {
    VMItemMaster *itemMaster;
    @try {
        if ([self.itemMasterUPCsArray containsObject:itemMasterUPC]) {
            NSUInteger index = [self.itemMasterUPCsArray indexOfObject:itemMasterUPC];
            itemMaster = [self.itemMastersArray objectAtIndex:index];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
    return itemMaster;
}

#pragma mark -
#pragma mark CRUD Methods
/****************************************************************************
 * CRUD Methods
 ****************************************************************************/

-(NSMutableArray *)deleteItemMasters {
//    NSMutableArray *deletedItemMasters;
    @autoreleasepool {
        @try {
            /*
            //NSArray *indexes = [itemMastersTableView indexPathsForSelectedRows];
            NSArray *indexes = [self getCheckedOrSelectedIndexes:self.itemMastersTableView];
            if (indexes.count > 0) {
                for (NSIndexPath *path in indexes) {
                    NSUInteger index = [path indexAtPosition:[path length] - 1];
                    VMItemMaster *itemMaster = [[self.itemMasterDictionariesArray objectAtIndex:index] objectForKey:@"object"];
                    [deletedItemMasters addObject:itemMaster.name];
                }
            }
             */
            [VMInventoryService deleteItemMasterByNames:self.deletedItemMasterNamesArray];
        }
        @catch (NSException *e) {
            NSLog(@"An exception has occurred: %@", e.reason);
        }
    }
//    return deletedItemMasters;
    return self.deletedItemMasterNamesArray;
}

#pragma mark -
#pragma mark VerbiBarcodeReaderDelegate Methods
/****************************************************************************
 * VerbiBarcodeReaderDelegate Methods
 ****************************************************************************/

-(void)handleBarcodeReaderReadEvent:(NSString *)barcode symbology:(NSString *)symbology event:(VMBarcodeReaderEvent *)event {
    [self lookupItem:barcode];
}

@end
