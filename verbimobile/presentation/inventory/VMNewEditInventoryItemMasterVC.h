//
//  VMNewEditInventoryItemMasterVC.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMAutoIDCapableVC.h"
#import "VMItemMaster.h"

@class VMAddInventoryItemsVC;

@interface VMNewEditInventoryItemMasterVC : VMAutoIDCapableVC <UIAlertViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate>

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@property(nonatomic, strong) VMAddInventoryItemsVC *addInventoryItemsController;

// Edit Properties
@property(nonatomic, strong) VMItemMaster *itemMasterToEdit;

// Scroll View Properties
@property(nonatomic, weak) IBOutlet UIScrollView *scrollView;

// Picker Properties
@property(nonatomic, weak) IBOutlet UIPickerView *typePickerView;
@property(nonatomic, weak) IBOutlet UIPickerView *uomPickerView;
@property(nonatomic, weak) IBOutlet UIPickerView *timeUnitPickerView;
//@property(nonatomic, strong) NSArray *typePickerArray;
//@property(nonatomic, strong) NSArray *uomPickerArray;
//@property(nonatomic, strong) NSArray *timeUnitPickerArray;

// Image View
@property(nonatomic, weak) IBOutlet UIImageView *itemMasterImageView;
@property(nonatomic, strong) UIPopoverController *popoverController;

// Buttons
@property(nonatomic, weak) IBOutlet UIButton *cancelButton;
@property(nonatomic, weak) IBOutlet UIButton *okButton;
@property(nonatomic, weak) IBOutlet UIButton *cameraButton;

// Text Field objects
@property(nonatomic, weak) IBOutlet UITextField *nameTextField;
@property(nonatomic, weak) IBOutlet UITextField *descriptionTextField;
@property(nonatomic, weak) IBOutlet UITextField *upcTextField;
@property(nonatomic, weak) IBOutlet UITextField *vendorTextField;
@property(nonatomic, weak) IBOutlet UITextField *vendorCodeTextField;
@property(nonatomic, weak) IBOutlet UITextField *reorderPointQuantityTextField;
@property(nonatomic, weak) IBOutlet UITextField *leadTimeDaysTextField;
@property(nonatomic, weak) IBOutlet UITextField *safetyStockQuantityTextField;
@property(nonatomic, weak) IBOutlet UITextField *optimumOrderingQuantityTextField;
@property(nonatomic, weak) IBOutlet UITextField *incrementQuantityTextField;
@property(nonatomic, weak) IBOutlet UITextField *priceTextField;
@property(nonatomic, weak) IBOutlet UISwitch *expiresSwitch;
@property(nonatomic, weak) IBOutlet UITextField *timePeriodTextField;
@property(nonatomic, weak) IBOutlet UISwitch *enabledSwitch;

@end
