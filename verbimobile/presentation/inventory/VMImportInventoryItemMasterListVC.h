//
//  VMImportInventoryItemMasterListVC.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMParentVC.h"

@interface VMImportInventoryItemMasterListVC : VMParentVC

// Text Field objects
@property (nonatomic, weak) IBOutlet UITextField *importFileField;

@end
