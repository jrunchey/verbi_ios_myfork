//
//  VMImportInventoryItemMasterVC.h
//  verbimobile
//
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "VMParentVC.h"

@interface VMImportInventoryItemMasterVC : VMParentVC

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@property (nonatomic, strong) NSURL *importFileURL;
@property (nonatomic, weak) IBOutlet UITextField *importFileField;

@end
