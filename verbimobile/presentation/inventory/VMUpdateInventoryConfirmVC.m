//
//  VMUpdateInventoryConfirmViewController.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMUpdateInventoryConfirmVC.h"
#import "VMUpdateInventorySummaryVC.h"
#import "VMUpdateInventoryConfirmWorkflowStep.h"
#import "VMAddInventoryItemsWorkflowStep.h"
#import "VMImportInventoryFileConfirmWorkflowStep.h"
#import "VMInventoryService.h"

@interface VMUpdateInventoryConfirmVC ()

@end

@implementation VMUpdateInventoryConfirmVC

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@synthesize itemsTableView = _itemsTableView;

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose {
    @try {
        [super dispose];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark View Lifecycle Public
/****************************************************************************
 * View Lifecycle Public
 ****************************************************************************/

- (void)initializeUIOnFirstLoad {
    @try {
        [super initializeUIOnFirstLoad];
        
        // Set workflow step
        if (self.workflow) {
            VMWorkflowStep *workflowStep = [[VMUpdateInventoryConfirmWorkflowStep alloc] init:self.workflow];
            if (workflowStep) {
                self.workflowStep = workflowStep;
                [self.workflow addWorkflowStep:self.workflowStep];
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIBeforeRendering {
    @try {
        [super initializeUIBeforeRendering];
        [self reloadTableView];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIAfterRendering {
    @try {
        [super initializeUIAfterRendering];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO methods
 ****************************************************************************/

- (IBAction)cancelPressed:(id)sender{
    [self loadMainViewController];
}

- (IBAction)fixEditPressed:(id)sender{
    [self loadLastViewController];
}

- (IBAction)acceptPressed:(id)sender{
    // Load the update inventory summary controller
    VMUpdateInventorySummaryVC *updateInventorySummaryViewController = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        updateInventorySummaryViewController = [[VMUpdateInventorySummaryVC alloc] initWithNibName:@"VMUpdateInventorySummaryVC_iPhone" bundle:nil];
    } else {
        updateInventorySummaryViewController = [[VMUpdateInventorySummaryVC alloc] initWithNibName:@"VMUpdateInventorySummaryVC_iPad" bundle:nil];
    }
//    updateInventorySummaryViewController.mainViewController = self.mainViewController;
    updateInventorySummaryViewController.lastViewController = self;
    updateInventorySummaryViewController.workflow = self.workflow;
    [self presentModalViewController:updateInventorySummaryViewController animated:NO];
    [self dispose];
}

#pragma mark -
#pragma mark Table view methods
/****************************************************************************
 * Table view methods
 ****************************************************************************/

// Defines the table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Defines the table - Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int count = 0;
    VMUpdateInventoryConfirmWorkflowStep *workflowStep = [self.workflow.workflowSteps objectForKey:WKFL_STEP_INV_UPDATE_INVENTORY_CONFIRM];
    if (workflowStep && workflowStep.itemsArray) {
        count = workflowStep.itemsArray.count;
    }
    return count;
}

// Fill the table - Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    VMUpdateInventoryConfirmWorkflowStep *workflowStep = [self.workflow.workflowSteps objectForKey:WKFL_STEP_INV_UPDATE_INVENTORY_CONFIRM];
    if (workflowStep && workflowStep.itemsArray && workflowStep.itemsArray.count > 0) {
        VMItem *item = [workflowStep.itemsArray objectAtIndex:indexPath.row];
        UIColor *color = nil;
        if (workflowStep.itemsColorArray && workflowStep.itemsColorArray.count > 0)
            color = [workflowStep.itemsColorArray objectAtIndex:indexPath.row];
        if (item.itemMaster == nil) {
            item.itemMaster = [VMInventoryService readItemMasterById:item.itemMasterGUID];
        }
        if (item && item.itemMaster) {
            // Cell Title Text
            
            // define the range you're interested in
            NSRange stringRange = {0, MIN([item.itemMaster.type length], 1)};
            // adjust the range to include dependent chars
            stringRange = [item.itemMaster.type rangeOfComposedCharacterSequencesForRange:stringRange];
            // Now you can create the short string
            NSString *typeSubstring = [[item.itemMaster.type uppercaseString] substringWithRange:stringRange];
            
            cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@", typeSubstring, item.itemMaster.name];
            if (workflowStep.itemsColorArray && workflowStep.itemsColorArray.count > 0)
                cell.textLabel.textColor = color;
            
            // Cell Subtitle Text
            
            NSMutableString *detailText = [[NSMutableString alloc] init];
            
            // Add quantity and value
            [detailText appendFormat:@"%1.2f ", item.quantity];
            if (item.lackQuantity > 0)
                [detailText appendFormat:@"(Expect %1.2f => -%1.2f) ", item.expectedQuantity, item.lackQuantity];
            if (item.excessQuantity > 0)
                [detailText appendFormat:@"(Expect %1.2f => +%1.2f) ", item.expectedQuantity, item.excessQuantity];
            [detailText appendFormat:@"| $%1.2f ", item.value];
            
            // Add vendor and/or vendor code
            if (item.itemMaster.vendor && [item.itemMaster.vendor length] > 0 && item.itemMaster.vendorCode && [item.itemMaster.vendorCode length] > 0) {
                [detailText appendFormat:@"| %@ | %@ ", item.itemMaster.vendor, item.itemMaster.vendorCode];
            }
            else if (item.itemMaster.vendor && [item.itemMaster.vendor length] > 0) {
                [detailText appendFormat:@"| %@ ", item.itemMaster.vendor];
            }
            else if (item.itemMaster.vendorCode && [item.itemMaster.vendorCode length] > 0) {
                [detailText appendFormat:@"| %@ ", item.itemMaster.vendorCode];
            }
            
            // Add description
            if (item.itemMaster.description && [item.itemMaster.description length] > 0) {
                [detailText appendFormat:@"| %@ ", item.itemMaster.description];
            }
            
            cell.detailTextLabel.text = detailText;
            if (workflowStep.itemsColorArray && workflowStep.itemsColorArray.count > 0)
                cell.detailTextLabel.textColor = color;
        }
    }
    
    // Convert all row selections to check marks
    NSIndexPath *selection = [tableView indexPathForSelectedRow];
    if (selection && selection.row == indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        // uncheck
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else {
        // check
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
}

 - (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
 {
 return @"Items";
 }

/*
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger) section
{
    //section text as a label
    UILabel *lbl = [[UILabel alloc] init];
    lbl.textAlignment = UITextAlignmentLeft;
    
    lbl.text = @"Items";
    [lbl setBackgroundColor:[UIColor clearColor]];
    
    return lbl;
}
 */

-(void)reloadTableView {
    [self reloadItemsArray];
    [self.itemsTableView reloadData];
}

-(void)reloadItemsArray {
    @autoreleasepool {
        @try {
            // Determine inventory workflow type (1 - New, 2 - Import, 3 - Audit)
            int inventoryWorkflowType = 1;  // assume new inventory workflow
            if ([[self.workflow.type lowercaseString] caseInsensitiveCompare:WKFL_INV_IMPORT_INVENTORY] == NSOrderedSame)
                inventoryWorkflowType = 2;
            else if ([[self.workflow.type lowercaseString] caseInsensitiveCompare:WKFL_INV_AUDIT_INVENTORY] == NSOrderedSame)
                inventoryWorkflowType = 3;

            VMAddInventoryItemsWorkflowStep *addInventoryItemsWorkflowStep = [self.workflow.workflowSteps objectForKey:WKFL_STEP_INV_ADD_INVENTORY_ITEMS];
            NSMutableArray *actualItems = addInventoryItemsWorkflowStep.itemsArray;
            
            VMUpdateInventoryConfirmWorkflowStep *updateInventoryConfirmWorkflowStep = [self.workflow.workflowSteps objectForKey:WKFL_STEP_INV_UPDATE_INVENTORY_CONFIRM];
            if (updateInventoryConfirmWorkflowStep.itemsArray == nil)
                updateInventoryConfirmWorkflowStep.itemsArray = [[NSMutableArray alloc] init];
            if (updateInventoryConfirmWorkflowStep.itemsColorArray == nil)
                updateInventoryConfirmWorkflowStep.itemsColorArray = [[NSMutableArray alloc] init];
            // Clear items array to allow for reload
            if (updateInventoryConfirmWorkflowStep.itemsArray && updateInventoryConfirmWorkflowStep.itemsArray.count > 0) {
                [updateInventoryConfirmWorkflowStep.itemsArray removeAllObjects];
                [updateInventoryConfirmWorkflowStep.itemsColorArray removeAllObjects];
            }
            
            // If workflow is New Inventory, then the actual items collected in
            // Add Inventory Workflow Step become the expected items in
            // Update Inventory Workflow Step. There is no comparison.
            if (inventoryWorkflowType == 1) {
                [updateInventoryConfirmWorkflowStep.itemsArray addObjectsFromArray:actualItems];
            }
            // If workflow is Import Inventory, then the actual items collected in
            // Add Inventory Workflow Step are compared to the expected items in
            // Import Inventory Confirm Workflow Step.
            else if (inventoryWorkflowType == 2) {
                NSMutableArray *expectedItems = updateInventoryConfirmWorkflowStep.itemsArray;
                [self compareActualToExpectedItems:inventoryWorkflowType actualItems:actualItems expectedItems:expectedItems];
            }
            // If workflow is Audit Inventory, then the actual items collected in
            // Add Inventory Workflow Step are compared to the expected items in
            // the database.
            else if (inventoryWorkflowType == 3) {
                NSMutableArray *totalExpectedItems = [[NSMutableArray alloc] init];
                for (VMItem *actualItem in actualItems) {
                    NSMutableArray *expectedItems = [VMInventoryService readItems:actualItem.itemMasterGUID locationId:actualItem.locationGUID status:actualItem.status];
                    if (expectedItems && expectedItems.count > 0) {
                        [totalExpectedItems addObjectsFromArray:expectedItems];
                    }
                }
                [self compareActualToExpectedItems:inventoryWorkflowType actualItems:actualItems expectedItems:totalExpectedItems];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"An exception occured: %@", [exception reason]);
        }
    }
}

#pragma mark -
#pragma mark Helper methods
/****************************************************************************
 * Helper methods
 ****************************************************************************/

/*
 * Compares the actual items collected in the AddInventoryItemsWorkflowStep
 * with the expected items collected in the ImportInventoryItemsWorkflowStep
 * (if workflow is an VMImportInventoryWorkflow) or in the database (if
 * workflow is VMAuditInventoryWorkflow).
 */
- (void)compareActualToExpectedItems:(int)inventoryWorkflowType actualItems:(NSMutableArray *)actualItems expectedItems:(NSMutableArray *)expectedItems {
    if (actualItems && actualItems.count > 0) {
        @autoreleasepool {
            VMUpdateInventoryConfirmWorkflowStep *updateInventoryConfirmWorkflowStep = [self.workflow.workflowSteps objectForKey:WKFL_STEP_INV_UPDATE_INVENTORY_CONFIRM];
            
            NSMutableArray *expectedItemsFoundInActual = [[NSMutableArray alloc] init];
            NSString *locationGUID = nil;
            
            // Add all expected items found in actual items with comparison colors where red means less or lack, orange means more or excess and black means matching
            for (VMItem *actualItem in actualItems) {
                if (locationGUID == nil)
                    locationGUID = actualItem.locationGUID;
                
                // reset compare properties since we will recalculate
                actualItem.expectedQuantity = actualItem.quantity;
                actualItem.lackQuantity = 0;
                actualItem.excessQuantity = 0;
                
                BOOL expectedFoundInActual = NO;
                if (expectedItems && expectedItems.count > 0) {
                    for (VMItem *expectedItem in expectedItems) {
                        if (expectedItem.itemMasterGUID && actualItem.itemMasterGUID && [[expectedItem.itemMasterGUID lowercaseString] caseInsensitiveCompare:actualItem.itemMasterGUID] == NSOrderedSame) {
                            expectedFoundInActual = YES;
                            actualItem.expectedQuantity = expectedItem.quantity;
                            [expectedItemsFoundInActual addObject:actualItem];
                            break;
                        }
                    }
                }
                
                if (!expectedFoundInActual) {
                    // If there are no expected items at all, then actual item must be in excess
                    if (expectedItems == nil || expectedItems.count == 0) {
                        actualItem.expectedQuantity = 0;
                        actualItem.excessQuantity = actualItem.quantity - actualItem.expectedQuantity;
                        [updateInventoryConfirmWorkflowStep.itemsColorArray addObject:[UIColor orangeColor]];
                    }
                    // If there are expected items but expected not found in actuals, then this actual item must be in lack
                    else {
                        actualItem.lackQuantity = actualItem.expectedQuantity;
                        [updateInventoryConfirmWorkflowStep.itemsColorArray addObject:[UIColor redColor]];
                    }
                    
                }
                else if (expectedFoundInActual && actualItem.quantity >= 0) {
                    // Lack
                    if (actualItem.quantity < actualItem.expectedQuantity) {
                        actualItem.lackQuantity = actualItem.expectedQuantity - actualItem.quantity;
                        [updateInventoryConfirmWorkflowStep.itemsColorArray addObject:[UIColor redColor]];
                    }
                    // Excess
                    else if (actualItem.quantity > actualItem.expectedQuantity) {
                        actualItem.excessQuantity = actualItem.quantity - actualItem.expectedQuantity;
                        [updateInventoryConfirmWorkflowStep.itemsColorArray addObject:[UIColor orangeColor]];
                    }
                    // Matching
                    else
                        [updateInventoryConfirmWorkflowStep.itemsColorArray addObject:[UIColor blackColor]];
                }
                [updateInventoryConfirmWorkflowStep.itemsArray addObject:actualItem];
            }
            
            // Add missing expected items not found in actual by looping thru all expected items with compare to those actually found and add zero quantity with red color for those expected item that were not found in actual items
            NSMutableArray *expectedItemsNotFoundInActual = [[NSMutableArray alloc] init];
            NSMutableArray *allExpectedItemsAtThisLocation = [VMInventoryService readItemsByLocationId:locationGUID];
            if (allExpectedItemsAtThisLocation && allExpectedItemsAtThisLocation.count > 0) {
                // Gather list of expected items that are not found in actual
                for (VMItem *expectedItemAtThisLocation in allExpectedItemsAtThisLocation) {
                    BOOL expectedFoundInActual = NO;
                    for (VMItem *expectedItemFoundInActual in expectedItemsFoundInActual) {
                        // Compare item masters and statuses
                        if (expectedItemFoundInActual.itemMasterGUID && expectedItemAtThisLocation.itemMasterGUID && [[expectedItemFoundInActual.itemMasterGUID lowercaseString] caseInsensitiveCompare:expectedItemAtThisLocation.itemMasterGUID] == NSOrderedSame && expectedItemFoundInActual.status && expectedItemAtThisLocation.status && [[expectedItemFoundInActual.status lowercaseString] caseInsensitiveCompare:expectedItemAtThisLocation.status] == NSOrderedSame) {
                            expectedFoundInActual = YES;
                            break;
                        }
                    }
                    if (!expectedFoundInActual)
                        [expectedItemsNotFoundInActual addObject:expectedItemAtThisLocation];
                }
            }
            for (VMItem *expectedItemNotFoundInActual in expectedItemsNotFoundInActual) {
                expectedItemNotFoundInActual.quantity = 0;
                if (expectedItemNotFoundInActual.quantity == expectedItemNotFoundInActual.expectedQuantity) {
                    [updateInventoryConfirmWorkflowStep.itemsColorArray addObject:[UIColor blackColor]];
                }
                else {
                    expectedItemNotFoundInActual.lackQuantity = expectedItemNotFoundInActual.expectedQuantity;
                    [updateInventoryConfirmWorkflowStep.itemsColorArray addObject:[UIColor redColor]];
                }
                [updateInventoryConfirmWorkflowStep.itemsArray addObject:expectedItemNotFoundInActual];
            }
            
            // Cleanup
            [expectedItemsFoundInActual removeAllObjects];
            expectedItemsFoundInActual = nil;
            [expectedItemsNotFoundInActual removeAllObjects];
            expectedItemsNotFoundInActual = nil;
            [allExpectedItemsAtThisLocation removeAllObjects];
            allExpectedItemsAtThisLocation = nil;
            locationGUID = nil;
        }
    }
}

@end
