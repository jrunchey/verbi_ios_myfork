//
//  VMInventoryItemMasterListVC.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMAutoIDCapableVC.h"
#import "VMNewEditInventoryItemVC.h"

// Identifier so we know what UITextField the user pressed return in
#define kSearchFieldTag 1

@interface VMInventoryItemMasterListVC : VMAutoIDCapableVC <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate, UITextFieldDelegate>

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@property(nonatomic, strong) VMNewEditInventoryItemVC *inventoryItemController;

@property(nonatomic, weak) IBOutlet UIButton *addButton;
@property(nonatomic, weak) IBOutlet UIButton *deleteButton;
@property(nonatomic, weak) IBOutlet UIButton *editButton;
@property(nonatomic, weak) IBOutlet UIButton *selectAllButton;
@property(nonatomic, weak) IBOutlet UITextField *searchTextField;
@property(nonatomic, weak) IBOutlet UIButton *clearButton;
@property(nonatomic, weak) IBOutlet UITableView *itemMastersTableView;

@end
