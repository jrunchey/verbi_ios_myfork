//
//  VMImportInventoryViewController.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMImportInventoryVC.h"
#import "VMImportInventoryConfirmVC.h"
#import "VMImportInventoryFileWorkflowStep.h"
#import "VMLocation.h"

@interface VMImportInventoryVC ()

@end

@implementation VMImportInventoryVC

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@synthesize importFileURL = _importFileURL;
@synthesize importFileField = _importFileField;

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose {
    @try {
        self.importFileField = nil;
        
        [super dispose];
        
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark View Lifecycle Public
/****************************************************************************
 * View Lifecycle Public
 ****************************************************************************/

- (void)initializeUIOnFirstLoad {
    @try {
        [super initializeUIOnFirstLoad];
        
        // Set workflow step
        if (self.workflow) {
            VMWorkflowStep *workflowStep = [[VMImportInventoryFileWorkflowStep alloc] init:self.workflow];
            if (workflowStep) {
                self.workflowStep = workflowStep;
                [self.workflow addWorkflowStep:self.workflowStep];
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIBeforeRendering {
    @try {
        [super initializeUIBeforeRendering];
        self.activityIndicatorView.hidden = YES;
        if (self.importFileURL && self.importFileField) {
            self.importFileField.text = self.importFileURL.absoluteString;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIAfterRendering {
    @try {
        [super initializeUIAfterRendering];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO methods
 ****************************************************************************/

- (IBAction)cancelPressed:(id)sender{
    [self loadMainViewController];
}

- (IBAction)nextPressed:(id)sender{
    /*
    self.activityIndicatorView.hidden = NO;
    [self.activityIndicatorView startAnimating];
    @autoreleasepool {
        @try {
            NSError *outError;
            NSString *importFileContents = nil;
            NSURL *url = nil;
            if (self.importFileURL)
                url = self.importFileURL;
            if (url == nil && self.importFileField.text)
                url = [NSURL fileURLWithPath:self.importFileField.text];
            if (url) {
                importFileContents = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&outError];
                //NSLog(importFileContents);
                
                if (importFileContents && self.workflowStep) {
                    ((VMImportInventoryFileWorkflowStep *)self.workflowStep).itemsArray = [self loadInventoryItemsFromCSV:[importFileContents csvRows]];
                    [self loadImportInventoryConfirmScreen];
                }
                else {
                    NSString *alertViewTitle;
                    alertViewTitle = @"Import - Error";
                    NSString *alertMessage = @"Error importing file";
                    UIAlertView *alert = [[UIAlertView alloc]
                                          initWithTitle: alertViewTitle
                                          message: alertMessage
                                          delegate: self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
                    [alert show];
                }
            }
            else {
                NSString *alertViewTitle;
                alertViewTitle = @"Import";
                NSString *alertMessage = @"Browse for a file.";
                UIAlertView *alert = [[UIAlertView alloc]
                                      initWithTitle: alertViewTitle
                                      message: alertMessage
                                      delegate: self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
                [alert show];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"An exception occured: %@", [exception reason]);
        }
    }
     */
}

- (IBAction)browsePressed:(UITextView *)dataStr{
    @autoreleasepool {        
        /***
        
        //CREATE DIRECTORY
        NSFileManager *fileManager;
        
        fileManager = [NSFileManager defaultManager];
        
        NSURL *newDir = [NSURL fileURLWithPath:@"/Users/mhotaling/Desktop/VerbiMobile/Data"];
        [fileManager createDirectoryAtURL: newDir withIntermediateDirectories:YES attributes: nil error:nil];
        
        
        //WRITE FILE
        NSString *filePath = @"/Users/mhotaling/Desktop/VerbiMobile/Data/test123456.txt";
        //Uncomment the next three lines to save file to iPhone simulator directory
        //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        //NSString *documentsDirectory = [paths objectAtIndex:0];
        //NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"verbiData.data"];
        NSLog(@"filePath %@", filePath);
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) { // if file is not exist, create it.
            //NSString *dataStr = @"Pepperoni, Cheese, Mushroom";
            NSString *dataStr = self.importFileField.text;
            NSError *error;
            [dataStr writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
            
            //[importFileField setText:dataStr];
        }
        else {
            //NSString *dataStr = importFileField.text;
            //[importFileField setText:dataStr];
        }
        
        if ([[NSFileManager defaultManager] isWritableFileAtPath:filePath]) {
            NSLog(@"Writing to verbiData.data...");
        }else {
            NSLog(@"verbiData.data not found!");
        }

        
        //Files in the "Resource folder" are actually the contents of your application bundle
        NSString* path = [[NSBundle mainBundle] pathForResource:@"filename" 
                                                         ofType:@"txt"];
        //Load the Content into a string
        NSString* content = [NSString stringWithContentsOfFile:path
                                                      encoding:NSUTF8StringEncoding
                                                         error:NULL];
        
        
        ****/
        
        /*
        NSString *theText = @"Pepperoni, Cheese, Mushroom";
        [[NSUserDefaults standardUserDefaults] setObject:theText forKey:@"/Users/mhotaling/Desktop/VerbiMobile/Data/v123.txt"];
        //return [[NSUserDefaults standardUserDefaults] stringForKey:@"/Users/mhotaling/Desktop/VerbiMobile/Data/test123456.txt"];
         */
        
        
        //-- Open File Dialog Issue: NSOpenPanel Doesn't work with IOS
        /*
        int i; // Loop counter.
        
        // Create the File Open Dialog class.
        NSOpenPanel* openDlg = [NSOpenPanel openPanel];
        
        // Enable the selection of files in the dialog.
        [openDlg setCanChooseFiles:YES];
        
        // Enable the selection of directories in the dialog.
        [openDlg setCanChooseDirectories:YES];
        
        // Display the dialog.  If the OK button was pressed,
        // process the files.
        if ( [openDlg runModalForDirectory:nil file:nil] == NSOKButton )
        {
            // Get an array containing the full filenames of all
            // files and directories selected.
            NSArray* files = [openDlg filenames];
            
            // Loop through all the files and process them.
            for( i = 0; i < [files count]; i++ )
            {
                NSString* fileName = [files objectAtIndex:i];
                
                // Do something with the filename.
            }
        }
        */
        
    }
}

-(NSMutableArray *) loadInventoryItemsFromCSV:(NSArray *)csvArray {
    NSMutableArray *inventoryItems = nil;
    BOOL validFile = NO;
    BOOL loadByVendorCode = NO;
    BOOL loadByName = NO;
    BOOL loadByDescription = NO;
    int indexForQuantityHeader = -1;
    int indexForVendorCodeHeader = -1;
    int indexForNameHeader = -1;
    int indexForDescriptionHeader = -1;
    
    if (csvArray && csvArray.count > 0) {
        @autoreleasepool {
            int i = 0;
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
            [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
            
            inventoryItems = [[NSMutableArray alloc] init];
            for (NSArray *row in csvArray) {
                //1st row is a header - skip
                if (i == 0) {
                    for (int h = 0; h < [row count]; h++) {
                        NSString *header = [row objectAtIndex:h];
                        if ([[header lowercaseString] caseInsensitiveCompare:@"VendorCode"] == NSOrderedSame || [[header lowercaseString] caseInsensitiveCompare:@"Vendor Code"] == NSOrderedSame) {
                            loadByVendorCode = YES;
                            indexForVendorCodeHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Name"] == NSOrderedSame || [[header lowercaseString] caseInsensitiveCompare:@"Name"] == NSOrderedSame) {
                            loadByName = YES;
                            indexForNameHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Description"] == NSOrderedSame || [[header lowercaseString] caseInsensitiveCompare:@"Description"] == NSOrderedSame) {
                            loadByDescription = YES;
                            indexForDescriptionHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Quantity"] == NSOrderedSame || [[header lowercaseString] caseInsensitiveCompare:@"Qty"] == NSOrderedSame) {
                            indexForQuantityHeader = h;
                        }
                        // File is valid if there a quantity field and at least one of the following:
                        // Vendor Code, Name or Description
                        if (indexForQuantityHeader >= 0 && (indexForVendorCodeHeader >= 0 || indexForNameHeader >= 0 || indexForDescriptionHeader >= 0))
                        validFile = YES;
                    }
                }
                
                if (validFile && i > 0) {
                    VMItemMaster *itemMaster = nil;
                    NSString *lookupField = nil;
                    if (itemMaster == nil && loadByVendorCode) {
                        lookupField = [row objectAtIndex:indexForVendorCodeHeader];
                        itemMaster = [VMInventoryService readItemMasterByVendorCode:lookupField];
                    }
                    if (itemMaster == nil && loadByName) {
                        lookupField = [row objectAtIndex:indexForNameHeader];
                        itemMaster = [VMInventoryService readItemMasterByName:lookupField];
                    }
                    if (itemMaster == nil && indexForDescriptionHeader) {
                        lookupField = [row objectAtIndex:indexForNameHeader];
                        itemMaster = [VMInventoryService readItemMasterByDescription:lookupField];
                    }
                    
                    NSNumber *quantity = [numberFormatter numberFromString:[row objectAtIndex:indexForQuantityHeader]];
                
                    if (itemMaster) {
                        VMItem *item = [[VMItem alloc] init:nil itemMaster:itemMaster location:nil quantity:[quantity floatValue]];
                        if (item) {
                            item.value = itemMaster.price;
                            [inventoryItems addObject:item];
                        }
                    }
                }
                i++;
            }
        }
    }
    
    return inventoryItems;
}

#pragma mark -
#pragma mark Navigation Methods
/****************************************************************************
 * Navigation Methods
 ****************************************************************************/

-(void)loadImportInventoryConfirmScreen {
    // Load the import inventory confirm controller
    VMImportInventoryConfirmVC *importInventoryConfirmViewController = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        importInventoryConfirmViewController = [[VMImportInventoryConfirmVC alloc] initWithNibName:@"VMImportInventoryConfirmVC_iPhone" bundle:nil];
    } else {
        importInventoryConfirmViewController = [[VMImportInventoryConfirmVC alloc] initWithNibName:@"VMImportInventoryConfirmVC_iPad" bundle:nil];
    }
//    importInventoryConfirmViewController.mainViewController = self.mainViewController;
    importInventoryConfirmViewController.lastViewController = self;
    importInventoryConfirmViewController.workflow = self.workflow;
    [self presentModalViewController:importInventoryConfirmViewController animated:NO];
    [self dispose];
}

@end
