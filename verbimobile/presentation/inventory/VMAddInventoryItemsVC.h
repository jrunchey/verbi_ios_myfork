//
//  VMAddInventoryItemsViewController.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMAutoIDCapableVC.h"

@interface VMAddInventoryItemsVC : VMAutoIDCapableVC <UITableViewDelegate, UITableViewDataSource,UIAlertViewDelegate>

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@property(nonatomic, weak) IBOutlet UIButton *cancelButton;
@property(nonatomic, weak) IBOutlet UIButton *finishButton;
@property(nonatomic, weak) IBOutlet UIButton *selectAllButton;
@property(nonatomic, weak) IBOutlet UIButton *editButton;
@property(nonatomic, weak) IBOutlet UIButton *removeButton;
@property(nonatomic, weak) IBOutlet UIButton *addButton;
@property(nonatomic, weak) IBOutlet UITextField *locationTextField;
@property(nonatomic, weak) IBOutlet UITableView *itemsTableView;

@end
