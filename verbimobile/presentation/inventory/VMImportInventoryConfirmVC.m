//
//  VMImportInventoryConfirmViewController.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMImportInventoryConfirmVC.h"
#import "VMSelectInventoryLocationVC.h"
#import "VMImportInventoryFileWorkflowStep.h"
#import "VMImportInventoryFileConfirmWorkflowStep.h"
#import "VMItem.h"

@interface VMImportInventoryConfirmVC ()

@end

@implementation VMImportInventoryConfirmVC

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@synthesize importFileURL = _importFileURL;
@synthesize itemsTableView = _itemsTableView;

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose {
    @try {
        self.itemsTableView = nil;
        
        [super dispose];
        
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark View Lifecycle Public
/****************************************************************************
 * View Lifecycle Public
 ****************************************************************************/

- (void)initializeUIOnFirstLoad {
    @try {
        [super initializeUIOnFirstLoad];
        
        // Set workflow step
        if (self.workflow) {
            VMWorkflowStep *workflowStep = [[VMImportInventoryFileConfirmWorkflowStep alloc] init:self.workflow];
            if (workflowStep) {
                self.workflowStep = workflowStep;
                [self.workflow addWorkflowStep:self.workflowStep];
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIBeforeRendering {
    @try {
        [super initializeUIBeforeRendering];
        self.activityIndicatorView.hidden = YES;
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIAfterRendering {
    @try {
        [super initializeUIAfterRendering];
        self.activityIndicatorView.hidden = NO;
        [self.activityIndicatorView startAnimating];

        @autoreleasepool {
            @try {
                NSError *outError;
                NSString *importFileContents = nil;
                if (self.importFileURL) {
                    importFileContents = [NSString stringWithContentsOfURL:self.importFileURL encoding:NSUTF8StringEncoding error:&outError];
                    //NSLog(importFileContents);
                    
                    if (importFileContents && self.workflowStep) {
                        ((VMImportInventoryFileConfirmWorkflowStep *)self.workflowStep).itemsArray = [self loadInventoryItemsFromCSV:[importFileContents csvRows]];
                        [self reloadTableView];
                    }
                    else {
                        NSString *alertViewTitle;
                        alertViewTitle = @"Import - Error";
                        NSString *alertMessage = @"Error importing file";
                        UIAlertView *alert = [[UIAlertView alloc]
                                              initWithTitle: alertViewTitle
                                              message: alertMessage
                                              delegate: self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
                        [alert show];
                    }
                }
                else {
                    NSString *alertViewTitle;
                    alertViewTitle = @"Import - Error";
                    NSString *alertMessage = @"Import failed. A file was not specified.";
                    UIAlertView *alert = [[UIAlertView alloc]
                                          initWithTitle: alertViewTitle
                                          message: alertMessage
                                          delegate: self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
                    [alert show];
                }
            }
            @catch (NSException *exception) {
                NSLog(@"An exception occured: %@", [exception reason]);
            }
        }
        
        [self.activityIndicatorView stopAnimating];
        self.activityIndicatorView.hidden = YES;
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO methods
 ****************************************************************************/

- (IBAction)cancelPressed:(id)sender{
    [self loadMainViewController];
}

- (IBAction)verifyPressed:(id)sender{
    @autoreleasepool {
        // Load the select an inventory location controller
        VMSelectInventoryLocationVC *selectInventoryLocationViewController = nil;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            selectInventoryLocationViewController = [[VMSelectInventoryLocationVC alloc] initWithNibName:@"VMSelectInventoryLocationVC_iPhone" bundle:nil];
        } else {
            selectInventoryLocationViewController = [[VMSelectInventoryLocationVC alloc] initWithNibName:@"VMSelectInventoryLocationVC_iPad" bundle:nil];
        }
//        selectInventoryLocationViewController.mainViewController = self.mainViewController;
        selectInventoryLocationViewController.lastViewController = self;
        selectInventoryLocationViewController.workflow = self.workflow;
        [self presentModalViewController:selectInventoryLocationViewController animated:NO];
    }
}

-(NSMutableArray *) loadInventoryItemsFromCSV:(NSArray *)csvArray {
    NSMutableArray *inventoryItems = nil;
    BOOL validFile = NO;
    BOOL loadByVendorCode = NO;
    BOOL loadByName = NO;
    BOOL loadByDescription = NO;
    int indexForQuantityHeader = -1;
    int indexForVendorCodeHeader = -1;
    int indexForNameHeader = -1;
    int indexForDescriptionHeader = -1;
    
    if (csvArray && csvArray.count > 0) {
        @autoreleasepool {
            int i = 0;
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
            [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
            
            inventoryItems = [[NSMutableArray alloc] init];
            for (NSArray *row in csvArray) {
                //1st row is a header - skip
                if (i == 0) {
                    for (int h = 0; h < [row count]; h++) {
                        NSString *header = [row objectAtIndex:h];
                        if ([[header lowercaseString] caseInsensitiveCompare:@"VendorCode"] == NSOrderedSame || [[header lowercaseString] caseInsensitiveCompare:@"Vendor Code"] == NSOrderedSame) {
                            loadByVendorCode = YES;
                            indexForVendorCodeHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Name"] == NSOrderedSame || [[header lowercaseString] caseInsensitiveCompare:@"Name"] == NSOrderedSame) {
                            loadByName = YES;
                            indexForNameHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Description"] == NSOrderedSame || [[header lowercaseString] caseInsensitiveCompare:@"Description"] == NSOrderedSame) {
                            loadByDescription = YES;
                            indexForDescriptionHeader = h;
                        }
                        else if ([[header lowercaseString] caseInsensitiveCompare:@"Quantity"] == NSOrderedSame || [[header lowercaseString] caseInsensitiveCompare:@"Qty"] == NSOrderedSame) {
                            indexForQuantityHeader = h;
                        }
                        // File is valid if there a quantity field and at least one of the following:
                        // Vendor Code, Name or Description
                        if (indexForQuantityHeader >= 0 && (indexForVendorCodeHeader >= 0 || indexForNameHeader >= 0 || indexForDescriptionHeader >= 0))
                            validFile = YES;
                    }
                }
                
                if (validFile && i > 0) {
                    VMItemMaster *itemMaster = nil;
                    NSString *lookupField = nil;
                    if (itemMaster == nil && loadByVendorCode) {
                        lookupField = [row objectAtIndex:indexForVendorCodeHeader];
                        itemMaster = [VMInventoryService readItemMasterByVendorCode:lookupField];
                    }
                    if (itemMaster == nil && loadByName) {
                        lookupField = [row objectAtIndex:indexForNameHeader];
                        itemMaster = [VMInventoryService readItemMasterByName:lookupField];
                    }
                    if (itemMaster == nil && indexForDescriptionHeader) {
                        lookupField = [row objectAtIndex:indexForNameHeader];
                        itemMaster = [VMInventoryService readItemMasterByDescription:lookupField];
                    }
                    
                    NSNumber *quantity = [numberFormatter numberFromString:[row objectAtIndex:indexForQuantityHeader]];
                    
                    if (itemMaster) {
                        VMItem *item = [[VMItem alloc] init:nil itemMaster:itemMaster location:nil quantity:[quantity floatValue]];
                        if (item) {
                            item.value = itemMaster.price;
                            [inventoryItems addObject:item];
                        }
                    }
                }
                i++;
            }
        }
    }
    
    return inventoryItems;
}

#pragma mark -
#pragma mark Table view methods
/****************************************************************************
 * Table view methods
 ****************************************************************************/

// Defines the table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Defines the table - Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int count = 0;
    VMImportInventoryFileConfirmWorkflowStep *workflowStep = [self.workflow.workflowSteps objectForKey:WKFL_STEP_INV_IMPORT_INVENTORY_FILE_CONFIRM];
    if (workflowStep && workflowStep.itemsArray) {
        count = workflowStep.itemsArray.count;
    }
    return count;
}

// Fill the table - Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    @autoreleasepool {
        VMImportInventoryFileConfirmWorkflowStep *workflowStep = [self.workflow.workflowSteps objectForKey:WKFL_STEP_INV_IMPORT_INVENTORY_FILE_CONFIRM];
        if (workflowStep && workflowStep.itemsArray && workflowStep.itemsArray.count > 0) {
            VMItem *item = [workflowStep.itemsArray objectAtIndex:indexPath.row];
            if (item && item.itemMaster) {
                // Cell Title Text
                
                // define the range you're interested in
                NSRange stringRange = {0, MIN([item.itemMaster.type length], 1)};
                // adjust the range to include dependent chars
                stringRange = [item.itemMaster.type rangeOfComposedCharacterSequencesForRange:stringRange];
                // Now you can create the short string
                NSString *typeSubstring = [[item.itemMaster.type uppercaseString] substringWithRange:stringRange];
                
                cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@", typeSubstring, item.itemMaster.name];
                
                // Cell Subtitle Text
                
                NSMutableString *detailText = [[NSMutableString alloc] init];
                if (item.itemMaster.vendor && item.itemMaster.vendorCode) {
                    [detailText appendFormat:@"%@ | %@ | ", item.itemMaster.vendor, item.itemMaster.vendorCode];
                }
                else if (item.itemMaster.vendor) {
                    [detailText appendFormat:@"%@ | ", item.itemMaster.vendor];
                }
                else if (item.itemMaster.vendorCode) {
                    [detailText appendFormat:@"%@ | ", item.itemMaster.vendorCode];
                }
                [detailText appendFormat:@"%1.2f | %1.2f", item.quantity, item.value];
                cell.detailTextLabel.text = detailText;
            }
        }
    }
        
    // Convert all row selections to check marks
    NSIndexPath *selection = [tableView indexPathForSelectedRow];
    if (selection && selection.row == indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        // uncheck
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else {
        // check
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
}

/*
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
return @"Items";
}
 
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger) section
{
    //section text as a label
    UILabel *lbl = [[UILabel alloc] init];
    lbl.textAlignment = UITextAlignmentLeft;
    
    lbl.text = @"Items";
    [lbl setBackgroundColor:[UIColor clearColor]];
    
    return lbl;
}
 */

-(void)reloadTableView {
    [self reloadItemsArray];
    [self.itemsTableView reloadData];
}

-(void)reloadItemsArray {
    @try {
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

@end
