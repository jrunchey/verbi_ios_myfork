//
//  VMSelectInventoryLocationViewController.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMAutoIDCapableVC.h"

// Identifier so we know what UITextField the user pressed return in
#define kSearchFieldTag 1

@interface VMSelectInventoryLocationVC : VMAutoIDCapableVC <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate, UITextFieldDelegate>

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@property(nonatomic, weak) IBOutlet UIButton *cancelButton;
@property(nonatomic, weak) IBOutlet UIButton *nextButton;
@property(nonatomic, weak) IBOutlet UITextField *searchTextField;
@property(nonatomic, weak) IBOutlet UIButton *clearButton;
@property(nonatomic, weak) IBOutlet UITableView *locationsTableView;


@end
