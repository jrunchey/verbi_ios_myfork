//
//  VMUpdateInventorySummaryViewController.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMUpdateInventorySummaryVC.h"
#import "VMUpdateInventorySummaryWorkflowStep.h"
#import "VMSelectInventoryLocationWorkflowStep.h"
#import "VMUpdateInventoryConfirmWorkflowStep.h"
#import "VMSession.h"
#import "VMItemMaster.h"
#import "VMItem.h"
#import "VMInventoryService.h"

@interface VMUpdateInventorySummaryVC ()

@end

@implementation VMUpdateInventorySummaryVC

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@synthesize itemsTableView = _itemsTableView;

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose {
    @try {
        [super dispose];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark View Lifecycle Public
/****************************************************************************
 * View Lifecycle Public
 ****************************************************************************/

- (void)initializeUIOnFirstLoad {
    @try {
        [super initializeUIOnFirstLoad];
        
        // Set workflow step
        if (self.workflow) {
            VMWorkflowStep *workflowStep = [[VMUpdateInventorySummaryWorkflowStep alloc] init:self.workflow];
            if (workflowStep) {
                self.workflowStep = workflowStep;
                [self.workflow addWorkflowStep:self.workflowStep];
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIBeforeRendering {
    @try {
        [super initializeUIBeforeRendering];
        
        [self reloadTableView];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)initializeUIAfterRendering {
    @try {
        [super initializeUIAfterRendering];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO methods
 ****************************************************************************/

- (IBAction)okPressed:(id)sender{
    // PERSIST WORKFLOW
    [self saveItems];
    
    // SEND COMPARE REPORT TO LOGGED-IN USER'S EMAIL ADDRESS
    NSString *alertMessage = [NSString stringWithFormat:@"Would you like to email the report?"];
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: @"Confirm Email"
                          message: alertMessage
                          delegate: self
                          cancelButtonTitle:@"No"
                          otherButtonTitles:@"Yes",nil];    // value passed to override of alertView method
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		[self loadMainViewController];
	}
	else {
        if ([[alertView.title lowercaseString] caseInsensitiveCompare:@"Confirm Email"] == NSOrderedSame) {
            NSDate *date = [NSDate date];
            [self createEmailReport:[VMSession sharedInstance].user date:date];
        }
	}
}

#pragma mark -
#pragma mark Table view methods
/****************************************************************************
 * Table view methods
 ****************************************************************************/

// Defines the table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Defines the table - Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int count = 0;
    VMUpdateInventoryConfirmWorkflowStep *workflowStep = [self.workflow.workflowSteps objectForKey:WKFL_STEP_INV_UPDATE_INVENTORY_CONFIRM];
    if (workflowStep && workflowStep.itemsArray) {
        count = workflowStep.itemsArray.count;
    }
    return count;
}

// Fill the table - Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    VMUpdateInventoryConfirmWorkflowStep *workflowStep = [self.workflow.workflowSteps objectForKey:WKFL_STEP_INV_UPDATE_INVENTORY_CONFIRM];
    if (workflowStep && workflowStep.itemsArray && workflowStep.itemsArray.count > 0) {
        VMItem *item = [workflowStep.itemsArray objectAtIndex:indexPath.row];
        if (item && item.itemMaster) {
            // Cell Title Text
            
            // define the range you're interested in
            NSRange stringRange = {0, MIN([item.itemMaster.type length], 1)};
            // adjust the range to include dependent chars
            stringRange = [item.itemMaster.type rangeOfComposedCharacterSequencesForRange:stringRange];
            // Now you can create the short string
            NSString *typeSubstring = [[item.itemMaster.type uppercaseString] substringWithRange:stringRange];
            
            cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@", typeSubstring, item.itemMaster.name];
            
            // Cell Subtitle Text
            
            NSMutableString *detailText = [[NSMutableString alloc] init];
            
            // Add quantity and value
            [detailText appendFormat:@"%1.2f | $%1.2f ", item.quantity, item.value];
            
            // Add vendor and/or vendor code
            if (item.itemMaster.vendor && [item.itemMaster.vendor length] > 0 && item.itemMaster.vendorCode && [item.itemMaster.vendorCode length] > 0) {
                [detailText appendFormat:@"| %@ | %@ ", item.itemMaster.vendor, item.itemMaster.vendorCode];
            }
            else if (item.itemMaster.vendor && [item.itemMaster.vendor length] > 0) {
                [detailText appendFormat:@"| %@ ", item.itemMaster.vendor];
            }
            else if (item.itemMaster.vendorCode && [item.itemMaster.vendorCode length] > 0) {
                [detailText appendFormat:@"| %@ ", item.itemMaster.vendorCode];
            }
            
            // Add description
            if (item.itemMaster.description && [item.itemMaster.description length] > 0) {
                [detailText appendFormat:@"| %@ ", item.itemMaster.description];
            }
            
            cell.detailTextLabel.text = detailText;
        }
    }
    
    // Convert all row selections to check marks
    NSIndexPath *selection = [tableView indexPathForSelectedRow];
    if (selection && selection.row == indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        // uncheck
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else {
        // check
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
}

 - (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
 {
 return @"Items";
 }

/*
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger) section
{
    //section text as a label
    UILabel *lbl = [[UILabel alloc] init];
    lbl.textAlignment = UITextAlignmentLeft;
    
    lbl.text = @"Items";
    [lbl setBackgroundColor:[UIColor clearColor]];
    
    return lbl;
}
*/

-(void)reloadTableView {
    [self reloadItemsArray];
    [self.itemsTableView reloadData];
}

-(void)reloadItemsArray {
    @try {
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

#pragma mark -
#pragma mark Service methods
/****************************************************************************
 * Service methods
 ****************************************************************************/

- (void)saveItems {
    @autoreleasepool {
        @try {
            // Determine inventory workflow type (1 - New, 2 - Import, 3 - Audit)
            int inventoryWorkflowType = 1;  // assume new inventory workflow
            if ([[self.workflow.type lowercaseString] caseInsensitiveCompare:WKFL_INV_IMPORT_INVENTORY] == NSOrderedSame)
                inventoryWorkflowType = 2;
            else if ([[self.workflow.type lowercaseString] caseInsensitiveCompare:WKFL_INV_AUDIT_INVENTORY] == NSOrderedSame)
                inventoryWorkflowType = 3;
            
            // Get items from workflow
            NSMutableArray *items = nil;
            VMUpdateInventoryConfirmWorkflowStep *updateInventoryItemsWorkflowStep = [self.workflow.workflowSteps objectForKey:WKFL_STEP_INV_UPDATE_INVENTORY_CONFIRM];
            if (updateInventoryItemsWorkflowStep && updateInventoryItemsWorkflowStep.itemsArray) {
                items = updateInventoryItemsWorkflowStep.itemsArray;
            }

            // Get location
            VMLocation *location = nil;
            VMSelectInventoryLocationWorkflowStep *selectInventoryLocationWorkflowStep = [self.workflow.workflowSteps objectForKey:WKFL_STEP_INV_SELECT_INVENTORY_LOCATION];
            if (selectInventoryLocationWorkflowStep && selectInventoryLocationWorkflowStep.location) {
                location = selectInventoryLocationWorkflowStep.location;
            }

            // Proceed only if you have both items and a location
            if (items && items.count > 0 && location && location.name) {
                // Seperate items by location into new and existing
                NSMutableArray *itemEventsToPersist = [[NSMutableArray alloc] init]; // Create a new itemEvents array since an event is guaranteed to occur
                NSMutableArray *newItemsToPersist = nil;
                NSMutableArray *existingItemsToPersist = nil;
                
                for (VMItem *item in items) {
                    //int count = [[VMInventoryService sharedInstance] itemCount:item.itemMasterGUID locationId:location.guid status:item.status];
                    NSMutableArray *existingItemsFromDB = [VMInventoryService readItems:item.itemMasterGUID locationId:location.guid status:item.status];
                    if (existingItemsFromDB && existingItemsFromDB.count > 0) {
                        if (existingItemsToPersist == nil)
                            existingItemsToPersist = [[NSMutableArray alloc] init];
                        
                        // Since item already exists at this location and status, update existing item(s) from database
                        for (VMItem *existingItemFromDB in existingItemsFromDB) {
                            VMItemEvent *itemEvent = nil;
                            if (inventoryWorkflowType == 1) {
                                [VMInventoryService updateItemWithItem:item target:existingItemFromDB incrementQty:YES];
                                [existingItemsToPersist addObject:existingItemFromDB];
                                itemEvent = [[VMItemEvent alloc] initWithCopy:nil item:existingItemFromDB location:location];
                                itemEvent.type = @"New Inventory Update";
                            }
                            else if (inventoryWorkflowType == 2) {
                                [VMInventoryService updateItemWithItem:item target:existingItemFromDB incrementQty:YES];
                                [existingItemsToPersist addObject:existingItemFromDB];
                                itemEvent = [[VMItemEvent alloc] initWithCopy:nil item:existingItemFromDB location:location];
                                itemEvent.type = @"Import Inventory Update";
                            }
                            else if (inventoryWorkflowType == 3) {
                                [VMInventoryService updateItemWithItem:item target:existingItemFromDB incrementQty:NO];
                                [existingItemsToPersist addObject:existingItemFromDB];
                                itemEvent = [[VMItemEvent alloc] initWithCopy:nil item:existingItemFromDB location:location];
                                itemEvent.type = @"Audit Inventory Update";
                            }
                            [itemEventsToPersist addObject:itemEvent];
                        }
                    }
                    else {
                        if (newItemsToPersist == nil)
                            newItemsToPersist = [[NSMutableArray alloc] init];
                        [newItemsToPersist addObject:item];
                        
                        VMItemEvent *itemEvent = [[VMItemEvent alloc] initWithCopy:nil item:item location:location];
                        if (inventoryWorkflowType == 1) {
                            itemEvent.type = @"New Inventory Create";
                        }
                        else if (inventoryWorkflowType == 2) {
                            itemEvent.type = @"Import Inventory Create";
                        }
                        else if (inventoryWorkflowType == 3) {
                            itemEvent.type = @"Audit Inventory Create";
                        }
                        [itemEventsToPersist addObject:itemEvent];
                    }
                }
                
                [VMInventoryService createItemEvents:itemEventsToPersist];
                if (existingItemsToPersist && existingItemsToPersist.count > 0)
                    [VMInventoryService updateItems:existingItemsToPersist];
                if (newItemsToPersist && newItemsToPersist.count > 0)
                    [VMInventoryService createItems:newItemsToPersist];
            }

        }
        @catch (NSException *exception) {
            NSLog(@"An exception occured: %@", [exception reason]);
        }
    }
}

#pragma mark -
#pragma mark Email methods
/****************************************************************************
 * Email methods
 ****************************************************************************/

- (void)actionEmailComposer {
    NSDate *date = [NSDate date];
    [self createEmailReport:[VMSession sharedInstance].user date:date];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    if(error)
        NSLog(@"ERROR - mailComposeController: %@", [error localizedDescription]);
    [self loadMainViewController];
}

- (void)createEmailReport:(VMUser *)user date:(NSDate *)date {
    if ([MFMailComposeViewController canSendMail]) {
        @try {
            @autoreleasepool {
                MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
                if (mailComposer) {
                    mailComposer.mailComposeDelegate = self;
                    [mailComposer setModalPresentationStyle:UIModalPresentationFormSheet];

                    // Set To Recipient
                    NSMutableArray *toRecipients = [[NSMutableArray alloc] init];
                    if ([VMSession sharedInstance].user.email)
                        [toRecipients addObject:[VMSession sharedInstance].user.email];
                    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"reportsEmailSendReports"] && [[NSUserDefaults standardUserDefaults] stringForKey:@"reportsEmail"])
                        [toRecipients addObject:[[NSUserDefaults standardUserDefaults] stringForKey:@"reportsEmail"]];
                    [mailComposer setToRecipients:toRecipients];
                    
                    // Set Subject
                    NSMutableString *subject = [NSMutableString stringWithFormat:@"Verbi"];
                    if (self.workflow && self.workflow.type)
                        [subject appendFormat:@" %@", self.workflow.type];
                    [subject appendString:@" Report"];
                    [mailComposer setSubject:subject];
                    
                    // Get Location
                    VMLocation *location = nil;
                    VMSelectInventoryLocationWorkflowStep *selectInventoryLocationWorkflowStep = [self.workflow.workflowSteps objectForKey:WKFL_STEP_INV_SELECT_INVENTORY_LOCATION];
                    if (selectInventoryLocationWorkflowStep && selectInventoryLocationWorkflowStep.location) {
                        location = selectInventoryLocationWorkflowStep.location;
                    }
                    
                    // Get Items
                    NSMutableArray *items = nil;
                    VMUpdateInventoryConfirmWorkflowStep *updateInventoryItemsWorkflowStep = [self.workflow.workflowSteps objectForKey:WKFL_STEP_INV_UPDATE_INVENTORY_CONFIRM];
                    if (updateInventoryItemsWorkflowStep && updateInventoryItemsWorkflowStep.itemsArray) {
                        items = updateInventoryItemsWorkflowStep.itemsArray;
                    }
                    
                    // Set Body
                    NSMutableString *messageBody = [self formatSummaryAsString:[VMSession sharedInstance].user title:subject date:date location:location items:items];
                    [mailComposer setMessageBody:messageBody isHTML:YES];
                    
                    // Set Attachment
                    NSMutableString *messageAttachment = [self formatSummaryAsCSV:[VMSession sharedInstance].user title:subject date:date location:location items:items];
                    NSData *myData = [messageAttachment dataUsingEncoding:NSUTF8StringEncoding];
                    NSMutableString *fileName = [NSMutableString stringWithFormat:@"verbi-report"];
                    if (self.workflow.guid)
                        [fileName appendFormat:@"-%@", self.workflow.guid];
                    [fileName appendString:@".csv"];
                    [mailComposer addAttachmentData:myData mimeType:@"text/csv" fileName:fileName];
                    
                    [self presentModalViewController:mailComposer animated:YES];
                }
            }
        }
        @catch (NSException *exception) {
            NSLog(@"An exception occured: %@", [exception reason]);
        }
    }
    else {
        NSLog(@"Device is unable to send email in its current state.");
    }
}

- (NSMutableString *)formatSummaryAsString:(VMUser *)user title:(NSString *)title date:(NSDate *)date location:(VMLocation *)location items:(NSMutableArray *)items {
    NSMutableString *summary = nil;
    @autoreleasepool {
        summary = [NSMutableString stringWithString:@"<html><body>"];
        
        // Add Header
        [summary appendFormat:@"<h2>%@</h2><p>", title];

        // Add User
        if (user && user.name)
            [summary appendFormat:@"<b>User:</b> %@ [%@]<br>", user.name, user.username];
        // Add Date
        if (date)
            [summary appendFormat:@"<b>Date:</b> %@<br>", date];
        // Add Location Name
        if (location && location.name)
            [summary appendFormat:@"<b>Location:</b> %@<br>", location.name];
        
        // Add Items
        if (items && items.count > 0) {
            [summary appendString:@"<p><b>Items</b><br><table>"];
            // Add item table header
            [summary appendString:@"<tr>"];
            [summary appendString:@"<td><b>Name</b></td>"];
            [summary appendString:@"<td><b>Description</b></td>"];
            [summary appendString:@"<td><b>Vendor</b></td>"];
            [summary appendString:@"<td><b>Vendor Code</b></td>"];
            [summary appendString:@"<td><b>Type</b></td>"];
            [summary appendString:@"<td><b>Current Quantity</b></td>"];
            [summary appendString:@"<td><b>Current Price</b></td>"];
            [summary appendString:@"<td><b>Current Value</b></td>"];
            [summary appendString:@"<td><b>New Quantity</b></td>"];
            [summary appendString:@"<td><b>New Price</b></td>"];
            [summary appendString:@"<td><b>New Value</b></td>"];
            [summary appendString:@"</tr>"];
            // Add each item
            for (VMItem *item in items) {
                if (item && item.itemMaster && item.itemMaster.name) {
                    [summary appendString:@"<tr>"];
                    [summary appendFormat:@"<td>%@</td>", item.itemMaster.name];
                    [summary appendString:@"<td>"];
                    if (item.itemMaster.description)
                        [summary appendFormat:@"%@", item.itemMaster.description];
                    [summary appendString:@"</td>"];
                    [summary appendString:@"<td>"];
                    if (item.itemMaster.vendor)
                        [summary appendFormat:@"%@", item.itemMaster.vendor];
                    [summary appendString:@"</td>"];
                    [summary appendString:@"<td>"];
                    if (item.itemMaster.vendorCode)
                        [summary appendFormat:@"%@", item.itemMaster.vendorCode];
                    [summary appendString:@"</td>"];
                    [summary appendFormat:@"<td>%@</td>", item.itemMaster.type];
                    [summary appendString:@"<td></td>"];
                    [summary appendString:@"<td></td>"];
                    [summary appendString:@"<td></td>"];
                    [summary appendFormat:@"<td>%1.2f</td>", item.quantity];
                    [summary appendFormat:@"<td>%1.2f</td>", item.itemMaster.price];
                    [summary appendFormat:@"<td>%1.2f</td>", item.value];
                    [summary appendString:@"</tr>"];
                }
            }
            [summary appendString:@"</p></table>"];
        }
        
        [summary appendString:@"</body></html>"];
    }
    return summary;
}

- (NSMutableString *)formatSummaryAsCSV:(VMUser *)user title:(NSString *)title date:(NSDate *)date location:(VMLocation *)location items:(NSMutableArray *)items {
    NSMutableString *summary = nil;
    @autoreleasepool {
        summary = [NSMutableString stringWithString:@""];
        
        // Add Header
        [summary appendFormat:@"%@\n", title];
        
        // Add User
        if (user && user.name)
            [summary appendFormat:@"User, %@, [%@]\n", user.name, user.username];
        // Add Date
        if (date)
            [summary appendFormat:@"Date, %@\n", date];
        // Add Location Name
        if (location && location.name)
            [summary appendFormat:@"Location, %@\n", location.name];
        
        // Add Items
        if (items && items.count > 0) {
            // Add item table header
            [summary appendString:@"\n"];
            [summary appendString:@"Name,"];
            [summary appendString:@"Description,"];
            [summary appendString:@"Vendor,"];
            [summary appendString:@"Vendor Code,"];
            [summary appendString:@"Type,"];
            [summary appendString:@"Current Quantity,"];
            [summary appendString:@"Current Price,"];
            [summary appendString:@"Current Value,"];
            [summary appendString:@"New Quantity,"];
            [summary appendString:@"New Price,"];
            [summary appendString:@"New Value,"];
            [summary appendString:@"\n"];
            // Add each item
            for (VMItem *item in items) {
                if (item && item.itemMaster && item.itemMaster.name) {
                    [summary appendFormat:@"%@,", item.itemMaster.name];
                    if (item.itemMaster.description)
                        [summary appendFormat:@"%@", item.itemMaster.description];
                    [summary appendString:@","];
                    if (item.itemMaster.vendor)
                        [summary appendFormat:@"%@", item.itemMaster.vendor];
                    [summary appendString:@","];
                    if (item.itemMaster.vendorCode)
                        [summary appendFormat:@"%@", item.itemMaster.vendorCode];
                    [summary appendString:@","];
                    [summary appendFormat:@"%@,", item.itemMaster.type];
                    [summary appendString:@","];
                    [summary appendString:@","];
                    [summary appendString:@","];
                    [summary appendFormat:@"%1.2f,", item.quantity];
                    [summary appendFormat:@"%1.2f,", item.itemMaster.price];
                    [summary appendFormat:@"%1.2f,", item.value];
                    [summary appendString:@"\n"];
                }
            }
        }
    }
    return summary;
}

@end
