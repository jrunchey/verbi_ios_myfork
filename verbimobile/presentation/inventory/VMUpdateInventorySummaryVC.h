//
//  VMUpdateInventorySummaryViewController.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "VMParentVC.h"

@interface VMUpdateInventorySummaryVC : VMParentVC <MFMailComposeViewControllerDelegate, UIAlertViewDelegate>

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

@property(nonatomic, weak) IBOutlet UITableView *itemsTableView;

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO Methods
 ****************************************************************************/

- (void)actionEmailComposer;

@end
