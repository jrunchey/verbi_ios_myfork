//
//  HelpViewController.h
//  verbimobile
//
//  Created by Rob Hotaling on 3/9/13.
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIBarButtonItem *leftMenuBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *rightMenuBtn;

@end
