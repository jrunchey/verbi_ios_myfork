//
//  MainViewController.h
//  verbimobile
//
//  Created by Rob Hotaling on 3/8/13.
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMParentVC.h"

@interface MainViewController : VMParentVC
@property (strong, nonatomic) IBOutlet UIBarButtonItem *leftMenuBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *rightMenuBtn;
@property (strong, nonatomic) IBOutlet UIButton *connectBtn;
@property (strong, nonatomic) IBOutlet UIButton *readTagsBtn;
@property (strong, nonatomic) IBOutlet UIButton *inventoryBtn;

@end
