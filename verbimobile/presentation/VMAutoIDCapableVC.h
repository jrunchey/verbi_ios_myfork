//
//  VMAutoIDCapableVC.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMParentVC.h"
#import "VMRFIDReader.h"
#import "VMBarcodeReader.h"
//#import "BLE.h"

@interface VMAutoIDCapableVC : VMParentVC <VMRFIDReaderDelegate, VMBarcodeReaderDelegate>
//

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

//@property(nonatomic, weak) BLE *bleShield;
@property(weak, nonatomic) IBOutlet UIButton *autoIdModeButton;
@property(weak, nonatomic) IBOutlet UIButton *autoIdProcessButton;
@property(strong, nonatomic) NSTimer *rfidScanTimer;

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO Methods
 ****************************************************************************/

- (void)barcodeScan;
- (void)rfidScan;
- (void)startRFIDScan;  // Sends a start read command to connected RFID reader
- (void)stopRFIDScan;   // Sends a stop read command to connected RFID reader
- (NSString *)getLastScanValue;
- (NSString *)getLastBarcodeSymbology;

@end
