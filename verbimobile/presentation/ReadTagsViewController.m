//
//  ReadTagsViewController.m
//  verbimobile
//
//  Created by Rob Hotaling on 3/9/13.
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "ReadTagsViewController.h"
#import "ECSlidingViewController.h"
#import "LeftMenuViewController.h"
#import "RightMenuViewController.h"
#import "VMSession.h"

@interface ReadTagsViewController ()
@property(strong, nonatomic) NSMutableDictionary *tagDictionary;
@property(strong, nonatomic) NSMutableDictionary *tagCountsDictionary;
@end

@implementation ReadTagsViewController

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/
@synthesize leftMenuBtn;
@synthesize rightMenuBtn;
@synthesize clearBtn = _clearBtn;
@synthesize tagsTableView = _tagsTableView;
@synthesize tagDictionary = _tagDictionary;
@synthesize tagCountsDictionary = _tagCountsDictionary;

- (NSMutableDictionary *)tagDictionary {
    if (_tagDictionary == nil)
        _tagDictionary = [[NSMutableDictionary alloc] init];
    return _tagDictionary;
}

- (NSMutableDictionary *)tagCountsDictionary {
    if (_tagCountsDictionary == nil)
        _tagCountsDictionary = [[NSMutableDictionary alloc] init];
    return _tagCountsDictionary;
}

#pragma mark -
#pragma mark Lifecycle Methods
/****************************************************************************
 * Lifecycle Methods
 ****************************************************************************/
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // These 3 give effect that view controller is hovering slightly above menu
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    // Checks to see if we already have a Menu view controller from storyboard underneath our menu. If we don't, then create it
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[LeftMenuViewController class]]) {
        self.slidingViewController.underLeftViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LeftMenu"];
    }
    if (![self.slidingViewController.underRightViewController isKindOfClass:[RightMenuViewController class]]) {
        self.slidingViewController.underRightViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"RightMenu"];
    }
    
    // gives us the ability to swipe from left to right to reveal menu
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    [self.leftMenuBtn setTarget:self];
    [self.leftMenuBtn setAction:@selector(revealLeftMenu:)];
    [self.rightMenuBtn setTarget:self];
    [self.rightMenuBtn setAction:@selector(revealRightMenu:)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)revealLeftMenu:(id)sender {
    [self.slidingViewController anchorTopViewTo:ECRight];
}

- (IBAction)revealRightMenu:(id)sender {
    [self.slidingViewController anchorTopViewTo:ECLeft];
}

- (void)viewDidUnload {
    [self setLeftMenuBtn:nil];
    [self setRightMenuBtn:nil];
    [self setClearBtn:nil];
    [self setTagsTableView:nil];
    [self setBarcodeBtn:nil];
    [self setRfidBtn:nil];
    
    if (self.tagDictionary) {
        [self.tagDictionary removeAllObjects];
        self.tagDictionary = nil;
    }
    if (self.tagCountsDictionary) {
        [self.tagCountsDictionary removeAllObjects];
        self.tagCountsDictionary = nil;
    }
    
    [super viewDidUnload];
}

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/
- (void)dispose {
    @try {
        self.tagsTableView = nil;
        [self resetCollections];
        [super dispose];
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
}

- (void)resetCollections {
    if (self.tagDictionary && self.tagDictionary.count > 0)
        [self.tagDictionary removeAllObjects];
    if (self.tagCountsDictionary && self.tagCountsDictionary.count > 0)
        [self.tagCountsDictionary removeAllObjects];
    [self.tagsTableView reloadData];
}


#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO Methods
 ****************************************************************************/
- (IBAction)clearBtnPressed:(UIButton *)sender {
    [self resetCollections];
}

- (IBAction)barcodeBtnPressed:(UIButton *)sender {
    [self barcodeScan];
}

- (IBAction)rfidBtnPressed:(UIButton *)sender {
    [self rfidScan];
}

#pragma mark -
#pragma mark Table View Methods
/****************************************************************************
 * Table View Methods
 ****************************************************************************/

// Defines the table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Defines the table - Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tagCountsDictionary.count;
}

// Fill the table - Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    if (self.tagCountsDictionary && self.tagCountsDictionary.count > 0) {
        NSString *tagId = [[self.tagCountsDictionary allKeys] objectAtIndex:indexPath.row];
        VMTag *tag = [self.tagDictionary objectForKey:tagId];
        NSNumber *tagCount = [self.tagCountsDictionary objectForKey:tagId];
        NSString *detailText = nil;
        if (tagId) {
            // Cell Title Text
            if (tag.type == TAG_MAGNETIC_CARD) {
                VMMagneticCardTag *magstripeTag = (VMMagneticCardTag *)tag;
                cell.textLabel.text = [NSString stringWithFormat:@"%@ [%d]", magstripeTag.value, [tagCount intValue]];
                detailText = [NSString stringWithFormat:@"Magstripe | Count: %d", [tagCount intValue]];
            }
            else if (tag.type == TAG_BARCODE) {
                VMBarcodeTag *barcodeTag = (VMBarcodeTag *)tag;
                cell.textLabel.text = [NSString stringWithFormat:@"%@", barcodeTag.value];
                detailText = [NSString stringWithFormat:@"Barcode (%@) | Count: %d", barcodeTag.symbology, [tagCount intValue]];
            }
            else if (tag.type == TAG_RFID) {
                VMRFIDTag *rfidTag = (VMRFIDTag *)tag;
                cell.textLabel.text = [NSString stringWithFormat:@"%@", rfidTag.value];
                detailText = [NSString stringWithFormat:@"RFID | Count: %d", [tagCount intValue]];
                //detailText = [NSString stringWithFormat:@"Count: (%d) | RSSI: %f", [tagCount intValue], 0.0];
            }
            
            // Cell Subtitle Text
            cell.detailTextLabel.text = detailText; // set RSSI Info
        }
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Tag Ids";
}

#pragma mark -
#pragma mark Helper Methods
/****************************************************************************
 * Helper Methods
 ****************************************************************************/

- (void)addTag:(VMTag *)tag {
    if (tag) {
        VMTag *tagFound = [self.tagDictionary objectForKey:tag.value];
        NSNumber *tagCount = [self.tagCountsDictionary objectForKey:tag.value];
        if (tagFound && tagCount) {
            int tagCountIntVal = [tagCount intValue];
            tagCountIntVal++;
            tagCount = [NSNumber numberWithInt:tagCountIntVal];
        }
        else {
            tagCount = [NSNumber numberWithInt:1];
            tagFound = tag;
        }
        if (tagFound) {
            [self.tagDictionary setObject:tagFound forKey:tag.value];
            [self.tagCountsDictionary setObject:tagCount forKey:tag.value];
            [self.tagsTableView reloadData];
        }
    }
}

#pragma mark -
#pragma mark VMRFIDReaderDelegate Methods
/****************************************************************************
 * VMRFIDReaderDelegate Methods
 ****************************************************************************/

-(void)handleRFIDReaderStatusEvent:(NSString *)description event:(VMRFIDReaderEvent *)event {
    
}

-(void)handleRFIDReaderReadEvent:(NSArray *)tagIds event:(VMRFIDReaderEvent *)event {
    if (event && event.tags) {
        for (VMRFIDTag *tag in event.tags) {
            [self addTag:tag];
        }
    }
}

#pragma mark -
#pragma mark VMBarcodeReaderDelegate Methods
/****************************************************************************
 * VMBarcodeReaderDelegate Methods
 ****************************************************************************/

-(void)handleBarcodeReaderReadEvent:(NSString *)barcode symbology:(NSString *)symbology event:(VMBarcodeReaderEvent *)event {
    if (event && event.tag)
        [self addTag:event.tag];
}

-(void)handleMagneticCardReaderReadEvent:(NSString *)data event:(VMMagneticCardReaderEvent *)event {
    if (event && event.tag)
        [self addTag:event.tag];
}

@end
