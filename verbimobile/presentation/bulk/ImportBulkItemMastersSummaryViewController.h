//
//  ImportBulkItemMastersSummaryViewController.h
//  verbimobile
//
//  Created by Rob Hotaling on 3/19/13.
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImportBulkItemMastersSummaryViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIBarButtonItem *okMenuBtn;

@end
