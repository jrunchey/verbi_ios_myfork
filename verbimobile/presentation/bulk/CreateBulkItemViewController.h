//
//  CreateItemViewController.h
//  verbimobile
//
//  Created by Rob Hotaling on 3/12/13.
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateBulkItemViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIBarButtonItem *leftMenuBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *rightMenuBtn;

@end
