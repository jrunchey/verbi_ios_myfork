//
//  LeftMenuViewController.m
//  verbimobile
//
//  Created by Rob Hotaling on 3/8/13.
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "ECSlidingViewController.h"
#import "VMSession.h"

@interface LeftMenuViewController ()

@property (strong, nonatomic) NSArray *menu;
@property (strong, nonatomic) NSArray *mainSection;
@property (strong, nonatomic) NSArray *bulkItemsSection;
@property (strong, nonatomic) NSArray *assetItemsSection;
@property (strong, nonatomic) NSArray *otherSection;

@end

@implementation LeftMenuViewController

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/
@synthesize menu, mainSection, bulkItemsSection, assetItemsSection, otherSection;   // holds all items in the menu

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // Sections
    self.mainSection = [NSArray arrayWithObjects:@"Main", @"Connect", @"Read Tags", @"Inventory", nil];
    self.bulkItemsSection = [NSArray arrayWithObjects:@"Item Masters", @"Create Item Master", @"Import Item Masters", @"Export Item Masters", @"Items", @"Create Item", @"Import Items", @"Export Items", @"Transfer Items", nil];
    self.assetItemsSection = [NSArray arrayWithObjects:@"Assets", @"Create Asset", @"Import Assets", @"Export Assets", @"Transfer Assets", nil];
    self.otherSection = [NSArray arrayWithObjects:@"Account Settings", @"Report a Problem", @"Logout", nil];
    
    // Overall Menu
    self.menu = [NSArray arrayWithObjects:self.mainSection, self.bulkItemsSection, self.assetItemsSection, self.otherSection, nil];
//    self.menu = [NSArray arrayWithObjects:@"Main", @"Connect", @"Read Tags", @"Inventory", nil]; // names of each sub view controller that menu reveals
//    self.menu = [NSArray arrayWithObjects:@"Main", @"Connect", @"Read Tags", nil]; // names of each sub view controller that menu reveals
    
    [self.slidingViewController setAnchorRightPeekAmount:58.0f]; // when you slide menu to the right, topview on peek will be about 40 pixels wide
    self.slidingViewController.underLeftWidthLayout = ECVariableRevealWidth;  // lets us know we are working with a full width app rather than a smaller one
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [self.menu count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0)
        return [self.mainSection count];
    else if (section == 1)
        return [self.bulkItemsSection count];
    else if (section == 2)
        return [self.assetItemsSection count];
    else if (section == 3)
        return [self.otherSection count];
    else
        return 0;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0)
        return @" ";
    else if (section == 1)
        return @"Bulk Items";
    else if (section == 2)
        return @"Assets";
    else if (section == 3)
        return @" ";
    else
        return @"";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    // Adds from which menu you are looking at the name of the menu so we see all different items in menu showing in the table view
    if (indexPath.section == 0)
        cell.textLabel.text = [NSString stringWithFormat:@"%@", [self.mainSection objectAtIndex:indexPath.row]];
    else if (indexPath.section == 1)
        cell.textLabel.text = [NSString stringWithFormat:@"%@", [self.bulkItemsSection objectAtIndex:indexPath.row]];
    else if (indexPath.section == 2)
        cell.textLabel.text = [NSString stringWithFormat:@"%@", [self.assetItemsSection objectAtIndex:indexPath.row]];
    else if (indexPath.section == 3)
        cell.textLabel.text = [NSString stringWithFormat:@"%@", [self.otherSection objectAtIndex:indexPath.row]];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier;
    if (indexPath.section == 0)
        identifier = [NSString stringWithFormat:@"%@", [self.mainSection objectAtIndex:indexPath.row]];
    else if (indexPath.section == 1)
        identifier = [NSString stringWithFormat:@"%@", [self.bulkItemsSection objectAtIndex:indexPath.row]];
    else if (indexPath.section == 2)
        identifier = [NSString stringWithFormat:@"%@", [self.assetItemsSection objectAtIndex:indexPath.row]];
    else if (indexPath.section == 3)
        identifier = [NSString stringWithFormat:@"%@", [self.otherSection objectAtIndex:indexPath.row]];
    

    if ([[identifier lowercaseString] caseInsensitiveCompare:@"Logout"] == NSOrderedSame) {
        [self logout];
    }
    else {
        // add from storyboard and give it id from identifier variable so when you click on "Main", it will load view controller with id "Main"
        UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:identifier];
        
        // dismiss current view
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        [self dismissViewControllerAnimated:YES completion:nil];
        
        // allows view controller to slide menu off and back onto the screen
        [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
            CGRect frame = self.slidingViewController.topViewController.view.frame;
            self.slidingViewController.topViewController = newTopViewController;
            self.slidingViewController.topViewController.view.frame = frame;    // moves it onto the top
            [self.slidingViewController resetTopView];  // slides it back
        }];
    }
}

- (void)logout {
    // Clear out the session and anything related to the user
    VMSession *session = [VMSession sharedInstance];
    if (session) {
        [session dispose];
        session = nil;
    }
    
    // Load the authentication controller since we no longer have a user
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Login"];
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;    // moves it onto the top
    [self.slidingViewController resetTopView];  // slides it back
}

@end
