//
//  RightMenuViewController.m
//  verbimobile
//
//  Created by Rob Hotaling on 3/8/13.
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "RightMenuViewController.h"
#import "ECSlidingViewController.h"

@interface RightMenuViewController ()

@property (strong, nonatomic) NSArray *menu;
@property (strong, nonatomic) NSArray *helpSection;
@property (strong, nonatomic) NSArray *profileSection;
@property (strong, nonatomic) NSArray *aclSection;
@property (strong, nonatomic) NSArray *locationsSection;
@property (strong, nonatomic) NSArray *categoriesSection;

@end

@implementation RightMenuViewController

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/
@synthesize menu, helpSection, profileSection, aclSection, locationsSection, categoriesSection;   // holds all items in the menu

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // Sections
    self.helpSection = [NSArray arrayWithObjects:@"Help", nil];
    self.profileSection = [NSArray arrayWithObjects:@"Profile", nil];
    self.aclSection = [NSArray arrayWithObjects:@"Users", @"Create User", @"Groups", @"Create Group",  @"Import ACL", @"Export ACL", nil];
    self.locationsSection = [NSArray arrayWithObjects:@"Locations", @"Create Location", @"Import Locations", @"Export Locations", nil];
    self.categoriesSection = [NSArray arrayWithObjects:@"Categories", @"Create Category", @"Import Categories", @"Export Categories", nil];
    
    // Overall Menu
    self.menu = [NSArray arrayWithObjects:self.helpSection, self.profileSection, self.aclSection, self.locationsSection, self.categoriesSection, nil];
//    self.menu = [NSArray arrayWithObjects:@"Help", nil]; // names of each sub view controller that menu reveals
    
    [self.slidingViewController setAnchorLeftPeekAmount:58.0f]; // when you slide menu to the left, topview on peek will be about 40 pixels wide
    self.slidingViewController.underRightWidthLayout = ECVariableRevealWidth;  // lets us know we are working with a full width app rather than a smaller one
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [self.menu count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0)
        return [self.helpSection count];
    else if (section == 1)
        return [self.profileSection count];
    else if (section == 2)
        return [self.aclSection count];
    else if (section == 3)
        return [self.locationsSection count];
    else if (section == 4)
        return [self.categoriesSection count];
    else
        return 0;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0)
        return @"Help";
    else if (section == 1)
        return @"My Profile";
    else if (section == 2)
        return @"Access Control";
    else if (section == 3)
        return @"Locations";
    else if (section == 4)
        return @"Categories";
    else
        return @"";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    // Adds from which menu you are looking at the name of the menu so we see all different items in menu showing in the table view
    if (indexPath.section == 0)
        cell.textLabel.text = [NSString stringWithFormat:@"%@", [self.helpSection objectAtIndex:indexPath.row]];
    else if (indexPath.section == 1)
        cell.textLabel.text = [NSString stringWithFormat:@"%@", [self.profileSection objectAtIndex:indexPath.row]];
    else if (indexPath.section == 2)
        cell.textLabel.text = [NSString stringWithFormat:@"%@", [self.aclSection objectAtIndex:indexPath.row]];
    else if (indexPath.section == 3)
        cell.textLabel.text = [NSString stringWithFormat:@"%@", [self.locationsSection objectAtIndex:indexPath.row]];
    else if (indexPath.section == 4)
        cell.textLabel.text = [NSString stringWithFormat:@"%@", [self.categoriesSection objectAtIndex:indexPath.row]];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier;
    if (indexPath.section == 0)
        identifier = [NSString stringWithFormat:@"%@", [self.helpSection objectAtIndex:indexPath.row]];
    else if (indexPath.section == 1)
        identifier = [NSString stringWithFormat:@"%@", [self.profileSection objectAtIndex:indexPath.row]];
    else if (indexPath.section == 2)
        identifier = [NSString stringWithFormat:@"%@", [self.aclSection objectAtIndex:indexPath.row]];
    else if (indexPath.section == 3)
        identifier = [NSString stringWithFormat:@"%@", [self.locationsSection objectAtIndex:indexPath.row]];
    else if (indexPath.section == 4)
        identifier = [NSString stringWithFormat:@"%@", [self.categoriesSection objectAtIndex:indexPath.row]];
    
    // add from storyboard and give it id from identifier variable so when you click on "Help", it will load view controller with id "Help"
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:identifier];
    
    // dismiss current view
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    // allows view controller to slide menu off and back onto the screen
    [self.slidingViewController anchorTopViewOffScreenTo:ECLeft animations:nil onComplete:^{
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;    // moves it onto the top
        [self.slidingViewController resetTopView];  // slides it back
    }];
}

@end
