//
//  ReadTagsViewController.h
//  verbimobile
//
//  Created by Rob Hotaling on 3/9/13.
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMAutoIDCapableVC.h"

@interface ReadTagsViewController : VMAutoIDCapableVC <UITableViewDelegate,UITableViewDataSource, UIAlertViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIBarButtonItem *leftMenuBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *rightMenuBtn;
@property (strong, nonatomic) IBOutlet UIButton *clearBtn;
@property (strong, nonatomic) IBOutlet UIButton *barcodeBtn;
@property (strong, nonatomic) IBOutlet UIButton *rfidBtn;
@property (strong, nonatomic) IBOutlet UITableView *tagsTableView;

@end
