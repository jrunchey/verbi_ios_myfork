//
//  VMParentVC.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMConstants.h"
#import "VMWorkflow.h"
#import "VMWorkflowStep.h"
#import "VMUtilityService.h"

@interface VMParentVC : UIViewController <UIGestureRecognizerDelegate>

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/

//@property(nonatomic, weak) VMViewController *mainViewController;
@property(weak, nonatomic) UIViewController *lastViewController;
@property(strong, nonatomic) VMWorkflow *workflow;
@property(strong, nonatomic) VMWorkflowStep *workflowStep;
@property(weak, nonatomic) IBOutlet UIButton *homeBackButton;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property(weak, nonatomic) IBOutlet UILabel *titleLabel;

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose;

#pragma mark -
#pragma mark View lifecycle
/****************************************************************************
 * View Lifecycle Public
 ****************************************************************************/

- (void)initializeUIOnFirstLoad;                    // Called from viewDidLoad
- (void)initializeUIBeforeRendering;                // Called from viewWillAppear
- (void)initializeUIAfterRendering;                 // Called from viewDidAppear
- (void)cleanupUIBeforeDerendering;                 // Called from viewWillDisappear
- (void)cleanupUIOnDispose;                         // Called from viewDidUnload

#pragma mark -
#pragma mark Navigation Methods
/****************************************************************************
 * Navigation Methods
 ****************************************************************************/

- (void)loadMainViewController;         // Loads the main view controller
- (void)loadLastViewController;         // Loads the last view controller

#pragma mark -
#pragma mark IO Methods
/*****************************************************************************
 * IO Methods
 *****************************************************************************/

- (IBAction)homePressed:(id)sender;
- (IBAction)backPressed:(id)sender;

#pragma mark -
#pragma mark Helper Methods
/*****************************************************************************
 * Helper Methods
 *****************************************************************************/

- (void)uncheckAllItems:(UITableView *)tableView;
- (void)startActivityIndicator;
- (void)stopActivityIndicator;

@end
