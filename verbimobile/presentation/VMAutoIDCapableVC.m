//
//  VMAutoIDCapableVC.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMAutoIDCapableVC.h"
#import "VMAppDelegate.h"
#import "VMSession.h"

@interface VMAutoIDCapableVC ()
@property(weak, nonatomic) UIView *pickerSubview;
@property(weak, nonatomic) UIButton *pickerSubviewButton;
@property(weak, nonatomic) NSMutableString *status;
@property(weak, nonatomic) NSMutableString *debug;
@property(nonatomic) BOOL releaseOnAppear;
@property(nonatomic) BOOL scaledSubviewActive;
@property(nonatomic) BOOL scanProcessed;
@end

@implementation VMAutoIDCapableVC

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO Methods
 ****************************************************************************/

//@synthesize bleShield = _bleShield;
@synthesize autoIdModeButton = _autoIdModeButton;
@synthesize autoIdProcessButton = _autoIdProcessButton;
@synthesize pickerSubview = _pickerSubview;
@synthesize pickerSubviewButton = _pickerSubviewButton;
@synthesize status = _status;
@synthesize debug = _debug;
@synthesize releaseOnAppear = _releaseOnAppear;
@synthesize scaledSubviewActive = _scaledSubviewActive;
@synthesize scanProcessed = _scanProcessed;
@synthesize rfidScanTimer = _rfidScanTimer;

#pragma mark -
#pragma mark Constructors
/****************************************************************************
 * Constructors
 ****************************************************************************/

#pragma mark -
#pragma mark Destructors
/****************************************************************************
 * Destructors
 ****************************************************************************/

- (void)dispose {
    [super dispose];
}

#pragma mark -
#pragma mark View Lifecycle
/****************************************************************************
 * View Lifecycle
 ****************************************************************************/

- (void)initializeUIOnFirstLoad {
    if ([IS_SIMULATOR intValue] == 1) {
        self.autoIdModeButton.hidden = YES;
        self.autoIdProcessButton.hidden = YES;
    }
}

- (void)initializeUIBeforeRendering {
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    [self updateAutoIdSettings];
    [self updateAutoIdModeButtonImage];
    [self updateAutoIdStartStopButtonImage];
    
    [VMSession sharedInstance].barcodeReader.delegate = self;
    [VMSession sharedInstance].barcodeReader.currentViewController = self;
    [VMSession sharedInstance].rfidReader.delegate = self;
}

- (void)initializeUIAfterRendering {
}

- (void)cleanupUIBeforeDerendering {
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    [VMSession sharedInstance].barcodeReader.delegate = nil;
    [VMSession sharedInstance].barcodeReader.currentViewController = nil;
    [VMSession sharedInstance].rfidReader.delegate = nil;
}

- (void)cleanupUIOnDispose {
    [self dispose];
}

#pragma mark -
#pragma mark IO Methods
/****************************************************************************
 * IO Methods
 ****************************************************************************/

/**
 * The methods below are only needed to have the picker (when used as an overlay in a simple view)
 * rotate with the screen and change its dimensions accordingly. If the app
 * is solely used in portrait mode, these methods can be ignored.
 * If you use a tab layout, check out the 'tab' example. It shows how to make the ScanditSDKBarcodePicker
 * rotate itself.
 */

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    // We allow rotation in any direction to be able to show how the overlayed picker acts.
    return YES;
}

// Auto-Id Mode Button

- (IBAction)autoIdModePressed:(id)sender {
    [self updateAutoIdSettings];
    
    // The session's auto id mode can only be changed if not MAG/RFID or if MAG/RFID then process must not be started
    int autoIdMode = [VMSession sharedInstance].autoIdMode;
    BOOL autoIdProcessStarted = [VMSession sharedInstance].autoIdProcessStarted;
    if (autoIdMode == [AUTO_ID_MODE_BARCODE_1D_KEY intValue] || autoIdMode == [AUTO_ID_MODE_BARCODE_2D_KEY intValue] || ((autoIdMode == [AUTO_ID_MODE_MAGSTRIPE_KEY intValue] || autoIdMode == [AUTO_ID_MODE_RFID_C1G2_KEY intValue]) && !autoIdProcessStarted)) {
        [[VMSession sharedInstance] toggleAutoIdMode];
        [self updateAutoIdModeButtonImage];
        [self updateAutoIdStartStopButtonImage];
    }
}

- (void)updateAutoIdModeButtonImage {
    NSString *currentAutoIdButtonImageName = self.autoIdModeButton.titleLabel.text;
    NSString *autoIdButtonImageName = [[VMSession sharedInstance] getAutoIdModeButtonImageName];
    if (currentAutoIdButtonImageName == nil || ![[currentAutoIdButtonImageName lowercaseString] caseInsensitiveCompare:autoIdButtonImageName] == NSOrderedSame) {
        UIImage *autoIdButtonImage = [[VMSession sharedInstance] getAutoIdModeButtonImage];
        [self.autoIdModeButton setImage:autoIdButtonImage forState:UIControlStateNormal];
        [self.autoIdModeButton setTitle:autoIdButtonImageName forState:UIControlStateNormal];
    }
}

// Start-Stop Mode Button

- (IBAction)autoIdProcessPressed:(id)sender {
    [self updateAutoIdSettings];
    
    int autoIdMode = [VMSession sharedInstance].autoIdMode;
    BOOL autoIdProcessStarted = [VMSession sharedInstance].autoIdProcessStarted;
    
    if (autoIdMode == [AUTO_ID_MODE_RFID_C1G2_KEY intValue] && ([VMSession sharedInstance].rfidReader == nil || ![VMSession sharedInstance].rfidReader.connected)) {
        NSString *alertViewTitle = @"Error";
        NSString *alertMessage = @"No connection with an RFID device.";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else {
        // If MAG/RFID, handle starting and stopping
        if (autoIdMode == [AUTO_ID_MODE_MAGSTRIPE_KEY intValue] || autoIdMode == [AUTO_ID_MODE_RFID_C1G2_KEY intValue]) {
            if (!autoIdProcessStarted) {
                [VMSession sharedInstance].autoIdProcessStarted = YES;
                
                // The auto id and process mode buttons cannot be changed while process is started so disable them
                [self.autoIdModeButton setEnabled:NO];
                
                if (autoIdMode == [AUTO_ID_MODE_RFID_C1G2_KEY intValue]) {
                    [self startRFIDScan];
                }
            }
            else {
                [VMSession sharedInstance].autoIdProcessStarted = NO;
                
                // The auto id and process mode buttons can be changed while process is not started so enable them
                [self.autoIdModeButton setEnabled:YES];
                
                if (autoIdMode == [AUTO_ID_MODE_RFID_C1G2_KEY intValue]) {
                    [self stopRFIDScan];
                }
            }
        }
        
        // If barcode, then no starting/stopping, rather startStopButton becomes a scan button
        if (autoIdMode == [AUTO_ID_MODE_BARCODE_1D_KEY intValue] || autoIdMode == [AUTO_ID_MODE_BARCODE_2D_KEY intValue]) {
            [[VMSession sharedInstance].barcodeReader triggerRead];
        }
        
        [self updateAutoIdStartStopButtonImage];
    }
}

- (void)updateAutoIdStartStopButtonImage {
    NSString *currentAutoIdButtonImageName = self.autoIdProcessButton.titleLabel.text;
    NSString *autoIdButtonImageName = [[VMSession sharedInstance] getAutoIdStartStopButtonImageName];
    if (currentAutoIdButtonImageName == nil || ![[currentAutoIdButtonImageName lowercaseString] caseInsensitiveCompare:autoIdButtonImageName] == NSOrderedSame) {
        UIImage *autoIdButtonImage = [[VMSession sharedInstance] getAutoIdStartStopButtonImage];
        [self.autoIdProcessButton setImage:autoIdButtonImage forState:UIControlStateNormal];
        [self.autoIdProcessButton setTitle:autoIdButtonImageName forState:UIControlStateNormal];
    }
}

- (void)barcodeScan {
    VMSession *session = [VMSession sharedInstance];
    if (session.barcodeReader == nil || !session.barcodeReader.connected) {
        NSString *alertViewTitle = @"Error";
        NSString *alertMessage = @"No connection with a barcode device.";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else {
        [session.barcodeReader triggerRead];
    }
}

- (void)rfidScan {
    VMSession *session = [VMSession sharedInstance];
    if (session.rfidReader == nil || session.rfidReader.bleShield == nil || session.rfidReader.bleShield.activePeripheral == nil || !session.rfidReader.bleShield.activePeripheral.isConnected) {
        NSString *alertViewTitle = @"Error";
        NSString *alertMessage = @"No connection with an RFID device.";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else {
        [self startRFIDScan];
        [self scheduleRFIDScanTimer];
    }
}

// Sends a start read command to bluetooth module
- (void)startRFIDScan {
    VMSession *session = [VMSession sharedInstance];
    if (session.rfidReader == nil || session.rfidReader.bleShield == nil || session.rfidReader.bleShield.activePeripheral == nil || !session.rfidReader.bleShield.activePeripheral.isConnected) {
        NSString *alertViewTitle = @"Error";
        NSString *alertMessage = @"No connection with an RFID device.";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else {
        [self startActivityIndicator];
        [[VMSession sharedInstance].rfidReader startRFIDReading];
    }
}

// Sends a stop read command to bluetooth module
- (void)stopRFIDScan {
    VMSession *session = [VMSession sharedInstance];
    if (session.rfidReader == nil || session.rfidReader.bleShield == nil || session.rfidReader.bleShield.activePeripheral == nil || !session.rfidReader.bleShield.activePeripheral.isConnected) {
        NSString *alertViewTitle = @"Error";
        NSString *alertMessage = @"No connection with an RFID device.";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else {
        [[VMSession sharedInstance].rfidReader stopRFIDReading];
        [self stopActivityIndicator];
    }
}

- (void)scheduleRFIDScanTimer {
    self.rfidScanTimer = [NSTimer scheduledTimerWithTimeInterval:(float)[RFID_SCAN_TIME floatValue] target:self selector:@selector(stopRFIDScan) userInfo:nil repeats:NO];
}

- (NSString *)getLastScanValue {
    return [VMSession sharedInstance].barcodeReader.lastScanValue;
}

- (NSString *)getLastBarcodeSymbology {
    return [VMSession sharedInstance].barcodeReader.lastBarcodeSymbology;
}

-(void)debug:(NSString *)text
{
	NSDateFormatter *dateFormat=[[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:@"HH:mm:ss:SSS"];
	NSString *timeString = [dateFormat stringFromDate:[NSDate date]];
	
	if([self.debug length]>10000)
		[self.debug setString:@""];
	[self.debug appendFormat:@"%@-%@\n",timeString,text];
    
#ifdef LOG_FILE
	[debug writeToFile:[self getLogFile]  atomically:YES];
#endif
}

-(void)resetForm
{
    [VMSession sharedInstance].barcodeReader.lastScanValue = nil;
    [VMSession sharedInstance].barcodeReader.lastBarcodeSymbology = nil;
    [VMSession sharedInstance].barcodeReader.lastCardName = nil;
    [VMSession sharedInstance].barcodeReader.lastCardNumber = nil;
    [VMSession sharedInstance].barcodeReader.lastExpDate = nil;
    [VMSession sharedInstance].barcodeReader.lastMagneticCardTrack1 = nil;
    [VMSession sharedInstance].barcodeReader.lastMagneticCardTrack2 = nil;
    [VMSession sharedInstance].barcodeReader.lastMagneticCardTrack3 = nil;
}

#pragma mark -
#pragma mark Helper Methods

- (void)updateAutoIdSettings {
    BOOL barcodeReaderUpdated = [[VMSession sharedInstance].barcodeReader updateReader];
    BOOL rfidReaderUpdated = [[VMSession sharedInstance].rfidReader updateReader];
    if (barcodeReaderUpdated || rfidReaderUpdated)
        [[VMSession sharedInstance] updateAvailableAutoIdModes];
}

#pragma mark -
#pragma mark VMRFIDReaderDelegate Methods
/****************************************************************************
 * VMRFIDReaderDelegate Methods
 ****************************************************************************/

-(void)handleRFIDReaderButtonPressedEvent:(int)which {
}

-(void)handleRFIDReaderButtonReleasedEvent:(int)which {
}

-(void)handleRFIDReaderStatusEvent:(NSString *)description event:(VMRFIDReaderEvent *)event {
}

-(void)handleRFIDReaderReadEvent:(NSArray *)tagIds event:(VMRFIDReaderEvent *)event {
}

-(void)handleRFIDReaderWriteEvent:(NSArray *)tagIds event:(VMRFIDReaderEvent *)event {
}

#pragma mark -
#pragma mark VMBarcodeReaderDelegate Methods
/****************************************************************************
 * VerbiBarcodeReaderDelegate Methods
 ****************************************************************************/

-(void)handleBarcodeReaderButtonPressedEvent:(int)which {
}

-(void)handleBarcodeReaderButtonReleasedEvent:(int)which {
}

-(void)handleBarcodeReaderStatusEvent:(NSString *)description event:(VMBarcodeReaderEvent *)event {
}

-(void)handleBarcodeReaderReadEvent:(NSString *)barcode symbology:(NSString *)symbology event:(VMBarcodeReaderEvent *)event {
}

-(void)handleMagneticCardReaderReadEvent:(NSString *)data event:(VMMagneticCardReaderEvent *)event {
}

@end
