//
//  ConnectViewController.m
//  verbimobile
//
//  Created by Rob Hotaling on 3/8/13.
//  Copyright (c) 2013 Verbi, Inc. All rights reserved.
//

#import "ConnectViewController.h"
#import "ECSlidingViewController.h"
#import "LeftMenuViewController.h"
#import "RightMenuViewController.h"
#import "VMSession.h"

@interface ConnectViewController ()
@property(strong, nonatomic) NSMutableArray *devices;
@property(strong, nonatomic) NSMutableArray *deviceUUIDs;
@property(nonatomic) BOOL bluetoothEnabled;
@property(strong, nonatomic) CBCentralManager *centralManager;
@property(strong, nonatomic) NSTimer *scanTimer;
@property(strong, nonatomic) NSTimer *waitTimer;
@end

@implementation ConnectViewController

#pragma mark -
#pragma mark Getter/Setter Methods
/****************************************************************************
 * Getter/Setter Methods
 ****************************************************************************/
@synthesize leftMenuBtn;
@synthesize rightMenuBtn;
@synthesize devicesTableView = _devicesTableView;
@synthesize devices = _devices;
@synthesize deviceUUIDs = _deviceUUIDs;
@synthesize bluetoothEnabled = _bluetoothEnabled;
@synthesize centralManager = _centralManager;
@synthesize scanTimer = _scanTimer;
@synthesize waitTimer = _waitTimer;

- (NSMutableArray *)devices {
    if (_devices == nil)
        _devices = [[NSMutableArray alloc] init];
    return _devices;
}

- (NSMutableArray *)deviceUUIDs {
    if (_deviceUUIDs == nil)
        _deviceUUIDs = [[NSMutableArray alloc] init];
    return _deviceUUIDs;
}

- (CBCentralManager *)centralManager {
    if (_centralManager == nil)
        _centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_main_queue()];
    return _centralManager;
}

#pragma mark -
#pragma mark Lifecycle Methods
/****************************************************************************
 * Lifecycle Methods
 ****************************************************************************/
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // These 3 give effect that view controller is hovering slightly above menu
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    // Checks to see if we already have a Menu view controller from storyboard underneath our menu. If we don't, then create it
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[LeftMenuViewController class]]) {
        self.slidingViewController.underLeftViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LeftMenu"];
    }
    if (![self.slidingViewController.underRightViewController isKindOfClass:[RightMenuViewController class]]) {
        self.slidingViewController.underRightViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"RightMenu"];
    }
    
    // gives us the ability to swipe from left to right to reveal menu
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    [self.leftMenuBtn setTarget:self];
    [self.leftMenuBtn setAction:@selector(revealLeftMenu:)];
    [self.rightMenuBtn setTarget:self];
    [self.rightMenuBtn setAction:@selector(revealRightMenu:)];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Get Bluetooth state and update status
    if ([self.centralManager state] == CBCentralManagerStatePoweredOn) {
        self.bluetoothEnabled = YES;
        self.connectLabel.text = @"Bluetooth: On";
    }
    else if ([self.centralManager state] == CBCentralManagerStateUnknown) {
        self.bluetoothEnabled = YES;
        self.connectLabel.text = @"Bluetooth: Detecting...";
    }
    else {
        self.bluetoothEnabled = NO;
        self.connectLabel.text = @"Bluetooth: Off";
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (self.bluetoothEnabled) {
        @try {
            VMSession *session = [VMSession sharedInstance];
            if (session.rfidReader.bleShield) {
                if (session.rfidReader.bleShield.activePeripheral) {
                    // Add active peripheral to devices array so it can be auto binded to devicesTableView
                    [self.devices addObject:session.rfidReader.bleShield.activePeripheral];
                    [self.deviceUUIDs addObject:(__bridge NSString*)CFUUIDCreateString(nil, session.rfidReader.bleShield.activePeripheral.UUID)];
                    [self.devicesTableView reloadData];
                    // If active peripheral is connected
                    if (session.rfidReader.bleShield.activePeripheral.isConnected) {
                        // Loop thru devicesTableView and set text color to blue to indicate it's already connected
                        int rowCount = [self.devicesTableView numberOfRowsInSection:0];
                        for (int i = 0; i < rowCount; i++) {
                            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
                            UITableViewCell *cell = [self.devicesTableView cellForRowAtIndexPath:indexPath];
                            cell.accessoryType = UITableViewCellAccessoryCheckmark;
                            cell.textLabel.textColor = [UIColor blueColor];
                            break;
                        }
                    }
                }
                else
                    [self lookForDevices];
            }
            else {
                NSLog(@"No RFID reader detected");
            }
        }
        @catch (NSException *exception) {
            NSLog(@"An exception occured: %@", [exception reason]);
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)revealLeftMenu:(id)sender {
    [self.slidingViewController anchorTopViewTo:ECRight];
}

- (IBAction)revealRightMenu:(id)sender {
    [self.slidingViewController anchorTopViewTo:ECLeft];
}

- (void)viewDidUnload {
    [self setLeftMenuBtn:nil];
    [self setRightMenuBtn:nil];
    [self setDevicesTableView:nil];
    [self setConnectLabel:nil];
    [self shutdownTimers];
    [super viewDidUnload];
}

#pragma mark -
#pragma mark Table view methods
/****************************************************************************
 * Table view Methods
 ****************************************************************************/

// Defines the table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Defines the table - Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.devices.count;
}

// Fill the table - Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    if (self.devices && self.devices.count > 0) {
        CBPeripheral *peripheral = [self.devices objectAtIndex:indexPath.row];
        if (peripheral && peripheral.name) {
            
            // Cell Title Text
            NSString *uuid = (__bridge NSString*)CFUUIDCreateString(nil, peripheral.UUID);
            if (uuid == nil)
                uuid = @"Unknown";
            NSNumber *rssi = peripheral.RSSI;
            if (rssi == nil)
                rssi = [NSNumber numberWithFloat:0];
            cell.textLabel.text = [NSString stringWithFormat:@"[%@] %@", rssi, peripheral.name];
            
            // Cell Subtitle Text
            NSString *detailText = [NSString stringWithFormat:@"%@", uuid];
            cell.detailTextLabel.text = detailText;
        }
        
        // Convert all row selections to check marks and blue text
        NSIndexPath *selection = [tableView indexPathForSelectedRow];
        if (selection && selection.row == indexPath.row) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            cell.textLabel.textColor = [UIColor blueColor];
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.textLabel.textColor = [UIColor blackColor];
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self startActivityIndicator];
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        [self disconnectFromDevice:indexPath];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.textLabel.textColor = [UIColor blackColor];
    }
    else {
        [self connectToDevice:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cell.textLabel.textColor = [UIColor blueColor];
    }
}

#pragma mark -
#pragma mark Helper Methods
/****************************************************************************
 * Helper Methods
 ****************************************************************************/

- (void)scheduleScanTimer {
    [[VMSession sharedInstance].rfidReader.bleShield findBLEPeripherals:[BT_SCAN_TIME intValue]];
    self.scanTimer = [NSTimer scheduledTimerWithTimeInterval:(float)[BT_SCAN_TIME floatValue] target:self selector:@selector(connectionTimer:) userInfo:nil repeats:NO];
}

- (void)scheduleWaitTimer {
    self.waitTimer = [NSTimer scheduledTimerWithTimeInterval:(float)[BT_WAIT_TIME_UNTIL_SCAN floatValue] target:self selector:@selector(lookForDevices) userInfo:nil repeats:NO];
}

- (void)shutdownTimers {
    if (self.scanTimer) {
        [self.scanTimer invalidate];
        [self setScanTimer:nil];
    }
    if (self.waitTimer) {
        [self.waitTimer invalidate];
        [self setWaitTimer:nil];
    }
}

- (void)resetCollections {
    if (self.devices && self.devices.count > 0)
        [self.devices removeAllObjects];
    if (self.deviceUUIDs && self.deviceUUIDs.count > 0)
        [self.deviceUUIDs removeAllObjects];
    [self.devicesTableView reloadData];
}

- (void)viewDevice {
    @try {
        NSString *alertViewTitle = @"View";
        NSArray *indexes = [VMUtilityService getCheckedOrSelectedIndexes:self.devicesTableView];
        if (indexes.count == 0) {
            NSString *alertMessage = @"Select a device.";
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: alertViewTitle
                                  message: alertMessage
                                  delegate: self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
        else if (indexes.count == 1) {
            for (NSIndexPath *path in indexes) {
                [self viewDevice:path];
                break;
            }
        }
        else {
            NSString *alertMessage = @"Select a single device.";
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: alertViewTitle
                                  message: alertMessage
                                  delegate: self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}


// Handles connection, reconnection and disconnection from device
-(void)connectToDevice:(NSIndexPath *)indexPath {
    @try {
        VMSession *session = [VMSession sharedInstance];
        NSLog(@"Getting active BT device...");
        CBPeripheral *activeDevice = [session.rfidReader.bleShield activePeripheral];
        NSString *activeDeviceUUID = nil;
        if (activeDevice) {
            NSLog(@"Active BT device '%@' found.", activeDevice.name);
            if (activeDevice.UUID) {
                NSLog(@"Getting UUID of active BT device '%@'...", activeDevice.name);
                activeDeviceUUID = (__bridge NSString*)CFUUIDCreateString(nil, activeDevice.UUID);
                if (activeDeviceUUID)
                    NSLog(@"Active BT device '%@' UUID: %@", activeDevice.name, activeDeviceUUID);
            }
        }
        else {
            NSLog(@"Active BT device not found.");
        }
        
        NSLog(@"Getting selected BT device from devices array");
        NSUInteger index = [indexPath indexAtPosition:[indexPath length] - 1];
        CBPeripheral *selectedDevice = [self.devices objectAtIndex:index];
        NSString *selectedDeviceUUID = nil;
        if (selectedDevice) {
            NSLog(@"Selected BT device '%@' found", selectedDevice.name);
            if (selectedDevice.UUID) {
                NSLog(@"Getting UUID of selected BT device '%@'...", selectedDevice.name);
                selectedDeviceUUID = (__bridge NSString*)CFUUIDCreateString(nil, selectedDevice.UUID);
                if (selectedDeviceUUID)
                    NSLog(@"Selected BT device '%@' UUID: %@", selectedDevice.name, selectedDeviceUUID);
            }
            
            if (activeDevice == nil) {
                NSLog(@"No active BT device found. Connecting to selected BT device: %@", selectedDevice.name);
                [session.rfidReader.bleShield connectPeripheral:selectedDevice];
                NSLog(@"Now connected to selected BT device: %@", selectedDevice.name);
            }
            else if (activeDevice && (!activeDevice.isConnected || (activeDevice.isConnected && activeDeviceUUID && [[activeDeviceUUID lowercaseString] caseInsensitiveCompare:selectedDeviceUUID] != NSOrderedSame))) {
                NSLog(@"Active BT device is disconnected. Connecting to selected BT device: %@", selectedDevice.name);
                [session.rfidReader.bleShield connectPeripheral:selectedDevice];
                [session.rfidReader.bleShield setActivePeripheral:selectedDevice];
                NSLog(@"Now connected to selected BT device: %@", selectedDevice.name);
            }
            else {
                NSLog(@"Selected BT device found. Disconnecting from selected BT device: %@", selectedDevice.name);
                [session.rfidReader.bleShield setActivePeripheral:selectedDevice];
                if (session.rfidReader.bleShield.activePeripheral && session.rfidReader.bleShield.activePeripheral.isConnected) {
                    CBCentralManager *cbCentralManager = [session.rfidReader.bleShield CM];
                    if (cbCentralManager)
                        [cbCentralManager cancelPeripheralConnection:selectedDevice];
                    session.rfidReader.bleShield.activePeripheral = nil;
                }
                NSLog(@"Now disconnected to selected BT device: %@", selectedDevice.name);
            }
        }
        else {
            NSLog(@"Selected BT device not found in devices array");
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

// Handles disconnection from device
-(void)disconnectFromDevice:(NSIndexPath *)indexPath {
    @try {
        VMSession *session = [VMSession sharedInstance];
        NSLog(@"Getting active BT device...");
        CBPeripheral *activeDevice = [session.rfidReader.bleShield activePeripheral];
        NSString *activeDeviceUUID = nil;
        if (activeDevice) {
            NSLog(@"Active BT device '%@' found.", activeDevice.name);
            if (activeDevice.UUID) {
                NSLog(@"Getting UUID of active BT device '%@'...", activeDevice.name);
                activeDeviceUUID = (__bridge NSString*)CFUUIDCreateString(nil, activeDevice.UUID);
                if (activeDeviceUUID)
                    NSLog(@"Active BT device '%@' UUID: %@", activeDevice.name, activeDeviceUUID);
            }
        }
        else {
            NSLog(@"Active BT device not found.");
        }
        
        NSLog(@"Getting selected BT device from devices array");
        NSUInteger index = [indexPath indexAtPosition:[indexPath length] - 1];
        CBPeripheral *selectedDevice = [self.devices objectAtIndex:index];
        NSString *selectedDeviceUUID = nil;
        if (selectedDevice) {
            NSLog(@"Selected BT device '%@' found", selectedDevice.name);
            if (selectedDevice.UUID) {
                NSLog(@"Getting UUID of selected BT device '%@'...", selectedDevice.name);
                selectedDeviceUUID = (__bridge NSString*)CFUUIDCreateString(nil, selectedDevice.UUID);
                if (selectedDeviceUUID)
                    NSLog(@"Selected BT device '%@' UUID: %@", selectedDevice.name, selectedDeviceUUID);
            }
            
            NSLog(@"Selected BT device found. Disconnecting from selected BT device: %@", selectedDevice.name);
            [session.rfidReader.bleShield setActivePeripheral:selectedDevice];
            if (session.rfidReader.bleShield.activePeripheral && session.rfidReader.bleShield.activePeripheral.isConnected) {
                CBCentralManager *cbCentralManager = [session.rfidReader.bleShield CM];
                if (cbCentralManager)
                    [cbCentralManager cancelPeripheralConnection:selectedDevice];
                session.rfidReader.bleShield.activePeripheral = nil;
            }
            NSLog(@"Now disconnected to selected BT device: %@", selectedDevice.name);
        }
        else {
            NSLog(@"Selected BT device not found in devices array");
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

-(void)viewDevice:(NSIndexPath *)indexPath {
    @try {
        VMSession *session = [VMSession sharedInstance];
        CBPeripheral *activeDevice = [session.rfidReader.bleShield activePeripheral];
        NSString *activeDeviceUUID = nil;
        if (activeDevice)
            activeDeviceUUID = (__bridge NSString*)CFUUIDCreateString(nil, activeDevice.UUID);
        
        NSUInteger index = [indexPath indexAtPosition:[indexPath length] - 1];
        CBPeripheral *selectedDevice = [self.devices objectAtIndex:index];
        NSString *selectedDeviceUUID = nil;
        if (selectedDevice) {
            selectedDeviceUUID = (__bridge NSString*)CFUUIDCreateString(nil, selectedDevice.UUID);
        }
        
        NSString *alertViewTitle = @"View";
        NSMutableString *alertMessage = [[NSMutableString alloc] initWithFormat:@"Name: %@\n\nUUID: %@\n\nRSSI: %@", selectedDevice.name, selectedDeviceUUID, selectedDevice.RSSI];
        
        if (activeDevice && activeDevice.isConnected && activeDeviceUUID && [[activeDeviceUUID lowercaseString] caseInsensitiveCompare:selectedDeviceUUID] == NSOrderedSame) {
            [alertMessage appendString:@"\n\nYou are connected to this device."];
        }
        
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: alertViewTitle
                              message: alertMessage
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown: %@", [exception reason]);
    }
    @finally {
    }
}

// Called when "wait until next scan time" interval elapses
- (void)lookForDevices {
    // release timer used to call this method
    if (self.waitTimer) {
        [self.waitTimer invalidate];
        [self setWaitTimer:nil];
    }

    VMSession *session = [VMSession sharedInstance];
    if (self.bluetoothEnabled && (session.rfidReader.bleShield.activePeripheral == nil || !session.rfidReader.bleShield.activePeripheral.isConnected)) {
        @try {
            [self startActivityIndicator];
            
            if (session.rfidReader.bleShield.peripherals)
                session.rfidReader.bleShield.peripherals = nil;
            [self resetCollections];
            
            self.connectLabel.text = @"Bluetooth: Looking for Devices...";
            [self scheduleScanTimer];
        }
        @catch (NSException *exception) {
            NSLog(@"Exception thrown: %@", [exception reason]);
        }
        @finally {
        }
    }
}

// Called when scan time interval elapses
-(void) connectionTimer:(NSTimer *)timer {
    // release timer used to call this method
    if (self.scanTimer) {
        [self.scanTimer invalidate];
        [self setScanTimer:nil];
    }

    VMSession *session = [VMSession sharedInstance];
    if (self.bluetoothEnabled && (session.rfidReader.bleShield.activePeripheral == nil || !session.rfidReader.bleShield.activePeripheral.isConnected)) {
        int devicesFound = session.rfidReader.bleShield.peripherals.count;
        if (devicesFound > 0) {
            self.devices = [session.rfidReader.bleShield peripherals];
            if (self.devices && self.devices.count > 0) {
                [self.deviceUUIDs removeAllObjects];
                for (CBPeripheral *peripheral in self.devices) {
                    [self.deviceUUIDs addObject:(__bridge NSString*)CFUUIDCreateString(nil, peripheral.UUID)];
                }
            }
            [self.devicesTableView reloadData];
        }
        
        [self stopActivityIndicator];
        self.connectLabel.text = @"Bluetooth: On";
        [self scheduleWaitTimer];
    }
}

// Since this class is a delegate to Apple's CoreBluetooth API, this method is called asynchronously
- (void) centralManagerDidUpdateState:(CBCentralManager *)central {
    static CBCentralManagerState previousState = -1;
    
	switch ([self.centralManager state]) {
		case CBCentralManagerStatePoweredOff:
		{
            self.bluetoothEnabled = NO;
            self.connectLabel.text = @"Bluetooth: Off";
            [self shutdownTimers];
            [self resetCollections];
			break;
		}
            
		case CBCentralManagerStateUnauthorized:
		{
			/* Tell user the app is not allowed. */
            self.bluetoothEnabled = NO;
            self.connectLabel.text = @"Bluetooth: Off";
            [self shutdownTimers];
            [self resetCollections];
			break;
		}
            
        case CBCentralManagerStateUnsupported:
        {
            self.bluetoothEnabled = NO;
            self.connectLabel.text = @"Bluetooth: Off";
            [self shutdownTimers];
            [self resetCollections];
            break;
        }
            
		case CBCentralManagerStateUnknown:
		{
			/* Bad news, let's wait for another event. */
            self.bluetoothEnabled = NO;
            self.connectLabel.text = @"Bluetooth: Waiting...";
			break;
		}
            
		case CBCentralManagerStatePoweredOn:
		{
            self.bluetoothEnabled = YES;
            self.connectLabel.text = @"Bluetooth: On";
            VMSession *session = [VMSession sharedInstance];
            if (session.rfidReader.bleShield.activePeripheral == nil || !session.rfidReader.bleShield.activePeripheral.isConnected)
                [self scheduleScanTimer];
			break;
		}
            
		case CBCentralManagerStateResetting:
		{
            self.bluetoothEnabled = YES;
            self.connectLabel.text = @"Bluetooth: Resetting";
			break;
		}
	}
    
    previousState = [self.centralManager state];
}

#pragma mark -
#pragma mark VMRFIDReaderDelegate Methods
/****************************************************************************
 * VMRFIDReaderDelegate Methods
 ****************************************************************************/

-(void)handleRFIDReaderStatusEvent:(NSString *)description event:(VMRFIDReaderEvent *)event {
    VMSession *session = [VMSession sharedInstance];
    NSString *status = [event.details objectForKey:@"status"];
    NSString *name = [event.details objectForKey:@"name"];
    NSString *uuid = [event.details objectForKey:@"uuid"];
    NSNumber *rssi = [event.details objectForKey:@"rssi"];
    //NSLog(@"Received RFID reader status event: Status = %@; Name = %@; UUID = %@; RSSI = %@", status, name, uuid, rssi);
    
    if ([[status lowercaseString] caseInsensitiveCompare:@"connected"] == NSOrderedSame) {
        NSUInteger index = [self.deviceUUIDs indexOfObject:uuid];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];    // convert iterator to indexPath
        if (indexPath) {
            UITableViewCell *cell = [self.devicesTableView cellForRowAtIndexPath:indexPath];
            if (cell) {
                cell.textLabel.textColor = [UIColor blueColor];
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                [VMSession sharedInstance].rfidReader.connected = YES;
            }
        }
        
        [self stopActivityIndicator];
    }
    else if ([[status lowercaseString] caseInsensitiveCompare:@"disconnected"] == NSOrderedSame) {
        NSUInteger index = [self.deviceUUIDs indexOfObject:uuid];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];    // convert iterator to indexPath
        if (indexPath) {
            UITableViewCell *cell = [self.devicesTableView cellForRowAtIndexPath:indexPath];
            if (cell) {
                cell.textLabel.textColor = [UIColor blackColor];
                cell.accessoryType = UITableViewCellAccessoryNone;
                [VMSession sharedInstance].rfidReader.connected = NO;
            }
        }
        
        [self stopActivityIndicator];
        // Since active peripheral is now disconnected, starting scanning for devices
        if (self.bluetoothEnabled && (session.rfidReader.bleShield.activePeripheral == nil || !session.rfidReader.bleShield.activePeripheral.isConnected)) {
            if ((self.scanTimer == nil && self.waitTimer == nil) || ((self.scanTimer || self.waitTimer) && (!self.scanTimer.isValid || !self.waitTimer.isValid))) {
                [self shutdownTimers];
                self.connectLabel.text = @"Bluetooth: Looking for Devices...";
                [self scheduleScanTimer];
            }
        }
    }
    else if ([[status lowercaseString] caseInsensitiveCompare:@"rssi update"] == NSOrderedSame) {
        NSUInteger index = [self.deviceUUIDs indexOfObject:uuid];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];    // convert iterator to indexPath
        if (indexPath) {
            UITableViewCell *cell = [self.devicesTableView cellForRowAtIndexPath:indexPath];
            if (cell) {
                cell.textLabel.text = [NSString stringWithFormat:@"[%@] %@", rssi, name];
                cell.textLabel.textColor = [UIColor blueColor];
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
        }
        
        [self stopActivityIndicator];
    }
    else {
    }
}

@end
