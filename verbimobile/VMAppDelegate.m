//
//  AppDelegate.m
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#import "VMAppDelegate.h"
#import "VMConstants.h"

@implementation VMAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self initializeDatabase];
    
    // Override point for customization after application launch.
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)initializeDatabase {
    // The AppDelegate is responsible for copying the database to the iPhone "Documents" folder. This operation is only performed when the database does not exists in the "Documents" folder.
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *sourceDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DB_FULL_NAME];
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *documentsDBPath = [documentsDirectory stringByAppendingPathComponent:DB_FULL_NAME];    // DB must be in documents for iOS to write to it
    
    BOOL foundInBundle = [fileManager fileExistsAtPath:sourceDBPath];
    BOOL foundInDocumentsPath = [fileManager fileExistsAtPath:documentsDBPath];
    if (foundInDocumentsPath) {
        NSLog(@"Database '%@' found in writeable Documents directory: '%@'", DB_FULL_NAME, documentsDBPath);
    }
    else {
        if(foundInBundle) {
            NSLog(@"Database '%@' found in source Bundle directory. Copying from Bundle directory '%@' to writeable Documents directory '%@' ...", DB_FULL_NAME, sourceDBPath, documentsDBPath);
            NSError *error;
            BOOL copySuccess = [fileManager copyItemAtPath:sourceDBPath toPath:documentsDBPath error:&error];
            if (copySuccess) {
                NSLog(@"Successfully copied database '%@' from Bundle directory '%@' to Documents directory '%@'.", DB_FULL_NAME, sourceDBPath, documentsDBPath);
            }
            else {
                NSLog(@"Failed to copy database '%@' from Bundle directory '%@' to Documents directory '%@'. Error: '%@'.", DB_FULL_NAME, sourceDBPath, documentsDBPath, [error localizedDescription]);
            }
        } else {
            NSLog(@"Source database '%@' not found in bundle directory: '%@'", DB_FULL_NAME, sourceDBPath);
        }
    }
}

-(BOOL) application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if (url && [url isFileURL]) {
//        [self.viewController handleOpenURL:url];
    }
    return YES;
}

//Please note: This method is deprecated. It’s added here for backward compatibility
-(BOOL) application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
    if (url && [url isFileURL]) {
//        [self.viewController handleOpenURL:url];
    }
    return YES;
    
}

@end
