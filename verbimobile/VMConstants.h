//
//  VMConstants.h
//  verbimobile
//
//  Copyright (c) 2012 Verbi, Inc. All rights reserved.
//

#ifndef verbimobile_VMConstants_h
#define verbimobile_VMConstants_h

#if (TARGET_IPHONE_SIMULATOR || TARGET_IPAD_SIMULATOR)
#define IS_SIMULATOR                                            @"1"
#else
#define IS_SIMULATOR                                            @"0"
#endif

#define BT_SCAN_TIME                                            @"4"
#define BT_WAIT_TIME_UNTIL_SCAN                                 @"7"
#define RFID_SCAN_TIME                                          @"4"
#define RFID_WAIT_TIME_UNTIL_SCAN                               @"7"

// WORKFLOW
#define WKFL_LOC_CATEGORY_NAME                                  @"Location"
#define WKFL_LOC_NEW_LOCATION                                   @"New Location"
#define WKFL_LOC_IMPORT_LOCATION                                @"Import Location"
#define WKFL_INV_CATEGORY_NAME                                  @"Inventory"
#define WKFL_INV_IMPORT_ITEM_MASTER                             @"Import Item Master"
#define WKFL_INV_IMPORT_INVENTORY                               @"Import Inventory"
#define WKFL_INV_NEW_INVENTORY                                  @"New Inventory"
#define WKFL_INV_AUDIT_INVENTORY                                @"Audit Inventory"
#define WKFL_STEP_LOC_CATEGORY_NAME                             @"Location"
#define WKFL_STEP_LOC_IMPORT_LOCATION_FILE                      @"Import Location File"
#define WKFL_STEP_LOC_IMPORT_LOCATION_FILE_CONFIRM              @"Import Location File Confirm"
#define WKFL_STEP_INV_CATEGORY_NAME                             @"Inventory"
#define WKFL_STEP_INV_IMPORT_ITEM_MASTER_FILE                   @"Import Item Master File"
#define WKFL_STEP_INV_IMPORT_ITEM_MASTER_FILE_CONFIRM           @"Import Item Master File Confirm"
#define WKFL_STEP_INV_IMPORT_INVENTORY_FILE                     @"Import Inventory File"
#define WKFL_STEP_INV_IMPORT_INVENTORY_FILE_CONFIRM             @"Import Inventory File Confirm"
#define WKFL_STEP_INV_SELECT_INVENTORY_LOCATION                 @"Select Inventory Location"
#define WKFL_STEP_INV_ADD_INVENTORY_ITEMS                       @"Add Inventory Items"
#define WKFL_STEP_INV_UPDATE_INVENTORY_CONFIRM                  @"Update Inventory Confirm"
#define WKFL_STEP_INV_UPDATE_INVENTORY_SUMMARY                  @"Update Inventory Summary"

// DATABASE
#define DB_NAME                                                 @"verbi_mobile"
#define DB_EXTENSION                                            @"sqlite"
#define DB_FULL_NAME                                            @"verbi_mobile.sqlite"
#define DB_TABLE_EPC                                            @"epc"
#define DB_TABLE_ITEM                                           @"item"
#define DB_TABLE_ITEM_EVENT                                     @"item_event"
#define DB_TABLE_ITEM_MASTER                                    @"item_master"
#define DB_TABLE_LOCATION                                       @"location"
#define DB_TABLE_REPORT                                         @"report"
#define DB_TABLE_REPORT_GROUP_MAP                               @"report_group_map"
#define DB_TABLE_REPORT_ITEM_MASTER_MAP                         @"report_item_master_map"
#define DB_TABLE_REPORT_LOCATION_MAP                            @"report_location_map"
#define DB_TABLE_REPORT_USER_MAP                                @"report_user_map"
#define DB_TABLE_USER                                           @"user"

/*
typedef enum {
    Available,
    Reserved,
    PendingShipment,
    Expired,
    Consumed
} InventoryItemStatus;
*/

// GENERAL IMAGE NAMES
#define UNAVAILABLE_IMAGE_NAME                                  @"button_unavailable.jpg"
#define IMAGE_NAME_NO_PHOTO_AVAILABLE                           @"icon_no_photo_available.gif"

typedef enum {
	AUTO_ID_MODE_MAGSTRIPE_KEY=0,
	AUTO_ID_MODE_BARCODE_1D_KEY,
	AUTO_ID_MODE_BARCODE_2D_KEY,
    AUTO_ID_MODE_RFID_C1G2_KEY
} AUTO_ID_MODES;

// AUTO ID MODE IMAGE NAMES
#define AUTO_ID_MODE_NONE_IMAGE_NAME                            @"button_none.jpg"
#define AUTO_ID_MODE_MAGSTRIPE_IMAGE_NAME                       @"button_ait_magstripe.jpg"
#define AUTO_ID_MODE_BARCODE_1D_IMAGE_NAME                      @"button_ait_barcode_1d.jpg"
#define AUTO_ID_MODE_BARCODE_2D_IMAGE_NAME                      @"button_ait_barcode_2d.jpg"
#define AUTO_ID_MODE_RFID_C1G2_IMAGE_NAME                       @"button_ait_rfid_c1g2.jpg"

// AUTO ID MODE DICTIONARY KEYS
#define AUTO_ID_MODE_NONE_KEY                                   @"0"
#define AUTO_ID_MODE_MAGSTRIPE_KEY                              @"1"
#define AUTO_ID_MODE_BARCODE_1D_KEY                             @"2"
#define AUTO_ID_MODE_BARCODE_2D_KEY                             @"3"
#define AUTO_ID_MODE_RFID_C1G2_KEY                              @"4"
#define AUTO_ID_MODE_MIN_KEY_VALUE                              @"1"
#define AUTO_ID_MODE_MAX_KEY_VALUE                              @"5"

// AUTO ID MODE DICTIONARY VALUES
#define AUTO_ID_MODE_NONE_VALUE                                 @"NONE"
#define AUTO_ID_MODE_MAGSTRIPE_VALUE                            @"MAG"
#define AUTO_ID_MODE_BARCODE_1D_VALUE                           @"1D"
#define AUTO_ID_MODE_BARCODE_2D_VALUE                           @"2D"
#define AUTO_ID_MODE_RFID_C1G2_VALUE                            @"C1G2"

// AUTO ID MODE DEFAULT
#define AUTO_ID_MODE_DEFAULT                                    @"0"

// AUTO ID PROCESS IMAGE NAMES
#define AUTO_ID_PROCESS_SCAN_IMAGE_NAME                         @"button_ait_scan.jpg"
#define AUTO_ID_PROCESS_START_RFID_IMAGE_NAME                   @"button_ait_start_rfid.jpg"
#define AUTO_ID_PROCESS_STOP_RFID_IMAGE_NAME                    @"button_ait_stop_rfid.jpg"

#define SCANDIT_SDK_APP_KEY                                     @"pYIQlOS7EeGMiKo/WWhaD5KPCebOJixxzs79Dt27kFM"

// INVENTORY CONSTANTS
#define INVENTORY_ITEM                                          @"Inventory Item"
#define INVENTORY_EVENT                                         @"Inventory Event"

#endif
